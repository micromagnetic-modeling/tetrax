"""
Materials (:mod:`tetrax.materials`)
===================================

The material can be assigned to an existing sample, for example, by

.. code-block:: python

   from tetrax import materials
   sample.material = materials.permalloy

   sample.material  # outputs the current material to your notebook

The list of available material parameters can be accessed with

.. code-block:: python

    from tetrax import materials
    materials.available

.. warning::
    Keep in mind that material parameters can greatly depend on the
    fabrication of the specific sample. Therefore, the given parameters
    are only to be used as approximate values, when comparing `TetraX`
    with experimental results.

Available materials
-------------------

.. autosummary::
    :toctree: generated/

    cobalt_fcc
    cobalt_hcp
    iron
    nickel_fcc
    permalloy
    yig


Other
-----

.. autosummary::
    :toctree: generated/

    available

"""  # noqa: D205, D400, D415
from __future__ import annotations

__all__ = ["permalloy", "yig", "iron", "cobalt_fcc", "cobalt_hcp", "nickel_fcc"]


available = __all__.copy()
"""List of available template materials."""

permalloy = {
    "Msat": 810e3,
    "Aex": 13e-12,
    "gamma": 1.8598224e11,
    "alpha": 0.008,
}
"""Ni80Fe20 (permalloy) from in PRL 125, 207203 (2020)."""

yig = {
    "Msat": 140e3,
    "Aex": 3.6e-12,
    "gamma": 1.8158405538e11,
    "alpha": 3e-4,
    "Ku1": 4500,
}
"""Yttrium Iron Garnet, approximately taken from PRB 95, 064409 (2017)."""

iron = {"Msat": 1.710e6, "Aex": 21e-12, "gamma": 1.838e11, "alpha": 0.01, "Kc1": 34.3e3}
"""Iron partly from Small 2019, 15, 1904315 and https://doi.org/10.1007/b138706."""

cobalt_fcc = {
    "Msat": 1.35e6,
    "Aex": 14e-12,
    "gamma": 1.8824e11,
    "alpha": 0.15,
}
"""Cobalt (fcc) partly from https://doi.org/10.1088/1757-899X/902/1/012060 and  J.Appl.Phys. 99, 08N503 (2006)."""

cobalt_hcp = {
    "Msat": 1.35e6,
    "Aex": 14e-12,
    "gamma": 1.8824e11,
    "alpha": 0.1,
    "Ku1": 50e3,
    "e_u": [0, 0, 1],
}
"""Cobalt (hcp) partly from https://doi.org/10.1088/1757-899X/902/1/012060 and  J.Appl.Phys. 99, 08N503 (2006)."""

nickel_fcc = {
    "Msat": 490e6,
    "Aex": 9e-12,
    "gamma": 1.943e11,
    "alpha": 0.1,
    "Ku1": -0.57e4,
    "e_u": [0, 0, 1],
}
"""Nickel (fcc) partly from https://doi.org/10.1088/1757-899X/902/1/012060."""
