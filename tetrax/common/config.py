"""
Config (:mod:`tetrax.common.config`)
====================================

Constants and other configuration (such as colors, axis labels, etc.) used throughout the package.
"""  # noqa: D205, D400, D415

from __future__ import annotations

from tetrax.common.typing import GeometryType

#####################
# General parameters
#####################

BOUNDARY_NODES_OF_BILAYER = 4

ENERGY_NAMES = {
    "confined": "energy",
    "confined_axial": "energy angle density",
    "waveguide": "energy length density",
    "waveguide_axial": "energy area density",
    "layer": "energy area density",
}
ENERGY_UNITS = {
    "confined": "J",
    "confined_axial": "J/rad",
    "waveguide": "J/m",
    "waveguide_axial": "J/(rad m)",
    "layer": "J/m^2",
}

##############################
# General plotting parameters
##############################

COLOR_CYCLE_SAMPLE = [0x0072B2, 0xE34234, 0x009E73, 0xE69F00, 0x56B4E9]
INTERACTION_COLORS = [
    "#3747A1",
    "#d16f6b",
    "#4ea166",
    "#D62728",
    "#2993A1",
    "#B171C2",
    "#7F7F7F",
    "#A34744",
    "#CEB927",
    "#E5AAA8",
]
MAG_COMPONENT_COLORS = [
    "#86337A",
    "#D16F6B",
    "#EAB44F",
    "#005061",
    "#177786",
    "#7D949D",
]

# Needs to be updated when new version of reference repo is created.
# current: https://zenodo.org/records/14674701
REFERENCE_DATA_ZENODO_ID = 14674701

#################################
# Sample-specific parameters
#################################

SAMPLE_DEFAULT_NAME = "my_sample"
SAMPLE_DEFAULT_SCALE = 1e-9
SAMPLE_DEFAULT_GEOMETRY_TYPE = "waveguide"
SAMPLE_DEFAULT_MAGNETIC_ORDER = "FM"
SAMPLE_DEFAULT_MATERIAL = {
    "Msat": 795e3,
    "Aex": 13e-12,
    "gamma": 1.760859644e11,
    "alpha": 0.008,
}

##################################
# Interaction-specific parameters
##################################

DIPOLE_DEFAULT_BNDINT_ORDER = 6

#################################
# Experiment-specific parameters
#################################

# Relax

RELAX_DEFAULT_NOISE = 1e-10
RELAX_DEFAULT_TOLERANCE = 1e-12
RELAX_DEFAULT_METHOD = "L-BFGS-B"

# Relax dynamic

RELAX_DYNAMIC_TORQUE_NAME = "dm/dt"

# Eigen

EIGEN_XLABELS = {
    GeometryType.WAVEGUIDE: "Wave vector <i>k</i>, (rad/{}m)",
    GeometryType.WAVEGUIDE_AXIAL: "Wave vector <i>k</i>, (rad/{}m)",
    GeometryType.LAYER: "Wave vector <i>k</i>, (rad/{}m)",
    GeometryType.CONFINED: "Mode number, <i>n</i>",
    GeometryType.CONFINED_AXIAL: "Azimuthal index, <i>m</i>",
}

EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME = "m"
EIGEN_WAVEVECTOR_COLUMN_NAME = "k (rad/m)"
EIGEN_FREQUENCY_COLUMN_PATTERN = "f{} (Hz)"
EIGEN_LINEWIDTH_COLUMN_PATTERN = "Gamma{}/2pi (Hz)"

EIGEN_DEFAULT_CPUS = -1  # all
EIGEN_DEFAULT_NUM_MODES = 20
EIGEN_DEFAULT_FREQUENCY_TOLERANCE = 1e-3
EIGEN_DEFAULT_INVERSE_TOLERANCE = 1e-4
EIGEN_DEFAULT_INVERSE_MAXITER = 1e3
EIGEN_DEFAULT_PARALLEL_BACKEND = "loky"
EIGEN_DEFAULT_KMIN = -40e6
EIGEN_DEFAULT_KMAX = 40e6
EIGEN_DEFAULT_NUM_K = 81
EIGEN_DEFAULT_MMIN = -5
EIGEN_DEFAULT_MMAX = 5

# Absorption

ABSORPTION_DEFAULT_MIN_RF_FREQUENCY = 0
ABSORPTION_DEFAULT_MAX_RF_FREQUENCY = 30e9
ABSORPTION_DEFAULT_NUM_FREQUENCIES = 200
