"""
Typing (:mod:`tetrax.common.typing`)
====================================

Types (mostly enums) that are used throughout the package.

.. autosummary::
    :toctree: generated/

    CartesianAxis
    GeometryType
    MagneticOrder
    SIPrefix
    UnitaryNumber
"""  # noqa: D205, D400, D415

from __future__ import annotations

from enum import Enum
from typing import TYPE_CHECKING, Dict, List, Union

import numpy as np

if TYPE_CHECKING:
    from typing import Self


class SIPrefix(Enum):
    """
    Enumeration representing SI prefixes.

    Each prefix has a symbol that represents it and is derived from its string value. The only exception is
    :attr:`MICRO` (``"u"``), whose symbol is "µ".


    Parameters
    ----------
    value : SIPrefix or str
        Value to instantiate the prefix.  Valid string values are ``"P"``, ``"T"``, ``"G"``,
        ``"M"``, ``"k"``, ``"h"``, ``"da"``, ``""``, ``"d"``, ``"c"``, ``"m"``, ``"u"``, ``"n"``, ``"p"``,
        or ``"f"``.

    """

    PETA = "P"
    """Peta (1e15)."""

    TERA = "T"
    """Tera (1e12)."""

    GIGA = "G"
    """Giga (1e9)."""

    MEGA = "M"
    """Mega (1e6)."""

    KILO = "k"
    """Kilo (1e3)."""

    HECTO = "h"
    """Hecto (1e2)."""

    DECA = "da"
    """Deca (10)."""

    NONE = ""
    """No prefix (1)."""

    DECI = "d"
    """Deci (0.1)."""

    CENTI = "c"
    """Centi (0.01)."""

    MILLI = "m"
    """Milli (1e-3)."""

    MICRO = "u"
    """Micro (1e-6)."""

    NANO = "n"
    """Nano (1e-9)."""

    PICO = "p"
    """Pico (1e-12)."""

    FEMPTO = "f"
    """Femto (1e-15)."""

    @classmethod
    def _missing_(cls, value: str) -> None:  # noqa: ANN102
        raise ValueError(
            "%r is not a valid %s.  Valid prefixes: %s"
            % (
                value,
                cls.__name__,
                ", ".join([repr(m.value) for m in cls]),
            )
        )

    @property
    def factor(self: Self) -> float:
        """Scaling factor corresponding to the prefix."""
        scale_factors = {
            SIPrefix.PETA: 1e15,
            SIPrefix.TERA: 1e12,
            SIPrefix.GIGA: 1e9,
            SIPrefix.MEGA: 1e6,
            SIPrefix.KILO: 1e3,
            SIPrefix.HECTO: 1e2,
            SIPrefix.DECA: 10,
            SIPrefix.NONE: 1,
            SIPrefix.DECI: 0.1,
            SIPrefix.CENTI: 0.01,
            SIPrefix.MILLI: 1e-3,
            SIPrefix.MICRO: 1e-6,
            SIPrefix.NANO: 1e-9,
            SIPrefix.PICO: 1e-12,
            SIPrefix.FEMPTO: 1e-15,
        }
        return scale_factors[self]

    @property
    def symbol(self: Self) -> str:
        """
        Symbol corresponding to the prefix.

        The symbol is almost always equal to the string value that can be provided upon instantiation of the
        prefix. The only exception is :attr:`MICRO` which has ``"u"`` as the string value but "µ" as a symbol.
        """
        value = "µ" if self.value == "u" else self.value
        return str(value)


class MagneticOrder(Enum):
    """
    Magnetic order of a sample (such as ferromagnet or antiferromagnet).

    .. note::
        Currently, only ferromagnetic samples are fully supported.
    """

    FERROMAGNET = "FM"
    """Ferromagnetic with one magnetization field."""

    ANTIFERROMAGNET = "AFM"
    """Antiferromagnetic with two magnetization fields and equal saturation magnetization."""

    @classmethod
    def _missing_(cls, value: Union[str, MagneticOrder]) -> None:  # noqa: ANN102
        raise ValueError(
            "%r is not a valid %s.  Valid orders: %s"
            % (
                value,
                cls.__name__,
                ", ".join([repr(m.value) for m in cls]),
            )
        )


class GeometryType(Enum):
    """
    Geometry type of a sample (usually implies one or more symmetries).

    Instances can be created by supplying the alias string. For example

    .. code-block:: python

        GeometryType("confined")
        # returns GeometryType.CONFINED

    Template geometries for each geometry type are found in the :mod:`tetrax.geometries` module.

    """

    CONFINED = "confined"
    """Confined magnetic samples with no particular symmetry."""

    CONFINED_AXIAL = "confined_axial"
    """
    Bodies of revolution with axial symmetry around the :math:`z` axis.

    Meshes need to be defined in the :math:`xz` plane.
    """

    WAVEGUIDE = "waveguide"
    """
    Waveguides with translational symmetry along the :math:`z` axis.

    Meshes need to be defined in the :math:`xy` plane.
    """

    WAVEGUIDE_AXIAL = "waveguide_axial"
    """
    Waveguides with translational symmetry along and axial symmetry around the :math:`z` axis.

    Meshes need to be defined on the :math:`x` axis.
    """

    LAYER = "layer"
    """
    Layered systems with translational symmetry in the :math:`xz` plane.

    Meshes need to be defined on the :math:`y` axis.
    """

    @classmethod
    def _missing_(cls, value: Union[str, GeometryType]) -> None:  # noqa: ANN102
        raise ValueError(
            "%r is not a valid %s.  Valid geometries: %s"
            % (
                value,
                cls.__name__,
                ", ".join([repr(m.value) for m in cls]),
            )
        )

    def is_satisfied_by(self: Self, mesh: MeshioMesh) -> Tuple[bool, str]:
        """
        Validate the shape of an input mesh based on the geometry type.

        This function checks if a given mesh conforms to the shape requirements the specific
        :class:`~tetrax.common.typing.GeometryType`.
        It returns a tuple consisting of a boolean indicating whether the mesh is properly shaped and a message
        explaining any issues found in the mesh shape.

        Parameters
        ----------
        mesh : MeshioMesh
            The mesh to be validated.

        Returns
        -------
        tuple[bool, str]
            A tuple where the first element is a boolean indicating if the mesh is properly shaped for the
            specified geometry type, and the second element is a message describing any shape issues found in the mesh.

        """

        properly_shaped: bool = False
        message: str = ""

        if self is GeometryType.CONFINED:
            properly_shaped = True

        elif self is GeometryType.CONFINED_AXIAL:
            # check if all z coordinates are 0.
            x = mesh.points.T[0]
            y = mesh.points.T[1]
            all_in_xz_plane = np.any(y) == 0

            if not all_in_xz_plane:
                message += "- Some nodes have a non-zero y coordinate.\n"

            all_x_are_positive = np.all(x >= 0)
            if not all_x_are_positive:
                message += "- Some nodes have a negative x coordinate.\n"

            properly_shaped = all_in_xz_plane and all_x_are_positive
            if not properly_shaped:
                message += "Meshes for axial confined samples are restricted to the xz plane (with non-negative x).\n"

        elif self is GeometryType.WAVEGUIDE:
            # check if all z coordinates are 0.
            z = mesh.points.T[2]
            all_in_xy_plane = np.any(z) == 0

            if not all_in_xy_plane:
                message += (
                    "- Some nodes have a non-zero z-coordinate. Meshes for waveguide cross-sections"
                    " are restricted to the xy plane.\n"
                )

            properly_shaped = all_in_xy_plane

        elif self is GeometryType.WAVEGUIDE_AXIAL:
            x = mesh.points.T[0]
            y = mesh.points.T[1]
            z = mesh.points.T[2]

            all_y_are_zero = np.all(y == 0)
            if not all_y_are_zero:
                message += "- Some nodes have a non-zero y coordinate.\n"

            all_z_are_zero = np.all(z == 0)
            if not all_z_are_zero:
                message += "- Some nodes have a non-zero z coordinate.\n"

            all_x_are_positive = np.all(x >= 0)
            if not all_x_are_positive:
                message += "- Some nodes have a negative x coordinate.\n"

            properly_shaped = all_x_are_positive and all_y_are_zero and all_z_are_zero

            if not properly_shaped:
                message += (
                    "Meshes for axial waveguides are restricted to the positive x axis.\n"
                )

        elif self is GeometryType.LAYER:
            x = mesh.points.T[0]
            z = mesh.points.T[2]

            all_x_are_zero = np.all(x == 0)
            if not all_x_are_zero:
                message += "- Some nodes have a non-zero x coordinate.\n"

            all_z_are_zero = np.all(z == 0)
            if not all_z_are_zero:
                message += "- Some nodes have a non-zero z coordinate.\n"

            properly_shaped = all_x_are_zero and all_z_are_zero
            if not properly_shaped:
                message += "Meshes for layer samples are restricted to the y axis.\n"

        return properly_shaped, message

class UnitaryNumber(Enum):
    """A number that can only take values of +1 or -1."""

    POSITIVE = 1
    NEGATIVE = -1

    @classmethod
    def _missing_(cls, value: Union[str, UnitaryNumber]) -> None:  # noqa: ANN102
        raise ValueError(
            f"{value} is not a valid {cls.__name__}. Only +1 and -1 are allowed."
        )


class CartesianAxis(Enum):
    """Axis in a cartesian coordinate system."""

    X = "x"
    Y = "y"
    Z = "z"

    @classmethod
    def _missing_(cls, value: Union[str, CartesianAxis]) -> None:  # noqa: ANN102
        raise ValueError(
            f"{value} is not a valid {cls.__name__}. Only x, y and z are allowed."
        )


Serializable = Union[List, Dict, int, float, bool, str, None]
"""Type of objects that can be serialized to a JSON file."""
