"""
Math (:mod:`tetrax.common.math`)
======================================

Mathematical functions and other operators.

.. autosummary::
    :toctree: generated/

    cross_operator
    cross_product
    cross_product_ndarray
    diag_matrix_from_field
    flattened_AFMmesh_vec_scalar_product_separate
    inner_product
    interpolate_along_curve
    is_positive
    is_strictly_positive
    normalize
    tensor_product
    weighted_average
"""  # noqa: D205, D400, D415
# TODO add mathematical equations in doc strings

from __future__ import annotations

from typing import TYPE_CHECKING, Tuple, Union

import numpy as np
from scipy.interpolate import griddata
from scipy.sparse import bmat, dia_matrix

from tetrax.sample.mesh import (
    FlattenedAFMMeshVector,
    MeshScalar,
    MeshVector,
)

if TYPE_CHECKING:
    import scipy
    from numpy.typing import NDArray

    from tetrax.sample.mesh.meshquantities import (
        AnyVector,
        FlattenedMeshVector,
        LabVector,
    )


def normalize(vec: MeshVector) -> MeshVector:
    """
    Normalize an un-flattened vector field.

    Parameters
    ----------
    vec : MeshVector
        Mesh vector of shape `(N,3)`.

    Returns
    -------
    vec_normalized : MeshVector
        Normalized mesh vector of shape `(N,3)`.

    """
    norm = np.sqrt(vec.T[0] ** 2 + vec.T[1] ** 2 + vec.T[2] ** 2)
    norm = np.repeat(norm, 3)
    norm = np.reshape(norm, (len(norm) // 3, 3))
    norm_vec = np.array(vec) / (norm + 1.0e-32)
    #    norm_vec[np.isnan(norm_vec)] = 0
    return norm_vec


def flattened_AFMmesh_vec_scalar_product_separate(
    vec1: FlattenedAFMMeshVector, vec2: FlattenedAFMMeshVector
) -> NDArray:
    r"""
    Calculate the node-wise inner product of two flattened mesh vectors :math:`\mathbf{v}` and :math:`\mathbf{w}`.

    Assumes that the mesh vectors are of shape `(3*N,)` (with `N` being the number of nodes in the mesh) and are ordered
    according to

    .. math:: \mathbf{v} = (v_{x_1}, ... , v_{x_N}, v_{y_1}, ... , v_{y_N}, v_{z_1}, ... , v_{z_N})

    and

    .. math:: \mathbf{w} = (w_{x_1}, ... , w_{x_N}, w_{y_1}, ... , w_{y_N}, w_{z_1}, ... , w_{z_N})

    At each node :math:`i`, the inner product is defined as

    .. math:: \mathbf{v}_i \cdot \mathbf{w}_i = \sum\limits_{j=x,y,z} v_{j_i}w_{j_i}

    Parameters
    ----------
    vec1 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    vec2 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    Returns
    -------
    MeshScalar
        Flattened mesh scalar of shape `(N,)`.

    """
    nx = vec1.shape[0] // 6
    assert vec1.shape[0] == 6 * nx and vec2.shape[0] == 6 * nx
    return np.concatenate(
        (
            vec1[0:nx] * vec2[0:nx]
            + vec1[nx : 2 * nx] * vec2[nx : 2 * nx]
            + vec1[2 * nx : 3 * nx] * vec2[2 * nx : 3 * nx],
            vec1[3 * nx : 4 * nx] * vec2[3 * nx : 4 * nx]
            + vec1[4 * nx : 5 * nx] * vec2[4 * nx : 5 * nx]
            + vec1[5 * nx : 6 * nx] * vec2[5 * nx : 6 * nx],
        )
    )


def weighted_average(
    field: Union[MeshScalar, MeshVector], weights: MeshScalar
) -> Union[MeshScalar, MeshVector]:
    """
    Compute the weighted average of a scalar or vector field over a mesh.

    This function calculates the weighted average of a field across a mesh, where weights
    are provided as a scalar field. The field can be either a scalar field (with shape `(nx,)`)
    or a vector field (with shape `(nx, 3)`).

    Parameters
    ----------
    field : Union[MeshScalar, MeshVector]
        The field to be averaged. This can be a scalar field (one-dimensional array)
        or a vector field (three-dimensional array with components along each axis).
    weights : MeshScalar
        The weights used to calculate the weighted average, represented as a scalar field
        of the same length as the number of elements in `field`.

    Returns
    -------
    Union[MeshScalar, MeshVector]
        The weighted average of the input field. Returns a `MeshScalar` if the input
        `field` is a scalar field, and a `MeshVector` if the input `field` is a vector field.

    Raises
    ------
    ValueError
        If the shapes of `field` and `weights` are incompatible.

    Notes
    -----
    - For a scalar field `field` with shape `(nx,)`, the function calculates a single
      averaged scalar value.
    - For a vector field `field` with shape `(nx, 3)`, the function calculates the
      averaged value for each component (x, y, z), resulting in a `MeshVector`.

    Examples
    --------
    >>> field = MeshScalar(np.array([1.0, 2.0, 3.0]))
    >>> weights = MeshScalar(np.array([0.2, 0.5, 0.3]))
    >>> weighted_average(field, weights)
    MeshScalar(2.0)

    >>> field = MeshVector(np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]]))
    >>> weights = MeshScalar(np.array([0.2, 0.5, 0.3]))
    >>> weighted_average(field, weights)
    MeshVector([4.6, 5.6, 6.6])

    """
    total_volume = np.sum(weights)

    nx = len(weights)

    if field.shape == (nx,):
        # scalar field case
        averaged_field = np.sum(field * weights) / total_volume
        return averaged_field

    elif field.shape == (nx, 3):
        # vector field case

        if not isinstance(field, MeshVector):
            field = MeshVector(field)

        averaged_field_x = np.sum(field.x * weights) / total_volume
        averaged_field_y = np.sum(field.y * weights) / total_volume
        averaged_field_z = np.sum(field.z * weights) / total_volume

        return np.array([averaged_field_x, averaged_field_y, averaged_field_z])


def interpolate_along_curve(  # noqa: PLR0913
    xyz: MeshVector,
    field: Union[MeshScalar, MeshVector],
    curve: Union[MeshVector, Tuple],
    num_points: int = 100,
    return_curve: bool = False,
    dim: int = 2,
) -> Union[MeshVector, Tuple[MeshVector, MeshVector]]:
    """
    Interpolate unstructured data along a specified line or curve.

    Parameters
    ----------
    xyz : MeshVector, shape (n, 3)
        The coordinates on the mesh.
    field : MeshScalar or MeshVector
        The data values  associated with each data point.
    curve : ndarray, shape (nx, 3) or tuple/list of shape (2, 3)
        The points along the interpolation curve or the start and end points of the curve
        ``((x1, y1, z1),(x2, y2, z2))``.
    num_points : int
        The number of points to interpolate along the curve. Will only be considered if start
        and end points are given (default is 100).
    return_curve : bool
        Next to the interpolated values, also return the interpolation curve.
    dim : int
        Dimension of the mesh (needs to be specified for interpolation to work properly).

    Returns
    -------
    interp_values : ndarray, shape (num_points, m)
        The interpolated values along the curve with ``m`` being the size of the second dimension of ``field``.
    curve_coords : MeshVector
        The coordinates values along the curve (only returned if ``return_curve=True``).

    """
    curve = np.array(curve)

    # start and end point was supplied
    if curve.shape == (2, 3):
        start_point = curve[0]
        end_point = curve[1]
        x = np.linspace(start_point[0], end_point[0], num_points)
        y = np.linspace(start_point[1], end_point[1], num_points)
        z = np.linspace(start_point[2], end_point[2], num_points)
        curve_to_fit = np.vstack((x, y, z)).T

    # A set of points was supplied for the curve
    elif curve.shape[0] > 2 and curve.shape[1] == 3:  # noqa: PLR2004
        curve_to_fit = curve

    # Nothing proper was supplied.
    else:
        raise ValueError(
            "Malshaped 'curve' argument. Should be either an array_like of shape "
            "(nx, 3) or a array_like of shape (2, 3) containing the start and end point as ((x1, y1, z1),"
            "(x2, y2, z2))."
        )

    # throwing away unecessary values depending on the dimension (need for griddata)
    # xyz_to_interpolate = xyz if dim == 3 else (xyz[:, :2] if dim == 2 else xyz[:, 1])
    curve_to_interpolate = (
        curve_to_fit
        if dim == 3  # noqa: PLR2004
        else (curve_to_fit[:, :2] if dim == 2 else curve_to_fit[:, 1])  # noqa: PLR2004
    )

    curve_to_fit = MeshVector(curve_to_fit)
    scan = griddata(xyz[:, :2], field, curve_to_interpolate[:, :2])
    scan = MeshVector(scan) if isinstance(field, MeshVector) else scan

    return (scan, curve_to_fit) if return_curve else scan


def diag_matrix_from_field(v: Union[MeshScalar, FlattenedMeshVector]) -> dia_matrix:
    """
    Create a sparse diagonal matrix from a scalar or flattened vector field.

    This function constructs a sparse diagonal matrix in DIAgonal (DIA) format using
    the values from the input field `v`. Each entry in `v` is placed on the main
    diagonal of the resulting matrix.

    Parameters
    ----------
    v : Union[MeshScalar, FlattenedMeshVector]
        The input field, which can be a scalar field (`MeshScalar`) or a flattened
        vector field (`FlattenedMeshVector`). The length of `v` determines the
        dimensions of the resulting square matrix.

    Returns
    -------
    dia_matrix
        A sparse diagonal matrix of shape `(len(v), len(v))` with the values
        of `v` on the main diagonal.

    Examples
    --------
    >>> v = MeshScalar(np.array([1.0, 2.0, 3.0]))
    >>> diag_matrix_from_field(v).toarray()
    array([[1.0, 0.0, 0.0],
           [0.0, 2.0, 0.0],
           [0.0, 0.0, 3.0]])

    >>> v = FlattenedMeshVector(np.array([4.0, 5.0, 6.0]))
    >>> diag_matrix_from_field(v).toarray()
    array([[4.0, 0.0, 0.0],
           [0.0, 5.0, 0.0],
           [0.0, 0.0, 6.0]])

    """
    return dia_matrix(([v], [0]), shape=(len(v), len(v)))


def cross_operator(nx: int) -> scipy.sparse.dia.dia_matrix:
    """Return the rotated cross product operator as a crs_matrix."""
    return dia_matrix(
        ([np.ones(2 * nx), -np.ones(2 * nx)], [-nx, nx]), shape=(2 * nx, 2 * nx)
    )


def is_positive(x: Union[float, np.ndarray]) -> bool:
    """
    Check if a number or array is positive (including zero).

    Parameters
    ----------
    x : float or ndarray
        The number or array to check.

    Returns
    -------
    bool
        True if the number or array is positive (including zero), False otherwise.

    """
    return np.all(np.array(x) >= 0)


def is_strictly_positive(x: Union[float, np.ndarray]) -> bool:
    """
    Check if a number or array is strictly positive.

    Parameters
    ----------
    x : float or ndarray
        The number or array to check.

    Returns
    -------
    bool
        True if the number or array is strictly positive, False otherwise.

    """
    return np.all(np.array(x) > 0)


def inner_product(vec1: AnyVector, vec2: AnyVector) -> MeshScalar:
    """
    Calculate the inner product of two vector fields at each point in space.

    Parameters
    ----------
    vec1 : AnyVector
        The first vector field.
    vec2 : AnyVector
        The second vector field.

    Returns
    -------
    MeshScalar
        The resulting scalar field representing the inner product.

    """
    nx = vec1.nx

    if (nx2 := vec2.nx) != nx:
        raise ValueError(
            f"Input vectors do not have the same number of nodes ({nx} and {nx2})"
        )

    return MeshScalar(
        np.sum([xi * xj for xi, xj in zip(vec1.components, vec2.components)], axis=0)
    )


def cross_product(vec1: LabVector, vec2: LabVector) -> LabVector:
    """
    Calculate the cross product of two lab-frame (3D) vector fields at each point in space.

    Parameters
    ----------
    vec1 : LabVector
        The first vector field.
    vec2 : LabVector
        The second vector field.

    Returns
    -------
    LabVector
        The resulting vector field representing the cross product.

    """
    nx = vec1.nx

    if (nx2 := vec2.nx) != nx:
        raise ValueError(
            f"Input vectors do not have the same number of nodes ({nx} and {nx2})"
        )

    vec3 = type(vec1)(np.empty_like(vec1))

    vec3.x = vec1.y * vec2.z - vec1.z * vec2.y
    vec3.y = vec1.z * vec2.x - vec1.x * vec2.z
    vec3.z = vec1.x * vec2.y - vec1.y * vec2.x

    return vec3


def cross_product_ndarray(vec1: NDArray, vec2: NDArray) -> NDArray:
    """
    Calculate the cross product of two lab-frame (3D) vector fields at each point in space.

    This is a faster version of :func:`cross_product` and requires the input vectors to be in
    flattened shape.

    Parameters
    ----------
    vec1 : NDArray
        The first vector field.
    vec2 : NDArray
        The second vector field.

    Returns
    -------
    NDArray
        The resulting vector field representing the cross product.

    """
    nx = vec1.shape[0] // 3

    if nx2 := vec2.shape[0] // 3 != nx:
        raise ValueError(
            f"Input vectors do not have the same number of nodes ({nx} and {nx2})"
        )

    vec3 = np.empty_like(vec1)

    vec3[:nx] = vec1[nx : 2 * nx] * vec2[-nx:] - vec1[-nx:] * vec2[nx : 2 * nx]
    vec3[nx : 2 * nx] = vec1[-nx:] * vec2[:nx] - vec1[:nx] * vec2[-nx:]
    vec3[-nx:] = vec1[:nx] * vec2[nx : 2 * nx] - vec1[nx : 2 * nx] * vec2[:nx]

    return vec3


def tensor_product(vec1: AnyVector, vec2: AnyVector) -> scipy.sparse.csr.csr_matrix:
    """
    Calculate the tensor product of two vector fields.

    The rows/columns of the tensor product will always be ordered in flattened order.

    Parameters
    ----------
    vec1 : AnyVector
        The first vector field.
    vec2 : AnyVector
        The second vector field.

    Returns
    -------
    csr_matrix
        The resulting sparse matrix representing the tensor product.

    """
    nx = vec1.nx

    if (nx2 := vec2.nx) != nx:
        raise ValueError(
            f"Input vectors do not have the same number of nodes ({nx} and {nx2})"
        )

    blocks = [
        [dia_matrix((c1 * c2, 0), shape=(nx, nx)) for c2 in vec2.components]
        for c1 in vec1.components
    ]
    sparse_matrix = bmat(blocks, format="csr")

    return sparse_matrix
