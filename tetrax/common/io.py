"""
Input/output (:mod:`tetrax.common.io`)
======================================

Functionality related to input and output of files and data.

.. autosummary::
    :toctree: generated/

    clear_folder
    fetch_reference_data
    left_align_html_cols
    pretty_array_print
    read_mode_from_vtk
    write_field_to_file

"""  # noqa: D205, D400, D415

from __future__ import annotations

import os
import shutil
from typing import TYPE_CHECKING, Optional, Tuple, Union

import meshio
import numpy as np
import pandas as pd
import pooch

from tetrax.common.config import REFERENCE_DATA_ZENODO_ID
from tetrax.common.typing import MagneticOrder
from tetrax.sample.mesh import (
    FlattenedAFMMeshVector,
    FlattenedMeshVector,
    MeshScalar,
    MeshVector,
    make_flattened_AFM,
)

if TYPE_CHECKING:
    from pathlib import Path

    from numpy._typing import ArrayLike

    from tetrax import Sample

# When creating a new record version on zenodo, only the record ID is changed, the hashes stay.
ONLINE_REF_DATA = {
    "example_dispersion_gallardo_exchange_coupled_bilayers_20nm":
        {
            "url": f"https://zenodo.org/records/{REFERENCE_DATA_ZENODO_ID}/files/example_dispersion_gallardo_exchange_coupled_bilayers_20nm.csv?download=1",
            "known_hash": "md5:60f3ece7e25109de47d7b3fe34df0f51"
        },
    "example_equilibrium_mumax3_neel_wall_iDMI":
        {
            "url": f"https://zenodo.org/records/{REFERENCE_DATA_ZENODO_ID}/files/example_equilibrium_mumax3_neel_wall_iDMI.csv?download=1",
            "known_hash": "md5:4da21aea10b43ddd5f17555fdba3cb09"
        },
    "example_dispersion_mumax3_JVK_neel_wall_iDMI":
        {
            "url": f"https://zenodo.org/records/{REFERENCE_DATA_ZENODO_ID}/files/example_dispersion_mumax3_JVK_neel_wall_iDMI.csv?download=1",
            "known_hash": "md5:b62f2f50abc66b67ba10fd7f0b28ce6b"
        },
    "example_dispersion_SWIIM_DE_50nm_Py_monolayer_20mT":
        {
            "url": f"https://zenodo.org/records/{REFERENCE_DATA_ZENODO_ID}/files/example_dispersion_SWIIM_DE_50nm_Py_monolayer_20mT.csv?download=1",
            "known_hash": "md5:35fcb181cd3cd18f008a112bb0604272",
        }

}
"""Links to available online reference data."""


def read_mode_from_vtk(
        filename: str,
        as_flattened: bool = False,
        magnetic_order: Union[str, MagneticOrder] = MagneticOrder.FERROMAGNET,
) -> Union[
    MeshVector,
    FlattenedMeshVector,
    Tuple[MeshVector, MeshVector],
    FlattenedAFMMeshVector,
]:
    """
    Read a complex-valued spatial mode profile from a `vtk` file.

    Parameters
    ----------
    filename : str
        Filename of vtk file.
    as_flattened : bool, optional
        Return mode profile as flattened mesh vector (default is `False`).
    magnetic_order : str or MagneticOrder
        Magnetic order of the mode.

    Returns
    -------
    MeshVector or FlattenedMeshVector
        Mode profile as mesh vector of shape `(N, 3)` if `as_flattened=False` or
        as flattened mesh vector of shape `(3*N,)` if `as_flattened=True`.

    Notes
    -----
    Requires that the vtk file contains the data fields "Re(m_x)", "Im(m_x)" and so forth.

    """
    magnetic_order = MagneticOrder(magnetic_order)

    vtk_file = meshio.read(filename)

    if magnetic_order is MagneticOrder.FERROMAGNET:
        mode = MeshVector(
            vtk_file.point_data["Re(m)"] + 1j * vtk_file.point_data["Im(m)"]
        )
        return mode.to_flattened() if as_flattened else mode

    if magnetic_order is MagneticOrder.ANTIFERROMAGNET:
        mode1 = MeshVector(
            vtk_file.point_data["Re(m1)"] + 1j * vtk_file.point_data["Im(m1)"]
        )
        mode2 = MeshVector(
            vtk_file.point_data["Re(m2)"] + 1j * vtk_file.point_data["Im(m2)"]
        )
        return make_flattened_AFM(mode1, mode2) if as_flattened else (mode1, mode2)


def write_field_to_file(
        field: Union[MeshScalar, MeshVector],
        sample: Sample,
        filename: str = "field.vtk",
        name_quantity: Optional[str] = None,
) -> None:
    """
    Write a scalar or vector field (MeshScalar or MeshVector) or a list of fields to a file using meshio.

    Parameters
    ----------
    field : MeshScalar, MeshVector array_like, or list
        Input scalar- or vector field(s) to be saved to the file. If input is not MeshScalar or MeshVector,
        a shape check will be performed to infer the data type.
    sample : AbstractSample
        The sample on which the scalar- or vector field is defined. Necessary to save mesh with vtk.
    filename : str
        Name of the file, has to contain a file ending from which the format can be
        inferred (Default is `"field.vtk"`)
    name_quantity : str or list(str)
        Name(s) of the scalar or vector quantity (quantities) to appear in the file (default is `None`). If `qname` is
        `None`, then the name(s) will be set to `scalar` or `vector`, depending on each input field.

    """
    # make to list if not already list
    fields = [field] if not isinstance(field, list) else field
    qnames = [name_quantity] if not isinstance(name_quantity, list) else name_quantity

    # dictionary to save to vtk
    save_dict = {}
    for qname, single_field in zip(qnames, fields):
        if single_field.shape == (sample.mesh.nx,):
            # scalar field case
            qname = "scalar" if qname is None else qname  # noqa: PLW2901

        elif field.shape == (sample.mesh.nx, 3):
            # vector field case

            if not isinstance(field, MeshVector):
                single_field = MeshVector(field)  # noqa: PLW2901

            qname = "vector" if qname is None else qname  # noqa: PLW2901

        else:
            print(
                "Malshaped input field. Only fields with shape of appropriate MeshScalar or MeshVector can be saved."
            )

        save_dict[qname] = single_field

    meshio.write_points_cells(
        filename=filename,
        points=sample.xyz,
        cells=sample.mesh._meshio_mesh.cells,
        point_data=save_dict,
    )


def pretty_array_print(array: ArrayLike) -> str:
    """Print a nice summary of an array in one line, showing only the first and last couple of values."""
    if np.shape(array) == (1,):
        return str(array[0])

    if np.shape(array)[0] <= 9:  # noqa: PLR2004
        return str(array)

    if np.shape(array)[0] >= 10:  # noqa: PLR2004
        return f"{array[0]}, {array[1]}, ..., {array[-2]}, {array[-1]}"


def left_align_html_cols(html_table: str) -> str:
    """Manually left-aligns the text all columns of a html table except the first column."""
    html_table = html_table.replace("/td><td", '/td><td style="text-align: left;"')
    return html_table.replace("/th><th", '/th><th style="text-align: left;"')


def clear_folder(path: Path) -> None:
    """Clear a folder on the disk, if it exists."""
    if os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path)


def fetch_reference_data(key: str) -> pd.DataFrame:
    """Fetch reference data from online repository and return as dataframe."""
    file_path = pooch.retrieve(
        url=ONLINE_REF_DATA[key]["url"],
        known_hash=ONLINE_REF_DATA[key]["known_hash"],
    )
    return pd.read_csv(file_path, comment="#")
