"""
.. currentmodule:: tetrax.common

This subpackage provides a collection of common functionality (such as mathematical functions, types, global
configuration, etc.) that is used throughout the package. For detailed information, click the individual functions.
"""  # noqa: D400, D415
