"""
Common utilities (:mod:`tetrax.common.utils`)
=========================================

Generic functionality for the whole package.

.. autosummary::
    :toctree: generated/

    deepcopy_mutable_attributes
"""  # noqa: D205, D400, D415

from __future__ import annotations

from copy import deepcopy


def deepcopy_mutable_attributes(instance: object) -> None:
    """
    Create deep copies of mutable attributes in a class instance.

    Parameters
    ----------
    instance : object
        The instance whose mutable attributes should be deep copied.

    Returns
    -------
    None
        This function modifies the instance in-place.

    Examples
    --------
    >>> my_list = [1, 2, 3]
    >>> obj = MyClass(internal_list=my_list)
    >>> deepcopy_mutable_attributes(obj)
    >>> my_list.append(4)
    >>> print(my_list.internal_list)
    [1, 2, 3]  # The internal_list has been deep copied and remains unchanged.

    """
    for attribute_name, attribute_value in instance.__dict__.items():
        if hasattr(attribute_value, "__setitem__"):  # Check if the attribute is mutable
            deep_copied_attribute = deepcopy(attribute_value)
            object.__setattr__(instance, attribute_name, deep_copied_attribute)
