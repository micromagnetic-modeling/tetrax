"""
Bibliography (:mod:`tetrax.common.bibliography`)
================================================

Functionality to generate the *BibTex* file that is created in the directory of the respective
:class:`Sample <tetrax.sample.sample.Sample>` and contains important references for the numerical experiments
that are performed.

.. autosummary::
    :toctree: generated/

    add_refereneces_to_bib
    get_existing_bib_ids
"""  # noqa: D205, D400, D415

from __future__ import annotations

import os
from typing import TYPE_CHECKING, List

import bibtexparser

if TYPE_CHECKING:
    from bibtexparser.bibdatabase import BibDatabase

    from tetrax import Sample

reference_tetrax = {
    "doi": "10.14278/rodare.1418",
    "month": "jan",
    "year": "2022",
    "author": "K{\\\"o}rber, Lukas and Quasebarth, Gwendolyn and Hempel, Alexander and Zahn, Friedrich and Andreas, Otto and Westphal, Elmar and Hertel, Riccardo and K{\\'a}kay, Attila",
    "title": "{{TetraX}}: {{Finite-Element Micromagnetic-Modeling Package}}",
    "ENTRYTYPE": "misc",
    "ID": "tetrax",
}
reference_dynmat2d = {
    "publisher": "AIP Publishing LLC",
    "year": "2021",
    "pages": "095006",
    "number": "9",
    "volume": "11",
    "journal": "AIP Advances",
    "author": "K{\\\"o}rber, Lukas and Quasebarth, Gwendolyn and Otto, Andreas and K{\\'a}kay, Attila",
    "title": "Finite-element dynamic-matrix approach for spin-wave dispersions in magnonic waveguides with arbitrary cross section",
    "ENTRYTYPE": "article",
    "ID": "fem_dynmat_SW",
    "comment": "For eigenmodes(). FEM dynamic matrix approach for propagating spin waves in waveguides with arbitrary cross section.",
}
reference_dynmat1d = {
    "copyright": "arXiv.org perpetual, non-exclusive license",
    "year": "2022",
    "publisher": "arXiv",
    "title": "Finite-element dynamic-matrix approach for propagating spin waves: Extension to mono- and multilayers of arbitrary spacing and thickness",
    "keywords": "Mesoscale and Nanoscale Physics (cond-mat.mes-hall), Materials Science (cond-mat.mtrl-sci), FOS: Physical sciences, FOS: Physical sciences",
    "author": "Körber, Lukas and Hempel, Alexander and Otto, Andreas and Gallardo, Rodolfo and Henry, Yves and Lindner, Jürgen and Kákay, Attila",
    "url": "https://arxiv.org/abs/2207.01519",
    "doi": "10.48550/ARXIV.2207.01519",
    "ENTRYTYPE": "misc",
    "ID": "fem_dynmat_SW_layers",
    "comment": "For eigenmodes(). FEM dynamic matrix approach for propagating spin waves in infinitely extended multilayers.",
}
reference_hexa = {
    "publisher": "APS",
    "year": "2021",
    "pages": "184429",
    "number": "18",
    "volume": "104",
    "journal": "Physical Review B",
    "author": 'K{\\"o}rber, Lukas and Zimmermann, Michael and Wintz, Sebastian and Finizio, Simone and Kronseder, Matthias and Bougeard, Dominique and Dirnberger, Florian and Weigand, Markus and Raabe, J{\\"o}rg and Ot{\\\'a}lora, Jorge A and others',
    "title": "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes",
    "ENTRYTYPE": "article",
    "ID": "korber2021symmetry",
    "comment": "For absorption(). Description how to calculate line widhts and absorption from mode profiles of propagating spin waves.",
}
reference_damping = {
    "keywords": "damping,gyrotropic,linear,theory,vortex",
    "doi": "10.1103/PhysRevB.98.104408",
    "pages": "104408",
    "number": "10",
    "volume": "98",
    "journal": "Physical Review B",
    "month": "apr",
    "year": "2018",
    "author": "Verba, Roman and Tiberkevich, Vasil and Slavin, Andrei",
    "title": "Damping of Linear Spin-Wave Modes in Magnetic Nanostructures: {{Local}}, Nonlocal, and Coordinate-Dependent Damping",
    "ENTRYTYPE": "article",
    "ID": "verbaDampingLinearSpinwave2018",
    "comment": "For absorption(). Description how to calculate line widhts and absorption from general mode profiles.",
}
reverse_engineering_reference = {
    "publisher": "APS",
    "year": "2021",
    "pages": "174414",
    "number": "17",
    "volume": "104",
    "journal": "Physical Review B",
    "author": "K{\\\"o}rber, Lukas and K{\\'a}kay, Attila",
    "title": "Numerical reverse engineering of general spin-wave dispersions: Bridge between numerics and analytics using a dynamic-matrix approach",
    "ENTRYTYPE": "article",
    "ID": "korber2021numerical",
    "comment": "Description how to numerically calculate perturbed SW dispersion and individual stiffness fields.",
}

references = {
    "tetrax": [reference_tetrax],
    "eigenmodes2d": [reference_dynmat2d],
    "eigenmodes3d": [reference_dynmat2d],
    "absorption1d": [reference_hexa, reference_damping],
    "absorption2d": [reference_hexa, reference_damping],
    "eigenmodes1d": [reference_dynmat1d],
    "perturbation1d": [reference_dynmat1d, reverse_engineering_reference],
    "perturbation2d": [reference_dynmat2d, reverse_engineering_reference],
    "perturbation3d": [reference_dynmat2d, reverse_engineering_reference],
    "linewidth": [reference_damping],
}


def add_refereneces_to_bib(sample: Sample, entries_for: str) -> None:
    """
    Add a list of references to the bibfile of the project.

    Add the references relevant for a certain numerical experiment (depending on the sample geometry) to the bibtex
    file ``references.bib`` of the sample. If this file does not exist, it will be created.

    Parameters
    ----------
    sample : AbstractSample
        Sample object at hand.
    entries_for : str, {'eigenmodes1d', 'eigenmodes2d', 'absorption', 'tetrax', 'linewidth'}
        Key indicating which references to add (e.g., 'eigenmodes1d', 'eigenmodes2d', 'absorption').

    """
    # TODO This function should be refactored at some point.

    bibfile_name = sample.name + "/references.bib"

    # if sample directory does not exist yet, create it
    if not os.path.exists(sample.name):
        os.makedirs(sample.name)

    # if the bib file does not exist, create it
    if not os.path.exists(bibfile_name):
        open(bibfile_name, "a").close()

    # now open the file
    with open(bibfile_name) as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file)

    ids = get_existing_bib_ids(bib_database)

    for entry in references[entries_for]:
        if entry["ID"] not in ids:
            bib_database.entries.append(entry)

    # write back to file
    with open(bibfile_name, "w") as bibtex_file:
        bibtexparser.dump(bib_database, bibtex_file)


def get_existing_bib_ids(db: BibDatabase) -> List[str]:
    """Get list of existing BibTeX entry IDs from the database generated from a BibTeX file."""
    return [entry["ID"] for entry in db.entries]
