"""
Confined samples
----------------

These template geometries with :attr:`GeometryType.CONFINED <tetrax.common.typing.GeometryType.CONFINED>` are fully
three-dimensional meshes that represent systems with a general shape and no particular symmetry.

.. currentmodule:: tetrax.geometries

.. autosummary::
    :toctree: generated/

    confined.disk
    confined.prism
    confined.tube

.. warning::
    These geometries so far only included for experimental purposes and mostly not supported.
"""  # noqa: D205, D400, D415
from __future__ import annotations

from typing import TYPE_CHECKING

import pygmsh

from tetrax.sample.mesh.sample_mesh import GeometryType

if TYPE_CHECKING:
    from tetrax.sample.mesh.sample_mesh import MeshioMesh


__all__ = ["disk", "tube", "prism"]


def disk(  # noqa: PLR0913
    radius: float,
    thickness: float,
    cell_size_lateral: float = 5,
    nl: int = 0,
    x0: float = 0,
    y0: float = 0,
) -> MeshioMesh:
    """
    Round disk.

    Parameters
    ----------
    radius : float
        Radius of the round wire.
    thickness : float
        thickness of the disk.
    cell_size_lateral : float
        Characteristic length of mesh in the (xy) plane (default is 5).
    nl : int
        The number of layers along the thickness (default is 3).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    """
    # TODO change such that function accepts cell size in z instead of number of layers
    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = cell_size_lateral
        geom.characteristic_length_max = cell_size_lateral
        disk = geom.add_disk([x0, y0, -thickness / 2], radius)
        nl = nl if nl != 0 else int(thickness / cell_size_lateral)
        geom.extrude(
            disk, translation_axis=(0.0, 0.0, thickness), num_layers=max(2, nl)
        )
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.CONFINED
    return mesh


def tube(  # noqa: PLR0913
    inner_radius: float,
    outer_radius: float,
    length: float,
    cell_size_lateral: float = 5,
    nl: int = 2,
    x0: float = 0,
    y0: float = 0,
) -> MeshioMesh:
    """
    Cylindrical tube with finite length.

    Parameters
    ----------
    inner_radius : float
        Inner radius of the tube.
    outer_radius : float
        Outer radius of the tube.
    length : float
        Length of the tube (along its symmetry axis).
    cell_size_lateral : float
        Characteristic length of mesh within the :math:`xy` plane (default is 5).
    nl : int
        The number of layers along the thickness (default is 3).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    """
    # TODO change such that function accepts cell size in z instead of number of layers

    if inner_radius <= 0 or outer_radius <= 0:
        raise ValueError("Radii cannot be negative.")

    if inner_radius >= outer_radius:
        raise ValueError(
            f"Inner radius ({inner_radius}) has to be strictly "
            f"smaller than outer radius ({outer_radius})."
        )

    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = cell_size_lateral
        geom.characteristic_length_max = cell_size_lateral
        disk1 = geom.add_disk([x0, y0], outer_radius)
        disk2 = geom.add_disk([x0, y0], inner_radius)
        ring = geom.boolean_difference(disk1, disk2)
        geom.extrude(ring, translation_axis=(0, 0, length), num_layers=max(2, nl))
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.CONFINED
    return mesh


def prism(  # noqa: PLR0913
    length: float,
    width: float,
    thickness: float,
    cell_size_length: float = 5,
    cell_size_width: float = 5,
    cell_size_thickness: float = 5,
) -> MeshioMesh:
    """
    Prism with regular mesh.

    .. image:: /usage/prism.png
       :width: 350

    Parameters
    ----------
    length : float
        Length of the prism.
    width : float
        Width of the prism.
    thickness : float
        Thickness of the prism.
    cell_size_length : float
        characteristic mesh length along the length (Default is 5).
        The number of FEM layers along the length will be `a/lca + 1`.
    cell_size_width : float
        characteristic mesh length along the width (Default is 5).
        The number of FEM layers along the width will be `b/lcb + 1`.
    cell_size_thickness : float
        characteristic mesh length along the thickness (Default is 5).
        The number of FEM layers along the thickness will be `c/lcc + 1`.

    Returns
    -------
    mesh : MeshioMesh

    """
    with pygmsh.geo.Geometry() as geom:
        nx = int(length / cell_size_length)
        ny = int(width / cell_size_width)
        nz = int(thickness / cell_size_thickness)
        p = geom.add_point(
            [-length / 2, -width / 2, -thickness / 2], mesh_size=cell_size_length
        )
        _, line, _ = geom.extrude(p, translation_axis=(length, 0, 0), num_layers=nx)
        _, surface, _ = geom.extrude(
            line, translation_axis=(0, width, 0), num_layers=ny
        )
        geom.extrude(surface, translation_axis=(0, 0, thickness), num_layers=max(2, nz))
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.CONFINED
    return mesh
