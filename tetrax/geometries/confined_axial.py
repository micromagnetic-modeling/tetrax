"""
Confined samples (axial)
------------------------

These template geometries with :attr:`GeometryType.CONFINED_AXIAL <tetrax.common.typing.GeometryType.CONFINED_AXIAL>`
represent meridional surfaces (cross-sections in the :math:`xz` plane) of bodies of revolution. To form the confined
sample, these surfaces are implicitly revolved around the :math:`z` axis. Therefore, axial symmetry of both geometry
and equilibrium magnetization around the :math:`z` axis is implied.

.. currentmodule:: tetrax.geometries

.. autosummary::
    :toctree: generated/

    confined_axial.disk
    confined_axial.ring


.. note::
    So far, interfacial, bulk DMI and dipolar interaction are not support for these geometries.
"""  # noqa: D205, D400, D415
from __future__ import annotations

from typing import TYPE_CHECKING

import pygmsh

from tetrax.sample.mesh.sample_mesh import GeometryType

if TYPE_CHECKING:
    from tetrax.sample.mesh.sample_mesh import MeshioMesh

__all__ = ["disk", "ring"]


def ring(  # noqa: PLR0913
    inner_radius: float,
    outer_radius: float,
    thickness: float,
    cell_size_radius: float = 5,
    cell_size_thickness: float = 5,
    z0: float = 0,
) -> MeshioMesh:
    """
    Meridional surface of a ring.

    Parameters
    ----------
    inner_radius : float
        Inner radius.
    outer_radius : float
        Outer radius.
    thickness :  float
        Thickness in :math:`z` direction.
    cell_size_radius : float
        characteristic mesh length along the radial direction (Default is 5).
        The number of FEM layers along the radius will be `r/lcr + 1`.
    cell_size_thickness : float
        characteristic mesh length along the axial (:math:`z`) direction. (Default is 5).
        The number of FEM layers along this direction will be `T/lcz + 1`.
    z0 : float
        Shift of the origin along the :math:`z` direction.

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    disk_cross_section

    """
    assert outer_radius > inner_radius

    with pygmsh.geo.Geometry() as geom:
        nx = int((outer_radius - inner_radius) / cell_size_radius)
        nz = int(thickness / cell_size_thickness)
        p = geom.add_point(
            [inner_radius, 0.0, -thickness / 2 - z0], mesh_size=cell_size_radius
        )
        _, line, _ = geom.extrude(
            p, translation_axis=(outer_radius - inner_radius, 0, 0), num_layers=nx
        )
        geom.extrude(line, translation_axis=(0, 0, thickness), num_layers=max(2, nz))
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.CONFINED_AXIAL
    return mesh


def disk(
    radius: float,
    thickness: float,
    cell_size_radius: float = 5,
    cell_size_thickness: float = 5,
    x0: float = 0,
) -> MeshioMesh:
    """
    Meridional surface of a disk.

    Parameters
    ----------
    radius : float
        Radius of the disk.
    thickness : float
        Thickness of the disk in :math:`z` direction.
    cell_size_radius : float
        Characteristic mesh length along the radial direction.
    cell_size_thickness:  float
        Characteristic mesh length along the thickness (:math:`z`) direction.
    x0 : float
        Shift of the mesh along the radial (:math:`x`) direction.

    Returns
    -------
        mesh: MeshioMesh

    """
    with pygmsh.geo.Geometry() as geom:
        nx = int(radius / cell_size_radius)
        nz = int(thickness / cell_size_thickness)
        p = geom.add_point([x0, 0.0, -thickness / 2], mesh_size=cell_size_radius)
        _, line, _ = geom.extrude(p, translation_axis=(radius, 0, 0), num_layers=nx)
        geom.extrude(line, translation_axis=(0, 0, thickness), num_layers=max(2, nz))
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.CONFINED_AXIAL
    return mesh
