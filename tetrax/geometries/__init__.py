"""
Geometries (:mod:`tetrax.geometries`)
=====================================

This module provides a couple of template geometries. The names of all available geometries are stored in the
:attr:`tetrax.geometries.available` list. They can also be found by calling :func:`tetrax.geometries.show_available`.
A geometry is assigned to a sample upon its creation. The units are given in the scale of the sample (nanometers
by default).

.. code-block:: python

    from tetrax.geometries.waveguide import tube

    sample = tx.Sample(tube(inner_radius=20,outer_radius=30,cell_size=2))

Each :class:`~tetrax.sample.sample.Sample` has a :attr:`~tetrax.sample.sample.Sample.geometry_type`
attribute of type :class:`~tetrax.common.typing.GeometryType` which is automatically inferred,
in case a template geometry is used.
Geometries can also be read from a file using the :func:`from_file` function. In this case, the geometry type
cannot be inferred and needs to be set manually.

.. code-block:: python

    from tetrax.geometries import from_file

    sample = tx.Sample(from_file("tube_mesh.vtk"), geometry_type="waveguide")

Importantly, the read-in mesh needs to comform with the specifications of the particular
:class:`~tetrax.common.typing.GeometryType`.

.. warning::
    If you model your geometry using `COMSOL`, it is not possible to directly export the mesh into a format
    readable by `TetraX`, as `COMSOL` somehow only saves disconnected nodes and no elements.
    However, you can simply define an arbitrary vector field on your mesh in `COMSOL`,
    export it in vtk/vtu format and read-in this file as the mesh in `TetraX`.

For a more detailed information on each geometry, click the individual function.

.. currentmodule:: tetrax.geometries

.. automodule:: tetrax.geometries.layer

.. automodule:: tetrax.geometries.waveguide

.. automodule:: tetrax.geometries.waveguide_axial

.. automodule:: tetrax.geometries.confined_axial

.. automodule:: tetrax.geometries.confined


.. currentmodule:: tetrax.geometries

Other
-----

.. autosummary::
    :toctree: generated/

    available
    from_file
    show_available

"""  # noqa: D205, D400, D415

from __future__ import annotations

import inspect
from typing import TYPE_CHECKING, List, Optional, Union

import meshio

if TYPE_CHECKING:
    import pathlib

from . import confined, confined_axial, layer, waveguide, waveguide_axial

MODULE_DICTIONARY = {
    "confined": confined,
    "confined_axial": confined_axial,
    "waveguide": waveguide,
    "waveguide_axial": waveguide_axial,
    "layer": layer,
}


def show_available(show_signatures: bool = True) -> None:
    """
    Display the available template geometries and optionally their signatures, for different geometry types.

    This function prints the names of available functions categorized by geometry types.
    Optionally, it can also display the function signatures.

    Parameters
    ----------
    show_signatures : bool, optional
        If True, display the function signatures along with the function names (default is True).

    Returns
    -------
    None
        This function does not return a value. It prints information to the console.

    """
    print("from_file")

    if show_signatures:
        print(f"\t{inspect.signature(from_file)}\n")
    for geometry_type, geometry_module in MODULE_DICTIONARY.items():
        for name, function in inspect.getmembers(geometry_module, inspect.isfunction):
            print(f"{geometry_type}.{name}")
            if show_signatures:
                print(f"\t{inspect.signature(function)}")
        print("")


def _make_list_of_available() -> List[str]:
    """Private function to generate a list of the available template geometries."""
    list_of_available = ["from_file"]
    for geometry_type, geometry_module in MODULE_DICTIONARY.items():
        for name, _function in inspect.getmembers(geometry_module, inspect.isfunction):
            list_of_available.append(f"{geometry_type}.{name}")

    return list_of_available


def from_file(filename: Union[str, pathlib.Path], file_format: Optional[str] = None) -> meshio.Mesh:
    """
    Read a mesh from a file.

    This function uses the `meshio` package to read mesh files in several formats.
    For details, see the documentation of meshio.read().

    Parameters
    ----------
    filename : str or pathlib.Path
        Name of the file to be read.
    file_format : str, Optional
        File extension (is usually inferred from the filename).

    Returns
    -------
    mesh : meshio.Mesh

    """
    print(
        "Reading mesh from a file, with unknown geometry type. Take care too specify it with "
        "Sample(mesh, geometry_type=...) when creating a sample with this mesh."
    )

    return meshio.read(filename, file_format)


available = _make_list_of_available()
"""List of available geometries."""
