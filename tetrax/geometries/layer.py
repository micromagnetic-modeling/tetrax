"""
Layers
------

These template geometries with :attr:`GeometryType.LAYER <tetrax.common.typing.GeometryType.LAYER>` represent
line traces along the normal (:math:`y`) direction of a layered system. Translational invariance of both geometry
and equilibrium magnetization in the :math:`xz` plane is implied.


.. currentmodule:: tetrax.geometries

.. autosummary::
    :toctree: generated/

    layer.bilayer
    layer.monolayer
    layer.multilayer
"""  # noqa: D205, D400, D415

from __future__ import annotations

from typing import TYPE_CHECKING

import pygmsh

from tetrax.sample.mesh.sample_mesh import GeometryType

if TYPE_CHECKING:
    from typing import List, Union

    from tetrax.sample.mesh.sample_mesh import MeshioMesh

__all__ = ["monolayer", "bilayer", "multilayer"]


def monolayer(thickness: float, cell_size: float = 2, y0: float = 0) -> MeshioMesh:
    """
    Line trace along the normal direction of a magnetic monolayer.

    .. image:: /usage/monolayer_line_trace.png
       :width: 350

    Parameters
    ----------
    thickness : float
        Thickness :math:`d` of the monolayer.
    cell_size : float
        Characteristic mesh length.
    y0 : float
        y coordinate of the center (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    bilayer, multilayer

    """
    return multilayer([thickness], [], cell_size, y0)


def bilayer(
        thickness1: float,
        thickness2: float,
        spacing: float,
        cell_sizes: Union[float, List[float]],
        y0: float = 0,
) -> MeshioMesh:
    """
    Line trace along the normal direction of a magnetic bilayer.

    .. image:: /usage/bilayer_line_trace.png
       :width: 350

    Parameters
    ----------
    thickness1 : float
        Thickness :math:`d_1` of the bottom magnetic layer.
    thickness2 : float
        Thickness :math:`d_2` of the top magnetic layer.
    spacing : float
        Thickness :math:`s` of the interlayer.
    cell_sizes : float or list(float)
        Characteristic lengths for the different magnetic layers. If list, has to contain two elements. If
        not a list it will be same for both layers.
    y0 : float
        y coordinate of the center (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    monolayer, multilayer

    """
    return multilayer([thickness1, thickness2], [spacing], cell_sizes, y0)


def multilayer(
        thicknesses: List[float],
        spacings: List[float],
        cell_sizes: Union[float, List[float]],
        y0: float = 0,
) -> MeshioMesh:
    r"""
    Line trace along the normal direction of a magnetic multi-layer.

    .. image:: /usage/multilayer_line_trace.png
       :width: 350

    Parameters
    ----------
    thicknesses : list(float)
        Thicknesses :math:`[d_1, d_2, d_3, ...]` of the magnetic layers (ordered with increasing :math:`y` coordinate). List must be one element
        longer than ``spacings``.
    spacings : list(float)
        Spacings :math:`[s_1, s_2, s_3, ...]` between the magnetic layers, which is are the thicknesses of the non-magnetic interlayers
        (ordered with increasing :math:`y` coordinate). List must be one element shorter than ``thicknesses``.
    cell_sizes : list(float) or float
        Characteristic lengths for the different magnetic layers. If list, has to be same length as ``thicknesses``. If
        not a list it will be same for all layers.
    y0 : float
        y coordinate of the center (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    monolayer, bilayer

    """
    cell_sizes = (
        [cell_sizes for d in thicknesses]
        if not isinstance(cell_sizes, list)
        else cell_sizes
    )

    if len(spacings) != len(thicknesses) - 1:
        print(
            f"Number of layer thicknesses ({len(thicknesses)}) and number of spacings ({len(spacings)}) do "
            f"not match (has to be off by one)."
        )

    if len(cell_sizes) != len(thicknesses):
        print(
            f"Number of layer thicknesses ({len(thicknesses)}) and number of characteristic lengths ({len(cell_sizes)}) do "
            f"not match (has to be the same)."
        )

    d_tot = sum(spacings) + sum(thicknesses)

    with pygmsh.geo.Geometry() as geom:
        y1_i = -d_tot / 2 + y0
        for d, s, lc in zip(thicknesses, [*spacings, 0], cell_sizes):
            ny_i = int(d / lc)
            p1 = geom.add_point([0.0, y1_i, 0.0], mesh_size=lc)
            geom.extrude(p1, translation_axis=(0.0, d, 0.0), num_layers=ny_i)
            y1_i += d + s
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.LAYER
    return mesh
