"""
Waveguides (axial)
-----------------

These template geometries with :attr:`GeometryType.WAVEGUIDE_AXIAL <tetrax.common.typing.GeometryType.WAVEGUIDE_AXIAL>`
represent line traces along the radial direction (here, coinciding with the $x$ axis) of infinitely long cylindrical
waveguides. Translational and axial symmetry of both geometry
and equilibrium magnetization along and around the :math:`z` axis are implied.

.. currentmodule:: tetrax.geometries

.. autosummary::
    :toctree: generated/

    waveguide_axial.multitube
    waveguide_axial.round_wire
    waveguide_axial.tube


.. note::
    So far, interfacial and bulk DMI are not yet supported for these geometries.
"""  # noqa: D205, D400, D415
from __future__ import annotations

from typing import TYPE_CHECKING, List, Union

import pygmsh

from tetrax.sample.mesh.sample_mesh import GeometryType

if TYPE_CHECKING:
    from tetrax.sample.mesh.sample_mesh import MeshioMesh

__all__ = ["tube", "round_wire", "multitube"]


def round_wire(radius: float, cell_size: float = 2) -> MeshioMesh:
    """
    Line trace along the radial direction of a round wire.

    Parameters
    ----------
    radius : float
        Radius :math:`R` of the round wire.
    cell_size : float
        Characteristic length of the mesh.

    """
    return multitube([0, radius], cell_size)


def tube(inner_radius: float, outer_radius: float, cell_size: float) -> MeshioMesh:
    """
    Line trace along the radial direction of a cylindrical tube.

    Parameters
    ----------
    inner_radius : float
        Radius :math:`r_1` of the inner mantel.
    outer_radius : float
        Radius :math:`r_2` of the outer mantel.
    cell_size : float
        Characteristic length of the mesh.

    See Also
    --------
    round_wire_line_trace, multitube_line_trace

    """
    return multitube([inner_radius, outer_radius], cell_size)


def multitube(radii: List[float], cell_sizes: Union[float, List[float]]) -> MeshioMesh:
    r"""
    Line trace along the radial direction of a multi-tube.

    Parameters
    ----------
    radii : list(float)
        Radii :math:`[r_1, r_2, r_3, ...]` of the mantel surfaces (ordered with increasing :math:`x`/:math:`\rho`
        coordinate). Has to contain only unique values.
    cell_sizes : list(float) or float
        Characteristic lengths for the different tubes/cylinders. If list, has to be one element shorter than as
        ``radii``. If not a list it will be same for all layers.

    Returns
    -------
    mesh : MeshioMesh

    """
    if len(radii) % 2 != 0:
        raise ValueError(
            f"Number of supplied radii ({len(radii)}) has to be an even number."
        )

    if len(set(radii)) != len(radii):
        raise ValueError(f"List of radii ({radii}) has to contain unique values.")

    cell_sizes = (
        [cell_sizes for d in range(len(radii) // 2)]
        if not isinstance(cell_sizes, list)
        else cell_sizes
    )

    if len(cell_sizes) != len(radii) // 2:
        print(
            f"Number of mantel radii ({len(radii)}) and number of characteristic lengths ({len(cell_sizes)}) do "
            f"not match (has to be half)."
        )

    radii = sorted(radii)

    with pygmsh.geo.Geometry() as geom:

        for i in range(len(cell_sizes)):
            d_i = radii[2 * i + 1] - radii[2 * i]

            nx_i = int(d_i / cell_sizes[i])
            p1 = geom.add_point([radii[2 * i], 0.0, 0.0], mesh_size=cell_sizes[i])
            geom.extrude(p1, translation_axis=(d_i, 0.0, 0.0), num_layers=nx_i)
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.WAVEGUIDE_AXIAL
    return mesh
