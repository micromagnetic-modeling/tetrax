"""
Waveguides
----------

These template geometries with :attr:`GeometryType.WAVEGUIDE <tetrax.common.typing.GeometryType.WAVEGUIDE>` represent
cross-sections within the :math:`xy` plane of infinitely long waveguides. Translational invariance of both geometry
and equilibrium magnetization along the :math:`z` axis is implied.

.. currentmodule:: tetrax.geometries

.. autosummary::
    :toctree: generated/

    waveguide.polygonal
    waveguide.rectangular
    waveguide.round_wire
    waveguide.round_wire_refined
    waveguide.tube
    waveguide.tube_segment

"""  # noqa: D205, D400, D415
from __future__ import annotations

from typing import TYPE_CHECKING, Callable, List

import numpy as np
import pygmsh

from tetrax.sample.mesh.sample_mesh import GeometryType

if TYPE_CHECKING:
    from tetrax.sample.mesh.sample_mesh import MeshioMesh

__all__ = [
    "tube",
    "tube_segment",
    "round_wire",
    "round_wire_refined",
    "polygonal",
    "rectangular",
    "round_wire_refined",
]


def tube(
    inner_radius: float,
    outer_radius: float,
    cell_size: float = 5,
    x0: float = 0,
    y0: float = 0,
) -> MeshioMesh:
    """
    Cross-section of a tube.

    .. image:: /usage/tube_cross_section.png
       :width: 350

    Parameters
    ----------
    inner_radius : float
        Inner radius of the tube.
    outer_radius : float
        Outer radius of the tube.
    cell_size : float
        Characteristic length of mesh (default is 5).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    tube_segment, round_wire, polygonal

    """
    if inner_radius <= 0 or outer_radius <= 0:
        raise ValueError("Radii cannot be negative.")

    if inner_radius >= outer_radius:
        raise ValueError(
            f"Inner radius ({inner_radius}) has to be strictly "
            f"smaller than outer radius ({outer_radius})."
        )

    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = cell_size
        geom.characteristic_length_max = cell_size
        disk1 = geom.add_disk([x0, y0], outer_radius)
        disk2 = geom.add_disk([x0, y0], inner_radius)
        geom.boolean_difference(disk1, disk2)
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.WAVEGUIDE
    return mesh


def tube_segment(
    arc_length: float,
    thickness: float,
    angle: float,
    cell_size: float = 5,
    shifted_to_origin: bool = True,
) -> MeshioMesh:
    """
    Cross-section of a tube segment.

    .. image:: /usage/tube_segment_cross_section.png
       :width: 350

    Parameters
    ----------
    arc_length : float
        Average arc length of the tube segment (at the center surface).
    thickness : float
        Thickness of the tube segment.
    angle : float
        Opening angle of the tube segment (degrees).
    cell_size : float
        Characteristic length of mesh (default is 5).
    shifted_to_origin : bool
        If True, shift the tube segment to the coordinate origin (default is True).
        Otherwise, the origin will be the symmetry axis of the tube.

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    tube, round_wire, polygonal

    """
    angle = angle * np.pi / 180.0
    if angle == 2 * np.pi:
        r_avrg = arc_length / angle
        r = r_avrg - thickness / 2
        R = r_avrg + thickness / 2
        return tube(r, R, cell_size, y0=-r_avrg)
    elif angle == 0:
        return rectangular(arc_length, thickness, cell_size, cell_size)
    else:
        r_avrg = arc_length / angle
        shift = r_avrg if shifted_to_origin else 0
        r = r_avrg - thickness / 2
        R = r_avrg + thickness / 2
        with pygmsh.occ.Geometry() as geom:

            p0 = geom.add_point(
                [-r * np.sin(-angle / 2), r * np.cos(-angle / 2) - shift], cell_size
            )
            p1 = geom.add_point(
                [-R * np.sin(-angle / 2), R * np.cos(-angle / 2) - shift], cell_size
            )
            poly = geom.add_line(
                p0,
                p1,
            )
            geom.revolve(poly, [0.0, 0.0, 1.0], [0.0, -shift, 0.0], angle)
            mesh = geom.generate_mesh()
        return mesh


def round_wire(radius: float, cell_size: float = 5, x0: float = 0, y0: float = 0) -> MeshioMesh:
    """
    Cross-section of a round wire.

    .. image:: /usage/round_wire_cross_section.png
       :width: 350

    Parameters
    ----------
    radius : float
        Radius of the round wire.
    cell_size : float
        Characteristic length of mesh (default is 5).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    polygonal, rectangular, round_wire_refined

    """
    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = cell_size
        geom.characteristic_length_max = cell_size
        geom.add_disk([x0, y0], radius)
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.WAVEGUIDE
    return mesh


def round_wire_refined(
    radius: float,
    refinement_function: Callable[[float, float, float], float],
    x0: float = 0,
    y0: float = 0,
) -> MeshioMesh:
    """
    Refined cross-section of a round wire.

    Instead of a fixed characteristic mesh length ``cell_size``, a scalar field ``func(x,y,z)`` is
    supplied to define ``cell_size``
    at each position of the mesh. This mesh is preferable, for example, for round wires housing a vortex string.

    .. image:: /usage/round_wire_cross_section.png
       :width: 350

    Parameters
    ----------
    radius : float
        Radius of the round wire.
    refinement_function : func
        Scalar field of signture ``func(x,y,z)`` which returns the characteristic mesh length at each position.
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    tube, round_wire

    """
    with pygmsh.occ.Geometry() as geom:
        geom.add_disk([x0, y0], radius)
        geom.set_mesh_size_callback(
            lambda dim, tag, x, y, z, lc: refinement_function(x, y, z)
        )
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.WAVEGUIDE
    return mesh


def polygonal(points: List[List], cell_size: float = 5) -> MeshioMesh:
    """
    Cross-section of a polygonal wire.

    .. image:: /usage/polygonal_cross_section.png
       :width: 350

    Parameters
    ----------
    points : list (Point)
        List of points defining the corners of the polygon.
    cell_size : float
        Characteristic length of mesh (default is 5).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    round_wire, rectangular

    """
    with pygmsh.geo.Geometry() as geom:
        geom.add_polygon(points, mesh_size=cell_size)
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.WAVEGUIDE
    return mesh


def rectangular(  # noqa: PLR0913
    width: float,
    thickness: float,
    cell_size_width: float = 5,
    cell_size_thickness: float = 5,
    x0: float = 0,
    y0: float = 0,
) -> MeshioMesh:
    """
    Cross-section of a rectangular waveguide.

    .. image:: /usage/rectangle_cross_section.png
       :width: 350


    Parameters
    ----------
    width : float
        Width of the rectangle.
    thickness : float
        Thickness of the rectangle.
    cell_size_width : float
        characteristic mesh length along the width (Default is 5).
        The number of FEM layers along the width will be `a/lca + 1`.
    cell_size_thickness : float
        characteristic mesh length along the thickness (Default is 5).
        The number of FEM layers along the thickness will be `b/lcb + 1`.
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : MeshioMesh

    See Also
    --------
    round_wire, polygonal

    """
    with pygmsh.geo.Geometry() as geom:
        nx = int(width / cell_size_width)
        ny = int(thickness / cell_size_thickness)
        p = geom.add_point([-width / 2 + x0, -thickness / 2 + y0, 0.0], mesh_size=cell_size_width)
        _, line, _ = geom.extrude(p, translation_axis=[width, 0, 0], num_layers=nx)
        geom.extrude(line, translation_axis=[0, thickness, 0], num_layers=max(2, ny))
        mesh = geom.generate_mesh()

    mesh.geometry_type = GeometryType.WAVEGUIDE
    return mesh
