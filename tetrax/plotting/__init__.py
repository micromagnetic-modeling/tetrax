"""
Plotting (:mod:`tetrax.plotting`)
=================================

This module provides a bunch of plotting routines that are typically used by result objects. This means that all
functions are mostly used under the hood. For user-side functionality, refer to the plotting methods of the different
result objects in the :mod:`tetrax.experiments` subpackage and :doc:`usage/visualization`.

For a more detailed information on each routine, click the individual function.

.. currentmodule:: tetrax.plotting

.. automodule:: tetrax.plotting.plotly

"""  # noqa: D205, D400, D415
