"""
Plotly
------

A collection of plotting routines implemented using plotly.

.. currentmodule:: tetrax.plotting

.. autosummary::
    :toctree: generated/

    plotly.plotly_plot_1d_susceptibility
    plotly.plotly_plot_2d_susceptibility
    plotly.plotly_plot_dynamic_relaxation
    plotly.plotly_plot_relaxation
    plotly.plotly_plot_spectral_data
    plotly.rescale_plotting_k

"""  # noqa: D205, D400, D415

from __future__ import annotations

from typing import TYPE_CHECKING, Optional, Tuple, Union

import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from tetrax.common.config import (
    EIGEN_XLABELS,
    ENERGY_NAMES,
    ENERGY_UNITS,
    INTERACTION_COLORS,
    MAG_COMPONENT_COLORS,
    RELAX_DYNAMIC_TORQUE_NAME, EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME, EIGEN_WAVEVECTOR_COLUMN_NAME,
)
from tetrax.common.typing import GeometryType, SIPrefix

if TYPE_CHECKING:
    from typing import List

    from pandas import DataFrame

    from tetrax.interactions.interaction import InteractionName


def plotly_plot_relaxation(  # noqa: PLR0913
    energy_mag_log: DataFrame,
    component_names: List[str],
    interaction_names: List[InteractionName],
    geometry_type: GeometryType,
    style: str = "markers+lines",
    renderer: Optional[str] = None,
) -> None:
    """
    Plot magnetization components and energy contributions of relaxation in different subplots.

    Parameters
    ----------
    energy_mag_log : DataFrame
        A DataFrame containing the time evolution of magnetization components
        and energy contributions. Expected to have columns such as "<mag.component_name>"
        for each component in `component_names` and "E_tot" for total energy, as well as
        "E_{interaction_name}" for each interaction in `interaction_names`.
    component_names : List[str]
        List of magnetization component names to plot (e.g., ["x", "y", "z"]).
    interaction_names : List[InteractionName]
        List of energy interaction names to plot. Each entry should correspond to a valid
        field in `energy_mag_log` prefixed with "E_".
    geometry_type : GeometryType
        Type of geometry, used to determine energy units and labels based on the geometry.
    style : str, optional
        Style of the plot line and markers. Defaults to "markers+lines".
    renderer : Optional[str], optional
        Renderer to use for displaying the plot, such as 'notebook', 'browser', etc.
        Defaults to None, which uses the default Plotly renderer.

    """
    fig = make_subplots(2, 1)

    index = energy_mag_log.index.values

    components_legend_title = "<b>Magnetization components</b>"
    energy_legend_title = "<b>Energy contributions</b>"
    energy_name = ENERGY_NAMES[geometry_type.value]
    energy_unit = ENERGY_UNITS[geometry_type.value]

    for idx, component_name in enumerate(component_names):
        component_values = energy_mag_log[f"<mag.{component_name}>"].values.astype(
            float
        )

        fig.add_trace(
            go.Scatter(
                x=index,
                y=component_values,
                mode=style,
                hovertemplate="%{y} (" + component_name + ")<extra></extra>",
                name=f"<mag.{component_name}>",
                line={
                    "width": 1.5,
                    "color": MAG_COMPONENT_COLORS[idx % len(MAG_COMPONENT_COLORS)],
                },
                legendgroup="1",
                legendgrouptitle_text=components_legend_title,
            ),
            row=1,
            col=1,
        )

    fig.add_trace(
        go.Scatter(
            x=index,
            y=energy_mag_log["E_tot"].values,
            mode=style,
            name="total",
            hovertemplate="%{y} " + energy_unit + " (total)<extra></extra>",
            line={"color": "black", "width": 2},
            legendgroup="2",
            legendgrouptitle_text=energy_legend_title,
        ),
        row=2,
        col=1,
    )

    for idx, interaction_name in enumerate(interaction_names):
        energy_values = energy_mag_log[f"E_{interaction_name.value}"].values.astype(
            float
        )

        fig.add_trace(
            go.Scatter(
                x=index,
                y=energy_values,
                mode=style,
                name=interaction_name.value,
                line={
                    "width": 1.5,
                    "color": INTERACTION_COLORS[idx % len(INTERACTION_COLORS)],
                },
                hovertemplate="%{y} "
                + energy_unit
                + " ("
                + interaction_name.value
                + ")<extra></extra>",
                legendgroup="2",
                legendgrouptitle_text=energy_legend_title,
            ),
            row=2,
            col=1,
        )

        all_zeroes = not np.any(energy_values)
        if all_zeroes:
            fig.update_traces(
                visible="legendonly", selector={"name": interaction_name.value}
            )

    fig.update_yaxes(title_text="Average mag.", row=1, exponentformat="e")
    fig.update_yaxes(
        title_text=f"{energy_name} ({energy_unit})", row=2, exponentformat="e"
    )
    fig.update_xaxes(title_text="Iteration step", row=2)
    fig.update_xaxes(title_text="Iteration step", row=1)

    fig.update_traces(marker={"size": 3})

    fig.update_layout(
        template="simple_white",
        height=800,
        width=600,
        legend_tracegroupgap=270,
        legend={"groupclick": "toggleitem", "itemdoubleclick": False},
        hovermode="x",
        hoverlabel={"bordercolor": "rgba(255,255,255,1)"},
    )

    fig.show(renderer=renderer)


def plotly_plot_dynamic_relaxation(
    torque_mag_log: DataFrame,
    component_names: List[str],
    style: str = "markers+lines",
    renderer: Optional[str] = None,
) -> None:
    """
    Plot magnetization components and torque during a dynamic relaxation.

    Parameters
    ----------
    torque_mag_log : DataFrame
        A DataFrame containing the time evolution of magnetization components and torque values.
        Expected to have columns such as "<mag.component_name>" for each component in `component_names`,
        and column for the total torque.
    component_names : List[str]
        List of magnetization component names to plot (e.g., ["x", "y", "z"]).
    style : str, optional
        Style of the plot line and markers. Defaults to "markers+lines".
    renderer : Optional[str], optional
        Renderer to use for displaying the plot, such as 'notebook', 'browser', etc.
        Defaults to None, which uses the default Plotly renderer.

    """
    fig = make_subplots(2, 1)

    index = torque_mag_log.index.values

    components_legend_title = "<b>Magnetization components</b>"
    torque_legend_title = "<b>dm/dt [Hz]</b>"

    for idx, component_name in enumerate(component_names):
        component_values = torque_mag_log[f"<mag.{component_name}>"].values.astype(
            float
        )

        fig.add_trace(
            go.Scatter(
                x=torque_mag_log["total_t"].values,
                y=component_values,
                mode=style,
                hovertemplate="%{y} (" + component_name + ")<extra></extra>",
                name=f"<mag.{component_name}>",
                line={
                    "width": 1.5,
                    "color": MAG_COMPONENT_COLORS[idx % len(MAG_COMPONENT_COLORS)],
                },
                legendgroup="1",
                legendgrouptitle_text=components_legend_title,
            ),
            row=1,
            col=1,
        )

    fig.add_trace(
        go.Scatter(

            x=torque_mag_log["total_t"].values,
            y=torque_mag_log[RELAX_DYNAMIC_TORQUE_NAME].values,
            mode=style,
            name="total",
            hovertemplate="%{y} <extra></extra>",
            line={"color": "black", "width": 2},
            legendgroup="2",
            legendgrouptitle_text=torque_legend_title,
        ),
        row=2,
        col=1,
    )

    fig.update_yaxes(title_text="Average mag.", row=1, exponentformat="e")

    fig.update_yaxes(
        title_text=f"dm/dt [Hz]", row=2, exponentformat="e", type="log"
    )
    fig.update_xaxes(title_text="Total time [ps]", row=2)
    fig.update_xaxes(title_text="Total time [ps]", row=1)


    fig.update_traces(marker={"size": 3})

    fig.update_layout(
        template="simple_white",
        height=800,
        width=600,
        legend_tracegroupgap=270,
        legend={"groupclick": "toggleitem", "itemdoubleclick": False},
        hovermode="x",
        hoverlabel={"bordercolor": "rgba(255,255,255,1)"},
    )

    fig.show(renderer=renderer)


def plotly_plot_1d_susceptibility(
    microwave_frequency: np.ndarray,
    susceptibility: np.ndarray,
    style: str,
    fscale: SIPrefix = SIPrefix.NONE,
    renderer: Optional[str] = None,
) -> None:
    r"""
    Plot the 1D susceptibility as a function of microwave frequency.

    This function visualizes the real and imaginary parts of susceptibility as separate
    traces on a 1D plot, with microwave frequency on the x-axis and susceptibility on
    the y-axis. The real part of the susceptibility represents the dispersion (:math:`\mathrm{Re}\,(\chi)`),
    and the imaginary part represents the absorption (:math:`\mathrm{Im}\,(\chi)`). The function allows for
    scaling of the frequency axis using SI prefixes and offers customizable line and
    marker styles.

    Parameters
    ----------
    microwave_frequency : np.ndarray
        Array of microwave frequency values, representing the x-axis of the plot.
    susceptibility : np.ndarray
        Complex array where `susceptibility.real` corresponds to the dispersion
        (real part) and `susceptibility.imag` corresponds to the absorption
        (imaginary part) of the material.
    style : str
        Line and marker style for the plot traces.
    fscale : SIPrefix, optional
        Scaling factor for the frequency axis, allowing for SI prefix adjustments
        (e.g., kHz, MHz). Defaults to SIPrefix.NONE.
    renderer : Optional[str], optional
        Plotly renderer to use for displaying the plot, such as "notebook" or "browser".
        Defaults to None, which uses the default renderer.

    """
    fscale = SIPrefix(fscale)

    fig = go.Figure()

    fig.add_trace(
        go.Scatter(
            x=microwave_frequency,
            y=susceptibility.real[0],
            mode=style,
            name="Dispersion, Re(<i>&#967;</i>)",
        )
    )
    fig.add_trace(
        go.Scatter(
            x=microwave_frequency / fscale.factor,
            y=susceptibility.imag[0],
            mode=style,
            name="Absorption, Im(<i>&#967;</i>)",
        )
    )
    fig.update_layout(
        template="simple_white",
        # height=600,
        # width=600,
        legend={"groupclick": "toggleitem"},
        hoverlabel={"bordercolor": "rgba(255,255,255,1)"},
    )

    fig.update_xaxes(
        title_text=f"Microwave frequency ({fscale.symbol}Hz)", exponentformat="e"
    )
    fig.update_yaxes(title_text="Susceptibility, <i>&#967;</i>", exponentformat="e")

    fig.show(renderer=renderer)


def plotly_plot_2d_susceptibility(  # noqa: PLR0913
    k: np.ndarray,
    microwave_frequency: np.ndarray,
    susceptibility: np.ndarray,
    fscale: SIPrefix = SIPrefix.NONE,
    kscale: SIPrefix = SIPrefix.NONE,
    renderer: Optional[str] = None,
) -> None:
    """
    Plot a 2D heatmap of susceptibility as a function of wave vector and microwave frequency.

    This function generates a 2D heatmap visualization showing the real and imaginary parts
    of the susceptibility as separate heatmaps. The x-axis represents the wave vector (k),
    and the y-axis represents the microwave frequency. The function uses SI prefix scaling
    for both axes, allowing customization of units. The color scale represents the magnitude
    of susceptibility, with the option to adjust the color range to the maximum absolute
    value found in the susceptibility data.

    Parameters
    ----------
    k : np.ndarray
        Array of wave vector values for the x-axis.
    microwave_frequency : np.ndarray
        Array of microwave frequency values for the y-axis.
    susceptibility : np.ndarray
        2D complex array representing the susceptibility, where `susceptibility.real`
        is the real part (dispersion) and `susceptibility.imag` is the imaginary part
        (absorption).
    fscale : SIPrefix, optional
        Frequency scaling factor for the y-axis, allowing for SI prefix adjustments
        (e.g., kHz, MHz). Defaults to SIPrefix.NONE.
    kscale : SIPrefix, optional
        Wave vector scaling factor for the x-axis, allowing for SI prefix adjustments.
        Defaults to SIPrefix.NONE.
    renderer : Optional[str], optional
        Plotly renderer to use for displaying the plot, such as "notebook" or "browser".
        Defaults to None, which uses the default renderer.

    """
    fscale = SIPrefix(fscale)
    kscale = SIPrefix(kscale)

    fig = make_subplots(
        1,
        2,
        subplot_titles=(
            "Dispersion, Re(<i>&#967;</i>)",
            "Absorption, Im(<i>&#967;</i>)",
        ),
    )

    max_susc = max(
        np.max(np.abs(susceptibility.real)),
        np.max(np.abs(susceptibility.imag)),
    )

    fig.add_trace(
        go.Heatmap(
            x=k * kscale.factor,
            y=microwave_frequency / fscale.factor,
            z=susceptibility.real,
            type="heatmap",
            transpose=True,
            colorscale="Viridis",
            coloraxis="coloraxis",
            zmax=max_susc,
            zmin=-max_susc,
        ),
        row=1,
        col=1,
    )

    fig.add_trace(
        go.Heatmap(
            x=k * kscale.factor,
            y=microwave_frequency / fscale.factor,
            z=susceptibility.imag,
            type="heatmap",
            transpose=True,
            zmax=max_susc,
            zmin=-max_susc,
            colorscale="Viridis",
            coloraxis="coloraxis",
        ),
        row=1,
        col=2,
    )

    fig.update_yaxes(
        title_text=f"Microwave frequency ({fscale.symbol}Hz)",
        row=1,
        col=1,
        exponentformat="e",
    )
    fig.update_xaxes(
        title_text=f"Wave vector, <i>k{kscale.symbol}</i>", row=1, exponentformat="e"
    )

    fig.update_layout(
        coloraxis={"colorscale": "RdBu"},
        coloraxis_cmin=-max_susc,
        coloraxis_cmax=max_susc,
    )
    fig.layout.coloraxis.colorbar.title = "Susceptibility, <i>&#967;</i>"
    fig.layout.coloraxis.colorbar.exponentformat = "e"

    fig.show(renderer=renderer)


def plotly_plot_spectral_data(  # noqa: PLR0913
    dataframe_with_km: DataFrame,
    geometry_type: GeometryType,
    style: Optional[str] = None,
    fscale: SIPrefix = SIPrefix.NONE,
    kscale: SIPrefix = SIPrefix.NONE,
    renderer: Optional[str] = None,
    ylabel: str = "Frequency",
) -> None:
    """
    Plot spectral data based on geometry type, wave vector, and frequency scaling.

    This function generates a spectral plot using Plotly, with different plotting
    approaches depending on the specified geometry type. The x-axis represents the
    wave vector or azimuthal index, while the y-axis represents the frequency
    values. SI scaling factors for both frequency and wave vector can be applied,
    and the plot style is customizable. The function supports various geometry
    types including waveguides, layers, axial geometries, and confined geometries,
    each with unique visual representations.

    Parameters
    ----------
    dataframe_with_km : DataFrame
        A pandas DataFrame containing spectral data with wave vector or azimuthal
        index as one of the columns and frequency values in the subsequent columns.
    geometry_type : GeometryType
        The type of geometry for the spectral data, which determines how the data
        is plotted. Possible values include `GeometryType.WAVEGUIDE`, `GeometryType.LAYER`,
        `GeometryType.WAVEGUIDE_AXIAL`, and `GeometryType.CONFINED_AXIAL`.
    style : Optional[str], optional
        Plot style for line and markers, such as "lines" or "markers+lines".
        If None, a default style will be selected based on the geometry type.
    fscale : SIPrefix, optional
        Scaling factor for frequency, allowing for SI unit adjustments (e.g., kHz,
        MHz). Can be given as a string, e.g. "u" for micro or "G" for giga. Defaults to SIPrefix.NONE.
    kscale : SIPrefix, optional
        Scaling factor for the wave vector, enabling SI unit adjustments.
        Can be given as a string, e.g. "u" for micro or "G" for giga. Defaults to SIPrefix.NONE.
    renderer : Optional[str], optional
        Plotly renderer to use for displaying the plot, such as "notebook" or
        "browser". Defaults to None, which uses the default renderer.
    ylabel : str, optional
        Label for the y-axis, typically set to "Frequency" by default.

    """
    fscale = SIPrefix(fscale)
    kscale = SIPrefix(kscale)

    cmap = px.colors.sequential.Plasma

    fig = go.Figure()

    frequency_headers = dataframe_with_km.columns[2:]
    num_headers = len(frequency_headers)

    if geometry_type in [GeometryType.WAVEGUIDE, GeometryType.LAYER]:
        style = "lines" if style is None else style

        k = dataframe_with_km[EIGEN_WAVEVECTOR_COLUMN_NAME]

        for idx_header, header in enumerate(frequency_headers):
            linecolor = px.colors.sample_colorscale(
                cmap, abs(idx_header) / (num_headers - 0.99) * 0.85
            )[0]

            linestyle = {"color": linecolor}
            fig.add_trace(
                go.Scatter(
                    x=k * kscale.factor,
                    y=dataframe_with_km[header] / fscale.factor,
                    mode=style,
                    name=header.split()[0],
                    line=linestyle,
                )
            )

    elif geometry_type is GeometryType.WAVEGUIDE_AXIAL:
        style = "lines" if style is None else style

        m_range = np.unique(dataframe_with_km[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME].values)
        m_max_abs = np.max(np.abs(m_range))

        for m in m_range:
            df_m = dataframe_with_km[
                dataframe_with_km[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME] == m
                ]

            k = df_m[EIGEN_WAVEVECTOR_COLUMN_NAME]

            if len(m_range) == 1:
                linecolor = px.colors.sample_colorscale(cmap, [0])[0]
            else:
                linecolor = px.colors.sample_colorscale(cmap, abs(m) / m_max_abs * 0.9)[
                    0
                ]

            linestyle = (
                {"dash": "dash", "color": linecolor} if m < 0 else {"color": linecolor}
            )

            for header in frequency_headers:
                fig.add_trace(
                    go.Scatter(
                        x=k * kscale.factor,
                        y=df_m[header] / fscale.factor,
                        mode=style,
                        name=f"{header.split()[0]}",
                        legendgroup=f"m{m}",
                        legendgrouptitle_text=f"<i>m</i> = {int(m)}",
                        line=linestyle,
                    ),
                )

    elif geometry_type is GeometryType.CONFINED_AXIAL:
        style = "markers+lines" if style is None else style

        m = dataframe_with_km[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME]

        for idx_header, header in enumerate(frequency_headers):
            linecolor = px.colors.sample_colorscale(
                cmap, abs(idx_header) / (num_headers - 0.99) * 0.85
            )[0]

            linestyle = {"color": linecolor}
            fig.add_trace(
                go.Scatter(
                    x=m,
                    y=dataframe_with_km[header] / fscale.factor,
                    mode=style,
                    name=header.split()[0],
                    line=linestyle,
                )
            )

    elif geometry_type is GeometryType.CONFINED:
        style = "markers+lines" if style is None else style

        fig.add_trace(
            go.Scatter(
                y=dataframe_with_km.iloc[0][2:] / fscale.factor,
                mode=style,
                line={"color": cmap[0]},
            )
        )

    fig.update_yaxes(title_text=f"{ylabel} ({fscale.symbol}Hz)", exponentformat="power")

    xlabel = EIGEN_XLABELS[geometry_type]
    if geometry_type in [
        GeometryType.WAVEGUIDE,
        GeometryType.LAYER,
        GeometryType.WAVEGUIDE_AXIAL,
    ]:
        xlabel = xlabel.format(kscale.symbol)

    fig.update_xaxes(title_text=xlabel, exponentformat="power")

    fig.update_layout(
        template="simple_white",
        height=600,
        width=600,
        hoverlabel={"bordercolor": "rgba(255,255,255,1)"},
    )

    fig.show(renderer=renderer)


def rescale_plotting_k(
    k: Optional[Union[float, Tuple, List]], kscale: float
) -> Optional[Union[float, Tuple, List]]:
    """
    Scale provided wave-vector range for plotting to rad/m.

    When the user sets an SI-prefix for the wave-vector scale, e.g., when plotting a dispersion, then the
    ``k=[kmin, kmax]`` is taken to be in that unit. Therefore, before reducing the frequency dataframe, the input
    needs to be converted back to rad/m.
    """
    if isinstance(k, (int, float)):
        return k / kscale

    elif isinstance(k, (tuple, list)) and np.shape(k) == (2,):
        k0 = k[0] if k[0] is None else k[0] / kscale
        k1 = k[1] if k[1] is None else k[1] / kscale

        return k0, k1

    return None
