from __future__ import annotations

import logging

txlogger = logging.getLogger("TetraX")


txlogger_stream_handler = logging.StreamHandler()
txlogger_stream_handler.setLevel("DEBUG")

# _formatter = logging.Formatter(
#    fmt="%(asctime)s %(name)s [%(levelname)s]: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
# )

_formatter = logging.Formatter(
    fmt="%(name)s [%(levelname)s]: %(message)s",
)

txlogger_stream_handler.setFormatter(_formatter)
txlogger.addHandler(txlogger_stream_handler)


def set_logging(level: str = "INFO") -> None:
    """
    Set level for logging.

    Parameters
    ----------
    level : str, {"DEBUG", "INFO", "WARNING", "ERROR", "CRTICICAL"}
        Level for logging. P

    """
    txlogger.setLevel(level)
    txlogger_stream_handler.setLevel(level)
