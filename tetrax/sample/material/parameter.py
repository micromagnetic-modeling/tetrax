"""Definition of material parameter objects."""
from __future__ import annotations

from enum import Enum
from numbers import Real
from typing import TYPE_CHECKING

import numpy as np
from tabulate import tabulate

from tetrax.common.io import left_align_html_cols
from tetrax.common.math import normalize
from tetrax.sample.material.exceptions import (
    ParameterAccessError,
    ParameterAssignmentError,
)
from tetrax.sample.mesh.meshquantities import MeshScalar, MeshVector

if TYPE_CHECKING:
    from typing import Callable, List, Optional, Self, Union

    from numpy.typing import ArrayLike

    from tetrax import Sample
    from tetrax.sample.mesh.sample_mesh import SampleMesh


class ParameterType(Enum):
    """
    Possible types of material parameters.

    Parameters
    ----------
    value : ParameterType or str
        Value to instantiate the prefix.  Valid string values are ``"boolean"``, ``"scalar"``, ``"direction"``.

    """

    BOOLEAN = "boolean"
    """Parameter that can be ``True`` or ``False`` globally or at any point in the mesh."""

    SCALAR = "scalar"
    """Scalar (field) defined globally or at any point in the mesh."""

    DIRECTION = "direction"
    """Unit vector (field) defined globally or at any point in the mesh."""

    @classmethod
    def _missing_(cls, value: str) -> None:  # noqa: ANN102
        raise ValueError(
            "%r is not a valid %s.  Valid prefixes: %s"
            % (
                value,
                cls.__name__,
                ", ".join([repr(m.value) for m in cls]),
            )
        )


class MaterialParameter:
    """
    Single material parameter with metadata, constraints, and getter/setter functionality.

    This class encapsulates a material property associated with a sample, such as its name,
    type, unit, and whether it is globally defined or read-only. It supports custom getter and setter functions
    for retrieving and modifying the parameter's value, which can be constrained by a specified condition. This
    class also allows computing the average value of the parameter within a mesh and compressing values if the
    parameter is homogeneous across the sample. Material parameters are typically accessed through the
    :attr:`~tetrax.sample.sample.material` attribute of a particular sample
    (see also :class:`~tetrax.sample.material.material.SampleMaterial`).

    Parameters
    ----------
    sample : Sample
        The sample object to which this material parameter belongs.
    name : str
        The name of the parameter.
    description : str
        A description of the parameter.
    default_value : float or List[float]
        The default value of the parameter, which can be a scalar or a list of three floats for directions.
    param_type : ParameterType
        The type of the parameter (e.g., scalar, direction), which influences its behavior and setter function.
    unit : str
        The unit of measurement for the parameter (e.g., "m", "kg").
    is_global : bool
        Indicates whether the parameter is globally defined (i.e., uniform across the entire sample). If a parameter
        is not global, assinging a single value will automatically repeat it accross the whole mesh.
    read_only : bool
        Specifies whether the parameter is read-only, preventing modifications.
    getter : Optional[Callable], optional
        A custom getter function for retrieving the parameter's value, by default ``None``.
    custom_setter : Optional[Callable], optional
        A custom setter function for assigning values to the parameter, by default ``None``.
    constraint : Optional[Callable], optional
        A function that imposes a constraint on the parameter's values (e.g., positivity), by default ``None``.

    Notes
    -----
    This class includes support for custom getters and setters, enabling advanced behaviors for reading and modifying
    parameter values. It also raises errors if attempting to set read-only parameters or values that do not satisfy
    specified constraints.

    Examples
    --------
    A single material parameter can be created as

    >>> param = MaterialParameter(
    ...     sample=sample,
    ...     name="density",
    ...     description="Material density",
    ...     default_value=2.5,
    ...     param_type=ParameterType.SCALAR,
    ...     unit="g/cm^3",
    ...     is_global=True,
    ...     read_only=False
    ... )
    >>> print(param.value)
    2.5
    >>> param.value = 3.0
    >>> print(param.value)
    3.0

    However, in normal use cases, material parameters are set and accessed through the
    :class:`~tetrax.sample.material.material.SampleMaterial` of the respective sample.

    >>> sample.material["Msat"] = 795e3
    >>> sample.material["Msat"].value
    MeshScalar([795000.0, 795000.0, 795000.0, ...])
    >>> sample.material["Msat"].average
    795000.0

    """

    def __init__(  # noqa: PLR0913, D107
        self: Self,
        sample: Sample,
        name: str,
        description: str,
        default_value: Union[float, List[float]],
        param_type: ParameterType,
        unit: str,
        is_global: bool,
        read_only: bool,
        getter: Optional[Callable] = None,
        custom_setter: Optional[Callable] = None,
        constraint: Optional[Callable] = None,
    ) -> None:
        self.name: str = name
        """Name of the parameter."""

        self.description: str = description
        """Description of the parameter."""

        self.param_type: ParameterType = param_type
        """Type of the parameter."""

        self.unit: str = unit
        """Unit of he parameter."""

        self.is_global: bool = is_global
        """Whether the parameter is globally defined or not."""

        self.read_only = read_only
        """Whether the parameter is read-only or not."""

        self.sample: Sample = sample
        """Reference to the sample object that the material parameter belongs to."""

        self.constraint = constraint
        """Possible constraint on the values of parameter (such as :func:`~tetrax.common.math.is_positive`)"""

        self._value = default_value
        self._getter_function: Callable = getter

        self._setter_function = (
            _default_scalar_setter
            if self.param_type is ParameterType.SCALAR
            else _default_direction_setter
        )
        if custom_setter is not None:
            self._setter_function = custom_setter

    def _get_repr_attributes(self: Self) -> List:
        return [
            ["name", self.name],
            ["description", self.description],
            ["sample", self.sample.name],
            ["param_type", self.param_type.value],
            ["is_global", self.is_global],
            ["read_only", self.read_only],
            [
                "constraint",
                self.constraint.__name__ if self.constraint is not None else "None",
            ],
            ["unit", self.unit],
            ["average", [self.average]],
        ]

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"MaterialParameter(name={self.name}, sample={self.sample.name}, param_type={self.param_type}, ...)"

    def _repr_html_(self: Self) -> str:
        repr_attributes = self._get_repr_attributes()
        return (
            "<h4>MaterialParameter</h4>"
            + left_align_html_cols(
                tabulate(
                    repr_attributes, tablefmt="html", headers=["attribute", "value"]
                )
            )
            + f"<br>value: {self.value.__repr__()}"
        )

    @property
    def value(self: Self) -> Union[float, ArrayLike, MeshVector, MeshScalar]:
        """Current value of the material parameter."""
        return (
            self._value if self._getter_function is None else self._getter_function(self)
        )

    @value.setter
    def value(self: Self, value: Union[float, ArrayLike]) -> None:
        if self.read_only:
            raise ParameterAccessError(f"MaterialParameter '{self.name}' is read-only.")

        if self.constraint is not None and not self.constraint(value):
            raise ParameterAssignmentError(
                f"MaterialParameter '{self.name}' cannot be assigned the value '{value}' as it "
                f"violates the constraint '{self.constraint.__name__}'."
            )

        self._value = self._setter_function(
            self.name, value, self.sample.mesh, self.is_global
        )

    @property
    def average(self: Self) -> Union[float, ArrayLike]:
        """Average of the parameter within a the mesh of the corresponding sample."""
        # TODO make this nicer by implementing proper averageing behavior for single numbers.
        # TODO This leads to problems when the number of nodes is 3.

        if (
            self.is_global
            or isinstance(self.value, Real)
            or np.shape(self.value) == (3,)
        ):
            return self.value

        return self.sample.average(self.value)

    @average.setter
    def average(self: Self, value) -> None:  # noqa: ANN001
        raise ParameterAccessError(
            f"Cannot set average of MaterialParameter '{self.name}' directly."
        )

    def compress(self: Self) -> Union[float, List]:
        """
        Compress the values of the parameter to a single number/vector, if it is either global, or homogeneous.

        This function returns the parameter as is if it has heterogeneous values throughout the sample.
        """
        if self.is_global:
            return self.value
        if np.all(self.value == self.value[0]):
            return self.value[0].tolist()
        return self.value.tolist()


def _default_scalar_setter(
    name: str, value: Union[Real, ArrayLike], mesh: SampleMesh, is_global: bool
) -> Union[Real, MeshScalar]:
    """
    Validate a scalar value for a material parameter.

    This function is responsible for validating and setting scalar values for material parameters. It checks if the
    material parameter is global or not. If it is global, only a single number is allowed as the assignment. If it is
    not global, the value should be either a scalar or an array with the same shape as the mesh. If none of these
    conditions are met, an error is raised.

    Parameters
    ----------
    name : str
        The name of the material parameter.
    value : Union[Real, ArrayLike]
        The new value for the material parameter.
    mesh : SampleMesh
        The mesh information used for adapting the scalar value.
    is_global : bool
        A boolean indicating whether the material parameter is global.

    Returns
    -------
    MeshScalar
        The adapted material value as a MeshScalar.

    Raises
    ------
    ParameterAssignmentError
        If the assignment is invalid for the material parameter.

    """
    if is_global:
        if not isinstance(value, Real):
            raise ParameterAssignmentError(
                f"MaterialParameter '{name}' is a global scalar and can "
                f"only be assigned a single real number."
            )
        return value

        # If it's not global, the value should be a scalar or an array with the same shape as the mesh.
    if isinstance(value, Real):
        return MeshScalar(np.full((mesh.nx,), value))

    if np.shape(value) == (mesh.nx,):
        return MeshScalar(value)

    raise ParameterAssignmentError(
        f"Invalid assignment. MaterialParameter '{name}' requires a single scalar value or a "
        f"MeshScalar of shape ({mesh.nx},)."
    )


def _default_direction_setter(
    name: str, value: Union[Real, ArrayLike], mesh: SampleMesh, is_global: bool
) -> Union[ArrayLike, MeshVector]:
    """
    Validate and adapt a directional value for a material parameter.

    This function validates and potentially adapts directional values for material parameters. It checks whether the
    material parameter is global or not. If it's global, only a single 3D vector is allowed for assignment. If it's not
    global, the value should be either a single 3D vector or a MeshVector with the same shape as the mesh. If none
    of these conditions are met, an error is raised.

    Parameters
    ----------
    name : str
        The name of the material parameter.
    value : Union[Real, ArrayLike]
        The new value for the material parameter. For directional parameters, this should be a 3D vector.
    mesh : SampleMesh
        The mesh information used for adapting the directional value.
    is_global : bool
        A boolean indicating whether the material parameter is global.

    Returns
    -------
    MeshVector
        The adapted material value as a MeshVector.

    Raises
    ------
    ParameterAssignmentError
        If the assignment is invalid for the material parameter.

    """
    value = np.asarray(value)

    if is_global:
        if value.shape == (3,):
            return normalize(value)

        raise ParameterAssignmentError(
            f"MaterialParameter '{name}' is a global vector and can only be assigned a single 3D vector."
        )

    if value.shape == (3,):
        return MeshVector(
            np.repeat(value, mesh.nx).reshape((3, mesh.nx)).T
        ).normalized()

    if value.shape == (mesh.nx, 3):
        return MeshVector(value).normalized()

    raise ParameterAssignmentError(
        f"Invalid assignment. MaterialParameter '{name}' requires a single 3D vector value or a MeshVector of shape ({mesh.nx}, 3)."
    )
