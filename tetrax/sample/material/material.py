"""Definition of the sample material."""
from __future__ import annotations

from typing import TYPE_CHECKING, List, Union

from tabulate import tabulate

from tetrax._logging import txlogger
from tetrax.common.io import left_align_html_cols
from tetrax.sample.material.exceptions import MissingParameterError
from tetrax.sample.material.parameter import MaterialParameter, ParameterType

if TYPE_CHECKING:
    from collections.abc import dict_items
    from typing import Self

    from numpy.typing import ArrayLike

    from tetrax.sample.sample import Sample


class SampleMaterial:
    """
    Material properties of a sample.

    This class stores and manages material parameters for a sample, allowing to access and update them. Material
    parameters can be scalars or vectors and are typically used to define the physical properties of the sample.

    Parameters
    ----------
    sample : Sample
        The sample object to which this material belongs.
    default_parameters : dict
        A dictionary mapping material parameter names to their initial properties. Each property is specified as a
        dictionary of keyword arguments to create a
        :class:`~tetrax.sample.material.parameter.MaterialParameter` object (for reference, see
        :attr:`~tetrax.sample.material.standards.STANDARD_PARAMS_FERROMAGNETIC` or
        :attr:`~tetrax.sample.material.custom.PARAMS_INTERLAYER_EXCHANGE`)


    Notes
    -----
    This class behaves similar to a python :class:`dict` with additional functionality such as ownership checks and
    triggering the magnetic interactions of sample to update. Individual items can be set and
    accessed with the subscript operation.

    .. code-block:: python

        sample.material["Msat"] = 795e3
        # assigns a value to the parameter and triggers respective
        # interactions of the sample to update their matrices

        sample.material["Msat"]
        # return the stored parameter object.

        sample.material["Msat"].value
        # return the value(s) of the parameter

        sample.material["Msat"].average
        # return the average of the parameter


    For very large samples, it can be costly to update each material parameter individually, as each
    setting will trigger respective the interactions of the sample to update (note that, for example,
    almost all interactions depend on ``"Msat"``). In this case it can be better to assign the desired
    parameters all at once.

    .. code-block:: python

        sample.material = {"Msat": 1.2e6, "Aex": 16e-12, ...}
    """

    def __init__(self: Self, sample: Sample, default_parameters: dict) -> None:  # noqa: D107
        # Initialize all material parameters
        self._param_dict = {
            param: MaterialParameter(sample, **properties)
            for (param, properties) in default_parameters.items()
        }

        self.sample = sample

        # Reset all material parameters in case they are spatially dependent.
        for parameter, value_dict in default_parameters.items():
            if not value_dict["read_only"]:
                self.__setitem__(parameter, value_dict["default_value"])

    def _get_repr_attributes(self: Self) -> List:
        return [
            [
                param,
                f"{self._param_dict[param].average} {self._param_dict[param].unit}"
                + ("(is_global)" if self._param_dict[param].is_global else ""),
                self._param_dict[param].description,
            ]
            for param in self._param_dict
        ]

    def __repr__(self: Self) -> str:
        """Return a string representation of the SampleMaterial object."""
        repr_attributes = self._get_repr_attributes()

        return (
            f"SampleMaterial (Sample.material) of '{self.sample.name}'\n"
            + tabulate(
                repr_attributes,
                headers=["name", "average", "description"],
                tablefmt="grid",
            )
            + "\nHint: Access each parameter using material['name']."
        )

    def _repr_html_(self: Self) -> str:
        repr_attributes = self._get_repr_attributes()

        return (
            f"<h4>SampleMaterial (Sample.material) of '{self.sample.name}'</h4>"
            + left_align_html_cols(
                tabulate(
                    repr_attributes,
                    headers=["name", "average", "description"],
                    tablefmt="html",
                )
            )
            + "<i>Hint:</i> Access each parameter using <tt>material['name']</tt>."
        )

    def __getitem__(self: Self, parameter: str) -> MaterialParameter:
        """
        Retrieve a material parameter.

        Parameters
        ----------
        parameter : str
            The name of the material parameter to retrieve.

        Returns
        -------
        MaterialParameter
            The MaterialParameter object representing the specified material parameter.

        Notes
        -----
        Checks if the parameter name exists in the sample material.

        """
        self._assert_ownership_of(parameter)
        return self._param_dict[parameter]

    def __setitem__(self: Self, parameter: str, value: Union[float, ArrayLike]) -> None:
        """
        Set the value of a material parameter and update interactions that depend on it.

        Parameters
        ----------
        parameter : str
            The name of the material parameter to set.
        value : float or ndarray
            The new value for the material parameter.

        Notes
        -----
        Checks if the parameter name exists in the sample material.

        """
        self._assert_ownership_of(parameter)
        self._param_dict[parameter].value = value
        self._update_respective_interactions([parameter])

    def _assert_ownership_of(self: Self, parameter: str) -> None:
        """
        Ensure that a specified parameter belongs to this material.

        Parameters
        ----------
        parameter : str
            The name of the material parameter to check.

        Raises
        ------
        MissingParameterError
            If the specified parameter is not part of this material's properties (returns a list of possible parameters).

        """
        if parameter not in self._param_dict:
            raise MissingParameterError(
                f"SampleMaterial of '{self.sample.name}' does not contain the parameter '{parameter}'. "
                f"Available parameters are:\n {list(self._param_dict.keys())}"
            )

    def _update_respective_interactions(self: Self, parameter_list: List[str]) -> None:
        """
        Update interactions in the sample that require the provided list of material parameters.

        This method iterates through the interactions associated with the material's sample and checks if each interaction
        requires any of the material parameters specified in the `parameter_list`. If an interaction does require any of
        these parameters, its matrices are updated.

        Parameters
        ----------
        parameter_list : list of str
          A list of material parameter names to check for updates in the interactions.

        Returns
        -------
        None
          This method updates the interactions in place and doesn't return a value.

        Notes
        -----
        This method is designed to update interactions in the sample based on changes to material parameters. It first
        checks if the sample has interactions (as interactions are typically created after the sample and material are
        initialized). If interactions are not present, the method returns without any updates.

        """
        if not hasattr(self.sample, "interactions"):
            return None

        for interaction in self.sample.interactions:
            txlogger.debug(f"Checking {interaction.name.value} interaction.")

            if interaction.requires_params(parameter_list):
                interaction.update_matrices()
                txlogger.debug(f"\t {interaction.name.value} interaction was updated.")

    def set_from_dict(self: Self, input_dict: dict, fill_missing: bool = False) -> None:
        """
        Update multiple material parameters from a dictionary and update dependent interactions.

        Since, here, the interactions are only updated at the very end, this method should be used
        when setting many material parameters for large systems.

        Parameters
        ----------
        input_dict : dict
            A dictionary where keys are material parameter names, and values are the new values for those parameters.

        fill_missing : bool, optional
            Scalar material parameters that are not specified in the input dictionary will be set to zero. Note that
            this will produce an error for parameters have constraints. For example, an error will be produced
            if "Msat" is not part of the input dictionary.

        Notes
        -----
        For each given parameter, checks if the parameter name exists in the sample material.

        """
        for parameter_name, value in input_dict.items():
            self._assert_ownership_of(parameter_name)
            self._param_dict[parameter_name].value = value

        parameter_names = list(input_dict.keys())

        if fill_missing:
            for parameter_name, parameter in self._param_dict.items():
                if (
                    parameter_name not in input_dict
                    and parameter.param_type is ParameterType.SCALAR
                ):
                    self._param_dict[parameter_name].value = 0
                    parameter_names.append(parameter_name)

        self._update_respective_interactions(parameter_names)

    @property
    def parameter_names(self: Self) -> List[str]:
        """List of the stored material parameter names."""
        return list(self._param_dict.keys())

    def keys(self: Self) -> List[str]:
        """Return :func:`parameter_names`."""
        return self.parameter_names

    @property
    def parameters(self: Self) -> List[MaterialParameter]:
        """List of the stored parameters."""
        return list(self._param_dict.values())

    def values(self: Self) -> List[MaterialParameter]:
        """Return :func:`parameters`."""
        return self.parameters

    def items(self: Self) -> dict_items:
        """Return :func:`parameter_names` and :func:`parameters` as key-value pairs."""
        return self._param_dict.items()

    def to_dict(self: Self) -> dict:
        """Export all parameters as a dictionary and :func:`~tetrax.sample.material.parameter.compress` them before if possible."""
        return {
            name: parameter.compress()
            for (name, parameter) in self._param_dict.items()
            if not parameter.read_only
        }

    def add_parameter(self: Self, parameter_dict: dict) -> None:
        """
        Add a new material parameter to the material.

        Parameters
        ----------
        parameter_dict : dict
            Dictionary defining the parameter (for reference, see
            :attr:`~tetrax.sample.material.standards.STANDARD_PARAMS_FERROMAGNETIC` or
            :attr:`~tetrax.sample.material.custom.PARAMS_INTERLAYER_EXCHANGE`)

        """
        self._param_dict[parameter_dict["name"]] = MaterialParameter(
            self.sample, **parameter_dict
        )

        self.__setitem__(parameter_dict["name"], parameter_dict["default_value"])

    def preview(self: Self, number_parameters: int = 4) -> str:
        """
        Return a short preview of the material parameters.

        Parameters
        ----------
        number_parameters: int, optional
            Number of parameters to include in the preview (default is 4).

        Examples
        --------
        >>> sample.material.preview()
        'Msat = 795000.0 A/m, Aex = 1.3000000000000002e-11 J/m, gamma = 176085964400.0 radHz/T, alpha = 0.008 , ...'

        """
        preview = ", ".join(
            [
                f"{parameter.name} = {parameter.average} {parameter.unit}"
                for (name, parameter) in list(self._param_dict.items())[
                    :number_parameters
                ]
            ]
        )

        if number_parameters < len(self._param_dict):
            preview += ", ..."

        return preview
