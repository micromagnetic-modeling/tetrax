"""Exceptions raised when accessing or assigning material parameters."""

from __future__ import annotations


class MissingParameterError(Exception):
    """Exception raised when a supplied parameter is missing from the SampleMaterial."""


class ParameterAccessError(Exception):
    """Exception raised when a MaterialParameter cannot be accessed in the desired way."""


class ParameterAssignmentError(Exception):
    """Exception raised when a MaterialParameter cannot be assigned in the desired way."""
