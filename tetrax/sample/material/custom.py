"""Custom material-parameter definitions."""
from __future__ import annotations

from typing import TYPE_CHECKING

from tetrax.common.config import BOUNDARY_NODES_OF_BILAYER
from tetrax.common.math import cross_product
from tetrax.sample.material.exceptions import ParameterAssignmentError
from tetrax.sample.material.parameter import MaterialParameter, ParameterType

if TYPE_CHECKING:
    from numbers import Real
    from typing import List, Union

    from numpy.typing import ArrayLike

    from tetrax.sample.mesh import MeshVector
    from tetrax.sample.mesh.sample_mesh import SampleMesh


def _interlayer_exchange_constant_setter(
        name: str, value: Union[Real, ArrayLike], mesh: SampleMesh, is_global: bool
) -> List[float]:
    number_of_spacers = 1
    num_boundary_nodes = mesh.nb

    if num_boundary_nodes >= BOUNDARY_NODES_OF_BILAYER:
        number_of_spacers = int(num_boundary_nodes / 2 - 1)

    if isinstance(value, float):
        return [value for i in range(number_of_spacers)]

    if isinstance(value, list) and len(value) != number_of_spacers:
        raise ParameterAssignmentError(
            f"You are trying to assign {len(value)} interlayer-exchange constants to "
            f"a sample whose mesh has only {number_of_spacers} spacers. Either provide "
            f"only a single number (for all) or a list of length {number_of_spacers}."
        )


PARAMS_INTERLAYER_EXCHANGE = {
    "name": "J1",
    "description": "interlayer-exchange constant",
    "default_value": 0.0,
    "param_type": ParameterType.SCALAR,
    "unit": "J/m^2",
    "is_global": True,
    "read_only": False,
    "getter": None,
    "custom_setter": _interlayer_exchange_constant_setter,
}
"""Definition of interlayer-exchange parameters in a ferromagnetic material."""


def _getter_cubic_third_axis(parameter: MaterialParameter) -> MeshVector:
    return cross_product(
        parameter.sample.material["e_c1"].value.to_flattened(),
        parameter.sample.material["e_c2"].value.to_flattened(),
    ).to_unflattened()
