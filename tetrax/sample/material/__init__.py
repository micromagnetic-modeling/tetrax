"""
Material (:mod:`tetrax.sample.material`)
------------------------------------

This module defines important components and logic to handle the material parameters of a sample.

.. currentmodule:: tetrax.sample.material

.. autosummary::
    :toctree: generated/

    ~material.SampleMaterial
    ~parameter.MaterialParameter
    ~parameter.ParameterType
    ~standards.STANDARD_PARAMS_FERROMAGNETIC
    ~custom.PARAMS_INTERLAYER_EXCHANGE

"""  # noqa: D205, D400, D415
