"""
Sample (:mod:`tetrax.sample.sample`)
------------------------------------

This module defines the :class:`~tetrax.sample.sample.Sample` class, which is the main entry point of any `TetraX`
simulation.

.. currentmodule:: tetrax.sample.sample

.. autosummary::
    :toctree: generated/

    Sample
    SampleSnapshot

"""  # noqa: D205, D400, D415

from __future__ import annotations

import warnings
from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, List, Optional, Tuple, Union

import k3d
import numpy as np
from scipy.constants import mu_0
from tabulate import tabulate

from tetrax.common.config import (
    BOUNDARY_NODES_OF_BILAYER,
    COLOR_CYCLE_SAMPLE,
    SAMPLE_DEFAULT_GEOMETRY_TYPE,
    SAMPLE_DEFAULT_MAGNETIC_ORDER,
    SAMPLE_DEFAULT_MATERIAL,
    SAMPLE_DEFAULT_NAME,
    SAMPLE_DEFAULT_SCALE,
)
from tetrax.common.io import left_align_html_cols, write_field_to_file
from tetrax.common.math import (
    interpolate_along_curve,
    weighted_average,
)
from tetrax.common.typing import GeometryType, MagneticOrder
from tetrax.common.utils import deepcopy_mutable_attributes
from tetrax.interactions.cubic_anisotropy import CubicAnisotropy
from tetrax.interactions.dipole import DipoleInteraction
from tetrax.interactions.dmi_bulk import BulkDMI
from tetrax.interactions.dmi_interfacial import InterfacialDMI
from tetrax.interactions.exchange import ExchangeInteraction
from tetrax.interactions.interaction import Interaction, InteractionName
from tetrax.interactions.interlayer_exchange import InterlayerExchangeInteraction
from tetrax.interactions.uniaxial_anisotropy import UniaxialAnisotropy
from tetrax.interactions.zeeman import ZeemanInteraction
from tetrax.sample.material.custom import PARAMS_INTERLAYER_EXCHANGE
from tetrax.sample.material.material import SampleMaterial
from tetrax.sample.material.parameter import _default_direction_setter
from tetrax.sample.material.standards import STANDARD_PARAMS_FERROMAGNETIC
from tetrax.sample.mesh import FlattenedMeshVector, MeshScalar, MeshVector
from tetrax.sample.mesh.sample_mesh import SampleMesh

if TYPE_CHECKING:
    from typing import Self

    import meshio
    from numpy.typing import ArrayLike

    from tetrax.experiments.antenna import MicrowaveAntenna


def create_sample(*args, **kwargs):  # noqa: ANN201, ANN002, ANN003
    """Deprecated function to create samples (will be removed)."""  # noqa: D401
    raise NotImplementedError(
        "This function has been deprecated. From now on, you can create sample directly"
        "by creating an instance of the Sample class as Sample(mesh) (which requires to"
        "pass a mesh)."
    )


deprecated_material_message = """
Material parameters are not accessible from the sample object anymore. They
are now organized in the Sample.material attribute. For example, retrieve the
saturation magnetization as Sample.material['Msat']"
"""


class Sample:
    r"""
    Sample object of `TetraX`.

    This class describes the main parameters of the magnetic system at hand (such as geometry,
    magnetic order, material parameters , etc.) and, therefore,
    provides the main entry point of most `TetraX` simulations. It also
    holds the current magnetization state and provides some post-processing and visualization functionality such as
    :func:`average`, :func:`scan_along_curve` or :func:`plot`.

    Parameters
    ----------
    input_mesh : meshio.Mesh
        Input mesh for the sample, used to create the underlying :class:`~tetrax.sample.mesh.SampleMesh` with
        associated normal vectors, volume elements, differential operators, etc.
    name: str, optional
        Name of the sample, used to name the results directory.
    geometry_type : GeometryType, optional
        The type of geometry of the sample, which is automatically inferred when using template geometries from
        the :mod:`tetrax.geometries` module.
    magnetic_order : MagneticOrder, optional
        The magnetic order of the sample.
    material : dict, optional
        A dictionary with the material parameters of the sample (default is permaloy-like with the
        standard gyromagnetic ratio of the free electron).
    mesh_scale : float, optional
        Scale of the mesh in meters (so ``1e-9`` means the mesh dimensions are in nanometers).


    Notes
    -----
    The most commonly accessed attributes are:
        - :attr:`material`
        - :attr:`mag`
        - :attr:`xyz` and
        - :attr:`external_field`.
    As listed below, the sample can both have a single :attr:`mag` field, or two :attr:`mag1` and :attr:`mag2` fields.
    Depending on the chosen magnetic order, unnecessary fields are deleted after instantiation.

    Examples
    --------
    >>> import tetrax as tx
    >>> mesh = tx.geometries.waveguide.rectangular(500, 20)
    >>> sample = tx.Sample(mesh)
    >>> sample.show()
    # Displays the sample in 3D when using a notebook.

    This creates a sample by providing a mesh and displays it in 3D when using a notebook.
    Set the magnetization or material parameters using

    >>> sample.mag = (1, 0, 0)
    >>> sample.material["Msat"] = 1e6

    and calculate averages with

    >>> sample.average(sample.mag)
    array([1, 0, 0])

    More information is found in :doc:`Getting started </quickstart_notebook>` or in the :doc:`User Guide
    </usage/sample>`.

    """

    def __init__(  # noqa: PLR0913, D107
        self: Self,
        input_mesh: meshio.Mesh,
        name: str = SAMPLE_DEFAULT_NAME,
        geometry_type: GeometryType = SAMPLE_DEFAULT_GEOMETRY_TYPE,
        magnetic_order: MagneticOrder = SAMPLE_DEFAULT_MAGNETIC_ORDER,
        material: dict = SAMPLE_DEFAULT_MATERIAL,
        mesh_scale: float = SAMPLE_DEFAULT_SCALE,
    ) -> None:
        self.name: str = name
        """Name of the sample, used to name the results directory."""

        self.magnetic_order: MagneticOrder = MagneticOrder(magnetic_order)
        """The magnetic order of the sample."""

        # Automatically infer GeometryType from input mesh.
        if hasattr(input_mesh, "geometry_type"):
            self.geometry_type = input_mesh.geometry_type
        else:
            self.geometry_type = GeometryType(geometry_type)

        self._mesh = SampleMesh(input_mesh, self, mesh_scale)
        self.xyz: MeshVector = self._mesh.xyz
        """Coordinates on the underlying mesh."""

        # initialize material object and then set to default values
        self._material = SampleMaterial(self, STANDARD_PARAMS_FERROMAGNETIC)
        self.material: SampleMaterial = material
        """Material of the sample."""

        self._mag = None
        self._mag1 = None
        self._mag2 = None

        self._ext_field = MeshVector(np.zeros(self.xyz.shape))
        self.antenna: Optional[MicrowaveAntenna] = None
        """
        Microwave antenna attached to the sample.

        Different antennae are found in the :mod:`tetrax.experiments` module.
        """

        if self.magnetic_order == MagneticOrder.FERROMAGNET:
            self.mag = (0, 0, 1)
            del self.mag1
            del self.mag2

        elif self.magnetic_order == MagneticOrder.ANTIFERROMAGNET:
            self.mag1 = (0, 0, 1)
            self.mag2 = (0, 0, -1)
            del self.mag

        self.interactions: List[Interaction] = [
            ExchangeInteraction(self),
            DipoleInteraction(self),
            ZeemanInteraction(self),
            UniaxialAnisotropy(self),
            CubicAnisotropy(self),
        ]

        if self.geometry_type in [
            GeometryType.WAVEGUIDE,
            GeometryType.CONFINED,
            GeometryType.LAYER,
        ]:
            # DMIs are not yet implemented for other geometries
            self.interactions.append(BulkDMI(self))
            self.interactions.append(InterfacialDMI(self))

        # Add interlayer exchange, if applicable
        if (
            self.geometry_type is GeometryType.LAYER
            and self.mesh.nb >= BOUNDARY_NODES_OF_BILAYER
        ):
            self.material.add_parameter(PARAMS_INTERLAYER_EXCHANGE)
            self.interactions.append(InterlayerExchangeInteraction(self))

    def _get_repr_attributes_(self: Self) -> List:
        repr_attributes = [
            ["name", self.name],
            ["magnetic_order", self.magnetic_order.value],
            [
                "interactions",
                [interaction.name.value for interaction in self.interactions],
            ],
            ["external_field (avrg.)", f"{self.average(self.external_field)} T"],
            [
                "antenna",
                "None" if self.antenna is None else self.antenna.__class__.__name__,
            ],
        ]
        if self.magnetic_order is MagneticOrder.FERROMAGNET:
            repr_attributes.append(["mag (avrg.)", f"{self.average(self.mag)}"])
        elif self.magnetic_order is MagneticOrder.ANTIFERROMAGNET:
            repr_attributes.append(["mag1 (avrg.)", f"{self.average(self.mag1)}"])
            repr_attributes.append(["mag2 (avrg.)", f"{self.average(self.mag2)}"])

        return repr_attributes

    def __repr__(self: Self) -> str:
        """Return a string representation of the SampleMaterial object."""
        repr_attributes = self._get_repr_attributes_()
        # print(repr_attributes)
        return (
            "Sample\n"
            + tabulate(repr_attributes, headers=["attribute", "value"], tablefmt="grid")
            + "\n\n"
            + self.mesh.__repr__()
            + "\n\n"
            + self.material.__repr__()
            + "\nHint: You can use Sample.get_field('interaction_name'). Same with get_energy and get_interaction."
        )

    def _repr_html_(self: Self) -> str:
        """Return a string representation of the SampleMaterial object."""
        repr_attributes = self._get_repr_attributes_()

        return (
            "<h4>Sample</h4>"
            + left_align_html_cols(
                tabulate(
                    repr_attributes, headers=["attribute", "value"], tablefmt="html"
                )
            )
            + self.mesh._repr_html_()
            + self.material._repr_html_()
            + "<br><i>Hint:</i> You can use <tt>Sample.get_field('interaction_name')</tt>. Same with <tt>get_energy</tt> and <tt>get_interaction</tt>."
        )

    @property
    def mesh(self: Self) -> SampleMesh:
        """Mesh of the sample."""
        return self._mesh

    @mesh.setter
    def mesh(self: Self, value: meshio.Mesh) -> None:
        self._mesh = SampleMesh(value, self, self.scale)

    @property
    def material(self: Self) -> SampleMaterial:
        """Material of the sample."""
        return self._material

    @material.setter
    def material(self: Self, value: dict) -> None:
        self._material.set_from_dict(value, fill_missing=True)

    @property
    def mag(self: Self) -> MeshVector:
        """Magnetization direction of the sample."""
        if hasattr(self, "_mag"):
            return self._mag
        else:
            raise AttributeError(
                "This sample is probably antiferromagnetic "
                "and, therefore, does not have a single magnetization field. Use the mag1 and mag2 "
                "attributes instead."
            )

    @mag.setter
    def mag(self: Self, value: ArrayLike) -> None:
        self._mag = _default_direction_setter("mag", value, self.mesh, is_global=False)

    @mag.deleter
    def mag(self: Self) -> None:
        del self._mag

    @property
    def mag1(self: Self) -> MeshVector:
        """Magnetization direction within the first sublattice of the sample."""
        if hasattr(self, "_mag1"):
            return self._mag1
        else:
            raise AttributeError(
                "This sample is probably ferromagnetic "
                "and, therefore, has only one magnetization field called mag."
            )

    @mag1.setter
    def mag1(self: Self, value: ArrayLike) -> None:
        self._mag1 = _default_direction_setter(
            "mag1", value, self.mesh, is_global=False
        )

    @mag1.deleter
    def mag1(self: Self) -> None:
        del self._mag1

    @property
    def mag2(self: Self) -> MeshVector:
        """Magnetization direction within the second sublattice of the sample."""
        if hasattr(self, "_mag2"):
            return self._mag2
        else:
            raise AttributeError(
                "This sample is probably ferromagnetic "
                "and, therefore, has only one magnetization field called mag."
            )

    @mag2.setter
    def mag2(self: Self, value: ArrayLike) -> None:
        self._mag2 = _default_direction_setter(
            "mag1", value, self.mesh, is_global=False
        )

    @mag2.deleter
    def mag2(self: Self) -> None:
        del self._mag2

    @property
    def mag_full(self: Self) -> MeshVector:
        """Full magnetization of the sample in units of A/m."""
        return (
            np.tile(self.material["Msat"].value, 3) * self.mag.to_flattened()
        ).to_unflattened()

    @property
    def external_field(self: Self) -> MeshVector:
        """External field applied to the sample (in units of T)."""
        return self._ext_field

    @external_field.setter
    def external_field(self: Self, value: ArrayLike) -> None:
        if np.array_equal(self.xyz.shape, np.shape(value)):
            self._ext_field = MeshVector(value)
        elif np.shape(value) == (3,):
            self._ext_field = MeshVector([value for i in range(self.mesh.nx)])
        else:
            print(
                "You are trying to set an external field which does not match the shape of the mesh."
            )

    @property
    def Bext(self: Self) -> MeshVector:
        """Alias for :attr:`external_field`."""
        return self.external_field

    @Bext.setter
    def Bext(self: Self, value: ArrayLike) -> None:
        self.external_field = value

    @property
    def nx(self: Self) -> int:
        """DEPRECATED number of nodes in the sample (will be removed in future release)."""
        raise NotImplementedError(
            "You tried to call nx from the sample! Try sample.mesh.nx instead."
        )

    @property
    def nb(self: Self) -> int:
        """DEPRECATED number of boundary nodes in the sample (will be removed in future release)."""
        raise NotImplementedError(
            "You tried to call nb from the sample! Try sample.mesh.nx instead."
        )

    @property
    def Msat(self: Self) -> None:
        """DEPRECATED Msat of the sample (use ``sample.material["Msat"]`` instead)."""
        raise NotImplementedError(deprecated_material_message)

    @Msat.setter
    def Msat(self: Self, value: float) -> None:
        raise NotImplementedError(deprecated_material_message)

    @property
    def Aex(self: Self) -> None:
        """DEPRECATED Aex of the sample (use ``sample.material["Aex"]`` instead)."""
        raise NotImplementedError(deprecated_material_message)

    @Aex.setter
    def Aex(self: Self, value: float) -> None:
        raise NotImplementedError(deprecated_material_message)

    @property
    def Ku1(self: Self) -> None:
        """DEPRECATED Ku1 of the sample (use ``sample.material["Ku1"]`` instead)."""
        raise NotImplementedError(deprecated_material_message)

    @Ku1.setter
    def Ku1(self: Self, value: float) -> None:
        raise NotImplementedError(deprecated_material_message)

    def show(  # noqa: PLR0913, PLR0912, PLR0915
        self: Self,
        fields: Optional[List[MeshScalar, MeshVector]] = None,
        show_node_labels: bool = False,
        show_mag: bool = True,
        comp: str = "vec",
        scale: float = 5,
        show_scaled_mag: bool = True,
        show_grid: bool = False,
        show_extrusion: bool = True,
        show_antenna: bool = True,
        labels: Optional[List[str]] = None,
    ) -> k3d.Plot:
        """
        Display the sample or plot a field on its mesh.

        Shows the mesh and, if no field to plot are provided, the magnetization(s) of the sample. If it exists and
        has a mesh, the antenna of the sample will also be shown.

        Parameters
        ----------
        fields: list[MeshScalar or MeshVector], optional
            List of scalar or vector fields to be plotted. If ``None``, the magnetization(s) will be plotted.
        show_node_labels : bool, optional
            If true, shows the label associated to each node (default is ``False``).
        show_mag : bool
            Show the magnetization (or magnetizations, in the case of antiferromagnetic samples).
        comp : {"vec", "x", "y", "z"}
           Determines which component will be plotted (default is ``vec``).
        scale : float
            Determines the scale of the vector glyphs used to visualize the magnetization (default is 5).
        show_scaled_mag : bool
            Scale magnetization with spatially dependent saturation magnetization (default is ``True``).
        show_grid : bool
            Show background grid (default is ``False``).
        show_extrusion : bool
            Show full 3D mesh by extruding 1D or 2D mesh (default is ``True``).
        show_antenna : bool
            Show antenna of the sample (default is ``True``).
        labels : list[str], optional
            List of labels for the fields to be plotted. If ``None``, no labels will be shown.

        Returns
        -------
        plot : k3d_plot_object

        Examples
        --------
        >>> sample.show()
        >>> sample.show(sample.mag)

        These two are equivalent. However, any scalar or vector field can be shown that matches the size of the mesh.

        >>> sample.show(sample.external_field)
        >>> sample.show(sample.get_energy_density("exchange"))

        These two commands will generate seperate plots. Multiple fields can be shown in the same plot simply by
        providing a list (possibly containing both scalar and vector fields).

        >>> sample.show([sample.mag, sample.external_field, sample.get_energy_density("exchange")])

        """
        # return k3d_show_sample()
        cmap = k3d.colormaps.matplotlib_color_maps.Viridis

        # create plot object
        light_level = 0.5 if self.geometry_type == GeometryType.CONFINED else 0
        plot = k3d.plot(lighting=light_level)
        plot.grid_visible = show_grid
        plot = self.mesh.add_to_plot(plot)

        if show_extrusion:
            plot = self.mesh.add_extrusion_to_plot(plot)

        if show_node_labels:
            text_obj = k3d.vectors(
                np.float32(self.xyz),
                np.float32(0 * self.xyz),
                labels=[str(i) for i in range(len(self.xyz))],
            )
            plot += text_obj

        if show_antenna and self.antenna is not None:
            span = np.max(np.abs(self.mesh.xyz.x)) * 4
            self.antenna.add_to_plot(plot, span)

        # Decide what to plot.
        # The fields parameter can contain a list of vector or scalar fields that will be plotted. If it is empty
        # and show_mag is True then the magnetization(s) will be plotted.

        fields_to_plot = []

        if fields is None and show_mag:
            if self.magnetic_order == MagneticOrder.ANTIFERROMAGNET:
                # TODO include scaled mag for inhomogeneous material parameters.
                fields_to_plot = [self.mag1, self.mag2]
                labels_to_plot = ["mag1", "mag2"]

            elif self.magnetic_order == MagneticOrder.FERROMAGNET:
                if show_scaled_mag:
                    mag_to_plot = self.mag_full / self.material["Msat"].average
                    fields_to_plot = [mag_to_plot]
                    labels_to_plot = ["mag"]
                if not show_scaled_mag:
                    fields_to_plot = [self.mag]
                    labels_to_plot = ["mag"]

        elif fields is not None:
            fields_to_plot = fields

            # convert input to list if it is not already
            if isinstance(fields_to_plot, np.ndarray):
                fields_to_plot = [fields_to_plot]

            if isinstance(labels, str):
                labels_to_plot = [labels]

            # do some tests on the input
            if labels is not None and len(labels) != len(fields_to_plot):
                raise ValueError(
                    f"You supplied labels for the fields you want to plot, but the number of labels"
                    f" ({len(labels)}) does not match the number of fields ({len(fields)})."
                )
            labels_to_plot = labels

        # finally plot the fields
        for i, field in enumerate(fields_to_plot):
            if not isinstance(field, np.ndarray):
                raise ValueError(
                    f"You are trying to plot something ({field}) which is not a numpy array or a sublcass thereof."
                )

            if field.shape in [(self.mesh.nx, 3), (self.mesh.nx,)]:
                label = labels_to_plot[i] if labels_to_plot is not None else None

                color = COLOR_CYCLE_SAMPLE[i % len(COLOR_CYCLE_SAMPLE)]
                colors = np.repeat(
                    np.array([(np.uint32(color), np.uint32(color))]), self.mesh.nx
                )

                if label is not None:
                    plot += k3d.text2d(
                        label,
                        position=[i / 2 / len(labels_to_plot), 0],
                        color=color,
                        size=1,
                    )

                if field.shape == (self.mesh.nx,):
                    # scalar field case
                    plot += k3d.mesh(
                        np.float32(self.xyz),
                        np.uint32(self.mesh._meshio_mesh.get_cells_type("triangle")),
                        attribute=field,
                        color_map=cmap,
                        interpolation=False,
                        side="double",
                        flat_shading=True,
                    )

                elif field.shape == (self.mesh.nx, 3):
                    field = MeshVector(field)

                    if comp == "vec":
                        plot += k3d.vectors(
                            np.float32(self.xyz),
                            np.float32(
                                scale * (np.vstack([field.x, field.y, field.z])).T
                            ),
                            head_size=2.5 * scale,
                            line_width=0.05 * scale,
                            colors=colors,
                        )

                    else:
                        comp_dict = {
                            "x": field.x,
                            "y": field.y,
                            "z": field.z,
                            "phi": np.arctan2(field.y, field.z),
                            "abs": np.sqrt(field.x**2 + field.y**2 + field.z**2),
                        }
                        color_dict = {
                            "abs": k3d.colormaps.matplotlib_color_maps.Inferno,
                            "x": k3d.colormaps.matplotlib_color_maps.RdBu,
                            "y": k3d.colormaps.matplotlib_color_maps.RdBu,
                            "z": k3d.colormaps.matplotlib_color_maps.RdBu,
                            "phi": k3d.colormaps.matplotlib_color_maps.Twilight_shifted,
                        }

                        # TODO check if comp is valid key
                        plot += k3d.mesh(
                            np.float32(self.xyz),
                            np.uint32(
                                self.mesh._meshio_mesh.get_cells_type("triangle")
                            ),
                            attribute=comp_dict[comp],
                            color_map=color_dict[comp],
                            interpolation=False,
                            side="double",
                            flat_shading=True,
                        )

            else:
                raise ValueError(
                    f"The vector field you are trying to plot is of shape {field.shape}, "
                    f"but should be either (nx,3) or (nx,)."
                )

        return plot

    def plot(self: Self, *args, **kwargs) -> None:  # noqa: ANN003, ANN002
        """DEPRECATED plotting function (use :func:`show` instead)."""  # noqa: D401
        message = (
            "Warning: The plot() method has been deprecated and will be deleted in a future release. Please "
            "use only the show() method from now on. For example, you can now plot a field with show(field)."
        )

        warnings.warn(message, DeprecationWarning, stacklevel=2)
        return self.show(*args, **kwargs)

    def field_to_file(
        self: Self,
        field: Union[MeshScalar, MeshVector],
        fname: str = "field.vtk",
        qname: Optional[str] = None,
    ) -> None:
        """
        Write a scalar or vector field to a file (per default, in `VTK` format).

        This function saves a scalar or vector field associated with the mesh to a
        specified file, using the :func:`~tetrax.common.io.write_field_to_file` function
        from the :mod:`tetrax.common` module. Per default, the field data is saved in `VTK` format,
        which is commonly used for scientific
        data visualization. The user can specify the filename, a different filetype, and an optional quantity
        name for the field.

        Parameters
        ----------
        field : MeshScalar or MeshVector
            The scalar or vector field to be saved to the file. This field is associated
            with the mesh represented by `self`.
        fname : str, optional
            Name of the output file where the field will be saved. Defaults to "field.vtk".
        qname : Optional[str], optional
            Optional name for the quantity being saved, which can be useful for identifying
            the field in visualization tools. Defaults to ``None``.

        """
        write_field_to_file(field, self, fname, qname)

    def average(
        self: Self, field: Union[MeshScalar, MeshVector]
    ) -> Union[MeshScalar, MeshVector]:
        """Calculate the average of a scalar of vector field within the sample."""
        return weighted_average(field, self.mesh.node_volumes)

    def scan_along_curve(
        self: Self,
        field: Union[MeshScalar, MeshVector],
        curve: np.ndarray,
        num_points: int = 100,
        return_curve: bool = False,
    ) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
        """
        Interpolate a given scalar or vector field along a specified curve.

        Parameters
        ----------
        field : MeshScalar or MeshVector
            Field to be interpolated.
        curve : ndarray, shape (nx, 3) or tuple/list of shape (2, 3)
            The points along the interpolation curve or the start and end points of the curve using the format
            ``((x1, y1, z1),(x2, y2, z2))``.
        num_points : int, optional
            The number of points to interpolate along the curve. Will only be considered if start
            and end points are given (default is 100).
        return_curve : bool, optional
            Next to the interpolated values, also return the interpolation curve.

        Returns
        -------
        interp_values : ndarray, shape (num_points, m)
            The interpolated values along the curve with ``m`` being the size of the second dimension of ``field``.
        curve_coords : MeshVector
            The coordinates values along the curve (only returned if ``return_curve=True``).

        Examples
        --------
        - :doc:`Linescan example on the magnetization-graded waveguides </examples/linescans_waveguides>`

        """
        return interpolate_along_curve(
            self.xyz, field, curve, num_points, return_curve, dim=self.mesh.dimension
        )

    def get_field(
        self: Self, name: InteractionName, vec: Optional[MeshVector] = None
    ) -> MeshVector:
        """
        Return the field corresponding to a given magnetic interaction in units of T.

        The field can be evaluated with respect to a provided :class:`~tetrax.sample.mesh.MeshVector`.
        If no field is provided, the field is calculated from the current magnetization of the sample.

        Parameters
        ----------
        name: InteractionName
            Name of the magnetic interaction (can be provided as a string).
        vec: MeshVector, optional
            Magnetization with respect to which the field should be calculated. Of none is provided,
            the field will be calculated with respect to the current magnetization direction
            of the sample. Default is ``None``.

        Returns
        -------
        field: MeshVector
            Magnetic field in T.

        """
        vec = self.mag if vec is None else MeshVector(vec)

        interaction = self.get_interaction(name)
        return FlattenedMeshVector(
            interaction.unitless_field(vec.to_flattened())
            * np.tile(self.material["Msat"].value, 3)
            * mu_0
        ).to_unflattened()

    def get_energy(
        self: Self, name: InteractionName, vec: Optional[MeshVector] = None
    ) -> float:
        """
        Return the energy corresponding to a given magnetic interaction.

        The energy can be evaluated with respect to a provided :class:`~tetrax.sample.mesh.MeshVector`.
        If no field is provided, the energy is calculated from the current magnetization of the sample.
        If the geometry_type of the sample is a waveguide, layer, etc., the energy will be given
        with respect to the cross-section and so on.

        Parameters
        ----------
        name: InteractionName
            Name of the magnetic interaction (can be provided as a string).
        vec: MeshVector, optional
            Magnetization with respect to which the energy should be calculated. Of none is provided,
            the energy will be calculated with respect to the current magnetization direction
            of the sample. Default is ``None``.

        Returns
        -------
        energy: float
            Energy in J, J/m or J/m^2, depending on the ``geometry_type`` of the sample.

        """
        vec = self.mag if vec is None else MeshVector(vec)

        interaction = self.get_interaction(name)
        return interaction.energy(vec.to_flattened())

    def get_energy_density(
        self: Self, name: InteractionName, vec: Optional[MeshVector] = None
    ) -> MeshScalar:
        """
        Return the energy density corresponding to a given magnetic interaction.

        The density can be evaluated with respect to a provided :class:`~tetrax.sample.mesh.MeshVector`.
        If no field is provided, the energy density is calculated from the current magnetization of the sample.

        Parameters
        ----------
        name: InteractionName
            Name of the magnetic interaction (can be provided as a string).
        vec: MeshVector, optional
            Magnetization with respect to which the energy should be calculated. Of none is provided,
            the energy will be calculated with respect to the current magnetization direction
            of the sample. Default is ``None``.

        Returns
        -------
        energy: float
            Energy density in J/m^3.

        """
        vec = self.mag if vec is None else MeshVector(vec)

        interaction = self.get_interaction(name)
        return interaction.energy_density(vec.to_flattened())

    def get_interaction(self: Self, name: InteractionName) -> Interaction:
        """Return the magnetic interaction with a given name."""
        interactions_found = [
            interaction
            for interaction in self.interactions
            if interaction.name is InteractionName(name)
        ]

        if len(interactions_found) == 0:
            raise ValueError(
                "This interaction does not seem to be part of this sample."
            )

        elif len(interactions_found) == 1:
            interaction = interactions_found[0]
            return interaction

        else:
            raise NameError("Something weird happened. This interaction exists twice.")

    def get_snapshot(self: Self) -> SampleSnapshot:
        """Return an immutable snapshot of the sample's current state."""
        return SampleSnapshot(
            self.name,
            self.geometry_type,
            self.magnetic_order,
            self.external_field,
            self.material.to_dict(),
            sample=self,
        )


@dataclass(frozen=True)
class SampleSnapshot:
    """
    Immutable snapshot of a sample.

    Sample snapshots are obtained using the :func:`Sample.get_snapshot` method and are used as containers
    that represent a frozen snapshot of a sample without any fancy functionality. They are used during experiments,
    and to store metadata of the corresponding results. For more details,
    see :doc:`User Guide </usage/experiments>`.

    Parameters
    ----------
    name : str
        The name of the sample.
    geometry_type : GeometryType
        The type of geometry of the sample.
    magnetic_order : MagneticOrder
        The magnetic order of the sample.
    external_field : MeshVector
        The external magnetic field applied to the sample.
    compressed_material : dict
        A dictionary containing compressed material information.
    sample : Sample, optional
        An optional reference to the original sample object.

    """

    name: str
    """The name of the sample."""

    geometry_type: GeometryType
    """The type of geometry of the sample."""

    magnetic_order: MagneticOrder
    """The magnetic order of the sample."""

    external_field: MeshVector
    """The external magnetic field applied to the sample."""

    compressed_material: dict
    """A dictionary containing compressed material information."""

    sample: Optional[Sample] = None
    """An optional reference to the original sample object."""

    def __post_init__(self: Self) -> None:  # noqa: D105
        deepcopy_mutable_attributes(self)

    def to_dict(self: Self) -> Dict:
        """Return a dictionary containing name, geometry_type, magnetic_order and compressed material."""
        return {
            "name": self.name,
            "geometry_type": self.geometry_type.value,
            "magnetic_order": self.magnetic_order.value,
            "material": self.compressed_material,
        }
