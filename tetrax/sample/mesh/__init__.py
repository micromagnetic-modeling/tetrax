"""
Mesh (:mod:`tetrax.sample.mesh`)
------------------------------------

This module defines the main logic and components related to the geometry of the sample.

.. currentmodule:: tetrax.sample.mesh

Mesh objects and functions
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autosummary::
    :toctree: generated/

    ~sample_mesh.SampleMesh
    ~sample_mesh.MeshDimension
    ~sample_mesh.MeshioMesh
    ~utils.validate_mesh_shape
    ~utils.get_mesh_dimension


Quantities defined on meshes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autosummary::
    :toctree: generated/

    ~meshquantities.AnyVector
    ~meshquantities.LabVector
    ~meshquantities.MeshScalar
    ~meshquantities.MeshVector
    ~meshquantities.FlattenedMeshVector
    ~meshquantities.FlattenedAFMMeshVector
    ~meshquantities.LocalMeshVector
    ~meshquantities.FlattenedLocalMeshVector
    ~meshquantities.FlattenedLocalAFMMeshVector
    ~meshquantities.FlattenedAFMMeshVectorSpherical
    ~meshquantities.FlattenedMeshVectorSpherical
    ~meshquantities.make_flattened_AFM
    ~meshquantities.flattened_spherical_to_cartesian_fm
    ~meshquantities.flattened_spherical_to_cartesian_afm

"""  # noqa: D205, D400, D415

from __future__ import annotations

from .meshquantities import *
