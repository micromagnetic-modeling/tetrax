"""Defines extrusion functions for sample mesh of different geometry types."""
from __future__ import annotations

import k3d
import numpy as np
import pygmsh

__all__ = [
    "extrude_confined",
    "extrude_confined_axial",
    "extrude_waveguide",
    "extrude_waveguide_axial",
    "extrude_layer",
]

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from tetrax.sample.mesh.sample_mesh import SampleMesh


def extrude_confined(mesh: SampleMesh, plot: k3d.Plot) -> k3d.Plot:
    """Add extrusion for confined mesh (does nothing)."""
    return plot


def extrude_confined_axial(mesh: SampleMesh, plot: k3d.Plot) -> k3d.Plot:
    """Add extrusion for confined_axial sample_mesh."""
    connections = mesh.boundary_elements.reshape((mesh.nb, 2))

    with pygmsh.geo.Geometry() as geom:
        # points = [[1,0,0], [2,0,0],[0,1,0]]

        for connection in connections:
            p0 = geom.add_point(mesh.xyz[connection[0]])
            p1 = geom.add_point(mesh.xyz[connection[1]])

            poly = geom.add_line(p0, p1)

            a, _, _ = geom.revolve(
                poly, [0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 0.99999 * np.pi
            )
            geom.revolve(a, [0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 0.99999 * np.pi)

        mesh_rev = geom.generate_mesh()

    mesh_extr = k3d.mesh(
        mesh_rev.points,
        np.uint32(mesh_rev.get_cells_type("triangle")),
        side="both",
        color=0x0002,
    )

    mesh_extr.opacity = 0.05

    plot += mesh_extr
    return plot


def extrude_waveguide_axial(mesh: SampleMesh, plot: k3d.Plot) -> k3d.Plot:
    """Add extrusion for waveguide_axial sample_mesh."""
    with pygmsh.occ.Geometry() as geom:
        outer_radius = mesh.xyz[1, 0]
        mesh.xyz[0, 0]

        lc = 2 * np.pi * outer_radius / 40

        p0 = geom.add_point([0, 0, 0.0], mesh_size=lc)

        radii = mesh.xyz.x[mesh.boundary_nodes]
        for radius in radii:
            p1 = geom.add_point([radius, 0, 0.0], mesh_size=lc)
            p2 = geom.add_point([-radius, 0, 0.0], mesh_size=lc)

            arc1 = geom.add_circle_arc(p1, p0, p2)
            arc2 = geom.add_circle_arc(p2, p0, p1)

            geom.add_curve_loop([arc1, arc2])

        geom.remove(p0)

        ghost_mesh = geom.generate_mesh()

    connections = ghost_mesh.get_cells_type("line")
    nb = len(connections)

    center_points = ghost_mesh.points

    y_diff_max = np.abs(np.min(center_points[:, 1]) - np.max(center_points[:, 1]))
    x_diff_max = np.abs(np.min(center_points[:, 0]) - np.max(center_points[:, 0]))

    z_span = 3 * np.max([y_diff_max, x_diff_max])

    center_points_shifted1 = center_points.copy()
    center_points_shifted2 = center_points.copy()

    center_points_shifted1[:, 2] -= z_span / 2

    center_points_shifted2[:, 2] += z_span / 2
    all_points = np.concatenate((center_points_shifted1, center_points_shifted2))

    connections_mantel = []

    for i in range(nb):
        i1 = connections[i][0]
        i2 = connections[i][1]

        j1 = i1  # boundary_nodes.index(i1)
        j2 = i2  # boundary_nodes.index(i2)

        new_connectiona = [j1, j2, j1 + nb]
        new_connectionb = [j2, j2 + nb, j1 + nb]

        connections_mantel.append(new_connectiona)
        connections_mantel.append(new_connectionb)

    new_faceta = k3d.mesh(all_points, connections_mantel, side="both", color=0x0002)

    new_faceta.opacity = 0.05
    plot += new_faceta
    plot += k3d.mesh(
        ghost_mesh.points, ghost_mesh.get_cells_type("triangle"), wireframe=True
    )

    return plot


def extrude_waveguide(mesh: SampleMesh, plot: k3d.Plot) -> k3d.Plot:
    """Add extrusion for waveguide sample_mesh."""
    boundary_nodes = mesh.boundary_nodes.tolist()
    center_points = mesh.xyz[mesh.boundary_nodes].copy()

    front_points = center_points.copy()
    front_points.z += 100

    y_diff_max = np.abs(np.min(mesh.xyz.y) - np.max(mesh.xyz.y))
    x_diff_max = np.abs(np.min(mesh.xyz.x) - np.max(mesh.xyz.x))
    z_span = 3 * np.max([y_diff_max, x_diff_max])

    center_points_shifted1 = center_points.copy()
    center_points_shifted2 = center_points.copy()

    center_points_shifted1.z -= z_span / 2

    center_points_shifted2.z += z_span / 2
    all_points = np.concatenate((center_points_shifted1, center_points_shifted2))

    connections = mesh.boundary_elements.reshape((mesh.nb, 2))
    connections_mantel = []

    for i in range(mesh.nb):
        i1 = connections[i][0]
        i2 = connections[i][1]

        j1 = boundary_nodes.index(i1)
        j2 = boundary_nodes.index(i2)

        new_connectiona = [j1, j2, j1 + mesh.nb]
        new_connectionb = [j2, j2 + mesh.nb, j1 + mesh.nb]

        connections_mantel.append(new_connectiona)
        connections_mantel.append(new_connectionb)

    new_faceta = k3d.mesh(
        np.float32(all_points),
        np.uint32(connections_mantel),
        side="both",
        color=0x0002,
        name="3D waveguide",
    )

    new_faceta.opacity = 0.05
    plot += new_faceta

    return plot


def extrude_layer(mesh: SampleMesh, plot: k3d.Plot) -> k3d.Plot:
    """Add extrusion for layer sample_mesh."""
    boundary_nodes = mesh.boundary_nodes.tolist()
    boundary_coords = np.sort(mesh.xyz.y[boundary_nodes])

    number_of_layers = mesh.nb // 2

    y_diff_max = np.abs(np.min(mesh.xyz.y) - np.max(mesh.xyz.y))
    x_diff_max = np.abs(np.min(mesh.xyz.x) - np.max(mesh.xyz.x))

    z_span = 3 * np.max([y_diff_max, x_diff_max])

    for i in range(number_of_layers):
        y1 = boundary_coords[2 * i]
        y2 = boundary_coords[2 * i + 1]

        # plot += k3d.line([[0, y1, 0], [0, y2, 0]])

        pointa_minus = [-z_span / 2, y1, z_span / 2]
        pointb_minus = [z_span / 2, y1, z_span / 2]
        pointc_minus = [z_span / 2, y1, -z_span / 2]
        pointd_minus = [-z_span / 2, y1, -z_span / 2]

        # plot += k3d.points(points)

        pointa_plus = [-z_span / 2, y2, z_span / 2]
        pointb_plus = [z_span / 2, y2, z_span / 2]
        pointc_plus = [z_span / 2, y2, -z_span / 2]
        pointd_plus = [-z_span / 2, y2, -z_span / 2]

        points = [
            pointa_minus,
            pointb_minus,
            pointc_minus,
            pointd_minus,
            pointa_plus,
            pointb_plus,
            pointc_plus,
            pointd_plus,
        ]

        connections = [
            [0, 1, 2],
            [2, 3, 0],
            [4, 5, 6],
            [6, 7, 4],
            [0, 1, 4],
            [1, 4, 5],
            [2, 3, 6],
            [3, 6, 7],
            [5, 6, 1],
            [6, 1, 2],
            [0, 4, 7],
            [0, 3, 7],
        ]

        layer_mesh = k3d.mesh(
            np.float32(points),
            np.uint32(connections),
            color=0x0002,
            side="both",
            flat_shading=False,
            name="3D layer",
        )
        layer_mesh.opacity = 0.05
        plot += layer_mesh

    return plot
