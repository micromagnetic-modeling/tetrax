from __future__ import annotations

from enum import Enum
from typing import TYPE_CHECKING, List, TypeAlias

import k3d
import meshio
import numpy as np
from scipy.sparse import bmat, csr_matrix, diags
from tabulate import tabulate

from tetrax.common.io import left_align_html_cols
from tetrax.common.math import normalize
from tetrax.common.typing import GeometryType
from tetrax.sample.mesh.fem_core.cythoncore import (
    get_matrices2D,
    get_matrices2D_radial,
    get_matrices3D,
)
from tetrax.sample.mesh.fem_core.fempreproc import get_matrices1D, get_matrices1D_radial

from tetrax.sample.mesh import FlattenedMeshVector, MeshScalar, MeshVector
from tetrax.sample.mesh.extrusions import (
    extrude_confined,
    extrude_confined_axial,
    extrude_layer,
    extrude_waveguide,
    extrude_waveguide_axial,
)
from tetrax.sample.mesh.utils import get_mesh_dimension

if TYPE_CHECKING:
    from typing import Self

    from tetrax import Sample

GEOMETRY_INFO = {
    "confined": {
        "dimension": 3,
        "extrusion_function": extrude_confined,
        "get_matrices_function": get_matrices3D,
        "meshio_cell_type_closure": "tetra",
        "meshio_cell_type_boundary": "triangle",
    },
    "confined_axial": {
        "dimension": 2,
        "extrusion_function": extrude_confined_axial,
        "get_matrices_function": get_matrices2D_radial,
        "meshio_cell_type_closure": "triangle",
        "meshio_cell_type_boundary": "line",
    },
    "waveguide": {
        "dimension": 2,
        "extrusion_function": extrude_waveguide,
        "get_matrices_function": get_matrices2D,
        "meshio_cell_type_closure": "triangle",
        "meshio_cell_type_boundary": "line",
    },
    "waveguide_axial": {
        "dimension": 1,
        "extrusion_function": extrude_waveguide_axial,
        "get_matrices_function": get_matrices1D_radial,
        "meshio_cell_type_closure": "line",
        "meshio_cell_type_boundary": "vertex",
    },
    "layer": {
        "dimension": 1,
        "extrusion_function": extrude_layer,
        "get_matrices_function": get_matrices1D,
        "meshio_cell_type_closure": "line",
        "meshio_cell_type_boundary": "vertex",
    },
}

MeshioMesh: TypeAlias = meshio.Mesh
"""
Alias for the meshio.Mesh class.

See https://pypi.org/project/meshio/ for details.
"""


class SampleMesh:
    """
    Mesh geometry associated with a sample.

    This class encapsulates the geometry, boundary conditions, and mathematical operators
    associated with the mesh of a sample.
    Instances are created by supplying an input mesh of type :class:`MeshioMesh`. Subsequently, the shape of
    the input mesh is validated against the geometry type of the sample and important properties like coordinates,
    dimension, node volumes, and normal vectors, as well as matrix operators (e.g., Laplace, Poisson) are derived
    or calculated.

    Parameters
    ----------
    mesh : MeshioMesh
        Input mesh (see :mod:`tetrax.geometries` for templates).
    sample : Sample
        The sample object that this mesh represents.
    scale : float
        Scaling factor for the mesh coordinates in meters (so ``1e-9`` means nanometers).

    See Also
    --------
    tetrax.geometries

    """

    def __init__(self: Self, mesh: MeshioMesh, sample: Sample, scale: float) -> None:  # noqa: D107
        self.geometry_type: GeometryType = sample.geometry_type
        """Geometry type of the sample."""

        # TODO dimension check could just be part of GeometryType.is_satisfied_by()
        # Validation
        input_mesh_dim = get_mesh_dimension(mesh)
        sample_mesh_dim = GEOMETRY_INFO[self.geometry_type.value]["dimension"]

        message_validation = ""
        dimension_correct = input_mesh_dim == sample_mesh_dim
        if not dimension_correct:
            message_validation += (
                f"- The dimension of the input mesh ({input_mesh_dim}) does "
                f"not fit your chosen geometry type.\n "
            )

        properly_shaped, message_shape = self.geometry_type.is_satisfied_by(mesh)
        message_validation += message_shape
        if not properly_shaped or not dimension_correct:
            raise ValueError(
                f"Provided mesh is not compatible with geometry_type '{self.geometry_type.value}' of sample, for the "
                f"following reasons:\n{message_validation}"
            )

        # Preprocessing
        cell_type_closure = GEOMETRY_INFO[self.geometry_type.value][
            "meshio_cell_type_closure"
        ]
        cell_type_boundary = GEOMETRY_INFO[self.geometry_type.value][
            "meshio_cell_type_boundary"
        ]
        get_matrices = GEOMETRY_INFO[self.geometry_type.value]["get_matrices_function"]

        self._meshio_mesh = mesh

        self.sample: Sample = sample
        """Reference to corresponding sample."""

        self.dimension: int = input_mesh_dim
        """Dimension of the mesh."""

        self.xyz: MeshVector = MeshVector(mesh.points)
        """Coordinates on the mesh."""

        self.nx: int = len(self.xyz)
        """Total number of nodes in the mesh."""

        self.scale: float = scale
        """Scaling of the mesh (in meters)."""

        self.boundary_nodes: List[int] = ...
        """Indices of boundary nodes."""

        self.normal_vectors: MeshVector = ...
        """Field of normal vectors directed outwards."""

        self.node_volumes: MeshScalar = ...
        """
        :math:`d`-dim volume associated with each node (with :math:`d` being the mesh dimension).

        For axial geometries, the node volumes on a regular radial mesh scale linear in the radius.
        """

        self.cartesian_node_volumes: MeshScalar = ...
        """
        :math:`d`-dim cartesian volume associated with each node (with :math:`d` being the mesh dimension).

        For axial geometries, the cartesian volumes on a regular radial mesh are homogeneous.
        """

        self.boundary_angles: List[float] = ...
        r"""Angles associated with the boundary nodes (:math:`\pi` for a smooth boundary)."""

        self.boundary_elements: List[int] = ...
        """
        List of boundary elements containing boundary nodes and the connections between them.

        Each element out of the :math: `bnelx` elements contains the indicies of :math: `d` number of nodes 
        connected with each other, with :math: `d` being the mesh dimension and :math: `bnelx` the number 
        of boundary elements. For a waveguide (mesh dimension 2), the boudnary nodes of the :math: `i` th element 
        can be accessed as
        ``self.boundary_elements(2*i)`` and ``self.boundary_elements(2*i+1)``.
        """

        self.poisson: csr_matrix = ...
        """
        Sparse matrix to solve the magnetostatic Poisson equation with Neumann boundary conditions.

        The magnetostatic field is computed with a hybrid finite-element/boundary-element method (FEM/BEM).
        This method involves the solution of two second order differential equations.
        For the first potential one solves a Poisson-like equation with Neumann boundary conditions,
        while for the second potential a Laplace-like equation is solved with Dirichlet boundary conditions.

        This is the sparse matrix used to solve the Poisson-like equation with Neumann boundary conditions
        to obtain the first potential.
        The matrix contains the wave-vector independent part of the matrix, 
        strictly connected to the underlying finite element mesh.
        """

        self.laplace: csr_matrix = ...
        """
        Sparse matrix to solve the magnetostatic Laplace equation with Dirichlet boundary conditions.

        The magnetostatic field is computed with a hybrid finite-element/boundary-element method (FEM/BEM).
        This method involves the solution of two second order differential equations.
        For the first potential one solves a Poisson-like equation with Neumann boundary conditions,
        while for the second potential a Laplace-like equation is solved with Dirichlet boundary conditions.

        The matrix contains the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.
        """

        self.div_x: csr_matrix = ...
        """
        Sparse matrix to compute the divergence of a vectorfield in :math:`x` direction.

        This is the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.

        For axial geometries, the direction will be the radial direction.
        """

        self.div_y: csr_matrix = ...
        r"""
        Sparse matrix to compute the divergence of a vectorfield in :math:`y` direction.

        This is the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.

        For axial geometries, the direction will be the :math:`\phi` direction.
        """

        self.div_z: csr_matrix = ...
        """
        Sparse matrix to compute the divergence of a vectorfield in :math:`z` direction.

        This is the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.

        For axial geometries, the direction will be the :math:`z` direction.
        """

        self.grad_x: csr_matrix = ...
        r"""
        Sparse matrix to compute the gradient of a vectorfield in :math:`x` direction.

        This is the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.

        For axial geometries, the direction will be the radial (:math:`\rho`) direction.
        """

        self.grad_y: csr_matrix = ...
        r"""
        Sparse matrix to compute the divergence of a vectorfield in :math:`y` direction.

        This is the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.

        For axial geometries, the direction will be the :math:`\phi` direction.
        """

        self.grad_z: csr_matrix = ...
        """
        Sparse matrix to compute the divergence of a vectorfield in :math:`z` direction.

        This is the wave-vector independent part of the matrix,
        strictly connected to the underlying finite element mesh.

        For axial geometries, the direction will be the :math:`z` direction.
        """

        (
            self.boundary_nodes,
            self.normal_vectors,
            self.node_volumes,
            self.cartesian_node_volumes,
            self.boundary_angles,
            self.boundary_elements,
            self.poisson,
            self.laplace,
            self.div_x,
            self.div_y,
            self.div_z,
            self.grad_x,
            self.grad_y,
            self.grad_z,
        ) = get_matrices(
            self.xyz,
            self._meshio_mesh.get_cells_type(cell_type_closure),
            self._meshio_mesh.get_cells_type(cell_type_boundary),
        )

        self.nb: int = len(self.boundary_nodes)
        """Number of boundary nodes in the mesh."""

        self._extrusion_function = GEOMETRY_INFO[self.geometry_type.value][
            "extrusion_function"
        ]

    def _get_repr_attributes(self: Self) -> List:
        return [
            ["geometry_type", self.geometry_type.value, "geometry type"],
            ["nx", self.nx, "total nodes"],
            ["nb", self.nb, "boundary nodes"],
            ["scale", self.scale, ""],
        ]

    def __repr__(self: Self) -> str:
        """Return a string representation of the SampleMaterial object."""
        repr_attributes = self._get_repr_attributes()

        return f"SampleMesh (Sample.mesh) of '{self.sample.name}'\n" + tabulate(
            repr_attributes,
            headers=["attribute", "value", "description"],
            tablefmt="grid",
        )

    def _repr_html_(self: Self) -> str:
        repr_attributes = self._get_repr_attributes()

        return (
            f"<h4>SampleMesh (Sample.mesh) of '{self.sample.name}'</h4>"
            + left_align_html_cols(
                tabulate(
                    repr_attributes,
                    headers=["attribute", "value", "description"],
                    tablefmt="html",
                )
            )
        )

    @property
    def normal_vectors_on_nodes(self: Self) -> FlattenedMeshVector:
        """Calculate and return the node-wise normal vectors."""
        bnel = int(self.boundary_elements.shape[0] / self.dimension)
        nv_n = np.zeros_like(self.xyz)
        dim = self.dimension
        if dim > 1:
            for i in range(bnel):
                for j in range(dim):
                    cn = self.boundary_elements[dim * i + j]
                    nv_n.x[cn] += self.normal_vectors[dim * i]  # / float(dim)
                    nv_n.y[cn] += self.normal_vectors[dim * i + 1]  # / float(dim)
                    if dim == 3:
                        nv_n.z[cn] += self.normal_vectors[dim * i + 2]  # / float(dim)

        else:
            for i in range(self.nb):
                nv_n.y[self.boundary_nodes[i]] += self.normal_vectors[i]

        nv_n = normalize(nv_n)
        normal_vectors_on_nodes = np.zeros_like(self.xyz)
        normal_vectors_on_nodes[self.boundary_nodes] = nv_n[self.boundary_nodes]

        normal_vectors_on_nodes.x[self.boundary_nodes] /= (
            dim * self.node_volumes[self.boundary_nodes]
        )
        normal_vectors_on_nodes.y[self.boundary_nodes] /= (
            dim * self.node_volumes[self.boundary_nodes]
        )
        normal_vectors_on_nodes.z[self.boundary_nodes] /= (
            dim * self.node_volumes[self.boundary_nodes]
        )

        return normal_vectors_on_nodes

    def get_rot(self: Self, open_boundary: bool) -> csr_matrix:
        """
        Return the rot/curl operator of the mesh, acting on FlattenedMeshVectors.

        Parameters
        ----------
        open_boundary : bool
            If False, include the boundary terms (important, e.g., for bulk DMI)

        Returns
        -------
        csr_matrix

        """
        sparse_matrix = bmat(
            [
                [None, -self.grad_z, self.grad_y],
                [self.grad_z, None, -self.grad_x],
                [-self.grad_y, self.grad_x, None],
            ],
            format="csr",
        )

        if not open_boundary:
            nv = self.normal_vectors_on_nodes
            boundary_matrix = bmat(
                [
                    [None, -diags(nv.z), diags(nv.y)],
                    [diags(nv.z), None, -diags(nv.x)],
                    [-diags(nv.y), diags(nv.x), None],
                ],
                format="csr",
            )
            sparse_matrix -= 0.5 * boundary_matrix

        return sparse_matrix

    def add_to_plot(self: Self, plot: k3d.Plot) -> k3d.Plot:
        """Append sample mesh (connections and points for vertices) to k3d plot object."""
        cell_type = (
            "line"
            if self.geometry_type in [GeometryType.LAYER, GeometryType.WAVEGUIDE_AXIAL]
            else "triangle"
        )
        plt_mesh = k3d.mesh(
            np.float32(self.xyz), np.uint32(self._meshio_mesh.get_cells_type(cell_type))
        )
        plt_mesh.color = 0xAAAAAA
        plt_mesh.wireframe = True

        if self.geometry_type == GeometryType.CONFINED:
            plot_mesh_3d = k3d.mesh(
                np.float32(self.xyz),
                np.uint32(self._meshio_mesh.get_cells_type(cell_type)),
            )
            plot_mesh_3d.wireframe = False
            # plot_mesh_3d.opacity = 0.95
            plot_mesh_3d.color = 0xAAAAAA
            plot += plot_mesh_3d

        plot += k3d.points(
            positions=np.float32(self.xyz),
            point_size=0.2,
            shader="3d",
            color=0xAAAAAA,
            side="both",
        )

        plot += plt_mesh

        return plot

    def add_extrusion_to_plot(self: Self, plot: k3d.Plot) -> k3d.Plot:
        """Add extrusion of mesh to plot k3d object."""
        return self._extrusion_function(self, plot)


class MeshDimension(Enum):
    """Dimension of the mesh."""

    ONE = 1
    TWO = 2
    THREE = 3

    @classmethod
    def _missing_(cls, value: str) -> None:  # noqa: ANN102
        raise ValueError(
            "%r is not a valid %s.  Valid dimensions: %s"
            % (
                value,
                cls.__name__,
                ", ".join([repr(m.value) for m in cls]),
            )
        )
