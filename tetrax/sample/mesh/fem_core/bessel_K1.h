//
// Created by Attila Kákay on 14.04.21.
//

#ifndef TETRAX_BESSEL_K1_H
#define TETRAX_BESSEL_K1_H
/*----------------------------------------------------------------------*/
double BESSI1(double x);
/*----------------------------------------------------------------------*/
double BESSK1(double x);
/*----------------------------------------------------------------------*/
#endif //TETRAX_BESSEL_K1_H
