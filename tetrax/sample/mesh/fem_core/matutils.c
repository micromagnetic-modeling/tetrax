//
// Created by Attila Kakay on 25.01.21.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "matutils.h"
#include "sorting.h"

//#include "akmag.h"
/*----------------------------------------------------------------------*/
void IniIVector(int *vector, int size, int value)
{
//    #pragma parallel for
    for(int i=0;i<size;i++) vector[i] = value;
}
/* End void IniIVector(int *vector, int size, int value) */
/*----------------------------------------------------------------------*/
void IniDVector(double *vector, int size, double value)
{
//#pragma parallel for
    for(int i=0;i<size;i++) vector[i] = value;
}
/* End void IniDVector(double *vector, int size, double value) */
/*----------------------------------------------------------------------*/
void sprstp(double sa[], int ija[], double sat[], int ijat[])
{
    int i, j, jl, jm, jp, ju, k, m, n, noff, inc, iv, lastrow, ih, changed;
    double v, dh;
    n = ija[0] - 1;
    ijat[n] = 0;
    ijat[0] = ija[0];
    /*  for (j = 0; j < n; j++) sat[j] = sa[j];*/
    memcpy(sat,sa,n*sizeof(double));
    iindexx (ija[n] - n - 1, ija + n + 1, ijat + n + 1);
    /*  for (i = n + 1; i < ija[n]; ++i)  */
    lastrow = 0;
    for (i = n + 1; i < ija[n]; ++i) {
        m = (ijat[i] + n + 1);
        sat[i] = sa[m];
        jl = 0;
        ju = n;
        while (ju - jl > 1) {
            jm = (ju + jl) / 2;
            if (ija[jm] > m)
                ju = jm;
            else
                jl = jm;
        }
        ijat[i] = jl;
        while (lastrow + 1 <= ija[m]) {
            ijat[++lastrow] = i;
        }
    }
    while (lastrow + 1 <= n) {
        ijat[++lastrow] = i;
    }
    for (i = 0; i < n; ++i)
        for (changed = 1, j = ijat[i + 1]; changed && j > ijat[i]; --j)
            for (changed = 0, k = ijat[i]; k < j - 1; ++k) {
                if (ijat[k + 1] < ijat[k]) {
                    changed = 1;
                    dh = sat[k];
                    sat[k] = sat[k + 1];
                    sat[k + 1] = dh;
                    ih = ijat[k];
                    ijat[k] = ijat[k + 1];
                    ijat[k + 1] = ih;
                }
            }
}
/*----------------------------------------------------------------------*/
void RISSprSAXS(RISSprMat spm, AKVec vec, AKVec *out, int sign)
{
    int i,k,ub, n=(int) vec.lvs;
    double a, *sa, *x;
    int *ija;
    sa = spm.sa;
    ija = spm.ija;
    x = vec.x;
    if (ija[0] != n+1) {
        printf("\n#Mismatched vector and matrix in RISSprSAXS!\n");
        exit(0);
    }
    //for (i=0;i<n;i++) b[i]=sa[i]*x[i];
//#pragma omp parallel for private(k,ub,a,sign) //schedule(dynamic,128)
    for (i=0;i<n;i++) {
        a=sa[i]*x[i];
        ub=ija[i+1];
        for (k=ija[i];k<ub;k++)
            a += sa[k]*x[ija[k]];
        out->x[i]=sign*a;
    }
}
/*----------------------------------------------------------------------*/
void dsprsax(double *sa, int *ija, double *x, double *b,int n)
{
    int i,k,ub;
    double a;
    if (ija[0] != n+1) {
        printf("\n#Mismatched vector and matrix in dsprsax!\n");
        exit(0);
    }
    //for (i=0;i<n;i++) b[i]=sa[i]*x[i];
//#pragma omp parallel for private(k,ub,a) //schedule(dynamic,128)
    for (i=0;i<n;i++) {
        a=sa[i]*x[i];
        ub=ija[i+1];
        for (k=ija[i];k<ub;k++)
            a += sa[k]*x[ija[k]];
        b[i]=a;
    }
}
/*----------------------------------------------------------------------*/
void dsprsaxadd(double *sa, int *ija, double *x, double *b,int n)
{
    //void nrerror(char error_text[]);
    int i,k,p;
    double tmp_b;
    if (ija[0] != n+1) {
        printf("\n#Mismatched vector and matrix in sprstx!\n");
        exit(0);
    }
    //  for (i=0;i<n;i++) b[i]=sa[i]*x[i];
//#pragma omp parallel for private(k,tmp_b) // schedule(dynamic,128)
    for (i=0;i<n;i++) {
        tmp_b=sa[i]*x[i];
        for (k=ija[i];k<ija[i+1];k++)
            tmp_b += sa[k]*x[ija[k]];
        b[i]+=tmp_b;
    }
}
/* End void dsprsax(double *sa, int *ija, double *x, double *b,int n)  */
/*----------------------------------------------------------------------*/
/*
void dsprstx_OMP(double *sa, int *ija, double *x, double *b,int n)
{
    int i,j,k,p,nt,myoffset;
    static double *temp_b=NULL;
    static int maxn=0;
    nt=omp_get_max_threads();
    if (n>maxn) {
        maxn=n;
        temp_b=realloc(temp_b,n*nt*sizeof(double));
    }
    if (ija[0] != n+1) {
        printf("\n#Mismatched vector and matrix in sprstx!\nija[0]=%d\n",ija[0]);
        exit(0);
    }
#pragma omp parallel for
    for(i=0;i<nt*n;++i) temp_b[i]=0;
#pragma omp parallel for private(p,k,myoffset)
    for (i=0;i<n;i++) {
        b[i]=sa[i]*x[i];
        p=ija[i+1];
        myoffset=omp_get_thread_num()*n;
        for (k=ija[i];k<p;k++) {
            temp_b[myoffset+ija[k]] += sa[k]*x[i];
        }
    }
#pragma omp parallel for private(j)
    for(i=0;i<n;++i)
        for(j=0;j<nt;++j)
            b[i]+=temp_b[j*n+i];
}
 */
/*----------------------------------------------------------------------*/
void dsprstx(double *sa, int *ija, double *x, double *b,int n)
{
    //  void nrerror(char error_text[]);
    int i,k,p;
    if (ija[0] != n+1) {
        printf("\n#Mismatched vector and matrix in sprstx!\nija[0]=%d\n",ija[0]);
        exit(0);
    }
    for(i=0;i<n;++i) b[i] = 0.0;
    for (i=0;i<n;i++) {
        b[i]+=sa[i]*x[i];
        p=ija[i+1];
        for (k=ija[i];k<p;k++) {
            b[ija[k]] += sa[k]*x[i];
        }
    }
}
/*----------------------------------------------------------------------*/