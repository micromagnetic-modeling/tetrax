//
// Created by attilak on 16.07.20.
//

#include "utils.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "matutils.h"
#include "sorting.h"

/*----------------------------------------------------------------------*/
void nrerror(char error_text[])
{
    fprintf(stderr,"Model run-time error...\n");
    fprintf(stderr,"%s\n",error_text);
    fprintf(stderr,"...now exiting to system...\n");
    exit(1);
}
/* End void nrerror(char error_text[])  */
/*----------------------------------------------------------------------*/
double dot_prod(double *a, double *b)
{
    return(a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
}
/* End double dot_prod(double *a, double *b)  */
/*----------------------------------------------------------------------*/
double dotprod(double *a, double *b)
{
    return(a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
}
/* End double dot_prod(double *a, double *b)  */
/*----------------------------------------------------------------------*/
double dot_prod2D(const double *a,const  double *b)
{
    return(a[0]*b[0] + a[1]*b[1]);
}
/* End double dot_prod(double *a, double *b)  */
/*----------------------------------------------------------------------*/
void cross_prod(double *a, double *b, double *c) {
    c[0] = a[1]*b[2] - a[2]*b[1];
    c[1] = a[2]*b[0] - a[0]*b[2];
    c[2] = a[0]*b[1] - a[1]*b[0];
}
/* End void cross_prod(double *a, double *b, double *c)  */
/*----------------------------------------------------------------------*/
double vectabs(double *r)
{
   return(sqrt(r[0]*r[0]+r[1]*r[1]+r[2]*r[2]));
}
/* End double vectabs(double *a)  */
/*----------------------------------------------------------------------*/
double vectnorm(double *r)
{
    return(r[0]*r[0]+r[1]*r[1]+r[2]*r[2]);
}
/* End double vectabs(double *a)  */
/*----------------------------------------------------------------------*/
double vectnorm2D(double *r)
{
    return(r[0]*r[0]+r[1]*r[1]);
}
/* End double vectabs(double *a)  */
/*----------------------------------------------------------------------*/
double vectabsmaxcomp(double *a)
{
    double v_max,tmp;
    tmp = DMAX(fabs(a[0]),fabs(a[1]));
    v_max = DMAX(tmp,fabs(a[2]));
    return(v_max);
}
/* double vectmaxcomp(double *a) */
/*----------------------------------------------------------------------*/
double vectabsmaxcomp2D(double *a)
{
    double v_max;
    v_max = DMAX(fabs(a[0]),fabs(a[1]));
    return(v_max);
}
/* double vectmaxcomp2D(double *a) */
/*----------------------------------------------------------------------*/
void normalize_v(double *a)
{
    double norm;
    norm = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
    for(int i=0; i<3; i++) a[i] /= norm;
}
/* End void normalize_v(double *a)  */
/*----------------------------------------------------------------------*/
void normalize_v2D(double *a)
{
    double norm;
    norm = sqrt(a[0]*a[0] + a[1]*a[1]);
    a[0] /= norm;
    a[1] /= norm;
}
/* End void normalize_v(double *a)  */
/*----------------------------------------------------------------------*/
double vectabs2D(const double *r)
{
    return(sqrt(r[0]*r[0]+r[1]*r[1]));
}
/* End double vectabs(double *a)  */
/*----------------------------------------------------------------------*/
void vectdiff2D(double *a, double *b, double *c)
{
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
//    c[2] = a[2] - b[2];
}
/* End void vect_diff(double *a, double *b, double *c)  */
/*----------------------------------------------------------------------*/
void vectdiff(double *a, double *b, double *c)
{
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
}
/* End void vect_diff(double *a, double *b, double *c)  */
/*----------------------------------------------------------------------*/
double distP2Line(double *x0, double *x1, double *x2)
{
    return(fabs((x2[0] - x1[0])*(x1[1] - x0[1])-(x1[0] - x0[0])*(x2[1] -
    x1[1]))/sqrt((x2[0] - x1[0])*(x2[0] - x1[0]) + (x2[1] - x1[1])*(x2[1] - x1[1])));
}
/*----------------------------------------------------------------------*/
double Det2(double a11, double a12, double a21, double a22)
{
    return(a11*a22-a12*a21);
}
/* End double Det2(double a11, double a12, double a21, double a22) */
/*----------------------------------------------------------------------*/
double area_el2(const double *a, const double *b, const double *c)
{
    double u[3], v[3], w[3];
//    #pragma parallel for
    for (int i = 0; i < 3; i++) {
        u[i] = b[i] - a[i];
        v[i] = c[i] - a[i];
    }
    cross_prod(u,v,w);
    return (vectabs(w));
}
/* End double area_el(double *a, double *b, double *c) */
/*----------------------------------------------------------------------*/
double Det3(double a11, double a12, double a13,
    double a21, double a22, double a23,
    double a31, double a32, double a33)
{
  return(a11*a22*a33 + a12*a23*a31 + a13*a21*a32
	  - a13*a22*a31 - a11*a23*a32 - a12*a21*a33);
}
/* End double Det3 */
/*----------------------------------------------------------------------*/
double vol_el6(double *a, double *b, double *c, double *d)
{
    double u[3], v[3], w[3];
//    #pragma parallel for
    for(int i=0;i<3;i++) {
        u[i] = b[i] - a[i];
        v[i] = c[i] - a[i];
        w[i] = d[i] - a[i];
    }
    return(u[0]*v[1]*w[2] + u[1]*v[2]*w[0] + u[2]*v[0]*w[1] -
    u[2]*v[1]*w[0] - u[1]*v[0]*w[2] - u[0]*v[2]*w[1]);
}
/* End double vol_el(double *a, double *b, double *c, double *d) */
/*----------------------------------------------------------------------*/
void dvcopy(double *rx, double *ry, int n, int inc)
{
 //   #pragma parallel for
    for(int i=0;i<n;i+=inc) ry[i] = rx[i];
}
/* End void dcopy(double *rx, double *ry, int n, int inc)  */
/*----------------------------------------------------------------------*/
void MakeInvNodeElmList(AKElm *elm)
{
  int nx, nelx, eld;
  int *pnode;
  int i,j,psum=0,cn,cel;

  nx = elm->nx;
  nelx = (int) elm->nelx;
  eld = elm->eld;

  pnode = (int *) malloc((size_t) nx*sizeof(int));
  if(!pnode) memerror("pnode");
  IniIVector(pnode,nx,0);

  elm->num_el = (int *) malloc((size_t) nx*sizeof(int));
  if(!elm->num_el) memerror("num_el");
  elm->pnel_list = (int *) malloc((size_t) nx*sizeof(int));
  if(!elm->pnel_list) memerror("pnel_list");
  IniIVector(elm->num_el,nx,0);

  /* elm->num_el[i] the number of elements the node i is contained in */
  for(i=0;i<nelx;i++)
    for(j=0;j<eld;j++)
      elm->num_el[elm->ijk[j][i]]++;

  elm->pnel_list[0] = 0;
  psum = elm->num_el[0];
  for(i=1;i<nx;i++) {
    elm->pnel_list[i] = psum;
    psum += elm->num_el[i];
  }

  elm->psum = psum;
  elm->nel_list = (int *) malloc((size_t) psum*sizeof(int));
  if(!elm->nel_list) memerror("nel_list");

  for(i=0;i<nelx;i++)
    for(j=0;j<eld;j++) {
      cn = elm->ijk[j][i];
      cel = elm->pnel_list[cn]+pnode[cn];
      elm->nel_list[cel] = i;
      pnode[cn]++;
    }
  free(pnode);
}
/*----------------------------------------------------------------------*/
void MakeAdjacentNodeList(AKElm *elm)
{
  int nx, eld;
  //int nelx;
  int *p, *list;
  int i, j, l, k, numadjn, p2adjn, el_no, node, flag, adjsum;

  nx = elm->nx;
//  nelx = elm->nelx;
  eld = elm->eld;

  list = (int *) malloc( 10000*sizeof(int));
  if(!list) memerror("list");
  elm->pnx_list = (int *) malloc(2*nx*sizeof(int));
  if(!elm->pnx_list) memerror("pnx_list");

  for(i=0;i<nx;i++) {
    IniIVector(list,10000,0);
    numadjn = 0;
    for(j=0;j<elm->num_el[i];j++) {
      el_no = elm->nel_list[elm->pnel_list[i]+j];
      for(k=0;k<eld;k++) {
	      node = elm->ijk[k][el_no];
	      if(node != i) {
	        flag = 0;
	        for(l=0;l<numadjn;l++) {
	          if(node == list[l]) {
	            flag = 1;
	            break;
	          }
	        }
	        if(!flag) {
	          list[numadjn] = node;
	          numadjn++;
	          if(numadjn >= 10000) {
	            printf("\n\n!!!Please check your mesh. It does not seem to be a proper mesh.\nYou have too many adjacent nodes!!!\n");
	            exit(0);
	          }
	        }
	      }
      }
    }
    elm->pnx_list[i] = numadjn;
  }

  elm->pnx_list[nx] = 0;
  adjsum = elm->pnx_list[0];
  for(i=1;i<nx;i++) {
    elm->pnx_list[nx+i] = adjsum;
    adjsum += elm->pnx_list[i];
  }
  elm->adjsum = adjsum;
  p = (int *) malloc((size_t) adjsum*sizeof(int));
  if(!p) memerror("adjacent node list (nx_list)");

  for(i=0;i<nx;i++) {
    numadjn = 0;
    for(j=0;j<elm->num_el[i];j++) {
      el_no = elm->nel_list[elm->pnel_list[i]+j];
      for(k=0;k<eld;k++) {
	      node = elm->ijk[k][el_no];
	      if(node != i) {
	        flag = 0;
	        for(l=0;l<numadjn;l++)
	        if(node == p[elm->pnx_list[nx+i]+l]) {
	          flag = 1;
	          break;
	        }
	        if(!flag) {
	          p[elm->pnx_list[nx+i]+numadjn] = node;
	          numadjn++;
	        }
	      }
      }
    }
  }

  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    p2adjn = elm->pnx_list[nx+i];
    sort(numadjn,p+p2adjn);
  }
  free(list);
  elm->nx_list = p;
}
/* End MakeAdjacentNodeList()  */
/*----------------------------------------------------------------------*/
int clean_mesh(AKElm *elm)
{
  int nx, nelx, eld, *node_flag, *change_f;
  int i, j, last_node, new_nx, cn, temp;

  nx = elm->nx;
  nelx = elm->nelx;
  eld = elm->eld;
  node_flag = (int *) malloc((size_t) (nx*sizeof(int)));
  change_f = (int *) malloc((size_t) (nx*sizeof(int)));
//#pragma parallel for
  for(i=0;i<nx;i++) {
    node_flag[i] = 0;
    change_f[i] = i;
  }

//#pragma parallel for private (j)
  for(i=0;i<nelx;i++)
    for(j=0;j<eld;j++)
      node_flag[elm->ijk[j][i]] = 1;

  last_node = nx-1;
  new_nx = nx;
  for(i=0;i<nx;i++) {
    if(i > last_node) {
      printf("Break\n");
      break;
    }
    if(!node_flag[i]) {
      new_nx--;
//      printf("%i\n",i);
	    while(!node_flag[last_node]) {
	      last_node--;
	    }
	    if (last_node>i) {
	      change_f[i] = last_node;
	      change_f[last_node] = i;
	      node_flag[i] = 1;
	      node_flag[last_node] = 0;
	      last_node--;
	    }
    }
  }

  if(new_nx == nx) {
//    printf("Clean_mesh could not find ghost nodes.\n");
//    printf("It seems the mesh is clean.\n");
//    fflush(stdout);
  } else {
    elm->nx = new_nx;
    for(i=new_nx;i<nx;i++) {
      cn = change_f[i];
      temp = elm->xyz[3*cn];
      elm->xyz[3*cn] = elm->xyz[3*i];
      elm->xyz[3*i] = temp;
      temp = elm->xyz[3*cn+1];
      elm->xyz[3*cn+1] = elm->xyz[3*i+1];
      elm->xyz[3*i+1] = temp;
      temp = elm->xyz[3*cn+2];
      elm->xyz[3*cn+2] = elm->xyz[3*i+2];
      elm->xyz[3*i+2] = temp;
    }
    for(j=0;j<nelx;j++)
      for(i=0;i<eld;i++)
	      elm->ijk[i][j] = change_f[elm->ijk[i][j]];
      //elm->ijk[4][j] = 1;

  }
  return(1);
}
/*----------------------------------------------------------------------*/
double deg2rad(double phi_d)
{
  return(phi_d*PI/180.);
}
/* End double deg2rad(double phi_d)  */
/*----------------------------------------------------------------------*/
double rad2deg(double phi_r)
{
  return(phi_r*180./PI);
}
/* End double rad2deg(double phi_r)  */
/*----------------------------------------------------------------------*/
/*
void phi2mx(double *w, AKVec *mmx)
{
    double t1, t2, t3, t4;
    int i, inx;
    inx = mmx->ivs;
//#pragma omp parallel for private(t1,t2,t3,t4)
    for(i=0;i<inx;i++) {
        t1 = sin(w[2*i]);
        t2 = cos(w[2*i]);
        t3 = sin(w[2*i+1]);
        t4 = cos(w[2*i+1]);
        mmx->x[i] = t1*t4; // sin(theta)*cos(phi) = mx
        mmx->y[i] = t1*t3; // sin(theta)*sin(phi) = my
        mmx->z[i] = t2; // cos(theta) = mz
    }
}
 */
/*----------------------------------------------------------------------*/
