from libc.stdlib cimport malloc, free
from .cythoncore cimport *
import numpy as np
cimport numpy as np
from scipy.sparse import csr_matrix, bmat, diags
from tetrax.common.typing import GeometryType

def ensure_contiguous(np.ndarray arr):
    """
    Ensures that the allocated arrays are contiguous to match C array storing standards.
    """
    if not arr.flags['C_CONTIGUOUS']:
        arr = np.ascontiguousarray(arr)
    return arr

def get_matrices2D(np.ndarray xyz, np.ndarray mesh, np.ndarray bmesh):
    nx = xyz.shape[0]
    nelx = mesh.shape[0]
    cdef CSRSprMat poiss
    cdef CSRSprMat lap
    cdef CSRSprMat div_x
    cdef CSRSprMat div_y
    cdef CSRSprMat grad_x
    cdef CSRSprMat grad_y
# Boundary list
    BNList = np.ascontiguousarray(np.zeros(nx, dtype=np.int32))
    cdef int[:] bnlist_memview = BNList
    cdef int* bnlist = &bnlist_memview[0]
# normal vectors
    nv = np.ascontiguousarray(np.zeros(2*nx, dtype=np.double))
    cdef double[:] nv_memview = nv
    cdef double* nv_carray = &nv_memview[0]
# area of elements
    area = np.ascontiguousarray(np.zeros(nx, dtype=np.double))
    cdef double[:] area_memview = area
    cdef double* area_carray = &area_memview[0]
# area of elements
    pang = np.ascontiguousarray(np.zeros(nx, dtype=np.double))
    cdef double[:] pang_memview = pang
    cdef double* pang_carray = &pang_memview[0]
# preparing belm_carray
    belm = np.ascontiguousarray(np.zeros(2*nx, dtype=np.int32))
    cdef int[::1] belm_memview = belm
    cdef int* belm_carray = &belm_memview[0]
    cdef int bnx
    dim = 2
# preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
# preparing mesh_carray - connectivities
    mesh = mesh.astype(np.int32)
    mesh = mesh.flatten()
    mesh = ensure_contiguous(mesh)
    cdef int[::1] mesh_memview = mesh
    cdef int* mesh_carray = &mesh_memview[0]
    err = OperatorsFromMesh2D(
        xyz_carray,
        mesh_carray,
        nx,
        nelx,
        dim,
        bnlist,
        &bnx,
        nv_carray,
        area_carray,
        &poiss,
        &lap,
        &div_x,
        &div_y,
        &grad_x,
        &grad_y,
        pang_carray,
        belm_carray,
        )
#    print("The C has done it's job!")
    bnlist_ret = BNList[0:bnx]
    nv_ret = nv[0:2*bnx]
    pang_ret = pang[0:bnx]
    belm_ret = belm[0:2*bnx]
# Poisson matrix for return
    nnz = poiss.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(poiss.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = poiss.csrValues_h[i]
        indices[i] = poiss.csrColIdx_h[i]
    for i in range(poiss.inx+1):
        indptr[i] = poiss.csrRowPtr_h[i]
    poiss_py = csr_matrix((data, indices, indptr), shape=(poiss.inx, poiss.inx))
# Laplace matrix for return
    nnz = lap.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(lap.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = lap.csrValues_h[i]
        indices[i] = lap.csrColIdx_h[i]
    for i in range(lap.inx+1):
        indptr[i] = lap.csrRowPtr_h[i]
    lap_py = csr_matrix((data, indices, indptr), shape=(lap.inx, lap.inx))
# Divergence matrix in X for return
    nnz = div_x.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_x.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_x.csrValues_h[i]
        indices[i] = div_x.csrColIdx_h[i]
    for i in range(div_x.inx+1):
        indptr[i] = div_x.csrRowPtr_h[i]
    div_x_py = csr_matrix((data, indices, indptr), shape=(div_x.inx, div_x.inx))
# Divergence matrix in Y for return
    nnz = div_y.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_y.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_y.csrValues_h[i]
        indices[i] = div_y.csrColIdx_h[i]
    for i in range(div_y.inx+1):
        indptr[i] = div_y.csrRowPtr_h[i]
    div_y_py = csr_matrix((data, indices, indptr), shape=(div_y.inx, div_y.inx))
    div_z = csr_matrix((nx, nx))  # empty matrix

# Gradient matrix in X for return
    nnz = grad_x.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_x.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_x.csrValues_h[i]
        indices[i] = grad_x.csrColIdx_h[i]
    for i in range(grad_x.inx+1):
        indptr[i] = grad_x.csrRowPtr_h[i]
    grad_x_py = csr_matrix((data, indices, indptr), shape=(grad_x.inx, grad_x.inx))
# Gradient matrix in Y for return
    nnz = grad_y.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_y.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_y.csrValues_h[i]
        indices[i] = grad_y.csrColIdx_h[i]
    for i in range(grad_y.inx+1):
        indptr[i] = grad_y.csrRowPtr_h[i]
    grad_y_py = csr_matrix((data, indices, indptr), shape=(grad_y.inx, grad_y.inx))
    grad_z = csr_matrix((nx, nx))  # empty matrix

#    bdmi_x = csr_matrix((nx, nx))  # empty matrix
# Returning all
    return bnlist_ret, nv_ret, area, area, pang_ret, belm_ret, -poiss_py, -lap_py, -div_x_py, -div_y_py, div_z, \
            -grad_x_py, -grad_y_py, grad_z
#, bdmi_x, bdmi_x, bdmi_x

def get_matrices2D_radial(np.ndarray xyz, np.ndarray mesh, np.ndarray bmesh):
    nx = xyz.shape[0]
    nelx = mesh.shape[0]
    cdef CSRSprMat poiss
    cdef CSRSprMat lap
    cdef CSRSprMat grad_poisson
    cdef CSRSprMat grad_laplace
    cdef CSRSprMat div_x
    cdef CSRSprMat div_z
    cdef CSRSprMat grad_x
    cdef CSRSprMat grad_z
# Boundary list
    BNList = np.ascontiguousarray(np.zeros(nx, dtype=np.int32))
    cdef int[:] bnlist_memview = BNList
    cdef int* bnlist = &bnlist_memview[0]
# normal vectors
    normal_vector = np.ascontiguousarray(np.zeros(2*nx, dtype=np.double))
    cdef double[:] normal_vector_memview = normal_vector
    cdef double* normal_vector_carray = &normal_vector_memview[0]
# volume of elements - Cartesian volume
    cartesian_volume = np.ascontiguousarray(np.zeros(nx, dtype=np.double))
    cdef double[:] cartesian_volume_memview = cartesian_volume
    cdef double* cartesian_volume_carray = &cartesian_volume_memview[0]
# solid angles of boundaries
    solid_angle = np.ascontiguousarray(np.zeros(nx, dtype=np.double))
    cdef double[:] solid_angle_memview = solid_angle
    cdef double* solid_angle_carray = &solid_angle_memview[0]
# preparing belm_carray
    belm = np.ascontiguousarray(np.zeros(2*nx, dtype=np.int32))
    cdef int[::1] belm_memview = belm
    cdef int* belm_carray = &belm_memview[0]
    cdef int bnx
    dim = 2
# preparing xyz
    rho = np.zeros(nx, dtype=np.double)
    xyz_r = xyz.flatten()
    for i in range(nx):
#        tmp = xyz_r[3*i+1]
#        xyz_r[3*i+1] = xyz_r[3*i+2]
#        xyz_r[3*i+2] = tmp
        rho[i] = xyz_r[3*i]
    xyz_r = ensure_contiguous(xyz_r)
    cdef double[::1] xyz_r_memview = xyz_r
    cdef double* xyz_r_carray = &xyz_r_memview[0]
# preparing mesh_carray - connectivities
    mesh = mesh.astype(np.int32)
    mesh = mesh.flatten()
    mesh = ensure_contiguous(mesh)
    cdef int[::1] mesh_memview = mesh
    cdef int* mesh_carray = &mesh_memview[0]
    err = OperatorsFromMesh2D_radial(
        xyz_r_carray,
        mesh_carray,
        nx,
        nelx,
        dim,
        bnlist,
        &bnx,
        normal_vector_carray,
        cartesian_volume_carray,
        &poiss,
        &grad_poisson,
        &lap,
        &grad_laplace,
        &div_x,
        &div_z,
        &grad_x,
        &grad_z,
        solid_angle_carray,
        belm_carray
        )
#    print("The C has done it's job!")
    bnlist_ret = BNList[0:bnx]
    normal_vector_ret = normal_vector[0:2*bnx]
    solid_angle_ret = solid_angle[0:bnx]
    belm_ret = belm[0:2*bnx]
# Poisson matrix for return
# stiffness part
    nnz = poiss.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(poiss.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = poiss.csrValues_h[i]
        indices[i] = poiss.csrColIdx_h[i]
    for i in range(poiss.inx+1):
        indptr[i] = poiss.csrRowPtr_h[i]
# gradient part
    nnz = grad_poisson.nnz
    data_grad = np.zeros(nnz, dtype=np.double)
    indices_grad  = np.zeros(nnz, dtype=np.int32)
    indptr_grad  = np.zeros(grad_poisson.inx+1, dtype=np.int32)
    for i in range(nnz):
        data_grad[i] = grad_poisson.csrValues_h[i]
        indices_grad[i] = grad_poisson.csrColIdx_h[i]
    for i in range(grad_poisson.inx+1):
        indptr_grad[i] = grad_poisson.csrRowPtr_h[i]
    poiss_py = csr_matrix((data, indices, indptr), shape=(poiss.inx, poiss.inx)) + \
               csr_matrix((data_grad, indices_grad, indptr_grad), shape=(grad_poisson.inx, grad_poisson.inx))
#    poiss_py = csr_matrix((data, indices, indptr), shape=(poiss.inx, poiss.inx))
# Laplace matrix for return
# stiffness part
    nnz = lap.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(lap.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = lap.csrValues_h[i]
        indices[i] = lap.csrColIdx_h[i]
    for i in range(lap.inx+1):
        indptr[i] = lap.csrRowPtr_h[i]
# gradient part
    nnz = grad_laplace.nnz
    data_grad = np.zeros(nnz, dtype=np.double)
    indices_grad  = np.zeros(nnz, dtype=np.int32)
    indptr_grad  = np.zeros(grad_laplace.inx+1, dtype=np.int32)
    for i in range(nnz):
        data_grad[i] = grad_laplace.csrValues_h[i]
        indices_grad[i] = grad_laplace.csrColIdx_h[i]
    for i in range(grad_laplace.inx+1):
        indptr_grad[i] = grad_laplace.csrRowPtr_h[i]
    lap_py = csr_matrix((data, indices, indptr), shape=(lap.inx, lap.inx)) + \
             csr_matrix((data_grad, indices_grad, indptr_grad), shape=(grad_laplace.inx, grad_laplace.inx))
#    lap_py = csr_matrix((data, indices, indptr), shape=(lap.inx, lap.inx))
# Divergence matrix in X for return
    nnz = div_x.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_x.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_x.csrValues_h[i]
        indices[i] = div_x.csrColIdx_h[i]
    for i in range(div_x.inx+1):
        indptr[i] = div_x.csrRowPtr_h[i]
    div_x_py = csr_matrix((data, indices, indptr), shape=(div_x.inx, div_x.inx))
# Divergence matrix in Z for return
    nnz = div_z.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_z.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_z.csrValues_h[i]
        indices[i] = div_z.csrColIdx_h[i]
    for i in range(div_z.inx+1):
        indptr[i] = div_z.csrRowPtr_h[i]
    div_z_py = csr_matrix((data, indices, indptr), shape=(div_z.inx, div_z.inx))
    div_y = csr_matrix((nx, nx))  # empty matrix

# Gradient matrix in X for return
    nnz = grad_x.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_x.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_x.csrValues_h[i]
        indices[i] = grad_x.csrColIdx_h[i]
    for i in range(grad_x.inx+1):
        indptr[i] = grad_x.csrRowPtr_h[i]
    grad_x_py = csr_matrix((data, indices, indptr), shape=(grad_x.inx, grad_x.inx))
# Gradient matrix in Z for return
    nnz = grad_z.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_z.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_z.csrValues_h[i]
        indices[i] = grad_z.csrColIdx_h[i]
    for i in range(grad_z.inx+1):
        indptr[i] = grad_z.csrRowPtr_h[i]
    grad_z_py = csr_matrix((data, indices, indptr), shape=(grad_z.inx, grad_z.inx))
    grad_y = csr_matrix((nx, nx))  # empty matrix

    #volume = np.zeros(nx, dtype=np.double)
    #for i in range(nx):
    #    volume[i] = cartesian_volume[i] * xyz[3*i]
    volume = cartesian_volume * rho

#    bdmi_x = csr_matrix((nx, nx))  # empty matrix
# Returning all
    return bnlist_ret, normal_vector_ret, volume, cartesian_volume, solid_angle_ret, belm_ret, \
           -poiss_py, -lap_py, -div_x_py, div_y, -div_z_py, -grad_x_py, grad_y, -grad_z_py

# Computation of the 2D exchange Laplacian including the node dependent exchange stiffness
def ExchangeOperator2D(np.ndarray xyz, np.ndarray mesh, np.ndarray bmesh, np.ndarray Aex, geometry_type):
    nx = xyz.shape[0]
    nelx = mesh.shape[0]
    dim = 2
    cdef CSRSprMat xc
    cdef CSRSprMat xc_grad
# preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
# preparing mesh_carray - connectivities
    mesh = mesh.astype(np.int32)
    mesh = mesh.flatten()
    mesh = ensure_contiguous(mesh)
    cdef int[::1] mesh_memview = mesh
    cdef int* mesh_carray = &mesh_memview[0]
# preparing Aex
    Aex = Aex.flatten()
    Aex = ensure_contiguous(Aex)
    cdef double[::1] Aex_memview = Aex
    cdef double* Aex_carray = &Aex_memview[0]
    radial_flag = 0
    if geometry_type == GeometryType.WAVEGUIDE:
        radial_flag = 1
    err = ExchangeOperatorFromMesh2D(
        xyz_carray,
        mesh_carray,
        nx,
        nelx,
        dim,
        Aex_carray,
        &xc,
        &xc_grad,
        radial_flag
        )
#    print("The C has done it's job!")
# Exchange matrix for return
    nnz = xc.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(xc.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = xc.csrValues_h[i]
        indices[i] = xc.csrColIdx_h[i]
    for i in range(xc.inx+1):
        indptr[i] = xc.csrRowPtr_h[i]
    xc_py = csr_matrix((data, indices, indptr), shape=(xc.inx, xc.inx))

# Returning all
    if geometry_type == GeometryType.WAVEGUIDE:
        return -xc_py
    elif geometry_type == GeometryType.CONFINED_AXIAL:
        nnz = xc_grad.nnz
        data = np.zeros(nnz, dtype=np.double)
        indices  = np.zeros(nnz, dtype=np.int32)
        indptr  = np.zeros(xc_grad.inx+1, dtype=np.int32)
        for i in range(nnz):
            data[i] = xc_grad.csrValues_h[i]
            indices[i] = xc_grad.csrColIdx_h[i]
        for i in range(xc_grad.inx+1):
            indptr[i] = xc_grad.csrRowPtr_h[i]
        xc_grad_py = csr_matrix((data, indices, indptr), shape=(xc_grad.inx, xc_grad.inx))
# the one over rho**2 part
        rho = xyz[::3]
        per_rho_squared = bmat([[diags(Aex / rho ** 2), diags(np.zeros(nx)), diags(np.zeros(nx))],
                                [diags(np.zeros(nx)), diags(Aex / rho ** 2), diags(np.zeros(nx))],
                                [diags(np.zeros(nx)), diags(np.zeros(nx)), diags(np.zeros(nx))]])

        return -xc_py - xc_grad_py - per_rho_squared
    else:
        print("Wrong geometry type was supplied!")

def ComputeDenseMatrix_2D(np.ndarray D2Dk, np.ndarray belm, np.ndarray boundary_nodes,
                               np.ndarray xyz, np.ndarray nv, np.ndarray solid_angle,
                               int op, double k, int m):
    bnelx = belm.shape[0] // 2
    bnx = boundary_nodes.shape[0]
    nx = xyz.shape[0]
#    print(bnelx,nv.shape[0],nv.shape[1])
#    exit()
    if nv.shape[0] != 2*bnelx:
        raise ValueError("Malformed nv or belm!")
    if solid_angle.shape[0] != bnx:
        raise ValueError("Malformed pang or boundary_nodes!")
    # preparing belm_carray
    belm = ensure_contiguous(belm)
    cdef int[::1] belm_memview = belm
    cdef int* belm_carray = &belm_memview[0]
    # preparing bnlist_carray
    boundary_nodes = ensure_contiguous(boundary_nodes)
    cdef int[::1] bnlist_memview = boundary_nodes
    cdef int* bnlist_carray = &bnlist_memview[0]
    # preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
    # preparing nv
    nv = ensure_contiguous(nv)
    cdef double[::1] nv_memview = nv
    cdef double* nv_carray = &nv_memview[0]
    # preparing pang
    solid_angle = ensure_contiguous(solid_angle)
    cdef double[::1] pang_memview = solid_angle
    cdef double* pang_carray = &pang_memview[0]
    # preparing D2Dk (empty ndarray)
#    D2Dk = np.empty((bnx*bnx,))
    D2Dk = ensure_contiguous(D2Dk)
    cdef double[::1] D2Dk_memview = D2Dk
    cdef double* D2Dk_carray = &D2Dk_memview[0]
    ComputeDenseMatrix_kdep_py(
        nx,
        belm_carray,
        bnx,
        bnelx,
        bnlist_carray,
        xyz_carray,
        nv_carray,
        pang_carray,
        op,
        k,
        m,
        D2Dk_carray
    )
    return 0

def ComputeDenseMatrix_2D_radial(np.ndarray D2Dk, np.ndarray belm, np.ndarray boundary_nodes,
                               np.ndarray xyz, np.ndarray nv, np.ndarray solid_angle,
                               int op, double k, int m):
    bnelx = belm.shape[0] // 2
    bnx = boundary_nodes.shape[0]
    nx = xyz.shape[0]
#    print(bnelx,nv.shape[0],nv.shape[1])
#    exit()
    if nv.shape[0] != 2*bnelx:
        raise ValueError("Malformed nv or belm!")
    if solid_angle.shape[0] != bnx:
        raise ValueError("Malformed pang or boundary_nodes!")
    # preparing belm_carray
    belm = ensure_contiguous(belm)
    cdef int[::1] belm_memview = belm
    cdef int* belm_carray = &belm_memview[0]
    # preparing bnlist_carray
    boundary_nodes = ensure_contiguous(boundary_nodes)
    cdef int[::1] bnlist_memview = boundary_nodes
    cdef int* bnlist_carray = &bnlist_memview[0]
    # preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
    # preparing nv
    nv = ensure_contiguous(nv)
    cdef double[::1] nv_memview = nv
    cdef double* nv_carray = &nv_memview[0]
    # preparing pang
    solid_angle = ensure_contiguous(solid_angle)
    cdef double[::1] pang_memview = solid_angle
    cdef double* pang_carray = &pang_memview[0]
    # preparing D2Dk (empty ndarray)
#    D2Dk = np.empty((bnx*bnx,))
    D2Dk = ensure_contiguous(D2Dk)
    cdef double[::1] D2Dk_memview = D2Dk
    cdef double* D2Dk_carray = &D2Dk_memview[0]
    ComputeDenseMatrix_kdep_radial(
        nx,
        belm_carray,
        bnx,
        bnelx,
        bnlist_carray,
        xyz_carray,
        nv_carray,
        pang_carray,
        op,
        k,
        m,
        D2Dk_carray
    )
    return 0

def rotation_matrix_py(np.ndarray mag):
    cdef CSRSprMat R
    nx = mag.shape[0] // 3
    cdef double *m0_array[3];
    mag = ensure_contiguous(mag)
    cdef double[::1] mag_memview = mag
    m0_array[0] = &mag_memview[0]
    m0_array[1] = &mag_memview[nx]
    m0_array[2] = &mag_memview[2*nx]
    R = rotation_matrix(
            m0_array,
            nx
        )
    nnz = R.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(R.inx+1, dtype=np.int32)
    cdef int i
    for i in range(nnz):
        data[i] = R.csrValues_h[i]
        indices[i] = R.csrColIdx_h[i]
    for i in range(R.inx+1):
        indptr[i] = R.csrRowPtr_h[i]
    return csr_matrix((data, indices, indptr), shape=(R.inx, R.inx))

def get_matrices3D(np.ndarray xyz, np.ndarray mesh, np.ndarray bmesh):
#    print("Reached get matrices 3D")
    dim = 3
    nx = xyz.shape[0]
    nelx = mesh.shape[0]
    cdef CSRSprMat poiss
    cdef CSRSprMat lap
    cdef CSRSprMat div_x
    cdef CSRSprMat div_y
    cdef CSRSprMat div_z
    cdef CSRSprMat grad_x
    cdef CSRSprMat grad_y
    cdef CSRSprMat grad_z
# Boundary list
    BNList = np.ascontiguousarray(np.zeros(nx, dtype=np.int32))
    cdef int[:] bnlist_memview = BNList
    cdef int* bnlist = &bnlist_memview[0]
# normal vectors
    nv = np.ascontiguousarray(np.zeros(dim*nelx, dtype=np.double))
    cdef double[:] nv_memview = nv
    cdef double* nv_carray = &nv_memview[0]
# area of elements
    nvolumes = np.ascontiguousarray(np.zeros(nx, dtype=np.double))
    cdef double[:] nvolumes_memview = nvolumes
    cdef double* nvolumes_carray = &nvolumes_memview[0]
# area of elements
    solid_angle = np.ascontiguousarray(np.zeros(nx, dtype=np.double))
    cdef double[:] pang_memview = solid_angle
    cdef double* pang_carray = &pang_memview[0]
# preparing belm_carray
    belm = np.ascontiguousarray(np.zeros(dim*nelx, dtype=np.int32))
    cdef int[::1] belm_memview = belm
    cdef int* belm_carray = &belm_memview[0]
    cdef int bnx
    cdef int bnelx
# preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
# preparing mesh_carray - connectivities
    mesh = mesh.astype(np.int32)
    mesh = mesh.flatten()
    mesh = ensure_contiguous(mesh)
    cdef int[::1] mesh_memview = mesh
    cdef int* mesh_carray = &mesh_memview[0]

    err = OperatorsFromMesh3D(
        xyz_carray,
        mesh_carray,
        nx,
        nelx,
        dim,
        bnlist,
        &bnx,
        &bnelx,
        nv_carray,
        nvolumes_carray,
        &poiss,
        &lap,
        &div_x,
        &div_y,
        &div_z,
        &grad_x,
        &grad_y,
        &grad_z,
        pang_carray,
        belm_carray
        )

    bnlist_ret = BNList[0:bnx]
    nv_ret = nv[0:dim*bnelx]
    solid_angle_ret = solid_angle[0:bnx]
    belm_ret = belm[0:dim*bnelx]
# Poisson matrix for return
    nnz = poiss.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(poiss.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = poiss.csrValues_h[i]
        indices[i] = poiss.csrColIdx_h[i]
    for i in range(poiss.inx+1):
        indptr[i] = poiss.csrRowPtr_h[i]
    poiss_py = csr_matrix((data, indices, indptr), shape=(poiss.inx, poiss.inx))
# Laplace matrix for return
    nnz = lap.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(lap.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = lap.csrValues_h[i]
        indices[i] = lap.csrColIdx_h[i]
    for i in range(lap.inx+1):
        indptr[i] = lap.csrRowPtr_h[i]
    lap_py = csr_matrix((data, indices, indptr), shape=(lap.inx, lap.inx))
# Divergence matrix in X for return
    nnz = div_x.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_x.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_x.csrValues_h[i]
        indices[i] = div_x.csrColIdx_h[i]
    for i in range(div_x.inx+1):
        indptr[i] = div_x.csrRowPtr_h[i]
    div_x_py = csr_matrix((data, indices, indptr), shape=(div_x.inx, div_x.inx))
# Divergence matrix in Y for return
    nnz = div_y.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_y.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_y.csrValues_h[i]
        indices[i] = div_y.csrColIdx_h[i]
    for i in range(div_y.inx+1):
        indptr[i] = div_y.csrRowPtr_h[i]
    div_y_py = csr_matrix((data, indices, indptr), shape=(div_y.inx, div_y.inx))
# Divergence matrix in Z for return
    nnz = div_z.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(div_z.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = div_z.csrValues_h[i]
        indices[i] = div_z.csrColIdx_h[i]
    for i in range(div_z.inx+1):
        indptr[i] = div_z.csrRowPtr_h[i]
    div_z_py = csr_matrix((data, indices, indptr), shape=(div_z.inx, div_z.inx))

# Gradient matrix in X for return
    nnz = grad_x.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_x.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_x.csrValues_h[i]
        indices[i] = grad_x.csrColIdx_h[i]
    for i in range(grad_x.inx+1):
        indptr[i] = grad_x.csrRowPtr_h[i]
    grad_x_py = csr_matrix((data, indices, indptr), shape=(grad_x.inx, grad_x.inx))
# Gradient matrix in Y for return
    nnz = grad_y.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_y.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_y.csrValues_h[i]
        indices[i] = grad_y.csrColIdx_h[i]
    for i in range(grad_y.inx+1):
        indptr[i] = grad_y.csrRowPtr_h[i]
    grad_y_py = csr_matrix((data, indices, indptr), shape=(grad_y.inx, grad_y.inx))
# Gradient matrix in Z for return
    nnz = grad_z.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(grad_z.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = grad_z.csrValues_h[i]
        indices[i] = grad_z.csrColIdx_h[i]
    for i in range(grad_z.inx+1):
        indptr[i] = grad_z.csrRowPtr_h[i]
    grad_z_py = csr_matrix((data, indices, indptr), shape=(grad_z.inx, grad_z.inx))

# Returning all
    return bnlist_ret, nv_ret, nvolumes, nvolumes, solid_angle_ret, belm_ret, -poiss_py, -lap_py, -div_x_py, -div_y_py, -div_z_py, \
            -grad_x_py, -grad_y_py, -grad_z_py

# Computation of the 3D exchange Laplacian including the node dependent exchange stiffness
def ExchangeOperator3D(np.ndarray xyz, np.ndarray mesh, np.ndarray bmesh, np.ndarray Aex, not_radial):
    nx = xyz.shape[0]
    nelx = mesh.shape[0]
    dim = 3
    cdef CSRSprMat xc
# preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
# preparing mesh_carray - connectivities
    mesh = mesh.astype(np.int32)
    mesh = mesh.flatten()
    mesh = ensure_contiguous(mesh)
    cdef int[::1] mesh_memview = mesh
    cdef int* mesh_carray = &mesh_memview[0]
# preparing Aex
    Aex = Aex.flatten()
    Aex = ensure_contiguous(Aex)
    cdef double[::1] Aex_memview = Aex
    cdef double* Aex_carray = &Aex_memview[0]

    err = ExchangeOperatorFromMesh3D(
        xyz_carray,
        mesh_carray,
        nx,
        nelx,
        dim,
        Aex_carray,
        &xc
        )
#    print("The C has done it's job!\n")
# Exchange matrix for return
    nnz = xc.nnz
    data = np.zeros(nnz, dtype=np.double)
    indices  = np.zeros(nnz, dtype=np.int32)
    indptr  = np.zeros(xc.inx+1, dtype=np.int32)
    for i in range(nnz):
        data[i] = xc.csrValues_h[i]
        indices[i] = xc.csrColIdx_h[i]
    for i in range(xc.inx+1):
        indptr[i] = xc.csrRowPtr_h[i]
    xc_py = csr_matrix((data, indices, indptr), shape=(xc.inx, xc.inx))
# Returning the CSR matrix
    return -xc_py

# Computation of the 3D dense matrix

def ComputeDenseMatrix_3D(np.ndarray D3D, np.ndarray belm, np.ndarray boundary_nodes,
                               np.ndarray xyz, np.ndarray nv, np.ndarray solid_angle,
                               int op, double k, int m):
    bnelx = belm.shape[0] // 3
    bnx = boundary_nodes.shape[0]
    nx = xyz.shape[0]
    opl = 2
#    print(bnelx,nv.shape[0],nv.shape[1])
#    exit()
    if nv.shape[0] != 3*bnelx:
        raise ValueError("Malformed nv or belm!")
    if solid_angle.shape[0] != bnx:
        raise ValueError("Malformed pang or boundary_nodes!")
    # preparing belm_carray
    belm = ensure_contiguous(belm)
    cdef int[::1] belm_memview = belm
    cdef int* belm_carray = &belm_memview[0]
    # preparing bnlist_carray
    boundary_nodes = ensure_contiguous(boundary_nodes)
    cdef int[::1] bnlist_memview = boundary_nodes
    cdef int* bnlist_carray = &bnlist_memview[0]
    # preparing xyz
    xyz = xyz.flatten()
    xyz = ensure_contiguous(xyz)
    cdef double[::1] xyz_memview = xyz
    cdef double* xyz_carray = &xyz_memview[0]
    # preparing nv
    nv = ensure_contiguous(nv)
    cdef double[::1] nv_memview = nv
    cdef double* nv_carray = &nv_memview[0]
    # preparing solid_angle / pang
    solid_angle = ensure_contiguous(solid_angle)
    cdef double[::1] pang_memview = solid_angle
    cdef double* pang_carray = &pang_memview[0]
    # preparing D3D (empty ndarray)
#    D3D = np.empty((bnx*bnx,))
    D3D = ensure_contiguous(D3D)
    cdef double[::1] D3D_memview = D3D
    cdef double* D3D_carray = &D3D_memview[0]
    ComputeDenseMatrix3D(
        nx,
        belm_carray,
        bnx,
        bnelx,
        bnlist_carray,
        xyz_carray,
        nv_carray,
        pang_carray,
        opl,
        D3D_carray
    )
    return 0

