//
// Created by Attila Kákay on 10.12.20.
//

#ifndef OPMATSPM_H
#define OPMATSPM_H

#include "akmag.h"

/*----------------------------------------------------------------------*/
int ContainNodes(int n1, int n2, AKElm *elm, int el, int *i1, int *i2);
/*----------------------------------------------------------------------*/
int CalcLofSpMatExch(AKElm *elm);
/*----------------------------------------------------------------------*/
void CalcSpMatExch(AKElm *elm, RISSprMat *spm, RISSprMat *spmt);
/*----------------------------------------------------------------------*/
int CalcLofSpMatDiv(AKElm *elm, int comp);
/*----------------------------------------------------------------------*/
void CalcSpMatDiv(AKElm *elm, RISSprMat *spm, int comp);
/*----------------------------------------------------------------------*/
int CalcLofSpMatPoisson(AKElm *elm);
/*----------------------------------------------------------------------*/
void CalcSpMatPoisson(AKElm *elm, RISSprMat *spm);
/*----------------------------------------------------------------------*/
int CalcLofSpMatLaplace(AKElm *elm);
/*----------------------------------------------------------------------*/
void CalcSpMatLaplace(AKElm *elm, RISSprMat *spm, RISSprMat *spmp);
/*----------------------------------------------------------------------*/
int CalcLofSpMatGrad(AKElm *elm, int comp);
/*----------------------------------------------------------------------*/
void CalcSpMatGrad(AKElm *elm, RISSprMat *spm, int comp);
/*----------------------------------------------------------------------*/
int CalcLofSpMatGrad_rho(AKElm *elm, int comp, int Dirichlet);
/*----------------------------------------------------------------------*/
void CalcSpMatGrad_rho(AKElm *elm, RISSprMat *spm, int comp, int Dirichlet);
/*----------------------------------------------------------------------*/
int CalcLofSpMatGrad_rad_exc(AKElm *elm, int comp);
/*----------------------------------------------------------------------*/
void CalcSpMatGrad_rad_exc(AKElm *elm, RISSprMat *spm, int comp);
/*----------------------------------------------------------------------*/
void CalcSpMatDMIbulk(AKElm *elm, RISSprMat *spm, int comp);
/*----------------------------------------------------------------------*/
int CalcLofSpMatDMIbulk(AKElm *elm, int comp);
/*----------------------------------------------------------------------*/
double sparsemat_at(RISSprMat x, int i, int j);
/*----------------------------------------------------------------------*/
RISSprMat rotation_operator(RISSprMat phix, RISSprMat phiy, RISSprMat phiz);
/*----------------------------------------------------------------------*/

#endif
