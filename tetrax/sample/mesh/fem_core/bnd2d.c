//
// Created by Attila Kákay on 20.07.20.
//

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include "bnd2d.h"
#include "akmag.h"
#include "matutils.h"
#include "math.h"
#include "utils.h"

#define SWAP_BND(a,b) itemp=(a);(a)=(b);(b)=itemp;

/*----------------------------------------------------------------------*/
int MakeEdges(AKElm *elm, AKElm *faces)
{
  int i, fc_count, nelx;
  nelx = elm->nelx;
  fc_count = 0;
  for(i=0;i<nelx;i++) {
      faces->ijk[0][fc_count] = elm->ijk[0][i];
      faces->ijk[1][fc_count] = elm->ijk[1][i];
      faces->ijk[2][fc_count] = i;
      fc_count+=1;
      faces->ijk[0][fc_count] = elm->ijk[1][i];
      faces->ijk[1][fc_count] = elm->ijk[2][i];
      faces->ijk[2][fc_count] = i;
      fc_count+=1;
      faces->ijk[0][fc_count] = elm->ijk[0][i];
      faces->ijk[1][fc_count] = elm->ijk[2][i];
      faces->ijk[2][fc_count] = i;
      fc_count+=1;
  }
  return(fc_count);
}
/*----------------------------------------------------------------------*/
int MakeFaces(AKElm *elm, AKElm *faces)
{
    int i, fc_count, nelx;
    nelx = elm->nelx;
    fc_count = 0;
    for(i=0;i<nelx;i++) {
        faces->ijk[0][fc_count] = elm->ijk[0][i];
        faces->ijk[1][fc_count] = elm->ijk[1][i];
        faces->ijk[2][fc_count] = elm->ijk[2][i];
        faces->ijk[3][fc_count] = i;
        fc_count+=1;
        faces->ijk[0][fc_count] = elm->ijk[1][i];
        faces->ijk[1][fc_count] = elm->ijk[2][i];
        faces->ijk[2][fc_count] = elm->ijk[3][i];
        faces->ijk[3][fc_count] = i;
        fc_count+=1;
        faces->ijk[0][fc_count] = elm->ijk[0][i];
        faces->ijk[1][fc_count] = elm->ijk[1][i];
        faces->ijk[2][fc_count] = elm->ijk[3][i];
        faces->ijk[3][fc_count] = i;
        fc_count+=1;
        faces->ijk[0][fc_count] = elm->ijk[0][i];
        faces->ijk[1][fc_count] = elm->ijk[2][i];
        faces->ijk[2][fc_count] = elm->ijk[3][i];
        faces->ijk[3][fc_count] = i;
        fc_count+=1;
    }
    return(fc_count);
}
/*----------------------------------------------------------------------*/
void SortEdgeList(AKElm *faces, int nof)
{
  int i,j;
  int l_min, l_mid, l_max;

  for(i=0;i<nof;i++) {
    l_min = IMIN(faces->ijk[0][i],faces->ijk[1][i]);
    l_max = IMAX(faces->ijk[0][i],faces->ijk[1][i]);
    faces->ijk[0][i] = l_min;
    faces->ijk[1][i] = l_max;
  }
}
/*----------------------------------------------------------------------*/
void SortFaceList(AKElm *faces, int nof)
{
    int i,j;
    int l_min, l_mid, l_max;
    int f_copy[4];

    for(i=0;i<nof;i++) {
        for(j=0;j<4;j++) {
            f_copy[j] = faces->ijk[j][i];
        }
        l_min = IMIN(f_copy[0],f_copy[1]);
        l_min = IMIN(l_min,f_copy[2]);
        l_max = IMAX(f_copy[0],f_copy[1]);
        l_max = IMAX(l_max,f_copy[2]);
        f_copy[0] = l_min;
        f_copy[2] = l_max;
        for(j=0;j<3;j++) {
            if ((faces->ijk[j][i] != l_min) && (faces->ijk[j][i] != l_max)) {
                f_copy[1] = faces->ijk[j][i];
            }
        }
        for(j=0;j<4;j++) {
            faces->ijk[j][i] = f_copy[j];
        }
    }
}
/*----------------------------------------------------------------------*/
void CalcSortnum(AKElm faces, long long *sortnum, int nof, int dim)
{
  int i;
  for(i=0;i<nof;i++) {
    sortnum[i] = 0;
    sortnum[i] += (long long)faces.ijk[0][i];
    sortnum[i] += (long long)faces.ijk[1][i]<<20;
    if(dim > 2)  sortnum[i] += (long long)faces.ijk[2][i]<<40;
  }
}
/*----------------------------------------------------------------------*/
void Count_bnelx(AKElm faces, int *bnelx, int nof, int *list, int dim)
/*
  This subroutine counts the number of boundary elements (bnelx).
*/
{
  int belem=0, i, j;
  int *last, *next;
  int flag=0;

  last = (int *) malloc((size_t) dim*sizeof(int));
  next = (int *) malloc((size_t) dim*sizeof(int));

  belem=0;
  j = 0;
  for(;;) {
    flag = 0;
    for(i=0;i<dim;i++) {
      last[i] = faces.ijk[i][list[j]];
      next[i] = faces.ijk[i][list[j+1]];
    }

    for(i=0;i<dim;i++)
	    if (next[i] != last[i]) flag = 1;

    if (flag == 1) {
      j+=1;
      belem++;
      if (j==nof-1) {
	    belem++;
	    *bnelx = belem;
        free(last);
        free(next);
	    return;
      }
    }
    if (flag == 0) {
      j += 2;
      if (j == nof-1) {
	    belem++;
	    *bnelx = belem;
        free(last);
        free(next);
	    return;
      }
      if (j > nof-1) {
	    *bnelx = belem;
	    free(last);
        free(next);
	    return;
      }
    }
  }
}
/*----------------------------------------------------------------------*/
void FindBELM(AKElm faces, int bnelx, int nof, int *list, AKElm *belm,
	 int dim)
/*
  This subroutine finds the boundary elements: belm.ijk[dim][bnelx].
*/
{
  int belem=0, i, j,k, l;
  int *last, *next;
  int flag=0;
  last = (int *) malloc ((size_t) (dim+1)*sizeof(int));
  next = (int *) malloc ((size_t) (dim+1)*sizeof(int));
  belem=0;
  j = 0;
  for(;;) {
    flag = 0;
    for(i=0;i<dim;i++) {
      last[i] = faces.ijk[i][list[j]];
      next[i] = faces.ijk[i][list[j+1]];
    }

    for(i=0;i<dim;i++)
	    if (next[i] != last[i]) flag = 1;

    if (flag == 1) {
      for(l=0;l<dim+1;l++)
    	belm->ijk[l][belem] = faces.ijk[l][list[j]];
      belem++;
      j++;
      if (j==nof-1) {
	    for(l=0;l<dim+1;l++)
	        belm->ijk[l][belem] = faces.ijk[l][list[j]];
	    free(last);
	    free(next);
	    return;
      }
    }

    if (flag == 0) {
      j+=2;
      if (j==nof-1) {
	    for(l=0;l<dim+1;l++)
	        belm->ijk[l][belem] = faces.ijk[l][list[j]];
        free(last);
        free(next);
    	return;
      }
      if (j > nof-1) {
        free(last);
        free(next);
    	return;
      }
    }
  }
}
/*----------------------------------------------------------------------*/
void CountBNXandMakeList(AKElm *elm, AKElm *belm)
{
  int nx, dim, eld, i, j, bncount=0, node, bnelx;
  int *bnflag;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  bnelx = belm->nelx;

  bnflag = (int *) malloc((size_t) nx*sizeof(int));
  if(!bnflag) memerror("CountBoundaryNodes (bnflag)");

  IniIVector(bnflag, nx, 0);

  for(i=0;i<bnelx;i++) {
    for(j=0;j<dim;j++) {
      node = belm->ijk[j][i];
      if(!bnflag[node]) {
	       bnflag[node] = 1;
	       bncount++;
      }
    }
  }
  elm->bnx = bncount;
  belm->nx = bncount;
  elm->bnlist = (int *) malloc((size_t) bncount*sizeof(int));

  IniIVector(bnflag, nx, 0);
  bncount = 0;
  for(i=0;i<bnelx;i++) {
    for(j=0;j<dim;j++) {
      node = belm->ijk[j][i];
      if(!bnflag[node]) {
         bnflag[node] = 1;
         elm->bnlist[bncount] = node;
         bncount++;
      }
    }
  }

  free(bnflag);
}
/* END void CountBNXandMakeList(AKElm *elm, AKElm *belm) */
/*----------------------------------------------------------------------*/
void MakeNormalVectors2D(AKElm *elm, AKElm *belm)
{
    int i,j,k;
    int bnelx, dim, eld;
    int n1, n2;
    double v1[2], v2[2], v3[2], v4[2], side;
    double P1[2], P2[2], P3[2];

    /*
     * We know the coordinates of the two points P1, P2
     * We also know the third point (volumic node) coordinates,
     * however we don't know the proper order.
     * Therefore I calculate a P3 as P3(1/3*sum(x_i),1/3*sum(y_i))
     */

    bnelx = elm->bnelx;
    dim = elm->dim;
    eld = elm->eld;

    elm->nv = (double *) malloc((size_t) dim*bnelx*sizeof(double));
    if(!elm->nv) memerror("nv (normal vectors of the boundary elements)");
    elm->area = (double *) malloc((size_t) bnelx*sizeof(double));
    if(!elm->area) memerror("area (area of boundary elements)");

    for(i=0; i<bnelx; i++) {
        n1 = belm->ijk[0][i];
        n2 = belm->ijk[1][i];
//        printf("nodes:%d\t%d\n",belm->ijk[0][i],belm->ijk[1][i]);
        P1[0] = elm->xyz[3*belm->ijk[0][i]];
        P1[1] = elm->xyz[3*belm->ijk[0][i]+1];
        P2[0] = elm->xyz[3*belm->ijk[1][i]];
        P2[1] = elm->xyz[3*belm->ijk[1][i]+1];
    // first we calculate the difference vector v1
        v1[0] = P1[0] - P2[0];
        v1[1] = P1[1] - P2[1];
        elm->area[i] = vectabs2D(v1);
    // then we rotate with 90 degree and obtain v2
        v2[0] = -1*v1[1];
        v2[1] = v1[0];
        P3[0] = 0.0;
        P3[1] = 0.0;
    // the third point is inside the triangular element P3 (see above)
        for(k=0; k<eld; k++) {
            P3[0] += (elm->xyz[3*elm->ijk[k][belm->ijk[2][i]]]/3.0);
            P3[1] += (elm->xyz[3*elm->ijk[k][belm->ijk[2][i]]+1]/3.0);
        }
    // vector pointing from P1 to P3
        v3[0] = P3[0] - P1[0];
        v3[1] = P3[1] - P1[1];
    // with a dot product of v3 and v2 we find out it's direction
        normalize_v2D(v2);
        normalize_v2D(v3);
        side = dot_prod2D(v2,v3);
        if (side == 0.0) {
            printf("\n\nSomething is wrong! Three colinear points!\n");
            exit(0);
        }
        if (side > 0)
            for(j=0; j<dim; j++) v2[j] *= -1.0;

        for(j=0; j<dim; j++) elm->nv[j+dim*i] = v2[j];
    }

// Normalize normal vectors with the length (area)

    for(i=0; i<bnelx; i++)
        for(j=0; j<dim; j++) elm->nv[j+dim*i] *= elm->area[i];

// Check the area sum
// normal vector times area should be zero around a closed surface
//#pragma parallel for private(j)
    v4[0] = 0.0;
    v4[1] = 0.0;
    for(i=0; i<bnelx; i++)
        for(j=0; j<dim; j++) v4[j] += elm->nv[j+dim*i];

    if(v4[0] > 1.0e-6 || v4[1] > 1.0e-6) {
        printf("\nWARNING: Areas don't seem to be compensated.\n");
        printf("nv*area[0]= %lf\tnv*area[1]= %lf\n",v4[0],v4[1]);
    }
}
/* END void MakeNormalVectors2D(AKElm *elm, AKElm *belm)                */
/*----------------------------------------------------------------------*/
int OPSIDE(double *a, double *b, double *c, double *d, double *e)
{
    double *ab, *ac, ddp, dmax, edp, emax,tol;
    double nrml1, nrml2, nrml3;
    int i, opside;

    ab = (double *)  malloc((size_t) 3*sizeof(double));
    ac = (double *)  malloc((size_t) 3*sizeof(double));

    tol = 1.e-16;
    for(i=0;i<3;i++) {
        ab[i] = b[i] - a[i];
        ac[i] = c[i] - a[i];
    }
    emax = vectabsmaxcomp(a);
    dmax = vectabsmaxcomp(b);
    dmax = DMAX(dmax,emax);
    emax = vectabsmaxcomp(c);
    dmax = DMAX(dmax,emax);
    emax = vectabsmaxcomp(d);
    dmax = DMAX(dmax,emax);

    nrml1 = ab[1]*ac[2] - ab[2]*ac[1];
    nrml2 = ab[2]*ac[0] - ab[0]*ac[2];
    nrml3 = ab[0]*ac[1] - ab[1]*ac[0];

    ddp = (d[0]-a[0])*nrml1 + (d[1]-a[1])*nrml2 + (d[2]-a[2])*nrml3;
    if(fabs(ddp) <= tol*dmax) {
        opside = 2;
        free(ab);
        free(ac);
        return(opside);
    }

    emax = vectabsmaxcomp(e);
    edp = (e[0]-a[0])*nrml1 + (e[1]-a[1])*nrml2 + (e[2]-a[2])*nrml3;
    if(fabs(edp) <= tol*emax) opside = 0;
    else
    if(ddp*edp < 0.0) opside = 1;
    else opside = -1;
    free(ab);
    free(ac);
    return(opside);
}
/*----------------------------------------------------------------------*/
void MakeNormalVectors(AKElm *elm, AKElm *belm)
{
    int i,j,k, side, bn1, bn2, bn3, cbel, cn;
    int bnelx, dim, eld;
    double v1[3], v2[3], v3[3];
    double P1[3], P2[3], P3[3], P4[3], P5[3];

    bnelx = elm->bnelx;
    dim = elm->dim;
    eld = elm->eld;

    elm->nv = (double *) malloc((size_t) dim*bnelx*sizeof(double));
    if(!elm->nv) memerror("nv (normal vectors of the boundary elements)");
    elm->area = (double *) malloc((size_t) bnelx*sizeof(double));
    if(!elm->area) memerror("area (area of boundary elements)");

    for(i=0;i<bnelx;i++) {
        for(j=0;j<3;j++) {
            P1[j] = elm->xyz[3*belm->ijk[0][i]+j];
            P2[j] = elm->xyz[3*belm->ijk[1][i]+j];
            P3[j] = elm->xyz[3*belm->ijk[2][i]+j];
            P4[j] = 0.0;
            v1[j] = P1[j] - P2[j];
            v2[j] = P2[j] - P3[j];
        }
        cross_prod(v1,v2,v3);
        elm->area[i] = 0.5*vectabs(v3);
        for(j=0;j<3;j++)
            for(k=0;k<4;k++) {
                cn = elm->ijk[k][belm->ijk[3][i]];
                P4[j]+=0.25*elm->xyz[j+3*cn];
            }
        for(j=0;j<3;j++) P5[j] = P1[j] + v3[j];
        side = OPSIDE(P1,P2,P3,P4,P5);
        if (side == 0 || side == 2) {
            printf("\n\nSomething is wrong! Four coplanar points!\n");
            printf("side = %i\n",side);
            exit(0);
        }
        if (side == -1)
            for(j=0;j<3;j++) v3[j] *= -1.0;
        normalize_v(v3);
        for(j=0;j<3;j++) {
            elm->nv[j+3*i] = v3[j];
        }
    }
// Normalize normal vectors with the area of the element
    for(i=0; i<bnelx; i++)
        for(j=0; j<dim; j++) elm->nv[j+3*i] *= elm->area[i];
// Check the area sum
// normal vector times area should be zero around a closed surface
//#pragma parallel for private(j)
    P5[0] = 0.0;
    P5[1] = 0.0;
    P5[2] = 0.0;
    for(i=0; i<bnelx; i++)
        for(j=0; j<dim; j++) P5[j] += elm->nv[j+dim*i];

    if(P5[0] > 1.0e-6 || P5[1] > 1.0e-6 || P5[2] > 1.0e-6) {
        printf("\nWARNING: Areas don't seem to be compensated.\n");
        printf("nv*area[0]= %lf\tnv*area[1]= %lf\tnv*area[2]= %lf\n",P5[0],P5[1],P5[2]);
    }
}
/*----------------------------------------------------------------------*/
double VectorsAngle2D(double *a, double *b, double rtol)
{
    double tol=1.e-8;
    double aDOTb, LA, LB, T, angle;

    aDOTb = dot_prod2D(a,b);
    LA = vectnorm2D(a);
    LB = vectnorm2D(b);
    if((LA > rtol) && (LB > rtol)) {
        T = aDOTb/sqrt(LA*LB);
        if(fabs(T) > (1.0-tol)) T = SIGN(T);
        angle = acos(T);
    } else angle = PI;
    return(angle);
}
/* End double VectorsAngle(double *a, double *b, double rtol) */
/*----------------------------------------------------------------------*/
double VectorsAngle(double *a, double *b, double rtol)
{
    double tol=1.e-8;
    double aDOTb, LA, LB, T;
    double angle;
    aDOTb = dot_prod(a,b);
    LA = vectnorm(a);
    LB = vectnorm(b);
    if((LA > rtol) && (LB > rtol)) {
        T = aDOTb/sqrt(LA*LB);
        if(fabs(T) > (1.0-tol)) T = SIGN(T);
        angle = acos(T);
    } else angle = PI;
    return(angle);
}
/* End double VectorsAngle(doule *a, double *b, double rtol) */
/*----------------------------------------------------------------------*/
double SolidAngle2D(double *x0, double *x1, double *x2)
{
    double AB[2], AC[2], sang;
    double tmp1, tmp2, tmp3, rtolsq, tol=1e-8;
    for(int i=0; i<2; i++) {
        AB[i] = x1[i] - x0[i];
        AC[i] = x2[i] - x0[i];
    }
    tmp1 = vectabsmaxcomp2D(x0);
    tmp2 = vectabsmaxcomp2D(x1);
    tmp3 = DMAX(tmp1,tmp2);
    tmp2 = vectabsmaxcomp2D(x2);
    tmp1 = DMAX(tmp2,tmp3);
    rtolsq = tmp1*tmp1*tol*tol;
    sang = VectorsAngle2D(AB,AC,rtolsq);
    return(sang);
//    return(2*sang);
}
/* End double SolidAngle2D(double *x0, double *x1, double *x2) */
/*----------------------------------------------------------------------*/
int areEqual(double x, double y) { // default value of ulp specified in header file
  double eps = DBL_MIN;
  int ulp = 100;
  int almostEqual = 0;
  if (fabs(x-y) < eps*fabs(x+y)*ulp) {almostEqual = 1;}
  // = std::abs(x-y) < (eps*std::abs(x+y) * ulp);
  if ((fabs(x) < eps * ulp) && (fabs(y) < eps * ulp)) almostEqual = 1; // case x,y = 0
  return almostEqual;
}
/*----------------------------------------------------------------------*/
double SolidAngleNew(double *x0, double *x1, double *x2, double *x3)
{
    double tol = 1.e-8, rtolsq;
    double r1[3], r2[3], r3[3];
    double r1n, r2n, r3n, r123, r12, r23, r31;
    double numer, denom, arg, omega;
    int i;
    for(i=0;i<3;i++) {
        r1[i] = x1[i] - x0[i];
        r2[i] = x2[i] - x0[i];
        r3[i] = x3[i] - x0[i];
    }
	r1n = vectabs(r1);
    r2n = vectabs(r2);
	r3n = vectabs(r3);
	r123 = r1n * r2n * r3n;
	r23 = dotprod(r2,r3);
	r12 = dotprod(r1,r2);
	r31 = dotprod(r3,r1);
	numer = r123 + r1n * r23 + r2n * r31 + r3n * r12;
	denom = sqrt(2.0 * (r2n * r3n + r23) * (r3n * r1n + r31)
					* (r1n * r2n + r12));
	arg = numer/denom;
	if (areEqual( arg, 1.)) { arg = 1.; } // roundoff errors can lead to arguments > 1 resulting in NaN
	omega = 2.0 * acos((double) arg);
	return omega;
}
/*----------------------------------------------------------------------*/
double SolidAngle(double *x0, double *x1, double *x2, double *x3)
{
    double tol = 1.e-8, rtolsq,tmp1,tmp2, tmp3;
    double *ab, *ac, *ad, *bc, *bd;
    double *nabc, *nabd, *nacd, *nbcd, *dang, sang;
    int i;

    ab = (double *)  malloc((size_t) 3*sizeof(double));
    ac = (double *)  malloc((size_t) 3*sizeof(double));
    ad = (double *)  malloc((size_t) 3*sizeof(double));
    bc = (double *)  malloc((size_t) 3*sizeof(double));
    bd = (double *)  malloc((size_t) 3*sizeof(double));

    nabc = (double *)  malloc((size_t) 3*sizeof(double));
    nabd = (double *)  malloc((size_t) 3*sizeof(double));
    nacd = (double *)  malloc((size_t) 3*sizeof(double));
    nbcd = (double *)  malloc((size_t) 3*sizeof(double));
    dang = (double *)  malloc((size_t) 3*sizeof(double));

    for(i=0;i<3;i++) {
        ab[i] = x1[i] - x0[i];
        ac[i] = x2[i] - x0[i];
        ad[i] = x3[i] - x0[i];
        bc[i] = x2[i] - x1[i];
        bd[i] = x3[i] - x1[i];
    }

    tmp1 = vectabsmaxcomp(x0);
    tmp2 = vectabsmaxcomp(x1);
    tmp3 = DMAX(tmp1,tmp2);
    tmp2 = vectabsmaxcomp(x2);
    tmp1 = DMAX(tmp3,tmp2);
    tmp2 = vectabsmaxcomp(x3);
    tmp3 = DMAX(tmp1,tmp2);

    rtolsq = tmp3*tmp3*tol*tol;

    nabc[0] = ac[1]*ab[2] - ac[2]*ab[1];
    nabc[1] = ac[2]*ab[0] - ac[0]*ab[2];
    nabc[2] = ac[0]*ab[1] - ac[1]*ab[0];
    nabd[0] = ab[1]*ad[2] - ab[2]*ad[1];
    nabd[1] = ab[2]*ad[0] - ab[0]*ad[2];
    nabd[2] = ab[0]*ad[1] - ab[1]*ad[0];
    nacd[0] = ad[1]*ac[2] - ad[2]*ac[1];
    nacd[1] = ad[2]*ac[0] - ad[0]*ac[2];
    nacd[2] = ad[0]*ac[1] - ad[1]*ac[0];
    nbcd[0] = bc[1]*bd[2] - bc[2]*bd[1];
    nbcd[1] = bc[2]*bd[0] - bc[0]*bd[2];
    nbcd[2] = bc[0]*bd[1] - bc[1]*bd[0];

    dang[0] = PI - VectorsAngle(nabc,nabd,rtolsq);
    dang[1] = PI - VectorsAngle(nabc,nacd,rtolsq);
    dang[2] = PI - VectorsAngle(nabd,nacd,rtolsq);
    /*
      dang[3] = PI - VectorsAngle(nabc,nabd,rtolsq);
      dang[4] = PI - VectorsAngle(nabc,nacd,rtolsq);
      dang[5] = PI - VectorsAngle(nabd,nacd,rtolsq);
    */
    sang = dang[0] + dang[1] + dang[2];

    free(ab);
    free(ac);
    free(ad);
    free(bc);
    free(bd);
    free(nabc);
    free(nabd);
    free(nacd);
    free(nbcd);
    free(dang);

    sang -= PI;
    return(sang);
}
/* End double SolidAngle(double *xyz, int nx, int el1, int el2, int el3, int el4) */
/*----------------------------------------------------------------------*/
void MakeSubList(AKElm *elm)
{
    int bnx, eld;
    int i,j,n,el;
    int cbn, anx, cel, cbn_position, *el_copy, itemp=0;
    int index1, index2, index3, index4;
    double d2PI = 1.0/(2*PI), d4PI = 1.0/(4*PI);


    bnx = elm->bnx;
    eld = elm->eld;
    elm->sub = (double *) malloc((size_t) bnx*sizeof(double));
    if(!elm->sub) memerror("sub (solid angles)");

    el_copy = (int *) malloc((size_t) eld*sizeof(int));
    for(n=0; n<bnx; n++) {
        cbn = elm->bnlist[n];
        elm->sub[n] = 0.0;
        anx = elm->num_el[cbn];
        for(el=0; el<anx; el++) {
            cel = elm->nel_list[elm->pnel_list[cbn]+el];
            for(j=0; j<eld; j++) {
                el_copy[j] = elm->ijk[j][cel];
                if(el_copy[j] == cbn) cbn_position = j;
            }
            if(cbn_position) {
                SWAP_BND(el_copy[0], el_copy[cbn_position]);
                cbn_position = 0;
            }
            index1 = 3*el_copy[0];
            index2 = 3*el_copy[1];
            index3 = 3*el_copy[2];
            if (eld == 3)
                elm->sub[n] += fabs(SolidAngle2D(elm->xyz+index1,elm->xyz+index2,elm->xyz+index3))*d2PI;
            else {
                index4 = 3*el_copy[3];
                elm->sub[n] += fabs(SolidAngle(elm->xyz+index1,elm->xyz+index2,elm->xyz+index3,elm->xyz+index4))*d4PI;
            }
        }
    }

    for(i=0; i<bnx; i++) {
        elm->sub[i] = (double) floor(elm->sub[i]*1.0e14 + 0.5)*1.0e-14;
        elm->sub[i] -= 1.0;
    }
    free(el_copy);
}
/*----------------------------------------------------------------------*/
int bndprocessing(AKElm *elm, AKElm *belm, int *bnelx)
{
  AKElm faces;
  int bnx, err, nof, *list, i, nelx, nx, dim;
  long long *sortnum;

  nelx = elm->nelx;
  nx = elm->nx;
  dim = elm->dim;
  N_AKElm(&faces,(dim+1)*nelx,nx,dim-1);

  if(elm->dim == 2) nof = MakeEdges(elm,&faces);
  else nof = MakeFaces(elm,&faces);
//  printf("Total number of faces (including multiples): %d.\n",nof);

  if(elm->dim == 2) SortEdgeList(&faces,nof);
  else SortFaceList(&faces,nof);
  sortnum = (long long *) malloc((size_t) nof*sizeof(long long));
  if(!sortnum) memerror("sortnum (sorting allocation)");
  CalcSortnum(faces,sortnum,nof,dim);

  list = (int *) malloc ((size_t) nof*sizeof(int));
  if(!list) memerror("list for sortnum (sorting allocation)");
  dindexx(nof,sortnum,list);

  Count_bnelx(faces,bnelx,nof,list,dim);
  elm->bnelx = *bnelx;
  N_AKElm(belm,*bnelx,nx,dim-1);
  FindBELM(faces,*bnelx,nof,list,belm,dim);
  CountBNXandMakeList(elm,belm);

//  printf("Calculating normal vectors.\n");
  if(elm->dim == 2) MakeNormalVectors2D(elm,belm);
  else MakeNormalVectors(elm,belm);
//  printf("DONE: Calculating normal vectors.\n");
//  printf("Calculating solid angles.\n");
  MakeSubList(elm);
//  printf("DONE: Calculating solid angles.\n");
//  WriteIJKB(belm,elm->ngname);
  free(sortnum);
  return(0);
}
/*----------------------------------------------------------------------*/
