//
// Created by Attila Kákay on 12/15/21.
//
#include <stdio.h>
#include <stdlib.h>
#include "fempreproc.h"
#include "akmag.h"
#include "utils.h"
#include "bnd2d.h"
#include "galerkin.h"
#include "opmatspm.h"

/*----------------------------------------------------------------------*/
int OperatorsFromMesh2D(double *xyz, int *ijk, int nx, int nelx, int dim,
                      int *bnlist, int *bnx, double *nv, double *area,
                      CSRSprMat *poiss, CSRSprMat *lap,
                      CSRSprMat *div_x, CSRSprMat *div_y,
                      CSRSprMat *grad_x, CSRSprMat *grad_y,
                      double *solid_angle, int *belm)
{
    int bnelx, err, eld, nrg=1, i,j;

    AKElm elm, belem;
    OperatorMat OpMat;

    N_AKElm(&elm, nelx, nx, dim);
    // Here I need to set elm.xyz from sample.xyz
    // elm.ijk from sample.mesh
    for(i=0; i<3*nx; i++) elm.xyz[i] = xyz[i];
//    elm.xyz = xyz;
    for(i=0; i<nelx; i++){
        elm.ijk[0][i] = ijk[3*i];
        elm.ijk[1][i] = ijk[3*i+1];
        elm.ijk[2][i] = ijk[3*i+2];
        elm.ijk[3][i] = 1;
    }

    clean_mesh(&elm);

    if(nx != elm.nx) {
//        printf("Found ghost nodes!\n");
        fflush(stdout);
        exit(0);
    }

    elm.nrg = nrg;
//    elm.dtet = dtet;
    elm.params = (MatParams *) malloc(nrg * sizeof(MatParams));
//    printf("Making the inverse node-element list.\n");
    MakeInvNodeElmList(&elm);
//    printf("Making the adjacent node list.\n");
    MakeAdjacentNodeList(&elm);

    // BEGINNING: Boundary elements, nodes
    bndprocessing(&elm, &belem, &bnelx);

    *bnx = elm.bnx;

    ShapeFGrad(&elm);
    GalerkinInteractions(&elm,&OpMat);
    ris2csrDirect(&OpMat.poiss,poiss);
    ris2csrDirect(&OpMat.lap,lap);
    ris2csrDirect(&OpMat.ldx,div_x);
    ris2csrDirect(&OpMat.ldy,div_y);
    ris2csrDirect(&OpMat.phix,grad_x);
    ris2csrDirect(&OpMat.phiy,grad_y);
//#pragma parallel for
    for(i=0;i<elm.bnx;i++) {
        bnlist[i] = elm.bnlist[i];
        solid_angle[i] = elm.sub[i];
        belm[2*i] = belem.ijk[0][i];
        belm[2*i+1] = belem.ijk[1][i];
    }
    for (i=0;i<elm.nx;i++) area[i] = elm.vol_n[i];
    for (j=0;j<dim*bnelx;j++) nv[j] = elm.nv[j];

    F_RISSprMat(&OpMat.poiss);
    F_RISSprMat(&OpMat.lap);
    F_RISSprMat(&OpMat.ldx);
    F_RISSprMat(&OpMat.ldy);
    F_RISSprMat(&OpMat.phix);
    F_RISSprMat(&OpMat.phiy);

    // freeing the element structure
    free(elm.xyz);
    FreeIMatrix(elm.ijk);
    free(elm.ijk_is_boundary);
    free(elm.vol_el);
    free(elm.vol_n);
    free(elm.dNa);
    free(elm.nv);
    free(elm.area);
    free(elm.sub);
    free(elm.num_el);
    free(elm.nel_list);
    free(elm.pnel_list);
    free(elm.nx_list);
    free(elm.pnx_list);
    free(elm.bnlist);
    free(elm.atbnd);

    return(1);
}

/*----------------------------------------------------------------------*/
int OperatorsFromMesh2D_radial(double *xyz, int *ijk, int nx, int nelx, int dim,
                      int *bnlist, int *bnx, double *nv, double *area,
                      CSRSprMat *poisson, CSRSprMat *grad_poisson,
                      CSRSprMat *laplace, CSRSprMat *grad_laplace,
                      CSRSprMat *div_x, CSRSprMat *div_y,
                      CSRSprMat *grad_x, CSRSprMat *grad_y,
                      double *solid_angle, int *belm)
{
    int bnelx, err, eld, nrg=1, i,j;

    AKElm elm, belem;
    OperatorMat OpMat;

    N_AKElm(&elm, nelx, nx, dim);
    elm.is_radial = 1;
    // Here I need to set elm.xyz from sample.xyz
    // elm.ijk from sample.mesh
    for(i=0; i<nx; i++) {
        elm.xyz[3*i] = xyz[3*i];
        elm.xyz[3*i+1] = xyz[3*i+2];
        elm.xyz[3*i+2] = xyz[3*i+1];
    }
//    elm.xyz = xyz;
    for(i=0; i<nelx; i++){
        elm.ijk[0][i] = ijk[3*i];
        elm.ijk[1][i] = ijk[3*i+1];
        elm.ijk[2][i] = ijk[3*i+2];
        elm.ijk[3][i] = 1;
    }

    clean_mesh(&elm);

    if(nx != elm.nx) {
//        printf("Found ghost nodes!\n");
        fflush(stdout);
        exit(0);
    }

    elm.nrg = nrg;
//    elm.dtet = dtet;
    elm.params = (MatParams *) malloc(nrg * sizeof(MatParams));
//    printf("Making the inverse node-element list.\n");
    MakeInvNodeElmList(&elm);
//    printf("Making the adjacent node list.\n");
    MakeAdjacentNodeList(&elm);

    // BEGINNING: Boundary elements, nodes
    bndprocessing(&elm, &belem, &bnelx);

    *bnx = elm.bnx;

    ShapeFGrad(&elm);
    GalerkinInteractions(&elm,&OpMat);

    ris2csrDirect(&OpMat.poiss,poisson);
    ris2csrDirect(&OpMat.lap,laplace);
    ris2csrDirect(&OpMat.grad_rho,grad_poisson);
    ris2csrDirect(&OpMat.grad_rho_DBC,grad_laplace);
    ris2csrDirect(&OpMat.ldx,div_x);
    ris2csrDirect(&OpMat.ldy,div_y);
    ris2csrDirect(&OpMat.phix,grad_x);
    ris2csrDirect(&OpMat.phiy,grad_y);
//#pragma parallel for
    for(i=0;i<elm.bnx;i++) {
        bnlist[i] = elm.bnlist[i];
        solid_angle[i] = elm.sub[i];
        belm[2*i] = belem.ijk[0][i];
        belm[2*i+1] = belem.ijk[1][i];
    }
    for (i=0;i<elm.nx;i++) area[i] = elm.vol_n[i];
    for (j=0;j<dim*bnelx;j++) nv[j] = elm.nv[j];

    F_RISSprMat(&OpMat.poiss);
    F_RISSprMat(&OpMat.lap);
    F_RISSprMat(&OpMat.grad_rho);
    F_RISSprMat(&OpMat.grad_rho_DBC);
    F_RISSprMat(&OpMat.ldx);
    F_RISSprMat(&OpMat.ldy);
    F_RISSprMat(&OpMat.phix);
    F_RISSprMat(&OpMat.phiy);

    // freeing the element structure
    free(elm.xyz);
    FreeIMatrix(elm.ijk);
    free(elm.ijk_is_boundary);
    free(elm.vol_el);
    free(elm.vol_n);
    free(elm.dNa);
    free(elm.nv);
    free(elm.area);
    free(elm.sub);
    free(elm.num_el);
    free(elm.nel_list);
    free(elm.pnel_list);
    free(elm.nx_list);
    free(elm.pnx_list);
    free(elm.bnlist);
    free(elm.atbnd);

    return(1);
}
/*----------------------------------------------------------------------*/
int ExchangeOperatorFromMesh2D(double *xyz, int *ijk, int nx, int nelx,
                               int dim, double *Aex, CSRSprMat *xc,
                               CSRSprMat *xc_grad, int not_radial)
{
    int bnelx, err, eld, nrg=1, i,j, l_nnz;

    AKElm elm, belem;
    OperatorMat OpMat;

    N_AKElm(&elm, nelx, nx, dim);
    // Here I need to set elm.xyz from sample.xyz
    // elm.ijk from sample.mesh
    if (not_radial)
        for(i=0; i<3*nx;i++) elm.xyz[i] = xyz[i];
    else {
        for(i=0; i<nx;i++) {
            elm.xyz[3*i] = xyz[3*i];
            elm.xyz[3*i+1] = xyz[3*i+2];
            elm.xyz[3*i+2] = xyz[3*i+1];
        }
        for(i=0; i<nx;i++) {
            elm.one_over_rho[i] = 1.0/xyz[3*i];
        }

    }
//    elm.xyz = xyz;
    for(i=0; i<nelx; i++){
        elm.ijk[0][i] = ijk[3*i];
        elm.ijk[1][i] = ijk[3*i+1];
        elm.ijk[2][i] = ijk[3*i+2];
        elm.ijk[3][i] = 1;
    }

    clean_mesh(&elm);

    if(nx != elm.nx) {
//        printf("Found ghost nodes!\n");
        fflush(stdout);
        exit(0);
    }

    elm.nrg = nrg;
//    elm.dtet = dtet;
    elm.params = (MatParams *) malloc(nrg * sizeof(MatParams));
    elm.params[0].A = Aex;
//    elm.params[0].Ms = Msat;

//    elm.params[0].Msat_avrg = 0.0;
//    for(i=0; i<nx; i++) elm.params[0].Msat_avrg += Msat[i];
//    elm.params[0].Msat_avrg /= nx;

//    elm.params[0].Bs = Msat*mu0;

//    printf("Making the inverse node-element list.\n");
    MakeInvNodeElmList(&elm);
//    printf("Making the adjacent node list.\n");
    MakeAdjacentNodeList(&elm);

// BEGIN: Computing operators/matrices
//    printf("Computing shape functions and matrices.\n");
    ShapeFGrad(&elm);
    ExchangeLaplacian(&elm,&OpMat);
    OpMat.xc_expanded =  expand_dimension(OpMat.xct);
    ris2csrDirect(&OpMat.xc_expanded,xc);

    if (not_radial == 0) {
        l_nnz = CalcLofSpMatGrad_rad_exc(&elm, 0);
        l_nnz += nx;
        N_RISSprMat(&(OpMat.grad_rho_exc), l_nnz);
        CalcSpMatGrad_rad_exc(&elm, &OpMat.grad_rho_exc, 0);
        OpMat.grad_rho_exc_expanded =  expand_dimension(OpMat.grad_rho_exc);
        ris2csrDirect(&OpMat.grad_rho_exc_expanded,xc_grad);
        free(elm.one_over_rho);
        F_RISSprMat(&OpMat.grad_rho_exc);
        F_RISSprMat(&OpMat.grad_rho_exc_expanded);
        printf("Gradient matrix for exchange radial case.\n");
    }

// free operator structure
    F_RISSprMat(&OpMat.xc);
    F_RISSprMat(&OpMat.xct);
    F_RISSprMat(&OpMat.xc_expanded);

    // freeing the element structure
    free(elm.xyz);
    FreeIMatrix(elm.ijk);
    free(elm.ijk_is_boundary);
    free(elm.vol_el);
    free(elm.vol_n);
    free(elm.dNa);
//    free(elm.nv);
//    free(elm.area);
//    free(elm.sub);
    free(elm.num_el);
    free(elm.nel_list);
    free(elm.pnel_list);
    free(elm.nx_list);
    free(elm.pnx_list);
//    free(elm.bnlist);

    return(1);
}

/*----------------------------------------------------------------------*/
int ExchangeOperatorFromMesh3D(double *xyz, int *ijk, int nx, int nelx,
                               int dim, double *Aex, CSRSprMat *xc)
{
    int err, eld, nrg=1, i, j;

    AKElm elm, belem;
    OperatorMat OpMat;

    eld = dim + 1;
    N_AKElm(&elm, nelx, nx, dim);
    // Here I need to set elm.xyz from sample.xyz
    // elm.ijk from sample.mesh
    for(i=0; i<3*nx; i++) elm.xyz[i] = xyz[i];
    //elm.xyz = xyz;
    for(i=0; i<nelx; i++){
        elm.ijk[0][i] = ijk[eld*i];
        elm.ijk[1][i] = ijk[eld*i+1];
        elm.ijk[2][i] = ijk[eld*i+2];
        elm.ijk[3][i] = ijk[eld*i+3];
        elm.ijk[4][i] = 1;
    }

    clean_mesh(&elm);

    if(nx != elm.nx) {
//        printf("Found ghost nodes!\n");
        fflush(stdout);
        exit(0);
    }

    elm.nrg = nrg;
//    elm.dtet = dtet;
    elm.params = (MatParams *) malloc(nrg * sizeof(MatParams));
    elm.params[0].A = Aex;
//    elm.params[0].Ms = Msat;

//    elm.params[0].Bs = Msat*mu0;


//    printf("Making the inverse node-element list.\n");
    MakeInvNodeElmList(&elm);
//    printf("Making the adjacent node list.\n");
    MakeAdjacentNodeList(&elm);

// BEGIN: Computing operators/matrices
//    printf("Computing shape functions and matrices.\n");
    ShapeFGrad(&elm);
    ExchangeLaplacian(&elm,&OpMat);
    OpMat.xc_expanded =  expand_dimension(OpMat.xct);
    ris2csrDirect(&OpMat.xc_expanded,xc);

// free operator structure
    F_RISSprMat(&OpMat.xc);
    F_RISSprMat(&OpMat.xct);
    F_RISSprMat(&OpMat.xc_expanded);

    // freeing the element structure
    free(elm.xyz);
    FreeIMatrix(elm.ijk);
    free(elm.ijk_is_boundary);
    free(elm.vol_el);
    free(elm.vol_n);
    free(elm.dNa);
//    free(elm.nv);
//    free(elm.area);
//    free(elm.sub);
    free(elm.num_el);
    free(elm.nel_list);
    free(elm.pnel_list);
    free(elm.nx_list);
    free(elm.pnx_list);
//    free(elm.bnlist);

    return(1);
}

/*----------------------------------------------------------------------*/
int OperatorsFromMesh3D(double *xyz, int *ijk, int nx, int nelx, int dim,
                        int *bnlist, int *bnx, int *bnelx, double *nv, double *node_volumes,
                        CSRSprMat *poiss, CSRSprMat *lap,
                        CSRSprMat *div_x, CSRSprMat *div_y, CSRSprMat *div_z,
                        CSRSprMat *grad_x, CSRSprMat *grad_y, CSRSprMat *grad_z,
//                      CSRSprMat *bdmi_x, CSRSprMat *bdmi_y, CSRSprMat *bdmi_z,
                        double *solid_angle, int *belm)
{
    int bnlx, err, eld, nrg=1, i,j;
    AKElm elm, belem;
    OperatorMat OpMat;

    eld = dim + 1;
    N_AKElm(&elm, nelx, nx, dim);
    // Here I need to set elm.xyz from sample.xyz
    // elm.ijk from sample.mesh
    for(i=0; i<3*nx; i++) elm.xyz[i] = xyz[i];
    //elm.xyz = xyz;
    for(i=0; i<nelx; i++){
        elm.ijk[0][i] = ijk[eld*i];
        elm.ijk[1][i] = ijk[eld*i+1];
        elm.ijk[2][i] = ijk[eld*i+2];
        elm.ijk[3][i] = ijk[eld*i+3];
        elm.ijk[4][i] = 1;
        elm.ijk_is_boundary[i] = 0;
    }

    clean_mesh(&elm);

    if(nx != elm.nx) {
        printf("Found ghost nodes!\n");
        fflush(stdout);
        exit(0);
    }

    elm.nrg = nrg;

//    elm.dtet = dtet;
    elm.params = (MatParams *) malloc(nrg * sizeof(MatParams));
//    elm.params[0].A = Aex;
//    elm.params[0].Ms = Msat;
//    elm.params[0].Bs = Msat*mu0;


//    printf("Making the inverse node-element list.\n");
    MakeInvNodeElmList(&elm);
//    printf("Making the adjacent node list.\n");
    MakeAdjacentNodeList(&elm);

    // BEGINNING: Boundary elements, nodes
//    printf("Counting the number of boundary elements and nodes.\n");
    bndprocessing(&elm, &belem, &bnlx);
//    printf("The number of boundary elements: %d\n",bnelx);
//    printf("The number of boundary nodes: %d\n",elm.bnx);

    *bnx = elm.bnx;
    *bnelx = bnlx;
//    printf("Computing the static Dense Matrix.\n");
//    printf("DONE.\n");
//    fflush(stdout);
// END: Boundary elements, nodes

// BEGIN: Computing operators/matrices
//    printf("Computing shape functions and matrices.\n");
    ShapeFGrad(&elm);
    GalerkinInteractions(&elm,&OpMat);
//    OpMat.xc_expanded =  expand_dimension(OpMat.xct);
//    ris2csrDirect(&OpMat.xc_expanded,xc);
    ris2csrDirect(&OpMat.poiss,poiss);
    ris2csrDirect(&OpMat.lap,lap);
    ris2csrDirect(&OpMat.ldx,div_x);
    ris2csrDirect(&OpMat.ldy,div_y);
    ris2csrDirect(&OpMat.ldz,div_z);
    ris2csrDirect(&OpMat.phix,grad_x);
    ris2csrDirect(&OpMat.phiy,grad_y);
    ris2csrDirect(&OpMat.phiz,grad_z);

//    printf("DONE.\n");
//#pragma parallel for
    for(i=0;i<elm.bnx;i++) {
        bnlist[i] = elm.bnlist[i];
        solid_angle[i] = elm.sub[i];
    }
    for(i=0;i<bnlx;i++) {
        belm[3*i] = belem.ijk[0][i];
        belm[3*i+1] = belem.ijk[1][i];
        belm[3*i+2] = belem.ijk[2][i];
    }
    for (i=0;i<elm.nx;i++) node_volumes[i] = elm.vol_n[i];
    for (j=0;j<dim*bnlx;j++) nv[j] = elm.nv[j];

// free operator structure
//    F_RISSprMat(&OpMat.xc);
//    F_RISSprMat(&OpMat.xct);
//    F_RISSprMat(&OpMat.xc_expanded);
    F_RISSprMat(&OpMat.poiss);
    F_RISSprMat(&OpMat.lap);
    F_RISSprMat(&OpMat.ldx);
    F_RISSprMat(&OpMat.ldy);
    F_RISSprMat(&OpMat.ldz);
    F_RISSprMat(&OpMat.phix);
    F_RISSprMat(&OpMat.phiy);
    F_RISSprMat(&OpMat.phiz);

    // freeing the element structure
    free(elm.xyz);
    FreeIMatrix(elm.ijk);
    free(elm.ijk_is_boundary);
    free(elm.vol_el);
    free(elm.vol_n);
    free(elm.dNa);
    free(elm.nv);
    free(elm.area);
    free(elm.sub);
    free(elm.num_el);
    free(elm.nel_list);
    free(elm.pnel_list);
    free(elm.nx_list);
    free(elm.pnx_list);
    free(elm.bnlist);
    free(elm.atbnd);
//    free(elm.params);

    return(1);
}
