//
// Created by Attila Kákay on 15.07.20.
//

#include "galerkin.h"
#include "utils.h"
#include "opmatspm.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

/*----------------------------------------------------------------------*/
void ShapeFGrad(AKElm *elm)
{
    if (elm->dim == 2)  Shape2DFGrad(elm);
    if (elm->dim == 3)  Shape3DFGrad(elm);
}
/*----------------------------------------------------------------------*/
void Shape2DFGrad(AKElm *elm)
{
  double v2, *xyz, dv2, sign, deter;
  int a, i1, i2, i3, kit, nel, dim, nx;
  int el1, el2, el3, eld;
  long int nelx;

  dim = elm->dim;
  eld = elm->eld;
  nelx = elm->nelx;
  elm->dNa = (double *) malloc((size_t) (dim*eld*nelx*sizeof(double)));
  xyz = (double *) malloc((size_t) (3*eld*sizeof(double)));

  for(nel=0;nel<nelx;nel++) {
    el1 = elm->ijk[0][nel];
    el2 = elm->ijk[1][nel];
    el3 = elm->ijk[2][nel];
    dvcopy(elm->xyz+3*el1,xyz,3,1);
    dvcopy(elm->xyz+3*el2,xyz+3,3,1);
    dvcopy(elm->xyz+3*el3,xyz+6,3,1);
    v2 = area_el2(xyz,xyz+3,xyz+6);
    elm->vol_el[nel] = fabs(v2)/2.0;
    dv2 = 1.0/v2;
    for(a=0;a<eld;a++) {
      kit = a+1;
      //sign = pow(-1,(double) kit);
      sign = -1;
      i1 = (a) % eld;
      i2 = (a+1) % eld;
      i3 = (a+2) % eld;
      deter = Det2(1.0,xyz[3*i2+1],
		   1.0,xyz[3*i3+1]);
      elm->dNa[dim*eld*nel+dim*i1] = sign*deter*dv2;
      deter = Det2(xyz[3*i2],1.0,
		   xyz[3*i3],1.0);
      elm->dNa[dim*eld*nel+dim*i1+1] = sign*deter*dv2;
    }
  }
  free(xyz);
}
/*----------------------------------------------------------------------*/
void Shape3DFGrad(AKElm *elm)
{
    double v6, *xyz, dv6, sign, deter;
    int a, i1, i2, i3, i4, kit, nel, dim, nx;
    int el1, el2, el3, el4, eld;
    long int nelx;

    dim = elm->dim;
    eld = elm->eld;
    nelx = elm->nelx;
    elm->dNa = (double *) malloc((size_t) (dim*eld*nelx*sizeof(double)));
    xyz = (double *) malloc((size_t) (3*eld*sizeof(double)));

    for(nel=0;nel<nelx;nel++) {
        el1 = elm->ijk[0][nel];
        el2 = elm->ijk[1][nel];
        el3 = elm->ijk[2][nel];
        el4 = elm->ijk[3][nel];
        dvcopy(elm->xyz+3*el1,xyz,3,1);
        dvcopy(elm->xyz+3*el2,xyz+3,3,1);
        dvcopy(elm->xyz+3*el3,xyz+6,3,1);
        dvcopy(elm->xyz+3*el4,xyz+9,3,1);
        v6 = vol_el6(xyz,xyz+3,xyz+6,xyz+9);
        elm->vol_el[nel] = fabs(v6)/2.0;
        dv6 = 1.0/v6;
        for(a=0;a<eld;a++) {
            kit = a+1;
            sign = pow(-1,(double) kit);
            i1 = (a) % eld;
            i2 = (a+1) % eld;
            i3 = (a+2) % eld;
            i4 = (a+3) % eld;
            deter = Det3(1.0,xyz[3*i2+1],xyz[3*i2+2],
                         1.0,xyz[3*i3+1],xyz[3*i3+2],
                         1.0,xyz[3*i4+1],xyz[3*i4+2]);
            elm->dNa[dim*eld*nel+dim*i1] = sign*deter*dv6;
            deter = Det3(xyz[3*i2],1.0,xyz[3*i2+2],
                         xyz[3*i3],1.0,xyz[3*i3+2],
                         xyz[3*i4],1.0,xyz[3*i4+2]);
            elm->dNa[dim*eld*nel+dim*i1+1] = sign*deter*dv6;
            deter = Det3(xyz[3*i2],xyz[3*i2+1],1.0,
                         xyz[3*i3],xyz[3*i3+1],1.0,
                         xyz[3*i4],xyz[3*i4+1],1.0);
            elm->dNa[dim*eld*nel+dim*i1+2] = sign*deter*dv6;
        }
    }
    free(xyz);
}
/*----------------------------------------------------------------------*/
void CalcNodesWignerSeitzVolume(AKElm *elm)
{
  int nx;
  int i, j, el, numelm;
  double deld = 1./elm->eld;
  nx = elm->nx;
  elm->small = 1e-25;
  for(i=0;i<nx;i++) {
    elm->vol_n[i] = 0.0;
    numelm = elm->num_el[i];
    for (j = 0; j < numelm; j++) {
      el = elm->nel_list[elm->pnel_list[i] + j];
      elm->vol_n[i] += deld * elm->vol_el[el];
    }
  }
}
/*----------------------------------------------------------------------*/
void GalerkinInteractions(AKElm *elm, OperatorMat *OpMat)
{
  int l_nnz, lofsprmat, nx;
  nx = elm->nx;
  CalcNodesWignerSeitzVolume(elm);

//Calculating the divergence Operator's x component (0)
  l_nnz = CalcLofSpMatDiv(elm, 0);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->ldx), lofsprmat);
  CalcSpMatDiv(elm, &OpMat->ldx, 0);
//Calculating the divergence Operator's y component (1)
  l_nnz = CalcLofSpMatDiv(elm, 1);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->ldy), lofsprmat);
  CalcSpMatDiv(elm, &OpMat->ldy, 1);
//Calculating the divergence Operator's z component (2)
  if(elm->dim == 3) {
    l_nnz = CalcLofSpMatDiv(elm, 2);
    lofsprmat = l_nnz + nx;
    N_RISSprMat(&(OpMat->ldz), lofsprmat);
    CalcSpMatDiv(elm, &OpMat->ldz, 2);
  }

// Calculating the Poisson Operator
  l_nnz = CalcLofSpMatPoisson(elm);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->poiss), lofsprmat);
  CalcSpMatPoisson(elm, &OpMat->poiss);

  if(elm->is_radial) {
      l_nnz = CalcLofSpMatGrad_rho(elm, 0, 0);
      lofsprmat = l_nnz + nx;
      N_RISSprMat(&(OpMat->grad_rho), lofsprmat);
      CalcSpMatGrad_rho(elm, &OpMat->grad_rho, 0, 0);
  }

// Calculating Laplace Operator
  l_nnz = CalcLofSpMatLaplace(elm);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->lap), lofsprmat);
  CalcSpMatLaplace(elm, &OpMat->lap, &OpMat->poiss);

  if(elm->is_radial) {
      l_nnz = CalcLofSpMatGrad_rho(elm, 0, 1);
      lofsprmat = l_nnz + nx;
      N_RISSprMat(&(OpMat->grad_rho_DBC), lofsprmat);
      CalcSpMatGrad_rho(elm, &OpMat->grad_rho_DBC, 0, 1);
  }

//Calculating the gradient Operator's x component (0)
  l_nnz = CalcLofSpMatGrad(elm, 0);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->phix), lofsprmat);
  CalcSpMatGrad(elm, &OpMat->phix, 0);
//Calculating the gradient Operator's y component (1)
  l_nnz = CalcLofSpMatGrad(elm, 1);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->phiy), lofsprmat);
  CalcSpMatGrad(elm, &OpMat->phiy, 1);
//Calculating the gradient Operator's z component (2)
  if(elm->dim == 3) {
    l_nnz = CalcLofSpMatGrad(elm, 2);
    lofsprmat = l_nnz + nx;
    N_RISSprMat(&(OpMat->phiz), lofsprmat);
    CalcSpMatGrad(elm, &OpMat->phiz, 2);
  }

}
/*----------------------------------------------------------------------*/
void ExchangeLaplacian(AKElm *elm, OperatorMat *OpMat)
{
  int l_nnz, lofsprmat, nx;
  nx = elm->nx;
  CalcNodesWignerSeitzVolume(elm);

//Calculating the exchange Operator
  l_nnz = CalcLofSpMatExch(elm);
  lofsprmat = l_nnz + nx;
  N_RISSprMat(&(OpMat->xct), lofsprmat);
  N_RISSprMat(&(OpMat->xc), lofsprmat);
  CalcSpMatExch(elm,&OpMat->xc,&OpMat->xct);
}
/*----------------------------------------------------------------------*/
