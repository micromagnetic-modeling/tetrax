#ifndef DENSE3D_H
#define DENSE3D_H


#include "akmag.h"

/*----------------------------------------------------------------------*/
int Lindholm3D(double *x0, double *x1, double *x2, double *x3,
               double *nv, double A, double *wg, int *failed);
/*----------------------------------------------------------------------*/
double dGreendnv(double *r, double *rp, double *nv);
/*----------------------------------------------------------------------*/
void ComputeDenseMatrix3D(int nx, int *bijk, int bnx, int bnelx,
                          int *bnlist, double *xyz, double *nv,
                          double *sang, int op, double *D3D);
/*----------------------------------------------------------------------*/

#endif
