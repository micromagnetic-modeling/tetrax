//
// Created by Attila Kákay on 25.09.23.
//

#ifndef TETRAX_INTEGRATION_H
#define TETRAX_INTEGRATION_H
/*----------------------------------------------------------------------*/
double trapzd(double (*func)(double *r, double *rp, double *nv, double x, int m), double *r, double *rp, double *nv, int m, double a, double b, int n);
/*----------------------------------------------------------------------*/
double qtrap(double (*func)(double *r, double *rp, double *nv, double x, int m), double *r, double *rp, double *nv, int m, double a, double b);

//double qtrap(double (*func)(double), double a, double b);
/*----------------------------------------------------------------------*/
//double qsimp(double (*func)(double), double a, double b);
/*----------------------------------------------------------------------*/
#endif // TETRAX_INTEGRATION_H