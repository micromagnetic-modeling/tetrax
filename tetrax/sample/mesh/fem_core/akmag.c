#include "akmag.h"
#include <stdlib.h>
#include <stdio.h>

/*----------------------------------------------------------------------*/
void memerror(char *e_message)
{
  printf("\n\nMemorry allocation problem for the %s.\n\n",e_message);
  exit(0);
}
/*----------------------------------------------------------------------*/
void N_AKVec(AKVec *array, int length)
{
  array->x = (double *) malloc((size_t) (3*length*sizeof(double)));
  if(!array->x) memerror("AKVec allocation");
  array->y = array->x + length;
  array->z = array->y + length;
  array->ivs = length;
  array->lvs = 3*length;
}
/*----------------------------------------------------------------------*/
void F_AKVec(AKVec *array)
{
  free(array->x);
}
/*----------------------------------------------------------------------*/
void N_AKFields(AKFields *fields, int comp_length)
{
    N_AKVec(&fields->Hexch,comp_length);
    N_AKVec(&fields->Hanis,comp_length);
    N_AKVec(&fields->Hdemag,comp_length);
    N_AKVec(&fields->Hext,comp_length);
    N_AKVec(&fields->Heff,comp_length);
}
/* Allocating the fields as arrays */
/*----------------------------------------------------------------------*/
int **IMatrix(int ni, int nj)
{
  int i;
  int **p;

  p = (int **) malloc((size_t)( ni*sizeof(int*)));
  if (!p) {
    printf("IMatrix: Allocation error of first order !\n");
    exit(1);
  }

   p[0] = (int*) malloc((size_t)(ni*nj*sizeof(int)));
   if (!p[0]) {
      printf("IMatrix: Allocation error of second order !\n");
      exit(2);
   }

   for (i = 1; i < ni; i++) p[i] = p[i-1] + nj;
   return p;
}
/*------------------------------------------------------------------*/
void FreeIMatrix(int **p)
{
   free((void*) (p[0]));
   free((void*) (p));
}
/*------------------------------------------------------------------*/
double **DMatrix(int ni, int nj)
{
    int i;
    double **p;

    p = (double **) malloc((size_t)( ni*sizeof(double*)));
    if (!p) {
        printf("DMatrix: Allocation error of first order !\n");
        exit(1);
    }

    p[0] = (double*) malloc((size_t)(ni*nj*sizeof(double)));
    if (!p[0]) {
        printf("DMatrix: Allocation error of second order !\n");
        exit(2);
    }

    for (i = 1; i < ni; i++) p[i] = p[i-1] + nj;
    return p;
}
/*------------------------------------------------------------------*/
void FreeDMatrix(double **p)
{
    free((void*) (p[0]));
    free((void*) (p));
}
/*------------------------------------------------------------------*/
int *IVector(int imax)
{
/* Allocates an integer vector with indexes between [imin,imax] interval
*/
    int *p;

    p = (int*) malloc((size_t) (imax*sizeof(int)));
    if (!p) {
        printf("IVector: allocation error !\n");
        exit(1);
    }
    return p;
}
/*------------------------------------------------------------------*/
double *DVector(int imax)
{
/* Allocates an integer vector with indexes between [imin,imax] interval
*/
    double *p;

    p = (double *) malloc((size_t) (imax*sizeof(double)));
    if (!p) {
        printf("DVector: allocation error !\n");
        exit(1);
    }
    return p;
}
/*------------------------------------------------------------------*/
void FreeIVector(int *p)
{
/* Deallocates the integer vector starting with index imin
*/
    free((void*) (p));
}
/*------------------------------------------------------------------*/
void FreeDVector(double *p)
{
/* Deallocates the double vector starting with index imin
*/
    free((void*) (p));
}
/*------------------------------------------------------------------*/
void N_AKElm(AKElm *elm, int nelx, int nx, int dim)
{
  elm->ijk = IMatrix(dim+2,nelx);
  if(!elm->ijk) memerror("AKElm allocation");
  elm->ijk_is_boundary = (int *) malloc((size_t) (nelx*sizeof(int)));
  elm->xyz = (double *) malloc((size_t) (3*nx*sizeof(double)));
  elm->one_over_rho = (double *) malloc((size_t) (nx*sizeof(double)));
  elm->vol_el = (double *) malloc((size_t) (nelx*sizeof(double)));
  elm->vol_n = (double *) malloc((size_t) (nx*sizeof(double)));
  elm->nelx = nelx;
  elm->nx = nx;
  elm->dim = dim;
  elm->eld = dim+1;
  elm->is_radial = 0;
}
/*----------------------------------------------------------------------*/
void N_RISSprMat(RISSprMat *spm, int isize)
{
  spm->sa = (double *) malloc((size_t) (isize*sizeof(double)));
  if(!spm->sa) memerror("SparseMat allocation");
  spm->ija = (int *) malloc((size_t) (isize*sizeof(int)));
  if(!spm->ija) memerror("SparseMat allocation");
  spm->ilx = isize;
}
/*----------------------------------------------------------------------*/
void F_RISSprMat(RISSprMat *spmat)
{
  free(spmat->sa);
  free(spmat->ija);
}
/*----------------------------------------------------------------------*/
void ris2csrDirect(RISSprMat *risMat, CSRSprMat *csrDescr)
/*
 * This function convert the RIS sparse matrix risMat
 * int a CSR matrix format, output is csrDescr
 */

{
    int N = risMat->ija[0] - 1;
    int nnz = risMat->ija[N] - 1;

    csrDescr->csrRowPtr_h = (int *) malloc((N + 1) * sizeof(int));
    csrDescr->csrColIdx_h = (int *) malloc(nnz * sizeof(int));
    csrDescr->csrValues_h = (double *) malloc(nnz * sizeof(double));
    csrDescr->inx = N;
    csrDescr->nnz = nnz;

    int posCSR = 0;
    int posRIS = N + 1;

    for (int i = 0; i < N; i++) {
        csrDescr->csrRowPtr_h[i] = posCSR;
        int diagInserted = 0;
        for (int j = 0; j < risMat->ija[i + 1] - risMat->ija[i]; j++) {
            if ((risMat->ija[posRIS] > i) && (!diagInserted)) {
                csrDescr->csrColIdx_h[posCSR] = i;
                csrDescr->csrValues_h[posCSR] = risMat->sa[i];
                posCSR++;
                diagInserted = 1;
            }
            csrDescr->csrColIdx_h[posCSR] = risMat->ija[posRIS];
            csrDescr->csrValues_h[posCSR] = risMat->sa[posRIS];
            posCSR++;
            posRIS++;
        }
        if (!diagInserted) {
            csrDescr->csrColIdx_h[posCSR] = i;
            csrDescr->csrValues_h[posCSR] = risMat->sa[i];
            posCSR++;
            diagInserted = 1;
        }
    }
    csrDescr->csrRowPtr_h[N] = posCSR;
}
/* END void ris2csrDirect(RISSprMat *risMat, CSRSprMat *csrDescr) */
/*----------------------------------------------------------------------*/
/**
 * Turn a given SparseMat x (dimension n x n) into a SparseMat of dimension 3n x
 * 3n by duplicating it.
 *
 *     n = length of diagonal of xc
 *     expand_dimension(xc)[i][j] = xc[i%n][j%n] if i/n == j/n else 0
 */
RISSprMat expand_dimension(RISSprMat x) {
    int n = x.ija[0] - 1;
    int l = x.ija[n];  // length of x.sa / x.ija
    RISSprMat result;
    N_RISSprMat(&result, 3*(l-1) + 1);
    result.ija[0] = 3*n + 1;
    //  result.ija[3*n] = 3*l;
    int k = 3*n;
    //  printf("n=%i\tl=%i\tk=%i\n",n,l,k);
    for (int r = 0; r < 3*n; ++r) {
        // row r in matrix result
        // row r%n in matrix x
        // data for this row starts at row_idx_first in x
        int row_idx_first = x.ija[r % n];
        int row_idx_last = x.ija[r % n + 1];
        result.sa[r] = x.sa[r % n];
        for (int c = row_idx_first; c < row_idx_last; c++) {
            // x.ija[c] is the column in x
            // so n*(r/n) + x.ija[c] is the column in result
            ++k;
            result.sa[k] = x.sa[c];
            result.ija[k] = n*(r/n) + x.ija[c];
        }
        result.ija[r+1] = k + 1;
    }
    return result;
}
/*----------------------------------------------------------------------*/