from __future__ import annotations

import sys

import numpy as np
from scipy.sparse import bmat, csr_matrix, diags, lil_matrix
from scipy.special import kv

from tetrax.common.typing import GeometryType


def sort_with_index(arr):
    # Create an index array that contains the original indices
    index_array = np.argsort(arr)

    # Use the index array to get the sorted array
    sorted_array = arr[index_array]

    return sorted_array, index_array


def get_matrices1D(xyz, ijk, bijk):
    """
    Creating the sparse matrices based on the weak formulation.

    Parameters
    ----------
    xyz : coordinates of the nodes (np.array, shape = (NumONod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOElem,2))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of the left/right nodes associated with the element
                (indexing starting with 0)
    bijk : boundary finite elements arranged in matrix form (np.array, shape = (NumOElem,1))
            bijk[num,:] = [P1, region]
                P1 index of the node
                (indexing starting with 0)

    NumOElm : total number of finite elements
    NumONode : total number of magnetic nodes

    Returns
    -------
    boundary_nodes : integer, array
            list of boundary nodes
    normal_vectors : float, ndarray
            normal vectors of the boundary nodes
    VolNod : float, array
            volume of the nodes - the so-called Wigner-Seitz cell volume
    VolNod : float, array
            volume of the nodes - the so-called Wigner-Seitz cell volume
    solid_angles : float, ndarray
            solid angles of the boundary nodes
    boundary_elements : integer, ndarray
            boundary finite elements, same as bijk, but determined by the function
            bijk is from meshio/pygmsh
    poiss : CSR matrix, float
            sparse matrix for the computation of the first magnetostatic potential
    lap : CSR matrix, float
            sparse matrix for the computation of the second magnetostatic potential
    div_x, div_y, div_z : CSR matrices
            sparse matrices to compute divergences in FEM
    grad_x, grad_y, grad_z : CSR matrices
            sparse matrices to compute gradients in FEM

    """
    nx = xyz.shape[0]
    VolElem, VolNod, dNa = CalcdNa(xyz, ijk)
    boundary_elements, cnt = np.unique(
        np.array([ijk[:, 0], ijk[:, 1]]).flatten(), return_counts=True
    )
    #    cnt = bijk.shape[0]
    boundary_nodes = boundary_elements[cnt == 1]
    # boundary_nodes = bijk.flatten()
    div_x = csr_matrix((nx, nx))  # empty matrix
    div_y = -CalcDiv_y(VolNod, VolElem, dNa, ijk)  # Minus hinzugefuegt
    div_z = csr_matrix((nx, nx))  # empty matrix
    poiss = CalcPoiss(
        VolNod, VolElem, ijk, boundary_nodes, dNa
    )  # dim =1, DirichletBC = False
    lap = CalcPoiss(
        VolNod, VolElem, ijk, boundary_nodes, dNa, DirichletBC=True
    )  # dim = 1
    grad_x = csr_matrix((nx, nx))  # empty matrix
    grad_y = CalcGrad_y(VolNod, VolElem, dNa, ijk)
    grad_z = csr_matrix((nx, nx))  # empty matrix
    sort_indices = np.argsort(xyz[boundary_nodes, 1])
    normal_vectors = 1.0 * np.ones_like(boundary_nodes)
    normal_vectors[sort_indices[::2]] *= -1.0
    solid_angles = None
    # belm = ijk[sort_indices,:]
    return (
        boundary_nodes,
        normal_vectors,
        VolNod,
        VolNod,
        solid_angles,
        boundary_elements,
        poiss,
        lap,
        div_x,
        div_y,
        div_z,
        grad_x,
        grad_y,
        grad_z,
    )


def get_matrices1D_radial(xyz, ijk, bijk):
    """
    Creating the sparse matrices based on the weak formulation.

    Parameters
    ----------
    xyz : coordinates of the nodes (np.array, shape = (NumONod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOElem,3))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of left/right point associated with element
                (indexing starting with 0)

    NumOElm : total number of finite elements
    NumONode : total number of magnetic nodes

    Returns
    -------
    boundary_nodes : integer, array
            list of boundary nodes
    normal_vectors : float, ndarray
            normal vectors of the boundary nodes
    VolNod : float, array
            volume of the nodes
    VolNodWY : float, array
            volume of the nodes - the so-called Wigner-Seitz cell volume
    solid_angles : float, ndarray
            solid angles of the boundary nodes
    boundary_elements : integer, ndarray
            boundary finite elements, same as bijk, but determined by the function
            bijk is from meshio/pygmsh
    poiss : CSR matrix, float
            sparse matrix for the computation of the first magnetostatic potential
    lap : CSR matrix, float
            sparse matrix for the computation of the second magnetostatic potential
    div_x, div_y, div_z : CSR matrices
            sparse matrices to compute divergences in FEM
    grad_x, grad_y, grad_z : CSR matrices
            sparse matrices to compute gradients in FEM

    """
    nx = xyz.shape[0]
    rho = xyz.to_flattened().x
    rho_copy = rho.copy()
    VolElem, VolNodWZ, dNa = CalcdNa(xyz, ijk)
    belm, cnt = np.unique(
        np.array([ijk[:, 0], ijk[:, 1]]).flatten(), return_counts=True
    )
    #    cnt = bijk.shape[0]
    boundary_nodes = belm[cnt == 1]
    sort_indices = np.argsort(rho[boundary_nodes])
    normal_vectors = 1.0 * np.ones_like(boundary_nodes)
    normal_vectors[sort_indices[::2]] *= -1.0
    #    boundary_nodes = bijk.flatten()
    if np.any(rho == 0):
        rho_index = np.argsort(rho)
        boundary_nodes = np.delete(boundary_nodes, boundary_nodes == rho_index[0])
        sort_indices = np.argsort(rho[boundary_nodes])
        normal_vectors = 1.0 * np.ones_like(boundary_nodes)
        normal_vectors[sort_indices[1::2]] *= -1.0
        rho_copy[rho_index[0]] = rho[rho_index[1]] / 2

    div_rho = -CalcDiv_y(VolNodWZ, VolElem, dNa, ijk)  # Minus hinzugefuegt
    div_phi = csr_matrix((nx, nx))  # empty matrix
    div_z = csr_matrix((nx, nx))  # empty matrix

    poisson_matrix = CalcPoiss(
        VolNodWZ, VolElem, ijk, boundary_nodes, dNa
    ) + CalcGrad_rho(
        VolNodWZ, VolElem, boundary_nodes, dNa, ijk, rho, DirichletBC=False
    )

    laplace_matrix = CalcPoiss(
        VolNodWZ, VolElem, ijk, boundary_nodes, dNa, DirichletBC=True
    ) + CalcGrad_rho(VolNodWZ, VolElem, boundary_nodes, dNa, ijk, rho, DirichletBC=True)

    grad_rho = CalcGrad_y(VolNodWZ, VolElem, dNa, ijk)
    grad_phi = csr_matrix((nx, nx))  # empty matrix
    grad_z = csr_matrix((nx, nx))  # empty matrix

    solid_angle = None
    VolNod = VolNodWZ * rho_copy

    return (
        boundary_nodes,
        normal_vectors,
        VolNod,
        VolNodWZ,
        solid_angle,
        belm,
        poisson_matrix,
        laplace_matrix,
        div_rho,
        div_phi,
        div_z,
        grad_rho,
        grad_phi,
        grad_z,
    )


def ExchangeOperator1D(xyz, ijk, bijk, Aex, geometry_type):
    """
    Creates the exchange sparse matrix based on the weak formulation.

    Parameters
    ----------
    xyz : coordinates of the nodes (np.array, shape = (NumONod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOElem,2))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of the left/right nodes associated with the element
                (indexing starting with 0)
    bijk : boundary finite elements arranged in matrix form (np.array, shape = (NumOElem,1))
            bijk[num,:] = [P1, region]
                P1 index of the node
                (indexing starting with 0)
    Aex : float, array
            exchange stiffness associated to the nodes
    not_radial : boolean
            flag to decide if the exchange matrix should be computed for a layer or an axial waveguide

    NumOElm : total number of finite elements
    NumONode : total number of magnetic nodes

    Returns
    -------
    xc : CSR matrix, float
            exchange sparse matrix based on the weak formulation to compute exchange fields

    """
    VolElem, VolNod, dNa = CalcdNa(xyz, ijk)
    if not (
        geometry_type is GeometryType.WAVEGUIDE_AXIAL
        or geometry_type is GeometryType.CONFINED_AXIAL
    ):
        xc = CalcLaPl(VolNod, VolElem, ijk, bijk, dNa, Aex, dim=3)
    else:
        nx = xyz.shape[0]
        rho = xyz.to_flattened().x
        rho_index = np.argsort(rho)
        if rho[rho_index[0]] == 0:
            rho_corrected = rho.copy()
            rho_corrected[rho_index[0]] = rho[rho_index[1]] / 2
            per_rho = 1 / rho_corrected
            xc_lapl = CalcLaPl(
                VolNod, VolElem, ijk, bijk, dNa, Aex, dim=1
            ) + CalcGrad_rho_exc(
                VolNod, VolElem, bijk, dNa, ijk, rho, Aex, DirichletBC=False
            )
            #            xc_lapl_z = CalcLaPl(VolNod, VolElem, ijk, bijk, dNa, Aex, DirichletBC=True, dim=1) + \
            #                        CalcGrad_rho_exc(VolNod, VolElem, bijk, dNa, ijk, rho, Aex, DirichletBC=True)
            xc = bmat(
                [[xc_lapl, None, None], [None, xc_lapl, None], [None, None, xc_lapl]]
            ) - bmat(
                [
                    [diags(Aex * per_rho**2), None, None],
                    [None, diags(Aex * per_rho**2), None],
                    [None, None, diags(np.zeros(nx))],
                ]
            )
        else:
            per_rho = 1 / rho
            xc_lapl = CalcLaPl(
                VolNod, VolElem, ijk, bijk, dNa, Aex, dim=1
            ) + CalcGrad_rho_exc(
                VolNod, VolElem, bijk, dNa, ijk, rho, Aex, DirichletBC=False
            )
            xc = bmat(
                [[xc_lapl, None, None], [None, xc_lapl, None], [None, None, xc_lapl]]
            ) - bmat(
                [
                    [diags(Aex * per_rho**2), None, None],
                    [None, diags(Aex * per_rho**2), None],
                    [None, None, diags(np.zeros(nx))],
                ]
            )
    return -xc


def CalcVol(xyz, ijk):
    """
    Computes the volumes of the elements and nodes.

    Parameters
    ----------
    xyz : coordinates of the nodes (np.array, shape = (NumOfNod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of left/right point associated with element
                (indexing starting with 0)

    Returns
    -------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))

    """
    VolElem = np.zeros(ijk.shape[0])
    P1 = ijk[:, 0].astype(int)
    P2 = ijk[:, 1].astype(int)
    VolElem = np.sqrt(np.sum((xyz[P1, :] - xyz[P2, :]) ** 2, axis=1))

    VolNod = np.zeros(xyz.shape[0])
    for cnt in range(len(VolElem)):
        VolNod[ijk[cnt, 0]] += VolElem[cnt] / 2
        VolNod[ijk[cnt, 1]] += VolElem[cnt] / 2

    return VolElem, VolNod


def CalcdNa(xyz, ijk):
    """
    Computes the derivatives of the Shape functions.

    Parameters
    ----------
    xyz : coordinates of the nodes (np.array, shape = (NumOfNod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of left/right point associated with element
                (indexing starting with 0)

    dNa - derivative of the shape-function defined on the elements
            (np.array, shape = (2,NumOElem))
            dNa[0,num]  = derivative of the shapefunction in the
                        left point of the num element
            dNa[1, num] = derivative of the shapefunction in the
                        right point of the element

    Returns
    -------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    dNa : float, ndarray
            derivative of the shape-function defined on the elements

    """
    VolElem, VolNod = CalcVol(xyz, ijk)
    dNa = np.array([-1 / VolElem, 1 / VolElem])
    return VolElem, VolNod, dNa


def CalcLaPl(VolNod, VolElem, ijk, boundary_nodes, dNa, Aex, DirichletBC=False, dim=1):
    """
    Calculates the 2nd derivative in the y-direction as a sparse matrix in
    csr-format according to FEM, thus containing Neumann-boundary conditions.
    If DirichletBC = True, then the matrix is changed, so that it contains a 1
    at Lapl[bn,bn] as only entry in the row bn (for bn in BoundaryNodes),
    meaining the matrix can be used to solve an equation with Dirichlet-
    boundary-conditions.

    Parameters
    ----------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
            P1/P2 index of left/right point associated with element
            (indexing starting with 0)
    boundary_nodes : integer, array
            list of boundary nodes
    dNa : float, ndarray
            derivative of the shape-function defined on the elements
    Aex : float, array
            exchange stiffness associated to the nodes
    DirichletBC : boolean
            currently not yet active
            boundary condition for the axial waveguide exchange interaction for the rho=0 node
    dim : integer
            the dimension of the stiffness matrix, dim=1 is the default
            3 means for all x, y, and z components makes the copy of the dim=1 matrix

    Returns
    -------
    LaPL : CSR Matrix
            exchange stiffness matrix in the classical FEM formulation

    """
    if isinstance(Aex, (float, int)):
        Aex_ = np.full_like(VolNod, Aex)
    elif Aex.shape[0] != VolNod.shape[0]:
        print(
            "Something wrong with the length of the exchange constant scalar field."
        )
        sys.exit()
    else:
        Aex_ = Aex

    n = VolNod.size
    LaPL = lil_matrix((dim * n, dim * n), dtype=float)

    for cnt in range(dim):
        for nod in range(n):
            LElem = np.where(ijk[:, 1] == nod)[0]
            RElem = np.where(ijk[:, 0] == nod)[0]

            L = 0
            M = 0
            R = 0
            for ind in LElem:
                inx0 = ijk[ind, 0]
                inx1 = ijk[ind, 1]
                L = (
                    -(Aex_[inx0] * VolNod[inx0] + Aex_[inx1] * VolNod[inx1])
                    / (VolNod[inx0] + VolNod[inx1])
                    * VolElem[ind]
                    * dNa[0, ind]
                    * dNa[1, ind]
                    / VolNod[nod]
                )
                M += -Aex_[nod] * VolElem[ind] * dNa[1, ind] ** 2 / VolNod[nod]
            for ind in RElem:
                inx0 = ijk[ind, 0]
                inx1 = ijk[ind, 1]
                R = (
                    -(Aex_[inx0] * VolNod[inx0] + Aex_[inx1] * VolNod[inx1])
                    / (VolNod[inx0] + VolNod[inx1])
                    * VolElem[ind]
                    * dNa[0, ind]
                    * dNa[1, ind]
                    / VolNod[nod]
                )
                M += -Aex_[nod] * VolElem[ind] * dNa[0, ind] ** 2 / VolNod[nod]

            if L != 0:
                LaPL[nod + cnt * n, int(ijk[LElem[0], 0] + cnt * n)] = L
            LaPL[nod + cnt * n, nod + cnt * n] = M
            if R != 0:
                LaPL[nod + cnt * n, int(ijk[RElem[0], 1] + cnt * n)] = R

    if DirichletBC:
        LaPL[0, :] /= 2
        LaPL[0, 0] = 0

    return LaPL.tocsr()


def CalcPoiss(VolNod, VolElem, ijk, boundary_nodes, dNa, DirichletBC=False, dim=1):
    """
    Calculates the 2nd derivative in the y-direction as a sparse matrix in
    csr-format according to FEM, thus containing Neumann-boundary conditions.
    If DirichletBC = True, then the matrix is changed, so that it contains a 1
    at Lapl[bn,bn] as only entry in the row bn (for bn in BoundaryNodes),
    meaining the matrix can be used to solve an equation with Dirichlet-
    boundary-conditions.

    Parameters
    ----------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
            P1/P2 index of left/right point associated with element
            (indexing starting with 0)
    boundary_nodes : integer, array
            list of boundary nodes
    dNa : float, ndarray
            derivative of the shape-function defined on the elements
    Aex : float, array
            exchange stiffness associated to the nodes
    DirichletBC : boolean
            turn on and off the Dirichlet boundary conditions for the different magnetostatic potential matrices
    dim : integer
            the dimension of the stiffness matrix, dim=1 is the default
            3 means for all x, y, and z components makes the copy of the dim=1 matrix

    Returns
    -------
    LaPL : CSR Matrix
            Poisson stiffness matrix in the classical FEM formulation

    """
    n = VolNod.size
    LaPL = lil_matrix((dim * n, dim * n), dtype=float)

    for cnt in range(dim):
        for nod in range(n):
            if np.any(boundary_nodes == nod) and DirichletBC:
                LaPL[nod + cnt * n, nod + cnt * n] = 1

            else:
                LElem = np.where(ijk[:, 1] == nod)[0]
                RElem = np.where(ijk[:, 0] == nod)[0]

                L = 0
                M = 0
                R = 0
                for ind in LElem:
                    L = -VolElem[ind] * dNa[0, ind] * dNa[1, ind]
                    M += -VolElem[ind] * dNa[1, ind] ** 2
                for ind in RElem:
                    R = -VolElem[ind] * dNa[0, ind] * dNa[1, ind]
                    M += -VolElem[ind] * dNa[0, ind] ** 2

                if L != 0 and not (
                    np.any(boundary_nodes == ijk[LElem[0], 0]) and DirichletBC
                ):
                    LaPL[nod + cnt * n, int(ijk[LElem[0], 0] + cnt * n)] = L

                LaPL[nod + cnt * n, nod + cnt * n] = M

                if R != 0 and not (
                    np.any(boundary_nodes == ijk[RElem[0], 1]) and DirichletBC
                ):
                    LaPL[nod + cnt * n, int(ijk[RElem[0], 1] + cnt * n)] = R

    return LaPL.tocsr()


def CalcDiv_y(VolNod, VolElem, dNa, ijk):
    """
    Calculates the transposed matrix of the 1st derivative in y-direction as
    a sparse matrix in csr-format according to FEM and Attila (without the
    Volume associated to the nodes).

    Parameters
    ----------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    dNa : float, ndarray
            derivative of the shape-function defined on the elements
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
            P1/P2 index of left/right point associated with element
            (indexing starting with 0)

    Returns
    -------
    Div_y : CSR matrix
            returns a CSR matrix used to compute divergences along the y coordinate

    """
    n = VolNod.size
    Div_y = lil_matrix((n, n), dtype=float)

    for nod in range(n):
        LElem = np.where(ijk[:, 1] == nod)[0]
        RElem = np.where(ijk[:, 0] == nod)[0]

        L = 0
        M = 0
        R = 0
        for ind in LElem:
            L = dNa[0, ind] * VolElem[ind] / 2
            M += dNa[1, ind] * VolElem[ind] / 2

        for ind in RElem:
            R = dNa[1, ind] * VolElem[ind] / 2
            M += dNa[0, ind] * VolElem[ind] / 2

        if L != 0:
            Div_y[nod, ijk[LElem[0], 0]] = L
        Div_y[nod, nod] = M
        if R != 0:
            Div_y[nod, ijk[RElem[0], 1]] = R

    return Div_y.tocsr().T


def CalcGrad_y(VolNod, VolElem, dNa, ijk):
    """
    Calculates the 1st derivative in the y-direction as a sparse matrix in
    csr-format according to FEM, includes the volume associated to the nodes.

    Parameters
    ----------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    dNa : float, ndarray
            derivative of the shape-function defined on the elements
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
            P1/P2 index of left/right point associated with element
            (indexing starting with 0)

    Returns
    -------
    Grad_y : CSR matrix
            returns a CSR matrix used to compute gradients along the y coordinate

    """
    n = VolNod.size
    Grad_y = lil_matrix((n, n), dtype=float)

    for nod in range(n):
        LElem = np.where(ijk[:, 1] == nod)[0]
        RElem = np.where(ijk[:, 0] == nod)[0]

        L = 0
        M = 0
        R = 0
        for ind in LElem:
            L = dNa[0, ind] * VolElem[ind] / (2 * VolNod[nod])
            M += dNa[1, ind] * VolElem[ind] / (2 * VolNod[nod])
        for ind in RElem:
            R = dNa[1, ind] * VolElem[ind] / (2 * VolNod[nod])
            M += dNa[0, ind] * VolElem[ind] / (2 * VolNod[nod])

        if L != 0:
            Grad_y[nod, ijk[LElem[0], 0]] = L
        Grad_y[nod, nod] = M
        if R != 0:
            Grad_y[nod, ijk[RElem[0], 1]] = R

    return Grad_y.tocsr()


def CalcGrad_rho_exc(
    VolNod, VolElem, boundary_nodes, dNa, ijk, rho, Aex, DirichletBC=False
):
    """
    Calculates the 1st derivative in the rho-direction as a sparse matrix in
    csr-format according to FEM, includes the volume associated to the nodes
    and the averaged exhange stiffnesses.

    Parameters
    ----------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    boundary_nodes : integer, array
            list of boundary nodes
    dNa : float, ndarray
            derivative of the shape-function defined on the elements
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
            P1/P2 index of left/right point associated with element
            (indexing starting with 0)
    rho : float, array
            rho coordinates
    Aex : float, array
            exchange stiffness of the nodes
    DirichletBC : boolean
            Dirichlet boundary conditions flag, currently not supported

    Returns
    -------
    Grad_rho : CSR matrix
            returns a CSR matrix used to compute gradients along the rho coordinate for the exchange interaction

    """
    if isinstance(Aex, (float, int)):
        Aex_ = np.full_like(VolNod, Aex)
    elif Aex.shape[0] != VolNod.shape[0]:
        print("Something wrong with the length of the exchange constant scalar field.")
        sys.exit()
    else:
        Aex_ = Aex

    n = VolNod.size
    Grad_rho = lil_matrix((n, n), dtype=float)

    rho_index = np.argsort(rho)
    if rho[rho_index[0]] == 0:
        rho_corrected = rho.copy()
        rho_corrected[rho_index[0]] = rho[rho_index[1]] / 2
        per_rho = 1 / rho_corrected
    else:
        per_rho = 1 / rho

    for nod in range(n):
        LElem = np.where(ijk[:, 1] == nod)[0]
        RElem = np.where(ijk[:, 0] == nod)[0]

        L = 0
        M = 0
        R = 0
        for ind in LElem:
            inx0 = ijk[ind, 0]
            inx1 = ijk[ind, 1]
            L = (
                (Aex_[inx0] * VolNod[inx0] + Aex_[inx1] * VolNod[inx1])
                / (VolNod[inx0] + VolNod[inx1])
                * dNa[0, ind]
                * VolElem[ind]
                / (2 * VolNod[nod])
                * per_rho[nod]
            )
            M += (
                Aex_[nod]
                * dNa[1, ind]
                * VolElem[ind]
                / (2 * VolNod[nod])
                * per_rho[nod]
            )
        for ind in RElem:
            inx0 = ijk[ind, 0]
            inx1 = ijk[ind, 1]
            R = (
                (Aex_[inx0] * VolNod[inx0] + Aex_[inx1] * VolNod[inx1])
                / (VolNod[inx0] + VolNod[inx1])
                * dNa[1, ind]
                * VolElem[ind]
                / (2 * VolNod[nod])
                * per_rho[nod]
            )
            M += (
                Aex[nod] * dNa[0, ind] * VolElem[ind] / (2 * VolNod[nod]) * per_rho[nod]
            )

        if L != 0 and not (np.any(boundary_nodes == ijk[LElem[0], 0]) and DirichletBC):
            # if L != 0:
            Grad_rho[nod, ijk[LElem[0], 0]] = L

        Grad_rho[nod, nod] = M

        if R != 0 and not (np.any(boundary_nodes == ijk[RElem[0], 1]) and DirichletBC):
            # if R != 0:
            Grad_rho[nod, ijk[RElem[0], 1]] = R

    return Grad_rho.tocsr()


def CalcGrad_rho(VolNod, VolElem, boundary_nodes, dNa, ijk, rho, DirichletBC=False):
    """
    Calculates the 1st derivative in the rho-direction as a sparse matrix in
    csr-format according to FEM, includes the volume associated to the nodes.

    Parameters
    ----------
    VolElem : float, array
            Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : float, array
            Volume/length associated with the nods (np.array, shape = (NumONod))
    boundary_nodes : integer, array
            list of boundary nodes
    dNa : float, ndarray
            derivative of the shape-function defined on the elements
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
            P1/P2 index of left/right point associated with element
            (indexing starting with 0)
    rho : float, array
            rho coordinates
    DirichletBC : boolean
            Dirichlet boundary conditions flag

    Returns
    -------
    Grad_rho : CSR matrix
            returns a CSR matrix used to compute gradients along the rho coordinate

    """
    n = VolNod.size
    Grad_rho = lil_matrix((n, n), dtype=float)

    rho_index = np.argsort(rho)
    if rho[rho_index[0]] == 0:
        rho_corrected = rho.copy()
        rho_corrected[rho_index[0]] = rho_corrected[rho_index[1]] / 2
        per_rho = 1 / rho_corrected
    else:
        per_rho = 1 / rho

    for nod in range(n):
        LElem = np.where(ijk[:, 1] == nod)[0]
        RElem = np.where(ijk[:, 0] == nod)[0]
        L = 0
        M = 0
        R = 0
        for ind in LElem:
            L = dNa[0, ind] * VolElem[ind] * per_rho[nod] / 2
            M += dNa[1, ind] * VolElem[ind] * per_rho[nod] / 2
        for ind in RElem:
            R = dNa[1, ind] * VolElem[ind] * per_rho[nod] / 2
            M += dNa[0, ind] * VolElem[ind] * per_rho[nod] / 2

        if L != 0 and not (np.any(boundary_nodes == ijk[LElem[0], 0]) and DirichletBC):
            Grad_rho[nod, ijk[LElem[0], 0]] = L

        Grad_rho[nod, nod] = M

        if R != 0 and not (np.any(boundary_nodes == ijk[RElem[0], 1]) and DirichletBC):
            Grad_rho[nod, ijk[RElem[0], 1]] = R

    return Grad_rho.tocsr()


def ComputeDenseMatrix_1D(
    dense_matrix,
    boundary_elmements,
    boundary_nodes,
    xyz_coordinates,
    normal_vectors,
    solid_angles,
    boundary_integration_order,
    k,
    m,
):
    """
    Calculates the dense Matrix needed for the boundary conditions of psi2 in
    the calculation of the magnetostatic potential.

    Parameters
    ----------
    dense_matrix : float, ndarray
            the dense matrix itself
    boundary_elements : integer, ndarray
            boundary finite elements arranged in matrix form (np.array, shape = (NumOElem,1))
    boundary_nodes : integer, array
            list of boundary nodes
    xyz_coordinates : float, ndarray
            coordinates of the nodes (np.array, shape = (NumberOfNodes, 3))
    normal_vectors : float, ndarray
            normal vectors of the boundary nodes
    solid_angles : float, ndarray
            solid angles of the boundary nodes
    boundary_integration_order : integer
            here not used
    k : float
            wave vector in the propagation direction
    m : integer
            azimuthal mode index

    Returns
    -------
    dense_matrix : float, ndarray
            in-place return of the recalculated dense matrix

    """
    y = xyz_coordinates[:, 1]
    bn = len(boundary_nodes)
    dense_matrix = np.reshape(dense_matrix, (bn, bn))

    for i in range(bn):
        for j in range(bn):
            if i != j:
                kT = abs(k) * abs(y[boundary_nodes[i]] - y[boundary_nodes[j]])
                dense_matrix[i, j] = (
                    1
                    / 2
                    * np.exp(-kT)
                    * normal_vectors[j]
                    * np.sign(y[boundary_nodes[i]] - y[boundary_nodes[j]])
                )
            else:
                dense_matrix[i, j] = -1 / 2
    dense_matrix = np.reshape(dense_matrix, (bn * bn))
    return 0


def E(m, k, rho, rho_prime, num_phi=1000):
    """Elliptic integral which enters in the Dirichlet matrix in the Fredkin-Koehler method for tubular systems."""
    # Consider some special cases here first, which can be solved analytically.

    if (k == m == 0) and rho == rho_prime:
        return -np.pi / (2 * rho)

    if (k == 0 and m != 0) and rho == rho_prime:
        return 0

    phi = np.linspace(0, np.pi, num_phi, endpoint=True)
    integrand = np.zeros(num_phi)

    if k != 0 and rho == rho_prime:
        start_from = 1
        integrand[0] = -1 / (2 * rho)

    else:
        start_from = 0

    cos_m_phi = np.cos(m * phi[start_from:])
    n_dot_rho = rho * np.cos(phi[start_from:]) - rho_prime
    delta_rho = np.sqrt(
        rho**2 + rho_prime**2 - 2 * rho * rho_prime * np.cos(phi[start_from:])
    )

    integrand[start_from:] = (
        (cos_m_phi * n_dot_rho / delta_rho**2)
        if k == 0
        else (
            (cos_m_phi * n_dot_rho / delta_rho)
            * np.abs(k)
            * kv(1, np.abs(k) * delta_rho)
        )
    )

    return np.trapz(integrand, phi)


def ComputeDenseMatrix_1D_radial(
    dense_matrix,
    boundary_elmements,
    boundary_nodes,
    xyz_coordinates,
    normal_vectors,
    solid_angles,
    boundary_integration_order,
    k,
    m,
):
    """
    Calculates the Dirichlet boundary conditions in the form of a dense Matrix
    needed for the computation of chi2 in the calculation of the
    magnetostatic potential of a tubular system.

    Parameters
    ----------
    dense_matrix : float, ndarray
            the dense matrix itself
    boundary_elements : integer, ndarray
            boundary finite elements arranged in matrix form (np.array, shape = (NumOElem,1))
    boundary_nodes : integer, array
            list of boundary nodes
    xyz_coordinates : float, ndarray
            coordinates of the nodes (np.array, shape = (NumberOfNodes, 3))
    normal_vectors : float, ndarray
            normal vectors of the boundary nodes
    solid_angles : float, ndarray
            solid angles of the boundary nodes
    boundary_integration_order : integer
            here not used
    k : float
            wave vector in the propagation direction
    m : integer
            azimuthal mode index

    Returns
    -------
    dense_matrix : float, ndarray
            in-place return of the recalculated dense matrix

    """
    x = xyz_coordinates.x
    bn = len(boundary_nodes)
    dense_matrix = np.reshape(dense_matrix, (bn, bn))
    for i in range(bn):
        rho = x[boundary_nodes[i]]
        for j in range(bn):
            rho_prime = x[boundary_nodes[j]]
            dense_matrix[i, j] = (
                normal_vectors[j] * rho_prime / np.pi * E(m, k, rho, rho_prime)
            )
            if i == j:
                dense_matrix[i, j] -= 1 / 2

    dense_matrix = np.reshape(dense_matrix, (bn * bn))

    return 0
