//
// Created by Attila Kákay on 14.04.21.
//
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "dense2D_onthefly.h"
#include "bessel_K1.h"
#include "matutils.h"
#include "integration_of_functions.h"
//#include "utils.h"

/*----------------------------------------------------------------------*/
void ComputeDenseMatrix_kdep_py(int nx, int *bijk, int bnx, int bnelx,
                                int *bnlist, double *xyz, double *nv,
                                double *solid_angle, int op, double k, int m,
                                double *D2Dk)
// nx - total number of magnetic nodes
// bijk[2][bnelx] - boundary element connectivity
// bnx - number of boundary nodes
// bnelx - number of boundary elements
// bnlist[bnx] - lost of boundary nodes
// xyz[3*nx] - x,y,z coordinates of the mesh nodes
// nv[2*bnelx] - normal vectors of the boundary elements, normalized to the length
// pang[bnx] - plane angle of a boundary node
// op - the order of segmentation or higher order boundary element division
// k - the wave vector
// D2Dk[bnx*bnx] - the computed k dependent dense matrix

{
    int i, bel, is_radial=0;
    int cbn, bn1, bn2, lst1, lst2;
    int *n2bn;
    double L, nv_norm[2], wg[2];

    n2bn = (int *) malloc(nx*sizeof(int));
    if(!n2bn) {
        printf("Allocation error in densematrix.c (n2bn[nx])");
        exit(0);
    }
    IniIVector(n2bn,nx,0);
    IniDVector(D2Dk, bnx*bnx, 0.0);
/* The solid angles will contribute to the diagonals */
//    #pragma omp parallel for private(j)

    for(i=0; i<bnx; i++) {
        D2Dk[i*(bnx+1)] = solid_angle[i];
        n2bn[bnlist[i]] = i;
    }


    // The magic should happen here
    for(i=0; i<bnx; i++) {
        cbn = bnlist[i];
        for (bel = 0; bel<bnelx; bel++) {
            bn1 = bijk[2*bel];
            bn2 = bijk[2*bel+1];
            lst1 = n2bn[bn1];
            lst2 = n2bn[bn2];
            L = vectabs2D_py(nv + 2 * bel);
            nv_norm[0] = nv[2 * bel] / L;
            nv_norm[1] = nv[2 * bel + 1] / L;
            wg[0] = 0.0;
            wg[1] = 0.0;
            HOBoundaryElm2D_py(xyz+3*cbn, xyz+3*bn1, xyz+3*bn2,nv_norm,L,wg,op,k,m,is_radial);
            D2Dk[i * bnx + lst1] += wg[0];
            D2Dk[i * bnx + lst2] += wg[1];
        }
    }

    free(n2bn);
}
/* END void ComputeDenseMatrix_kdep */
/*----------------------------------------------------------------------*/
void LinSplit_py(int *ijk, int **ijk_n, double *xyz, double **w, int *nc)
/*
     A linear element 1-2 is subdivided into two elements:

       1------- 3 ------ 2

 first, it should be checked whether the new nodes have already been
 created.

 the weight "w" has a linear dependence on the distance from the node in focus
 w[nodes][element]

 the new coordinates xyz[3*nx] are in the middle of the boundary element
*/
{
    int cn[3], i;

    cn[0] = ijk[0];
    cn[1] = ijk[1];
    cn[2] = *nc;
    *nc += 1;

    /* Element I */
    ijk_n[0][0] = cn[0];
    ijk_n[1][0] = cn[2];
    /* Element 2 */
    ijk_n[0][1] = cn[2];
    ijk_n[1][1] = cn[1];

    for(i=0;i<2;i++) {
        xyz[3*cn[2]+i] = 0.5*(xyz[3*cn[0]+i]+xyz[3*cn[1]+i]);
        w[i][cn[2]] = 0.5*(w[i][cn[0]]+w[i][cn[1]]);
    }
}
/*----------------------------------------------------------------------*/
void HOBoundaryElm2D_py(double *x0, double *x1, double *x2, double *nv,
                     double L, double *wg, int op, double kwave, int m,
                     int is_radial)
{
    /*
      input:
      int op  -  is the requested order of segmentation
      segmentation order, o_p = 0 -> 1 element, o_p = 1 -> 2 element ...
      2^op number of elements
      2^op + 1 number of nodes
      x0 -  coordinates of the observation point
      x1, x2 -  coordinates of the original corners of the boundary element
      L - Length of the initial element
      nv - normal vector of the original boundary element

      output:
      double *wg - global weights to perform a high order integration
    */
    int nelx, nx;
    double *xyz, **w, Ln, X0[3];
    int **ijk, **ijk_n, ijk_tt[2];
    int i,j,k, it, oc, nc, nelxc, nelxn;

    // int nel_f - expected number of elements and nodes
    nelx = pow(2,op);
    nx = nelx + 1;

// expected node coordinates
    xyz = (double *) malloc((size_t) 3*nx*sizeof(double));

    // the boundary element in focus of splitting
    ijk_n = IMatrix(2,2);

    // all boundary elements after splitting
    ijk = IMatrix(2,nelx);

    // weights
    w = DMatrix(2,nx);

    /* define initial boundary element */
    if (is_radial) {
        X0[0] = x0[0];
        X0[1] = x0[2];
        X0[2] = x0[1];
        xyz[0] = x1[0];
        xyz[1] = x1[2];
        xyz[2] = x1[1];
        xyz[3] = x2[0];
        xyz[4] = x2[2];
        xyz[5] = x2[1];
    } else
        for(i=0;i<3;i++) {
            xyz[i] = x1[i];
            xyz[3+i] = x2[i];
        }
    /* initialize weights */
    for(i=0;i<2;i++) {
        for(j=0;j<2;j++) w[i][j] = 0.0;
        w[i][i] = 1.0;
    }

    oc = 0; // current order of segmentation
    nc = 2; // initial number of nodes
    nelxc = 0; // current number of elements
    nelxn = 0; // current total number of elements

    // the initial boundary element
    for(i=0;i<2;i++) ijk[i][0] = i;

    do {
        for(i=0;i<=nelxn;i++) {
            for(it=0;it<2;it++) ijk_tt[it] = ijk[it][i];
            LinSplit_py(ijk_tt,ijk_n,xyz,w,&nc);
            for(j=0;j<2;j++) {
                ijk[j][i] = ijk_n[j][0];
                ijk[j][nelxc+1] = ijk_n[j][1];
            }
            nelxc++;
        }
        nelxn = nelxc;
        oc++;
    } while(oc < op);

    IniDVector(wg,2,0.0);
    Ln = L/(2.0*(double)(nelx));
/*
    if(kwave == 0.0)
        for(i=0;i<=nelxn;i++)
            for(j=0;j<2;j++)
                for(k=0;k<2;k++)
                    wg[j] += w[j][ijk[k][i]]*dGreendnv2D_py(x0,xyz+3*ijk[k][i],nv);

    else
*/
    for(i=0;i<=nelxn;i++)
        for(j=0;j<2;j++)
            for(k=0;k<2;k++)
                if(is_radial)
                    wg[j] += w[j][ijk[k][i]]*dGreendnvE(X0,xyz+3*ijk[k][i],nv,kwave,m);
                else
                    wg[j] += w[j][ijk[k][i]]*dGreendnv2Dkdep_py(x0,xyz+3*ijk[k][i],nv,kwave);

    for(j=0;j<2;j++) wg[j] *= Ln;

    free(xyz);
    FreeIMatrix(ijk_n);
    free(ijk);
    FreeDMatrix(w);
}
/*----------------------------------------------------------------------*/
double dGreendnvE(double *r, double *rp, double *nv, int k, int m) {
// Here we should do an integral over phi
    double integral;
    int N=10;
    integral =  qtrap(E_for_dense_axial, r, rp, nv, m, -PI, PI);
//    integral = E(r, rp, nv, 0.0, m);
    return(integral/(4*PI));
}
/*----------------------------------------------------------------------*/
/*
   This is the 2D Green function derivative dG/dn, where n is the
   normal vector to the edge
   The Green function is Green(r,r') = ln(|r-r'|)
   dG/dn = (r-r')n/|r-r'|^2
*/
double dGreendnv2D_py(double *r, double *rp, double *nv)
{
    double dr[2], absdr, small, dg = 0.0, d2PI = (1.0/(2*PI));

    small = 1.e-15;

    dr[0] = r[0] - rp[0];
    dr[1] = r[1] - rp[1];

    absdr = vectabs2D_py(dr);
    if(absdr > small) dg = 1.0/(absdr*absdr);
    return(dot_prod2D_py(dr,nv)*dg*d2PI);
}
/* End double dGreendnv2D */
/*----------------------------------------------------------------------*/
/*
   This is the k dependent 2D Green function derivative dG/dn,
   where n is the normal vector to the edge
   The Green function is the Bessel function

   dG/dnv = n nabla(G_k) = |k|/2pi K_1(|k||r-r`|) (r-r')n/|r-r'|
   In Latex:
   \vec{n}\cdot \nabla G_k =
   \frac{\vert k \vert}{2\pi} K_1(\vert k \vert \vert \vec{\varrho} -
   \vec{\varrho}' \vert) \frac{(\vec{\varrho} - \vec{\varrho}' )}{\vert \vec{\varrho} -
   \vec{\varrho}' \vert} \cdot \vec{n}
*/
double dGreendnv2Dkdep_py(double *r, double *rp, double *nv, double k)
{
    double dr[2], absdr, small, dg = 0.0, d2PI = (1.0/(2*PI));
    double kabs;
    small = 1.e-15;

    dr[0] = r[0] - rp[0];
    dr[1] = r[1] - rp[1];

    kabs = fabs(k);
    absdr = vectabs2D_py(dr);
    if (kabs == 0.0) {
        if(absdr > small) dg = 1.0/(absdr*absdr);
        return(dot_prod2D_py(dr,nv)*dg*d2PI);
    } else {
        if(absdr > small) dg = 1.0/(absdr);
        dg *= kabs*d2PI*BESSK1(kabs*absdr)*dot_prod2D_py(dr,nv);
        return(dg);
    }
}
/* End double dGreendnv2Dkdep */
/*----------------------------------------------------------------------*/
double dot_prod2D_py(double *a, double *b)
{
    return(a[0]*b[0] + a[1]*b[1]);
}
/* End double dot_prod(double *a, double *b)  */
/*----------------------------------------------------------------------*/
double vectabs2D_py(double *r)
{
    return(sqrt(r[0]*r[0]+r[1]*r[1]));
}
/* End double vectabs2D(double *r)  */
/*----------------------------------------------------------------------*/
double E_for_dense_axial(double *r, double *rp, double *nv,  double phi, int m)
{
    double a, b, c;
    double rho, rhop, z, zp, nr, nz;
    rho = r[0];
    rhop = rp[0];
    if (rho == rhop)
        return(0);
    z = r[1];
    zp = rp[1];
    nr = nv[0];
    nz = nv[1];
    a = rhop*cos(m*phi);
    b = nr*(rho*cos(phi)-rhop) + nz*(z-zp);
    c = sqrt((rho*rho + rhop*rhop - 2*rho*rhop*cos(phi)+(z-zp)*(z-zp)));
    return (a*b/(c*c*c));
}
/*----------------------------------------------------------------------*/
void ComputeDenseMatrix_kdep_radial(int nx, int *bijk, int bnx, int bnelx,
                                int *bnlist, double *xyz, double *nv,
                                double *solid_angle, int op, double k, int m,
                                double *D2Dk)
// nx - total number of magnetic nodes
// bijk[2][bnelx] - boundary element connectivity
// bnx - number of boundary nodes
// bnelx - number of boundary elements
// bnlist[bnx] - lost of boundary nodes
// xyz[3*nx] - x,y,z coordinates of the mesh nodes
// nv[2*bnelx] - normal vectors of the boundary elements, normalized to the length
// pang[bnx] - plane angle of a boundary node
// op - the order of segmentation or higher order boundary element division
// k - the wave vector
// D2Dk[bnx*bnx] - the computed k dependent dense matrix

{
    int i, bel, is_radial=1;
    int cbn, bn1, bn2, lst1, lst2;
    int *n2bn;
    double L, nv_norm[2], wg[2];

    n2bn = (int *) malloc(nx*sizeof(int));
    if(!n2bn) {
        printf("Allocation error in densematrix.c (n2bn[nx])");
        exit(0);
    }
    IniIVector(n2bn,nx,0);
    IniDVector(D2Dk, bnx*bnx, 0.0);
/* The solid angles will contribute to the diagonals */
//    #pragma omp parallel for private(j)

    for(i=0; i<bnx; i++) {
        D2Dk[i*(bnx+1)] = solid_angle[i];
        n2bn[bnlist[i]] = i;
    }


    // The magic should happen here
    for(i=0; i<bnx; i++) {
        cbn = bnlist[i];
        for (bel = 0; bel<bnelx; bel++) {
            bn1 = bijk[2*bel];
            bn2 = bijk[2*bel+1];
            lst1 = n2bn[bn1];
            lst2 = n2bn[bn2];
            L = vectabs2D_py(nv + 2 * bel);
            nv_norm[0] = nv[2 * bel] / L;
            nv_norm[1] = nv[2 * bel + 1] / L;
            wg[0] = 0.0;
            wg[1] = 0.0;
            HOBoundaryElm2D_py(xyz+3*cbn, xyz+3*bn1, xyz+3*bn2,nv_norm,L,wg,op,k,m,is_radial);
            D2Dk[i * bnx + lst1] += wg[0];
            D2Dk[i * bnx + lst2] += wg[1];
        }
    }

    free(n2bn);
}
/* END void ComputeDenseMatrix_kdep_radial */
/*----------------------------------------------------------------------*/