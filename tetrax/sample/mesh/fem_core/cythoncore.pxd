cdef extern from "akmag.h":
    ctypedef struct CSRSprMat:
        int nnz
        int inx
        int *csrRowPtr_h
        int *csrColIdx_h
        double *csrValues_h

cdef extern from "dense2D_onthefly.h":
    void ComputeDenseMatrix_kdep_py(
        int nx,
        int *belm,
        int bnx,
        int bnelx,
        int *bnlist,
        double *xyz,
        double *nv,
        double *solid_angle,
        int op,
        double k,
        int m,
        double *D2Dk
    )

cdef extern from "dense2D_onthefly.h":
    void ComputeDenseMatrix_kdep_radial(
        int nx,
        int *belm,
        int bnx,
        int bnelx,
        int *bnlist,
        double *xyz,
        double *nv,
        double *solid_angle,
        int op,
        double k,
        int m,
        double *D2Dk
    )

cdef extern from "dense3D.h":
    void ComputeDenseMatrix3D(
        int nx,
        int *belm,
        int bnx,
        int bnelx,
        int *bnlist,
        double *xyz,
        double *nv,
        double *pang,
        int op,
        double *D3D
    )

cdef extern from "rotation_mat.h":
    CSRSprMat rotation_matrix(
        double *m0_array[3],
        int nx
    )

cdef extern from "fempreproc.h":
    int OperatorsFromMesh2D(
        double *xyz,
        int *ijk,
        int nx,
        int nelx,
        int dim,
        int *bnlist,
        int *bnx,
        double *nv_carray,
        double *area_carray,
        CSRSprMat *poiss,
        CSRSprMat *lap,
        CSRSprMat *div_x,
        CSRSprMat *div_y,
        CSRSprMat *grad_x,
        CSRSprMat *grad_y,
        double *solid_angle,
        int *belm,
    )

cdef extern from "fempreproc.h":
    int OperatorsFromMesh2D_radial(
        double *xyz,
        int *ijk,
        int nx,
        int nelx,
        int dim,
        int *bnlist,
        int *bnx,
        double *nv_carray,
        double *area_carray,
        CSRSprMat *poiss,
        CSRSprMat *grad_poisson,
        CSRSprMat *lap,
        CSRSprMat *grad_laplace,
        CSRSprMat *div_x,
        CSRSprMat *div_y,
        CSRSprMat *grad_x,
        CSRSprMat *grad_y,
        double *solid_angle,
        int *belm
    )

cdef extern from "fempreproc.h":
    int OperatorsFromMesh3D(
        double *xyz,
        int *ijk,
        int nx,
        int nelx,
        int dim,
        int *bnlist,
        int *bnx,
        int *bnelx,
        double *nv_carray,
        double *nvolume_carray,
        CSRSprMat *poiss,
        CSRSprMat *lap,
        CSRSprMat *div_x,
        CSRSprMat *div_y,
        CSRSprMat *div_z,
        CSRSprMat *grad_x,
        CSRSprMat *grad_y,
        CSRSprMat *grad_z,
        double *solid_angle,
        int *belm
    )

cdef extern from "fempreproc.h":
    int ExchangeOperatorFromMesh2D(
        double *xyz,
        int *ijk,
        int nx,
        int nelx,
        int dim,
        double *Aex,
        CSRSprMat *xc,
        CSRSprMat *xc_grad,
        int not_radial
    )

cdef extern from "fempreproc.h":
    int ExchangeOperatorFromMesh3D(
        double *xyz,
        int *ijk,
        int nx,
        int nelx,
        int dim,
        double *Aex,
        CSRSprMat *xc
    )