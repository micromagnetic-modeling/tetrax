#ifndef AKVEC
#define AKVEC

#include <math.h>
#define PI (4.0*atan(1.0))
#define mu0 (4.0*PI*1.0e-7)

/*----------------------------------------------------------------------*/
typedef struct {
  char material[255]; /* name of the FM material */
  double dtet;
  double *A; /* exchange stiffness */
  double *Bs; /* saturation polarization */
  double *Ms; /* saturation magnetization */
  double Msat_avrg;
  double Didmi;
  double Dbulk;
  double alpha;
  double gamma;
  /* uniaxial anisotropy constants Ku1 & Ku2 and
     its direction thetaKu & phiKu */
  double Ku1;
  double Ku2;
  double thetaKu;
  double phiKu;
  char uanistype[1024];
  double *kx, *ky, *kz;
  /* surface anisotropy constant */
  double Ks;
  /* cubic anisotropy constants Kc1 & Kc2 and the direction vectors */
  double Kc1;
  double Kc2;
  double v1Kc1[3];
  double v2Kc1[3];
  double v3Kc1[3];
} MatParams;
/*----------------------------------------------------------------------*/
typedef struct
{
  double *x; /* x component of the AKMag arrays */
  double *y; /* y component of the AKMag arrays */
  double *z; /* z component of the AKMag arrays */
  double norm;
  long int ivs; /* degrees of freedom */
  long int lvs; /* the total lenght of the array lvs = 3 * ivs */
} AKVec;
/*----------------------------------------------------------------------*/
typedef struct
/*
   Row indexed storage sparse matrix structure - for more info please
   check the Numerical recipes in C
*/
{
  int ilx; /* the number of  matrix entries */
  int inx; /* the number of degrees of freedom */
  double *sa; /* non-zero matrix values */
  int *ija; /* row indices of the sparse matrix */
} RISSprMat;
/*----------------------------------------------------------------------*/
typedef struct
/*
   Row indexed storage sparse matrix structure - for more info please
   check the Numerical recipes in C
*/
{
    int nnz; /* the number of  matrix entries */
    int inx; /* the number of degrees of freedom */
    int *csrRowPtr_h;
    int *csrColIdx_h; /* column indices of the sparse matrix */
    double *csrValues_h; /* non-zero matrix values */
} CSRSprMat;
/*----------------------------------------------------------------------*/
typedef struct
{
    float *numbers; /* non-zero values */
    long long *linens; /* first element of each line */
    int *lineps; /* first pointer of each line */
    int *pointers; /* numbers of consecutive (non)zero elements */
    int nnum,np; /* number of numbers/pointers */
    int bnx; /* size of matrix */
} DenseMat;
/*----------------------------------------------------------------------*/
typedef struct
{
  RISSprMat xc; /* The exchange matrix */
  RISSprMat xct; /* The transposed exchange matrix */
  RISSprMat xc_expanded; /* The expanded exchange matrix */
  RISSprMat poiss; /* The Poisson matrix */
  RISSprMat lap; /* The Laplace matrix */
  RISSprMat ldx; /* The divergence/load matrix */
  RISSprMat ldy; /* The divergence/load matrix */
  RISSprMat ldz; /* The divergence/load matrix */
  RISSprMat phix; /* The gradient matrix */
  RISSprMat phiy; /* The gradient matrix */
  RISSprMat phiz; /* The gradient matrix */
  RISSprMat grad_rho_exc; /* The gradient matrix */
  RISSprMat grad_rho_exc_expanded; /* the full sized radial gradient matrix */
  RISSprMat grad_rho; /* The gradient matrix */
  RISSprMat grad_rho_DBC; /* the full sized radial gradient matrix */
  RISSprMat bdmix; /* The boundary gradient matrix */
  RISSprMat bdmiy; /* The boundary gradient matrix */
  RISSprMat bdmiz; /* The boundary gradient matrix */
  RISSprMat Ku; // Uniaxial anisotropy matrix
  RISSprMat DMIbulk;
  DenseMat D3D; /* The 3D dense matrix in EW compressed format */
  double *D2D; /* 2D dense matrix */
  double *DiagIWOBNX;
  int *bnlist;
// Uniaxial Anisotropy
  AKVec kunorm;
  double *ku1n;
  double *jn;
  double *voln;
  int nx;
  int bnx;
} OperatorMat;
/*----------------------------------------------------------------------*/
typedef struct
{
  double *xyz; /* the coordinates of the nodes */
  double *one_over_rho; /* the in-plane rho component in cylindrical coordinates */
  int **ijk; /* the 2D elements in a matrix format*/
  int *ijk_is_boundary; // the current element is or isn't a boundary element
  double *vol_el; /* volume of the elements */
  double *vol_n; /* volume of the nodes */
  double *dNa; /* gradient of the shape functions */
  double *nv; /* normal vectors of the boundary elements */
  double *area; /* area of the boundary elements */
  double *sub; /* solid angle of the nodes at boundary */
  int *num_el; /* the number of elements the node i is contained in */
  int *nel_list;
  int *pnel_list;
  int *nx_list;
  int *pnx_list;
  int psum;
  int adjsum;
  int bnx; /* the number of boundary nodes */
  int *bnlist; /* the list of indeces of the  boundary nodes */
  int *atbnd;
  int nx; /* number of nodes */
  long int nelx; /* number of elements */
  int bnelx; /* number of boundary elements */
  int dim; /* dimension of the simulations */
  int eld; /* dimension of the elements eld = dim +1 */
  int is_radial; /* boolean to indicate that it's radial / confined axial sample */
  double small;
  /* a small number, below which the SPM entries are considered to be
     zero  */
  double dtet; /* the scaling factor of the FEM mesh */
  int nrg; /* the number of grains or different types of FMs */
  MatParams *params; /* ferromagnetic material (FM) parameters */
  char dirname[1024]; /* directory path matrices and misc are saved */
  char gname[1024]; /* the project name */
  char ngname[1024]; /* project name with the directory path */
  char mshfname[1024];
  char sparam[1024];
  char eparam[1024];

} AKElm;
/*----------------------------------------------------------------------*/
typedef struct {
    AKVec Hanis;
    AKVec Hexch;
    AKVec Hdemag;
    AKVec Hext;
    AKVec Hcubic;
    AKVec Hdmi;
    AKVec Hidmi;
    AKVec Heff;
    AKVec B;
} AKFields;
/*----------------------------------------------------------------------*/
typedef struct{
    int nx;
    OperatorMat OpMat;
    AKFields Fields;
    AKVec mag;
    AKVec xyz;
    double e_anis;
    double e_exch;
    double e_demag;
    double e_ext;
    double e_idmi;
    double e_bdmi;
    double e_cubic;
    double e_total;
    double dtotV;
    char dirname[1024];
    char operdir[1024];
    char sparam[1024];
    char eparam[1024];
    char inistate[32];
    int nodemag;
    MatParams *params; /* ferromagnetic material (FM) parameters */
} SimVar;
/*----------------------------------------------------------------------*/
void memerror(char *e_message);
/*----------------------------------------------------------------------*/
void N_AKVec(AKVec *array, int length);
/*----------------------------------------------------------------------*/
void F_AKVec(AKVec *array);
/*----------------------------------------------------------------------*/
void N_AKFields(AKFields *fields, int comp_length);
/*----------------------------------------------------------------------*/
int **IMatrix(int ni, int nj);
/*----------------------------------------------------------------------*/
void FreeIMatrix(int **p);
/*----------------------------------------------------------------------*/
double **DMatrix(int ni, int nj);
/*----------------------------------------------------------------------*/
void FreeDMatrix(double **p);
/*----------------------------------------------------------------------*/
double *DVector(int imax);
/*----------------------------------------------------------------------*/
void FreeDVector(double *p);
/*----------------------------------------------------------------------*/
int *IVector(int imax);
/*----------------------------------------------------------------------*/
void FreeIVector(int *p);
/*----------------------------------------------------------------------*/
void N_AKElm(AKElm *elm, int nelx, int nx, int dim);
/*----------------------------------------------------------------------*/
void N_RISSprMat(RISSprMat *spm, int isize);
/*----------------------------------------------------------------------*/
void F_RISSprMat(RISSprMat *spmat);
/*----------------------------------------------------------------------*/
void ris2csrDirect(RISSprMat *risMat, CSRSprMat *csrDescr);
/*----------------------------------------------------------------------*/
RISSprMat expand_dimension(RISSprMat x);
/*----------------------------------------------------------------------*/
#endif
