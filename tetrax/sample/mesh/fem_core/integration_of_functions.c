//
// Created by Attila Kákay on 25.09.23.
//

#include "integration_of_functions.h"
#include <math.h>

#define EPS_INT 1.0e-6
#define JMAX 500
//#define FUNC(x) ((*func)(x))

/*----------------------------------------------------------------------*/
double trapzd(double (*func)(double *r, double *rp, double *nv, double x, int m), double *r, double *rp, double *nv, int m, double a, double b, int n)
/*
    This routine computes the nth stage of refinement of an extended trapezoidal rule. func is input
    as a pointer to the function to be integrated between limits a and b, also input. When called with
    n=1, the routine returns the crudest estimate of  b f (x)dx. Subsequent calls with n=2,3,... a
    (in that sequential order) will improve the accuracy by adding 2n-2 additional interior points.
*/
{
    double x, tnm, sum, del;
    static double s;
    int it, j;
    if (n == 1) {
        return (s=0.5*(b-a)*((*func)(r, rp, nv, a, m)+(*func)(r, rp, nv, b, m)));
    } else {
        for (it=1,j=1;j<n-1;j++) it <<= 1;
            tnm=it;
            del=(b-a)/tnm; //This is the spacing of the points to be added.
            x=a+0.5*del;
        for (sum=0.0,j=1;j<=it;j++,x+=del) sum += (*func)(r, rp, nv, x, m);
        s=0.5*(s+(b-a)*sum/tnm); //This replaces s by its refined value.
        return s;
    }
}
/*----------------------------------------------------------------------*/
double qtrap(double (*func)(double *, double *, double *, double, int), double *r, double *rp, double *nv, int m, double a, double b)
/*
    Returns the integral of the function func from a to b.
    The parameters EPS can be set to the desired fractional accuracy and
    JMAX so that 2 to the power JMAX-1 is the maximum allowed number of steps.
    Integration is performed by the trapezoidal rule.
*/
{
    double trapzd(double (*func)(double *r, double *rp, double *nv, double x, int m), double *r, double *rp, double *nv, int m, double a, double b, int n);
//    void nrerror(char error_text[]);
    int j;
    double s,olds=0.0; // Initial value of olds is arbitrary.
    for (j=1;j<=JMAX;j++) {
        s=trapzd(func,r,rp,nv,m,a,b,j);
        if (j > 5) // Avoid spurious early convergence.
            if (fabs(s-olds) < EPS_INT*fabs(olds) || (s == 0.0 && olds == 0.0)) return s;
        olds=s;
    }
    return s;
//    nrerror("Too many steps in routine qtrap");
//    return 0.0; // Never get here.
}
/*----------------------------------------------------------------------*/
/*double qsimp(double (*func)(double), double a, double b)

    Returns the integral of the function func from a to b. The parameters EPS
    can be set to the desired fractional accuracy and JMAX so that 2 to the
    power JMAX-1 is the maximum allowed number of steps. Integration is
    performed by Simpson’s rule.
{
    double trapzd(double (*func)(double), double a, double b, int n);
    void nrerror(char error_text[]);
    int j;
    double s, st, ost=0.0, os=0.0;
    for (j=1;j<=JMAX;j++) {
        st=trapzd(func,a,b,j);
        s=(4.0*st-ost)/3.0;
        if (j > 5) ost=st;
    }
    nrerror("Too many steps in routine qsimp");
    return 0.0; // Never get here.
}
*/

/*----------------------------------------------------------------------*/
