#ifndef BND2D_H
#define BND2D_H

#include "akmag.h"
#include "sorting.h"
#include "utils.h"

#define IMIN(X,Y) ((X) <= (Y) ? (X) : (Y))
#define IMAX(X,Y) ((X) >= (Y) ? (X) : (Y))

/*----------------------------------------------------------------------*/
int MakeEdges(AKElm *elm, AKElm *faces);
/*----------------------------------------------------------------------*/
int MakeFaces(AKElm *elm, AKElm *faces);
/*----------------------------------------------------------------------*/
void SortEdgeList(AKElm *faces, int nof);
/*----------------------------------------------------------------------*/
void SortFaceList(AKElm *faces, int nof);
/*----------------------------------------------------------------------*/
void CalcSortnum(AKElm faces, long long *sortnum, int nof, int dim);
/*----------------------------------------------------------------------*/
void Count_bnelx(AKElm faces, int *bnelx, int nof, int *list, int dim);
/*----------------------------------------------------------------------*/
void FindBELM(AKElm faces, int bnelx, int nof, int *list, AKElm *belm,
	 int dim);
/*----------------------------------------------------------------------*/
void CountBNXandMakeList(AKElm *elm, AKElm *belm);
/*----------------------------------------------------------------------*/
void MakeNormalVectors2D(AKElm *elm, AKElm *belm);
/*----------------------------------------------------------------------*/
int OPSIDE(double *a, double *b, double *c, double *d, double *e);
/*----------------------------------------------------------------------*/
void MakeNormalVectors(AKElm *elm, AKElm *belm);
/*----------------------------------------------------------------------*/
void NormVectArea(AKElm *elm);
/*----------------------------------------------------------------------*/
void MakeSubList2D(AKElm *elm);
/*----------------------------------------------------------------------*/
void MakeSubList(AKElm *elm);
/*----------------------------------------------------------------------*/
int bndprocessing(AKElm *elm, AKElm *belm, int *bnelx);
/*----------------------------------------------------------------------*/
double VectorsAngle2D(double *a, double *b, double rtol);
/*----------------------------------------------------------------------*/
double SolidAngle2D(double *x0, double *x1, double *x2);
/*----------------------------------------------------------------------*/
double VectorsAngle(double *a, double *b, double rtol);
/*----------------------------------------------------------------------*/
double SolidAngle(double *x0, double *x1, double *x2, double *x3);
/*----------------------------------------------------------------------*/
double SolidAngleNew(double *x0, double *x1, double *x2, double *x3);
/*----------------------------------------------------------------------*/


#endif
