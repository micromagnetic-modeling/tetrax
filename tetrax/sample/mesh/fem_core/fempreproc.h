//
// Created by Attila Kákay on 12/15/21.
//

#ifndef TETRAX_TETRAXPY_H
#define TETRAX_TETRAXPY_H

#include "akmag.h"

/*----------------------------------------------------------------------*/
int OperatorsFromMesh2D(double *xyz, int *ijk, int nx, int nelx, int dim,
                      int *bnlist, int *bnx, double *nv, double *area,
                      CSRSprMat *poiss, CSRSprMat *lap,
                      CSRSprMat *div_x, CSRSprMat *div_y,
                      CSRSprMat *grad_x, CSRSprMat *grad_y,
                      double *solid_angle, int *belm);
/*----------------------------------------------------------------------*/
int OperatorsFromMesh2D_radial(double *xyz, int *ijk, int nx, int nelx, int dim,
                      int *bnlist, int *bnx, double *nv, double *area,
                      CSRSprMat *poiss, CSRSprMat *grad_poisson,
                      CSRSprMat *lap, CSRSprMat *grad_laplace,
                      CSRSprMat *div_x, CSRSprMat *div_y,
                      CSRSprMat *grad_x, CSRSprMat *grad_y,
                      double *solid_angle, int *belm);
/*----------------------------------------------------------------------*/
int OperatorsFromMesh3D(double *xyz, int *ijk, int nx, int nelx, int dim,
                      int *bnlist, int *bnx, int *bnelx, double *nv, double *node_volumes,
                      CSRSprMat *poiss, CSRSprMat *lap,
                      CSRSprMat *div_x, CSRSprMat *div_y, CSRSprMat *div_z,
                      CSRSprMat *grad_x, CSRSprMat *grad_y, CSRSprMat *grad_z,
                      double *solid_angle, int *belm);
/*----------------------------------------------------------------------*/
int ExchangeOperatorFromMesh2D(double *xyz, int *ijk, int nx, int nelx,
                               int dim, double *Aex, CSRSprMat *xc,
                               CSRSprMat *xc_grad, int not_radial);
/*----------------------------------------------------------------------*/
int ExchangeOperatorFromMesh3D(double *xyz, int *ijk, int nx, int nelx,
                               int dim, double *Aex, CSRSprMat *xc);
/*----------------------------------------------------------------------*/

#endif //TETRAX_TETRAXPY_H
