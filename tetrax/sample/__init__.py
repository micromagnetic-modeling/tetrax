"""
Sample (:mod:`tetrax.sample`)
=============================

.. automodule:: tetrax.sample.sample

.. automodule:: tetrax.sample.material

.. automodule:: tetrax.sample.mesh

"""  # noqa: D205, D400, D415