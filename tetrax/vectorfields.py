"""
Vectorfields (:mod:`tetrax.vectorfields`)
=========================================

.. currentmodule:: tetrax.vectorfields

This module provides a couple of template vectorfields. The names of all available vectorfields are stored in the
``tetrax.vectorfields.available`` list. They can also be found by calling :func:`tetrax.vectorfields.show_available`.
A vectorfield always needs the `xyz` coordinates of the sample and can be assigned, for example, to its magnetization
or as a material direction.

.. code-block:: python

    from tetrax.vectorfields import helical, radial, uniform

    # as magnetization
    sample.mag = helical(sample.xyz, theta=80, helicity=1)

    # as the direction of uniaxial anisotropy
    sample.material["e_u"] = radial(sample.xyz, polarity=1)

    # as the direction of iDMI
    sample.material["e_d"] = uniform(sample.xyz)

Vectorfields can also be read from a file using the :func:`tetrax.vectorfields.from_file` function.

Available fields
----------------

.. autosummary::
    :toctree: generated/

    alternating_layers
    antivortex
    domain_wall_bloch
    domain_wall_neel
    hedgehog
    helical
    homogeneous
    radial
    skyrmion_bloch
    skyrmion_neel
    uniform
    vortex_circular
    vortex_radial

Other
-----

.. autosummary::
    :toctree: generated/

    available
    from_file
    show_available

For a more detailed information on each vector field, click the individual function.
"""  # noqa: D205, D400, D415

from __future__ import annotations

import inspect
from typing import TYPE_CHECKING, Optional, Union

import meshio
import numpy as np

from tetrax.sample.mesh import MeshVector
from tetrax.common.typing import CartesianAxis, UnitaryNumber

if TYPE_CHECKING:
    import pathlib

    from numpy._typing import ArrayLike


def show_available(show_signatures: bool = True) -> None:
    """
    Display the available template vectorfields and optionally their signatures.

    This function prints the names of available functions categorized.
    Optionally, it can also display the function signatures.

    Parameters
    ----------
    show_signatures : bool, optional
        If True, display the function signatures along with the function names (default is True).

    """
    for vectorfield_function in _all_vectorfield_functions:
        print(f"{vectorfield_function.__name__}")
        if show_signatures:
            print(f"\t{inspect.signature(vectorfield_function)}")
        print("")


def from_file(
    filename: Union[str, pathlib.Path],
    file_format: Optional[str] = None,
    name_quantity: Optional[str] = None,
) -> MeshVector:
    """
    Read a vector field from a file and returns it as a MeshVector.

    Read in can be done from several different file formats. For details see the documentation of
    meshio.read().

    Parameters
    ----------
    filename : str or pathlib.Path
        Name of the file to be read.
    file_format : str, Optional
        File extension (is usually inferred from the filename).
    name_quantity : str, Optional
        Name of the quantity to be read in. For example, in `vtk` files vectorfields are stored under
        a certain name (e.g. "mag"). This parameter can be used to select from multiple fields that
        might be stored in the same file. If no name is specified, this function will read in the first
        available field.

    Returns
    -------
    MeshVector or FlattenedMeshVector

    """
    data_file = meshio.read(filename, file_format)

    if name_quantity is None:
        # Obtain first entry in point_data dictionary.
        data = next(iter(data_file.point_data.values())).astype("<f8")
    else:
        data = data_file.point_data[name_quantity].astype("<f8")

    return MeshVector(data)


def alternating_layers(xyz: MeshVector, surface_coordinates: ArrayLike, direction: ArrayLike = (0, 0, 1)) -> MeshVector:
    r"""
    Field of antiferromagnetically aligned segments. Intended for use in layered samples.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    surface_coordinates : ArrayLike
        :math:`y` coordinates of the surfaces of each layer (corresponds to the coordinates of the boundary nodes
        of a layer mesh.
    direction : ArrayLike
        Three-component vector denoting the axis antiferromagnetic alignment - or the Néel vector
        (default is `(0, 0, 1)`).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    """
    direction = np.array(direction)

    surface_coordinates = np.sort(surface_coordinates)

    rep = len(surface_coordinates) // 2
    return MeshVector(np.piecewise(xyz, [
        np.logical_and(surface_coordinates[2 * i] <= xyz.y, xyz.y <= surface_coordinates[2 * i + 1]) for i in
        range(rep)],
                                   [(-1) ** i * direction for i in range(rep)]))


def helical(
    xyz: MeshVector, theta: float, helicity: UnitaryNumber
) -> MeshVector:
    r"""
    Helical vector field with given angle :math:`\Theta` and circularity :math:`\chi`.

    The field is calculated according to

    .. math:: \mathbf{v} = \chi\sin(\Theta) \mathbf{e}_\phi + \cos(\Theta)\mathbf{e}_z

    which, in cartesian basis, is

    .. math:: \mathbf{v} = -\chi\sin(\Theta)\sin(\phi) \mathbf{e}_x + \chi\sin(\Theta)\cos(\phi) \mathbf{e}_y + \cos(\Theta)\mathbf{e}_z

    with :math:`\phi` being the azimuthal angle in a cylindrical coordinate system.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    theta : float
        Angle :math:`\Theta` of the helical field with the :math:`z` axis (given in `degree`).
    helicity : int
        Circularity :math:`\chi` or sense of rotation around the :math:`z` axis (either ``1`` or ``-1``).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    radial, vortex_circular, vortex_radial

    """
    theta = theta / 180 * np.pi
    phi = np.arctan2(xyz.y, xyz.x)
    chi = UnitaryNumber(helicity).value

    mx = chi * np.sin(theta) * (-np.sin(phi))
    my = chi * np.sin(theta) * (np.cos(phi))
    mz = np.cos(theta) * np.ones_like(xyz.x)

    return MeshVector(np.array([mx, my, mz]).T)


def domain_wall_bloch(  # noqa: PLR0913
    xyz: MeshVector,
    thickness: float = 10,
    x0: float = 0,
    domain_axis: CartesianAxis = "y",
    domain_polarity: UnitaryNumber = 1,
    wall_polarity: UnitaryNumber = 1,
) -> MeshVector:
    r"""
    Bloch-type domain wall running along the $z$ axis.

    The field is calculated according to

    .. math:: \mathbf{v} = \begin{cases} p_\mathrm{D} \cos(\theta)\mathbf{e}_y + p_\mathrm{DW} \sin(\theta)\mathbf{e}_z, & \text{domains polarized along } y\\
                                   p_\mathrm{DW} \sin(\theta) \mathbf{e}_y + p_\mathrm{D} \cos(\theta)\mathbf{e}_z, & \text{domains polarized along } z
                            \end{cases}

    with :math:`p_\mathrm{D}` and :math:`p_\mathrm{DW}` being the ``domain_polarity`` and the ``wall_polarity``,
    respectively. The domain-wall angle :math:`\theta = \theta(x)` is given by the Bloch wall solution

    .. math:: \theta(x) = 2 \arctan\left(e^{(x-x_0)/\Delta}\right)

    with :math:`\Delta` being the ``thickness`` and :math:`x_0 =` ``x0`` being the position of the wall.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    thickness : float
        Thickness of the domain wall (default is 10).
    x0 : float
        Position of the domain wall along the :math:`x` direction (default is 0).
    domain_axis : {"y", "z"}
        Axis, along which the domains adjecent to the Bloch wall are oriented (default is "y").
    domain_polarity : {1, -1}
        Polarity of the `first` domain with respect to the ``domain_axis``.
    wall_polarity : {1, -1}
        Polarity of the domain wall.

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    domain_wall_neel

    """
    domain_polarity = UnitaryNumber(domain_polarity).value
    wall_polarity = UnitaryNumber(wall_polarity).value
    domain_axis = CartesianAxis(domain_axis).value

    normal_coordinate = xyz.x
    theta = 2 * np.arctan(np.exp((normal_coordinate - x0) / thickness))

    normal_component = np.zeros_like(xyz.x)

    if domain_axis == "y":
        my = domain_polarity * np.cos(theta)
        mz = wall_polarity * np.sin(theta)
    elif domain_axis == "z":
        mz = domain_polarity * np.cos(theta)
        my = wall_polarity * np.sin(theta)
    else:
        raise ValueError("No valid domain axis has been supplied.")

    return MeshVector(np.array([normal_component, my, mz]).T)


def domain_wall_neel(  # noqa: PLR0913
    xyz: MeshVector,
    thickness: float = 10,
    x0: float = 0,
    domain_axis: CartesianAxis = "z",
    domain_polarity: UnitaryNumber = 1,
    wall_polarity: UnitaryNumber = 1,
) -> MeshVector:
    r"""
    Domain wall of Neel type perpendicular to the :math:`x` direction.

    This profile is simply being approximated as the profile of a Bloch wall (see :func:`domain_wall_bloch`), however,
    with rotation of the vector field perpendicular to the wall plane.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    thickness : float
        Thickness of the domain wall (default is 10).
    x0 : float
        Position of the domain wall along the :math:`x` direction (default is 0).
    domain_axis : {"y", "z"}
        Axis, along which the domains adjecent to the Bloch wall are oriented (default is "z").
    domain_polarity : {1, -1}
        Polarity of the `first` domain with respect to the ``domain_axis``.
    wall_polarity : {1, -1}
        Polarity of the domain wall.

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    domain_wall_bloch

    """
    wall_polarity = UnitaryNumber(wall_polarity).value
    domain_polarity = UnitaryNumber(domain_polarity).value
    domain_axis = CartesianAxis(domain_axis).value

    theta = 2 * np.arctan(np.exp((xyz.x - x0) / thickness))

    if domain_axis == "y":
        mx = wall_polarity * np.sin(theta)
        my = domain_polarity * np.cos(theta)
        mz = np.zeros_like(xyz.x)
    elif domain_axis == "z":
        mx = wall_polarity * np.sin(theta)
        my = np.zeros_like(xyz.x)
        mz = domain_polarity * np.cos(theta)
    else:
        raise ValueError("No valid domain axis has been supplied.")

    return MeshVector(np.array([mx, my, mz]).T)


def radial(xyz: MeshVector, polarity: UnitaryNumber) -> MeshVector:
    """
    Alias for hedghog.

    See Also
    --------
    hedgehog

    """
    return hedgehog(xyz, polarity)


def hedgehog(xyz: MeshVector, polarity: UnitaryNumber) -> MeshVector:
    r"""
    Radial vector field with given polarization :math:`p`.

    The field is calculated according to

    .. math:: \mathbf{v} = p \mathbf{e}_\rho.


    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    polarity : UnitaryNumber
        Polarization :math:`p` of the radial field (either ``1`` for outward or ``-1`` for inward).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    helical, vortex_circular, radial

    """
    p = UnitaryNumber(polarity).value

    x = xyz.x
    y = xyz.y
    mx = p * x / np.sqrt(x ** 2 + y ** 2)
    my = p * y / np.sqrt(x ** 2 + y ** 2)
    mz = np.zeros_like(y)
    return MeshVector(np.array([mx, my, mz]).T)


def uniform(xyz: MeshVector, theta: float = 0, phi: float = 0) -> MeshVector:
    """
    An alias for homogeneous.

    See Also
    --------
    homogeneous

    """  # noqa: D401
    return homogeneous(xyz, theta, phi)


def homogeneous(xyz: MeshVector, theta: float = 0, phi: float = 0) -> MeshVector:
    r"""
    Homogeneous magnetization state with direction given by spherical angles :math:`\theta` and :math:`\phi`.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    theta : float
        Polar angle :math:`\theta` given in `degree` (default is 0).
    phi : float
        Azimuthal angle :math:`\phi` given in `degree` (default is 0).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    Notes
    -----
    Most vectorial quantites in TetraX can also be set by simply supplying triplets. For example

    >>> sample.mag = [0, 0, 1]

    which assigns the same vector to all points on the mesh.

    See Also
    --------
    uniform

    """
    theta = theta / 180.0 * np.pi
    phi = phi / 180.0 * np.pi

    mx = np.sin(theta) * np.cos(phi)
    my = np.sin(theta) * np.sin(phi)
    mz = np.cos(theta)

    return MeshVector(np.full_like(xyz, [mx, my, mz]))


def vortex_circular(
    xyz: MeshVector,
    helicity: UnitaryNumber = 1,
    core_polarity: UnitaryNumber = 1,
    core_radius: float = 10,
) -> MeshVector:
    r"""
    Vortex state with given helicity :math:`\chi` and core polarity :math:`p`.

    The field is calculated according to

    .. math:: \mathbf{v} = \chi\sin[\theta(\rho)] \mathbf{e}_\phi +\cos[\theta(\rho)]\mathbf{e}_z

    which, in cartesian basis, is

    .. math:: \mathbf{v} = -\chi\sin[\theta(\rho)]\sin(\phi) \mathbf{e}_x + \chi\sin[\theta(\rho)]\cos(\phi) \mathbf{e}_y + \cos[\theta(\rho)]\mathbf{e}_z

    with :math:`\phi` being the azimuthal angle and :math:`\rho` being the radius in a cylindrical coordinate system.
    Here, :math:`\theta(\rho)` is the angle of the vector field with the :math:`z` axis and is approximated by
    the Usov-Pechany ansatz

    .. math:: \theta(\rho) = \begin{cases} 2\arctan(\rho/\rho_c), & \rho \leq \rho_c \\
                                \pi/2, & \rho > \rho_c\end{cases}

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    helicity : UnitaryNumber
        Helicity :math:`\chi` or sense of rotation around the :math:`z` axis (either ``1`` or ``-1``).
    core_polarity : UnitaryNumber
        Polarity :math:`p` of the vortex core along the :math:`z` axis (either ``1`` or ``-1``).
    core_radius : float
        Radius of the core (default is ``10``).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    helical, radial_vortex

    References
    ----------
    .. [1] N.A. Usov and S.E. Peschany, "Magnetization curling in a fine cylindrical particle", `J. Magn. Magn. Mater. 118, L290 (1993) <https://doi.org/10.1016/0304-8853(93)90428-5>`_.


    """
    core_polarity = UnitaryNumber(core_polarity).value
    helicity = UnitaryNumber(helicity).value

    rho = np.sqrt(xyz.x ** 2 + xyz.y ** 2)
    phi = np.arctan2(xyz.y, xyz.x)

    # Usov Pechany ansatz
    theta = np.piecewise(
        rho,
        [rho <= core_radius, rho > core_radius],
        [lambda x: 2 * np.arctan(x / core_radius), lambda x: np.pi / 2],
    )

    mx = helicity * np.sin(theta) * (-np.sin(phi))
    my = helicity * np.sin(theta) * (np.cos(phi))
    mz = core_polarity * np.cos(theta) * np.ones_like(xyz.x)

    return MeshVector(np.array([mx, my, mz]).T)


def antivortex(
    xyz: MeshVector,
    core_polarity: UnitaryNumber = 1,
    core_radius: float = 10,
) -> MeshVector:
    r"""
    Antivortex state with core polarity :math:`p`.

    The field is calculated according to

    .. math:: \mathbf{v} = -\sin[\theta(\rho)]\sin(\phi) \mathbf{e}_x + \sin[\theta(\rho)]\cos(\phi) \mathbf{e}_y + \cos[\theta(\rho)]\mathbf{e}_z

    with :math:`\phi` being the azimuthal angle and :math:`\rho` being the radius in a cylindrical coordinate system.
    Here, :math:`\theta(\rho)` is the angle of the vector field with the :math:`z` axis and is approximated by
    the Usov-Pechany ansatz

    .. math:: \theta(\rho) = \begin{cases} 2\arctan(\rho/\rho_c), & \rho \leq \rho_c \\
                                \pi/2, & \rho > \rho_c\end{cases}

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    core_polarity : UnitaryNumber
        Polarity :math:`p` of the vortex core along the :math:`z` axis (either ``1`` or ``-1``).
    core_radius : float
        Radius of the core (default is ``10``).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    vortex_radial, vortex_circular

    References
    ----------
    .. [1] N.A. Usov and S.E. Peschany, "Magnetization curling in a fine cylindrical particle", `J. Magn. Magn. Mater. 118, L290 (1993) <https://doi.org/10.1016/0304-8853(93)90428-5>`_.


    """
    core_polarity = UnitaryNumber(core_polarity).value

    rho = np.sqrt(xyz.x ** 2 + xyz.y ** 2)
    phi = np.arctan2(xyz.y, xyz.x)

    # Usov Pechany ansatz
    theta = np.piecewise(
        rho,
        [rho <= core_radius, rho > core_radius],
        [lambda x: 2 * np.arctan(x / core_radius), lambda x: np.pi / 2],
    )

    mx = np.sin(theta) * (np.sin(phi))
    my = np.sin(theta) * (np.cos(phi))
    mz = core_polarity * np.cos(theta) * np.ones_like(xyz.x)

    return MeshVector(np.array([mx, my, mz]).T)


def vortex_radial(
    xyz: MeshVector,
    core_polarity: UnitaryNumber = 1,
    skirt_polarity: UnitaryNumber = 1,
    core_radius: float = 10,
) -> MeshVector:
    r"""
    Vortex state with given skirt polarity :math:`\chi` and core polarity :math:`p`.

    The field is calculated according to

    .. math:: \mathbf{v} = \chi\sin[\theta(\rho)] \mathbf{e}_\phi +\cos[\theta(\rho)]\mathbf{e}_z

    which, in cartesian basis, is

    .. math:: \mathbf{v} = -\chi\sin[\theta(\rho)]\sin(\phi) \mathbf{e}_x + \chi\sin[\theta(\rho)]\cos(\phi) \mathbf{e}_y + \cos[\theta(\rho)]\mathbf{e}_z

    with :math:`\phi` being the azimuthal angle and :math:`\rho` being the radius in a cylindrical coordinate system.
    Here, :math:`\theta(\rho)` is the angle of the vector field with the :math:`z` axis and is approximated by
    the Usov-Pechany ansatz

    .. math:: \theta(\rho) = \begin{cases} 2\arctan(\rho/\rho_c), & \rho \leq \rho_c \\
                                \pi/2, & \rho > \rho_c\end{cases}

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    core_polarity : UnitaryNumber
        Polarity :math:`p` of the vortex core along the :math:`z` axis (either ``1`` or ``-1``).
    skirt_polarity : UnitaryNumber
        Polarity (inward or outward) of the vortex skirt in radial direction (either ``1`` or ``-1``).
    core_radius : float
        Radius of the core (default is ``10``).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    radial, vortex_circular, hedgehog

    References
    ----------
    .. [1] N.A. Usov and S.E. Peschany, "Magnetization curling in a fine cylindrical particle", `J. Magn. Magn. Mater. 118, L290 (1993) <https://doi.org/10.1016/0304-8853(93)90428-5>`_.


    """
    core_polarity = UnitaryNumber(core_polarity).value
    skirt_polarity = UnitaryNumber(skirt_polarity).value

    rho = np.sqrt(xyz.x ** 2 + xyz.y ** 2)
    phi = np.arctan2(xyz.y, xyz.x)

    # Usov Pechany ansatz
    theta = np.piecewise(
        rho,
        [rho <= core_radius, rho > core_radius],
        [lambda x: 2 * np.arctan(x / core_radius), lambda x: np.pi / 2],
    )

    mx = skirt_polarity * np.sin(theta) * (np.cos(phi))
    my = skirt_polarity * np.sin(theta) * (np.sin(phi))
    mz = core_polarity * np.cos(theta) * np.ones_like(xyz.x)

    return MeshVector(np.array([mx, my, mz]).T)


def skyrmion_neel(  # noqa: PLR0913
    xyz: MeshVector,
    skirt_polarity: UnitaryNumber = 1,
    core_polarity: UnitaryNumber = 1,
    radius: float = 10,
    x0: float = 0,
    y0: float = 0,
) -> MeshVector:
    r"""
    Néel skyrmion state with given skirt polarity and core polarity.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    skirt_polarity : UnitaryNumber
        Polarity of the skyrmion skirt in radial direction (either ``1`` for outward or ``-1`` for outward).
    core_polarity : UnitaryNumber
        Polarity :math:`p` of the skyrmion core along the :math:`z` axis (either ``1`` or ``-1``).
    radius : float
        Radius of the skyrmion, the position at which the in-plane component is maximum.
    x0 : float
        Position of the skyrmion center along the :math:`x` direction (default is 0).
    y0 : float
        Position of the skyrmion center along the :math:`y` direction (default is 0).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    skyrmion_bloch

    """
    skirt_polarity = UnitaryNumber(skirt_polarity).value
    core_polarity = UnitaryNumber(core_polarity).value

    rho = np.sqrt((xyz.x - x0) ** 2 + (xyz.y - y0) ** 2)
    theta = np.pi * np.exp(-rho * rho / radius ** 2)
    phi = np.arctan2(xyz.y - y0, xyz.x - x0) - np.pi / 2 * (skirt_polarity - 1)

    mx = np.sin(theta) * np.cos(phi)
    my = np.sin(theta) * np.sin(phi)
    mz = -core_polarity * np.cos(theta)

    return MeshVector(np.array([mx, my, mz]).T)


def skyrmion_bloch(  # noqa: PLR0913
    xyz: MeshVector,
    skirt_helicity: UnitaryNumber = 1,
    core_polarity: UnitaryNumber = 1,
    radius: float = 20,
    x0: float = 0,
    y0: float = 0,
) -> MeshVector:
    r"""
    Bloch skyrmion state with given circularity :math:`\chi` and core polarity :math:`p`.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    skirt_helicity : UnitaryNumber
        Circularity :math:`\chi` or sense of rotation around the :math:`z` axis (either ``1`` or ``-1``).
    core_polarity : UnitaryNumber
        Polarity :math:`p` of the skyrmion core along the :math:`z` axis (either ``1`` or ``-1``).
    radius : float
        Radius of the skyrmion, the position at which the in-plane component is maximum.
    x0 : float
        Position of the skyrmion center along the :math:`x` direction (default is 0).
    y0 : float
        Position of the skyrmion center along the :math:`y` direction (default is 0).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.


    See Also
    --------
    skyrmion_neel

    """
    core_polarity = UnitaryNumber(core_polarity).value
    skirt_helicity = UnitaryNumber(skirt_helicity).value

    rho = np.sqrt((xyz.x - x0) ** 2 + (xyz.y - y0) ** 2)

    mx = skirt_helicity * (xyz.y - y0) / rho / np.cosh((rho - radius) / radius)
    my = -skirt_helicity * (xyz.x - x0) / rho / np.cosh((rho - radius) / radius)
    mz = -core_polarity * np.tanh((rho - radius) / radius)

    return MeshVector(np.array([mx, my, mz]).T)


_all_vectorfield_functions = [
    from_file,
    antivortex,
    hedgehog,
    helical,
    homogeneous,
    radial,
    skyrmion_neel,
    skyrmion_bloch,
    vortex_circular,
    vortex_radial,
    uniform,
]

available = [f.__name__ for f in _all_vectorfield_functions]
"""List of available vectorfields."""
