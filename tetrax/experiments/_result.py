from __future__ import annotations

import json
import platform
import subprocess
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Protocol, Tuple, Union

from tetrax.common.utils import deepcopy_mutable_attributes

if TYPE_CHECKING:
    from typing import Dict, Self

    from matplotlib.axes._subplots import AxesSubplot
    from matplotlib.figure import Figure

    from tetrax.common.typing import Serializable
    from tetrax.experiments._config import ExperimentConfig
    from tetrax.sample.sample import SampleSnapshot


@dataclass(frozen=True)
class EnvironmentMetaData:
    """Metadata about the computational environment in which the TetraX calculations were performed."""

    tetrax_version: str
    """Version of `TetraX` that was used."""

    platform: str
    """Platform on which the computation was performed."""

    cpu: str
    """Type of CPU on which the computation was performed."""

    @classmethod
    def auto(cls: EnvironmentMetaData) -> EnvironmentMetaData:
        """Automatically generate metadata."""
        import tetrax

        return cls(tetrax.__version__, platform.system(), _get_processor_info())

    def to_dict(self: Self) -> Dict:
        """Convert metadata to a dictionary."""
        temp_dict = self.__dict__
        return temp_dict


class Result(Protocol):
    """
    Protocol representing a generic result of an experiment.

    Results are required to at most contain these attributes and methods.

    """

    result_info: ResultInfo
    """Information generic to all results, including type, path, sample snapshot, creation time, and machine details."""

    config: ExperimentConfig
    """Configuration (e.g., number of iteration steps) information for the experiment."""

    def save(self: Self, path: Optional[Path] = None, delete_old: bool = True) -> None:
        """Save the result to the specified path."""
        ...

    def plot(
        self: Self,
        **kwargs,  # noqa: ANN003
    ) -> Tuple[Figure, Union[AxesSubplot, Tuple[AxesSubplot, ...]]]:
        """Generate a plot representing the result, with optional additional keyword arguments."""
        ...


class ResultType(Enum):
    """Possible types for results."""

    EIGEN = "eigen"
    """Result of an eigenmode experiment."""

    RELAXATION = "relax"
    """Result of a relaxation (via energy minimization)."""

    RELAXATION_DYNAMIC = "relax_dynamic"
    """Result of a dynamic relaxation (via torque minimization)."""

    ABSORPTION = "absorption"
    """Result of an absorption calculation."""

    PERTURBATION = "perturbation"
    """Result of a perturbation calculation."""

    # Will be added in a future release
    # EVOLUTION = "evolution"


@dataclass(frozen=True)
class ResultInfo:
    """Container holding metadata that is structurally the same for all results."""

    result_type: ResultType
    """Type of the result."""

    label: Optional[str]
    """Label."""

    parent_path: Path
    """Directory of the parent directory (e.g. sample path or parent-experiment path)."""

    sample_snapshot: SampleSnapshot
    """Immutable snapshot holding the state of the sample for which the result was computed."""

    created: datetime = field(default_factory=datetime.now)
    """Date/time at which the result was created."""

    environment: EnvironmentMetaData = field(default_factory=EnvironmentMetaData.auto)
    """Computational environment (`TetraX` version, CPU type, etc.)"""

    @property
    def path(self: Self) -> Path:
        """Directory (including the possible label) of the result."""
        folder_name = self.result_type.value

        if self.label is not None:
            folder_name += f"_{self.label}"

        return self.parent_path / folder_name

    def __post_init__(self: Self) -> None:
        object.__setattr__(self, "parent_path", Path(self.parent_path))
        object.__setattr__(self, "result_type", ResultType(self.result_type))

        deepcopy_mutable_attributes(self)

    def to_dict(self: Self) -> Dict:
        """Convert result info to dictionary."""
        return {
            "type": self.result_type.value,
            "label": self.label,
            "path": str(self.path),
            "created": self.created.isoformat(),
            "environment": self.environment.to_dict(),
            "sample_snapshot": self.sample_snapshot.to_dict(),
        }


def _get_processor_info() -> str:
    """Return the name of the CPU used for compuation."""
    if platform.system() == "Windows":
        return platform.processor()
    elif platform.system() == "Darwin":
        return (
            subprocess.check_output(
                ["/usr/sbin/sysctl", "-n", "machdep.cpu.brand_string"]
            )
            .strip()
            .decode()
        )
    elif platform.system() == "Linux":
        command = "cat /proc/cpuinfo"
        return subprocess.check_output(command, shell=True).strip().decode()
    return "unkown"


def _assemble_report(
    result_info: ResultInfo, config: ExperimentConfig, outcome_report: Dict
) -> Dict[str, Serializable]:
    """
    Assemble a dictionary representing a comprehensive report of an experiment.

    Parameters
    ----------
    result_info : ResultInfo
        Information about the result, including type, path, sample snapshot, creation time, and machine details.
    config : ExperimentConfig
        Configuration information for the experiment.
    outcome_report : dict
        Dictionary containing the outcome report of the experiment, including info such as success status or file names.

    Returns
    -------
    dict
        A dictionary representing the assembled report, including the result type, configuration details,
        outcome report, and result information.

    """
    return {
        "result_type": result_info.result_type.value,
        "config": config.to_dict(),
        "outcome": outcome_report,
        "result_info": result_info.to_dict(),
    }


def _save_report(report: Dict, path: Path) -> None:
    """Save a report dictionary to a JSON file."""
    with open(path / "report.json", "w") as outfile:
        json.dump(report, outfile, indent=4)
