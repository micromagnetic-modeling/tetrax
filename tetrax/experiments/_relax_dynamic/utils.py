from __future__ import annotations

from tetrax.experiments._relax_dynamic.config import DynamicRelaxationConfiguration


def relax_dynamic_message(success: bool, config: DynamicRelaxationConfiguration) -> str:
    if success:
        return "\nSuccess!\n"

    if config.set_initial_on_failure:
        return f"\nFailure. max_iter={config.max_iter} was reached.  Returning initial state."

    return f"\nFailure. max_iter={config.max_iter} was reached. Returning last iteration step."
