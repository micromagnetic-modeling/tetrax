from __future__ import annotations

import pathlib
from functools import partial
from typing import TYPE_CHECKING, List

import numpy as np
import pandas as pd
from scipy.constants import mu_0
from scipy.integrate import solve_ivp

from tetrax.common.bibliography import add_refereneces_to_bib
from tetrax.common.math import cross_product_ndarray
from tetrax.common.typing import MagneticOrder
from tetrax.experiments._relax_dynamic.config import DynamicRelaxationConfiguration
from tetrax.experiments._relax_dynamic.result import DynamicRelaxationResult
from tetrax.experiments._relax_dynamic.utils import relax_dynamic_message
from tetrax.experiments._result import ResultInfo, ResultType
from tetrax.sample.mesh.meshquantities import FlattenedMeshVector

if TYPE_CHECKING:
    from tetrax import Sample
    from tetrax.interactions.interaction import Interaction


def LLG_rhs(
    t: float,
    y: FlattenedMeshVector,
    interactions: List[Interaction],
    gamma_prime: float,
    alpha_prime: float,
) -> FlattenedMeshVector:
    """Right-hand side of the Landau-Lifshitz-Gilbert equation."""
    effetive_field = 0
    for interaction in interactions:
        effetive_field += interaction.unitless_field(y)

    m_cross_b = cross_product_ndarray(y, effetive_field)
    precession_torque = -gamma_prime * m_cross_b
    damping_torque = -alpha_prime * cross_product_ndarray(y, m_cross_b)

    return precession_torque + damping_torque


def torques(y, interactions, gamma_prime, alpha_prime):
    b_eff = 0
    for interaction in interactions:
        b_eff += interaction.unitless_field(y)

    m_cross_b = cross_product_ndarray(y, b_eff)
    precession_torque = -gamma_prime * m_cross_b
    damping_torque = -alpha_prime * cross_product_ndarray(y, m_cross_b)

    return precession_torque, damping_torque


def relax_dynamic(
    sample: Sample,
    interactions=None,
    set_initial_on_failure=True,
    save=True,
    label=None,
    dt=1e-2,
    tolerance=1e-8,
    verbose=True,
    alpha=1,
    max_iter=100_000,
    output_step=20,
):
    """
    Relax a sample to its equilibrium by evolving the over-damped Landau-Lifshitz-Gilbert (LLG) equation.

    This experiment performs torque minimization on a magnetic sample in order to calculate a local magnetic
    equilibrium. By default, this will modify the magnetization of the sample. Though this method is generally
    slower than energy minimization using
    :func:`relax() <tetrax.experiments._relax.minimize.relax>`, it is much more reliable.

    Parameters
    ----------
    sample : Sample
        The sample to be relaxed.
    interactions : Optional[Iterable[Interaction]]
        The list of magnetic interactions to consider during relaxation. If ``None``, uses the sample's interactions.
    set_initial_on_failure : bool
        If True, reverts to the initial magnetization configuration if relaxation fails to converge (defauls is True).
    save : bool
        If True, saves the relaxation result (default is True).
    label : Optional[str]
        A label for the relaxation result, used for saving or identification purposes (default is ``None``).
    method : str
        The optimization method used by `scipy.optimize.minimize`.
        See https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html for details. Uses
        conjugate gradient by default.
    tolerance : float, optional
        Tolerance for the minimization process. If ``None``, uses the default tolerance.
    verbose : bool, optional
        If True, prints progress and information about the relaxation process.
    options : Optional[Dict]
        Additional options to pass to the minimization method (default is ``None``). See
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html for details.
    noise : float
        Noise level added to the initial magnetization configuration to avoid being stuck in unstable fixpoints.

    Returns
    -------
    RelaxationResult
        An object containing the result of the relaxation, including the final magnetization,
        success status, and relaxation log.

    Examples
    --------
    >>> exchange = sample.get_interaction("exchange")
    >>> anisotropy = sample.get_interaction("uniaxial_anisotropy")
    >>> result = relax(sample, interactions=[exchange, anisotropy], method="CG", tolerance=1e-11)

    """
    add_refereneces_to_bib(sample, "tetrax")

    if sample.magnetic_order is not MagneticOrder.FERROMAGNET:
        raise NotImplementedError(
            "Sorry. At the moment, only ferromagnets are supported for this feature."
        )

    interactions = sample.interactions if interactions is None else interactions

    m_evolve = sample.mag.to_flattened().copy()
    m_ini = m_evolve.copy()
    m_prev = m_evolve.copy()

    config = DynamicRelaxationConfiguration(
        dt,
        alpha,
        max_iter,
        output_step,
        tolerance,
        verbose,
        set_initial_on_failure,
        m_ini.to_unflattened(),
        [I.name for I in interactions],  # noqa: E741
        [I.name for I in interactions if I.is_toothless],  # noqa: E741
    )

    # gamma_prime = sample.material["gamma"].average / (1. + alpha ** 2)
    gamma_prime = 1.0 / (1.0 + alpha**2)
    alpha_prime = alpha * gamma_prime
    gamma_Hz = sample.material["gamma"].average * mu_0 * sample.material["Msat"].average
    t_scale = 1e12 / (
        sample.material["gamma"].average * mu_0 * sample.material["Msat"].average
    )

    RHS_function = partial(
        LLG_rhs,
        interactions=interactions,
        gamma_prime=gamma_prime,
        alpha_prime=alpha_prime,
    )

    diff_prev = 1000.0
    total_t = 0.0
    nx = m_evolve.shape[0] // 3
    was_success = False

    relax_log = pd.DataFrame(
        columns=["total_t", "dm/dt"]
        + [f"<mag.{name}>" for name in m_ini.component_names]
    )

    if config.verbose:
        print(
            f"Minimizing torque with over-damped LLG (alpha={config.alpha}, tolerance={config.tolerance}) ..."
        )

    for step in range(max_iter):
        sol = solve_ivp(fun=RHS_function, method="RK45", t_span=[0.0, dt], y0=m_evolve)
        total_t += dt
        treal = total_t * t_scale
        m_evolve = sol.y[:, 1]
        m_norm = np.sqrt(
            m_evolve[:nx] ** 2 + m_evolve[nx:-nx] ** 2 + m_evolve[-nx:] ** 2
        )
        m_evolve /= np.tile(m_norm, 3)
        diff = np.abs(np.max(m_evolve - m_prev))

        if step % output_step == 0:
            averaged_mag_components = [
                sample.average(component)
                for component in [m_evolve[:nx], m_evolve[nx:-nx], m_evolve[-nx:]]
            ]
            relax_log.loc[len(relax_log)] = [
                treal,
                diff / (dt * t_scale),
                *averaged_mag_components,
            ]

            current_components = ", ".join(
                [
                    f"<mag.{name}> = {averaged_component:.2f}"
                    for (name, averaged_component) in zip(
                        m_evolve.component_names, averaged_mag_components
                    )
                ]
            )
            if verbose:
                prec_torque, damp_torque = (
                    torques(
                        m_evolve,
                        interactions=interactions,
                        gamma_prime=gamma_prime,
                        alpha_prime=alpha_prime,
                    )
                    * gamma_Hz
                )
                dm_dt = np.abs(np.max(prec_torque + damp_torque))
                #                print(f't = {treal*1e-12} (s), dm/dt = {diff/(dt*t_scale)*1e12} [Hz], {current_components}',
                print(
                    f"t = {treal * 1e-12} (s), dm/dt = {dm_dt} [Hz], {current_components}",
                    end="\r",
                )
            if diff > diff_prev:
                dt /= 2

            diff_prev = diff

        if diff < tolerance:
            print(
                f"t = {treal * 1e-12} (s), dm/dt = {diff / (dt * t_scale) * 1e12} [Hz], {current_components}",
                end="\r",
            )
            sample.mag = FlattenedMeshVector(m_evolve).to_unflattened()
            was_success = True
            break

        m_prev = np.copy(m_evolve)

    else:
        # this is executed when max_iter was reached
        if not set_initial_on_failure:
            sample.mag = FlattenedMeshVector(m_evolve).to_unflattened()

    if config.verbose:
        print(relax_dynamic_message(was_success, config))

    result = DynamicRelaxationResult(
        ResultInfo(
            result_type=ResultType.RELAXATION_DYNAMIC,
            label=label,
            parent_path=pathlib.Path(f"./{sample.name}"),
            sample_snapshot=sample.get_snapshot(),
        ),
        config,
        was_success,
        step,
        FlattenedMeshVector(m_evolve).to_unflattened(),
        relax_log,
    )

    if save:
        result.save()

    return result
