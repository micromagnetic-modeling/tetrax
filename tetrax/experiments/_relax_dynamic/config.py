from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, List

from tetrax.common.typing import Serializable
from tetrax.interactions.interaction import InteractionName
from tetrax.interactions.utils import annotate_toothless_interactions
from tetrax.sample.mesh import MeshVector

if TYPE_CHECKING:
    from typing import Self


@dataclass(frozen=True)
class DynamicRelaxationConfiguration:
    """Configuration for relax_dynamic module."""

    initial_timestep: float
    alpha: float
    max_iter: int
    output_step: int
    tolerance: float
    verbose: bool
    set_initial_on_failure: bool

    initial_state: MeshVector
    interaction_names: List[InteractionName]
    toothless_interaction_names: List[InteractionName]

    def to_dict(self: Self) -> Dict[str, Serializable]:
        annotated_interactions = annotate_toothless_interactions(
            self.interaction_names, self.toothless_interaction_names
        )

        return {
            "initial_timestep": self.initial_timestep,
            "alpha": self.alpha,
            "max_iter": self.max_iter,
            "output_step": self.output_step,
            "tolerance": self.tolerance,
            "verbose": self.verbose,
            "set_initial_on_failure": self.set_initial_on_failure,
            "interactions": annotated_interactions,
            "_comment": "Toothless interactions (with relevant material parameters equal zero) are marked with '*'.",
        }
