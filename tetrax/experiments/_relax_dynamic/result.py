from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING

from tetrax.common.io import clear_folder
from tetrax.common.utils import deepcopy_mutable_attributes
from tetrax.experiments._result import (
    ResultInfo,
    _assemble_report,
    _save_report,
)
from tetrax.plotting.plotly import plotly_plot_dynamic_relaxation

if TYPE_CHECKING:
    from typing import Optional, Self

    import k3d
    import pandas

    from tetrax.experiments._relax.config import RelaxationConfiguration
    from tetrax.sample.mesh import MeshVector


@dataclass(frozen=True)
class DynamicRelaxationResult:
    """Container storing the result of a relaxation performed by minimizing the magnetic torque."""

    result_info: ResultInfo
    """Information generic to all results, including type, path, sample snapshot, creation time, and machine details."""

    config: RelaxationConfiguration
    """Configuration for the experiment."""

    was_success: bool
    """If the relaxation succeeded."""

    iterations: int
    """Number of iterations needed for the relaxation."""

    final_state: MeshVector
    """Final state of the relaxation."""

    torque_mag_log: pandas.DataFrame
    """Log of the torque and average magnetization components during the relaxation."""

    @property
    def initial_state(self: Self) -> MeshVector:
        """Initial state the relaxation."""
        return self.config.initial_state

    @property
    def external_field(self: Self) -> MeshVector:
        """Static external field applied during the relaxation."""
        return self.result_info.sample_snapshot.external_field

    def __post_init__(self: Self) -> None:
        deepcopy_mutable_attributes(self)

    # TODO make reprs once everything is finalized

    def save(self: Self, path: Optional[Path] = None, delete_old: bool = True) -> None:
        """
        Save the result to the disk.

        Parameters
        ----------
        path : Optional[Path]
            Path to store the result to.
        delete_old : bool
            If true, delete an possible existing folder with the same name (default is True).

        """
        path = self.result_info.path if path is None else Path(path)

        if delete_old:
            clear_folder(path)

        outcome_report = {
            "was_success": self.was_success,
            "iterations": self.iterations,
            "files": [],
        }

        field_files = [
            ("external_field.vtk", self.external_field, "Bext_(T)"),
            ("initial_state.vtk", self.initial_state, "mag"),
            ("final_state.vtk", self.final_state, "mag"),
        ]

        for filename, field, qty_name in field_files:
            self.result_info.sample_snapshot.sample.field_to_file(
                field, path / filename, qty_name
            )
            outcome_report["files"].append(filename)

        self.torque_mag_log.to_csv(path / "torque_mag_log.csv")
        outcome_report["files"].append("torque_mag_log.csv")

        full_report = _assemble_report(self.result_info, self.config, outcome_report)
        _save_report(full_report, path)

    def plot(self: Self, style: str = "lines", renderer: Optional[str] = None) -> None:
        """
        Plot the magnetization components and torque during the relaxation process using `plotly`.

        Parameters
        ----------
        style: str
            Style to plot the curves in (e.g. "lines", "markers" or "lines+markers").
        renderer: Optional[str]
            Renderer for displaying the `Plotly` figure.

        """
        plotly_plot_dynamic_relaxation(
            self.torque_mag_log, self.initial_state.component_names, style, renderer
        )

    def show(self: Self) -> Optional[k3d.Plot]:
        """Show initial and finial state in a k3d plot."""
        sample = self.result_info.sample_snapshot.sample
        if sample is None:
            print("No handle on original sample. Cannot show anything on it.")
            return None

        return sample.show(
            [self.initial_state, self.final_state], labels=["initial", "final"]
        )
