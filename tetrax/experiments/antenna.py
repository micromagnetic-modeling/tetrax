"""Definitions of microwave antennae."""

from __future__ import annotations

from abc import abstractmethod
from itertools import accumulate
from typing import TYPE_CHECKING, Any

import k3d
import numpy as np
import pygmsh
from numpy import dtype, ndarray

if TYPE_CHECKING:
    from typing import Callable, List, Self

    from tetrax.sample.mesh.meshquantities import MeshVector
    from tetrax.sample.mesh.sample_mesh import MeshioMesh


class MicrowaveAntenna:
    """
    Base class for generic microwave antannae.

    This class provides a blueprint for concrete microwwave antennae in terms of the methods they need to implement.
    A concrete microwave antenna needs to provide a method that outputs the (possibly wave-vector dependent) field
    distribution of the antenna. Additionally, a function that returns the mesh of the antenna needs to be
    provides. This mesh can also be empty.

    """

    def add_to_plot(self: Self, plot: k3d.Plot, span: float) -> k3d.Plot:
        """Add the antenna mesh to a k3d plot."""
        antenna_mesh = self.get_mesh(span)
        plot += k3d.mesh(
            np.float32(antenna_mesh.points),
            np.uint32(antenna_mesh.get_cells_type("triangle")),
            color=0xFFD700,
            side="double",
        )
        return plot

    @abstractmethod
    def get_mesh(self: Self, span: float) -> MeshioMesh:
        """
        Return the mesh of the antenna, given it's attritubes.

        Parameters
        ----------
        span : float
            Extent of the antenna mesh along the transversal direction.

        """

    @abstractmethod
    def get_microwave_field_function(self: Self, xyz: MeshVector) -> Callable:
        """
        Return the wave-vector spectrum of the antenna at each point in the mesh of a sample.

        This function returns a :math:`k`-depedent scalar field, where :math:`k` is to be taken in the units of
        the mesh.

        Parameters
        ----------
        xyz: MeshVector
            Coordinates on the mesh of a sample.

        """


class StriplineAntenna(MicrowaveAntenna):
    r"""
    Stripline antenna. The antenna is assumed to be infinitely thin.

    .. image:: /usage/exp_antenna_stripline.png
       :width: 150

    Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{strip}(k)=\frac{\sin(kL/2)}{kL/2} e^{-\vert k\vert s}

    with :math:`L` being the width of the antenna and :math:`s` the distance from its central plane to the coordinate
    origin.

    Parameters
    ----------
    width : float
        Width of the current lines (in propagation direction).
    gap : float
        Gap (not pitch) between the current lines.
    spacing : float
        Distance between coordinate origin and antenna center plane.

    References
    ----------
    .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
       waves in perpendicularly magnetized materials", `Physical
       Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.

    See Also
    --------
    MultiStriplineAntenna

    """

    def __init__(self: Self, width: float, spacing: float) -> None:  # noqa: D107
        self.width = width
        self.spacing = spacing
        self.k_spectrum = _kspectrum_strip

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"StriplineAntenna(width={self.width}, spacing={self.spacing})"

    def get_mesh(self: Self, span: float) -> MeshioMesh:  # noqa: D102
        x0 = [-span / 2, self.spacing - 2.5, -self.width / 2]
        with pygmsh.occ.Geometry() as geom:
            geom.add_box(x0, [span, 5, self.width], self.width)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self: Self, xyz: MeshVector) -> Callable:  # noqa: D102
        x_ = xyz.T[0]

        def antenna_func(k: float):  # noqa: ANN202
            return _kspectrum_strip(k, 1, self.width, np.abs(x_ - self.spacing))

        def antenna_field_func(k: float):  # noqa: ANN202
            return np.array(
                [np.zeros_like(x_), antenna_func(k), antenna_func(k)]
            ).flatten()

        return antenna_field_func


class MultiStriplineAntenna(MicrowaveAntenna):
    r"""
    Antenna composed of an arbitrary sequence of striplines.

    This class can be used to model any kind of antenna that is composed of several striplines in a row. This can
    include meandering antennae, coplanar-waveguide antennae, U-shaped antennae, and so on. The wave-vector spectrum of
    this antenna is calculated by translating the spectra of individual striplines that are assumed to be infinitely
    thin [1]_.

    .. math::
        h_\mathrm{multi}(k)= \sum\limits_i A_i e^{ik\delta_n} h_{\mathrm{strip},i}(k)

    Here, :math:`\delta` is the translation distance from one stripline :math:`i` to the next along the :math:`z`
    direction. Each individual stripline
    contributes the distribution :math:`h_{\mathrm{strip},i}(k)` and needs to be weighted
    by the relative current amplitude :math:`A_i` flowing through it (see [1]_ and the examples below).

    Parameters
    ----------
    widths : list[float]
        Widths of the current lines (in propagation direction) in units the sample mesh.
    gaps : list[float]
        Gaps (not pitch) between the current lines in units the sample mesh
        (needs to be one element shorter than ``widths``).
    amplitudes: list[float]
        Relative amplitudes (current directions/amplitudes) of the current lines, .e.g ``[-1,1]`` for
        a U-shaped antenna. Needs to have the same length as ``widths``.
    spacing : float
        Distance between coordinate origin and antenna center plane in units the sample mesh.

    References
    ----------
    .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
           waves in perpendicularly magnetized materials", `Physical
           Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.

    See Also
    --------
    StriplineAntenna, CPWAntenna

    """

    def __init__(  # noqa: D107
        self: Self,
        widths: List[float],
        gaps: List[float],
        amplitudes: List[float],
        spacing: float,
    ) -> None:
        self.widths = widths
        """Widths of the current lines (in propagation direction) in units the sample mesh."""

        self.gaps = gaps
        """Gaps (not pitch) between the current lines in units the sample mesh."""

        self.amplitudes = amplitudes
        """Relative amplitudes (current directions/amplitudes) of the current lines."""

        self.spacing = spacing
        """Distance between coordinate origin and antenna center plane in units the sample mesh."""

        self.k_spectrum = _kspectrum_multi_strip

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"MultStriplineAntenna(widths={self.widths}, gaps={self.gaps}, amplitudes={self.amplitudes}, spacing={self.spacing})"

    def get_mesh(self: Self, span: float) -> MeshioMesh:  # noqa: D102
        with pygmsh.occ.Geometry() as geom:
            # geom.add_box(x0, [span, 5, self.width], self.width)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self: Self, xyz: MeshVector) -> Callable:  # noqa: D102
        x_ = xyz.T[0]

        def antenna_func(k: float):  # noqa: ANN202
            return _kspectrum_multi_strip(
                k,
                spacing=np.abs(x_ - self.spacing),
                widths=self.widths,
                gaps=self.gaps,
                amplitudes=self.amplitudes,
            )

        def antenna_field_func(k: float):  # noqa: ANN202
            return np.array(
                [np.zeros_like(x_), antenna_func(k), antenna_func(k)]
            ).flatten()

        return antenna_field_func


class CPWAntenna(MicrowaveAntenna):
    r"""
    Coplanar-waveguide antenna. The antenna is assumed to be infinitely thin.

    .. image:: /usage/exp_antenna_cpw.png
       :width: 300

    Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{CPW}(k)=2\sin^2\big(k(g+L)/2\big)\,h_\mathrm{strip}(k).

    with :math:`L` and :math:`g` being width and gap of the antenna and :math:`s` the distance from its central plane
    to the coordinate origin.

    Parameters
    ----------
    width : float
        Width of the current lines (in propagation direction) in units of the sample mesh.
    gap : float
        Gap (not pitch) between the current lines in units of the sample mesh.
    spacing : float
        Distance between coordinate origin and antenna center plane in units of the sample mesh.

    References
    ----------
    .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
       waves in perpendicularly magnetized materials", `Physical
       Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.

    See Also
    --------
    MultiStriplineAntenna, StriplineAntenna

    """

    def __init__(self: Self, width: float, gap: float, spacing: float) -> None:  # noqa: D107
        self.width = width
        """Width of the current lines (in propagation direction)."""

        self.gap = gap
        """Gap (not pitch) between the current lines."""

        self.spacing = spacing
        """Distance between coordinate origin and antenna center plane."""

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"CPWAntenna(width={self.width}, gap={self.gap}, spacing={self.spacing})"

    def get_mesh(self: Self, span: float) -> MeshioMesh:  # noqa: D102
        with pygmsh.occ.Geometry() as geom:
            geom.add_box(
                [-span / 2, self.spacing - 2.5, -self.width * 1.5 - self.gap],
                [span, 5, self.width],
                self.width,
            )
            geom.add_box(
                [-span / 2, self.spacing - 2.5, -self.width / 2],
                [span, 5, self.width],
                self.width,
            )
            geom.add_box(
                [-span / 2, self.spacing - 2.5, self.width / 2 + self.gap],
                [span, 5, self.width],
                self.width,
            )
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self: Self, xyz: MeshVector) -> Callable:  # noqa: D102
        x_ = xyz.T[0]

        def antenna_func(k: float):  # noqa: ANN202
            return _kspectrum_multi_strip(
                k,
                spacing=np.abs(x_ - self.spacing),
                widths=[self.width, self.width, self.width],
                gaps=[self.gap, self.gap],
                amplitudes=[-1 / 2, 1, -1 / 2],
            )

        def antenna_field_func(k: float):  # noqa: ANN202
            return np.array(
                [np.zeros_like(x_), antenna_func(k), antenna_func(k)]
            ).flatten()

        return antenna_field_func


class HomogeneousAntenna(MicrowaveAntenna):
    r"""
    Antenna which produces a homogeneous microwave field.

    The wave-vector dependent magnitude is calculated according to

    .. math::
        h_\mathrm{hom}(k)=\delta(k).

    with :math:`\delta` being the Dirac distribution.

    Parameters
    ----------
    theta : float
        Polar angle of the field polarization (given in degrees).
    phi : float
        Azimuthal angle of the field polarization (given in degrees).

    """

    def __init__(self: Self, theta: float = 0, phi: float = 0) -> None:  # noqa: D107
        self.theta = theta / 180 * np.pi
        """Polar angle of the field polarization (given in degrees)."""

        self.phi = phi / 180 * np.pi
        """Azimuthal angle of the field polarization (given in degrees)."""

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"HomogeneousAntenna(theta={self.theta * 180 / np.pi}, phi={self.phi * 180 / np.pi})"

    def get_mesh(self: Self, span) -> MeshioMesh:  # noqa: D102, ANN001
        with pygmsh.occ.Geometry() as geom:
            # geom.add_box(x0, [span, 5, self.width], self.width)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self: Self, xyz: MeshVector) -> Callable:  # noqa: D102
        x_ = xyz.T[0]

        def antenna_func(k: float) -> float:
            return _kspectrum_homogeneous(k, np.ones_like(x_))

        def antenna_field_func(k: float) -> ndarray[Any, dtype[Any]]:
            return np.array(
                [
                    np.sin(self.theta) * np.cos(self.phi) * antenna_func(k),
                    np.sin(self.theta) * np.sin(self.phi) * antenna_func(k),
                    np.cos(self.theta) * antenna_func(k),
                ]
            ).flatten()

        return antenna_field_func


class CurrentLoopAntenna(MicrowaveAntenna):
    """
    Antenna with the shape of a current loop.

    .. image:: /usage/exp_antenna_loop.png
       :width: 180

    The current loop is oriented in the :math:`xy` plane. For calculations, the current loop is taken to be infinitely
    thin in radial direction.

    Parameters
    ----------
    width : float
        Width of the antenna (in propagation direction) in units of the sample mesh.
    radius : float
        Radius of the antenna in units of the sample mesh.

    """

    def __init__(self: Self, width: float, radius: float) -> None:  # noqa: D107
        self.width = width
        """Width of the antenna (in propagation direction)."""
        self.radius = radius
        """Radius of the antenna."""

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"CurrentLoopAntenna(width={self.width}, radius={self.radius})"

    def get_mesh(self: Self, span: float) -> MeshioMesh:  # noqa: D102
        with pygmsh.occ.Geometry() as geom:
            geom.characteristic_length_min = self.radius * np.pi / 30
            geom.characteristic_length_max = self.radius * np.pi / 20
            cylinder1 = geom.add_cylinder(
                [0, 0, -self.width / 2],
                [0, 0, self.width],
                self.radius + 2.5,
                2 * np.pi,
            )
            cylinder2 = geom.add_cylinder(
                [0, 0, -self.width / 2],
                [0, 0, self.width],
                self.radius - 2.5,
                2 * np.pi,
            )
            geom.boolean_difference(cylinder1, cylinder2)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self: Self, xyz: MeshVector) -> Callable:  # noqa: D102
        x_ = xyz.T[0]
        y_ = xyz.T[1]
        rho_ = np.sqrt(x_**2 + y_**2)
        phi_ = np.arctan2(y_, x_)

        def antenna_func(k: float):  # noqa: ANN202
            return _kspectrum_strip(k, 1, self.width, np.abs(rho_ - self.radius))

        def antenna_field_func(k: float):  # noqa: ANN202
            return np.array(
                [
                    antenna_func(k) * np.cos(phi_),
                    antenna_func(k) * np.sin(phi_),
                    antenna_func(k),
                ]
            ).flatten()

        return antenna_field_func


def _kspectrum_strip(k: float, amplitude: float, width: float, spacing: float):  # noqa: ANN202
    r"""
    Fourier transform in `z` direction of one component of the microwave field distribution of a stripline antenna.

    Parameters
    ----------
    k : float
        Wave vector along :math:`z` direction.
    amplitude : float
        Amplitude of the field.
    width : float
        Width of antenna (in :math:`z` direction).
    spacing : float
        Distance from the center plane of the antenna.

    Returns
    -------
    float

    See Also
    --------
    h_U, h_CPW, h_hom

    Notes
    -----
    Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{strip}(k)=A\frac{\sin(kL/2)}{kL/2} e^{-\vert k\vert s}.

    References
    ----------
    .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
           waves in perpendicularly magnetized materials", `Physical
           Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.

    """
    # sinc is evaluated using np.divide in order to catch divide-by-zero exception
    sinc = np.divide(
        np.sin(k * width / 2), (k * width / 2), out=np.ones_like(k), where=(k != 0)
    )

    return amplitude * sinc * np.exp(-np.abs(k * spacing))


def _kspectrum_homogeneous(k: float, amplitude: float):  # noqa: ANN202
    return amplitude * np.piecewise(k, [k == 0, k != 0], [1, 0])


def _kspectrum_multi_strip(  # noqa: ANN202
    k: float,
    spacing: float,
    widths: List[float],
    gaps: List[float],
    amplitudes: List[float],
):
    if len(widths) != len(amplitudes):
        raise ValueError(
            f"Length of widths ({len(widths)}) and amplitudes ({len(amplitudes)}) need to be the same."
        )

    if len(widths) != len(gaps) + 1:
        raise ValueError(
            f"Length of widths ({len(widths)}) needs to be 1 greater than ({len(gaps)})."
        )

    mid_shift = (sum(widths) + sum(gaps) - widths[0] / 2 - widths[-1] / 2) / 2

    translations = list(accumulate(g + L for (g, L) in zip(gaps, widths[:-1])))
    shifted_translations = [t - mid_shift for t in translations]
    shifted_translations = [-mid_shift, *shifted_translations]

    modulation = np.zeros_like(k) + 0j
    for L, ampl, delta in zip(widths, amplitudes, shifted_translations):
        modulation += ampl * np.exp(1j * k * delta) * _kspectrum_strip(k, 1, L, spacing)

    return modulation


# def _h_U(k, A, L, g, s):
#     r"""
#     Fourier transform in `z` direction of one component of the microwave-field distribution of a U-shaped antenna.
#
#     Parameters
#     ----------
#     k : float
#         Wave vector along :math:`z` direction.
#     A : float
#         Amplitude of the field.
#     L : float
#         Width of individual current lines (in :math:`z` direction).
#     g : float
#         Gap between individual current lines (in :math:`z` direction). Do not confuse with pitch.
#     s : float
#         Distance from the center plane of the antenna.
#
#     Returns
#     -------
#     float
#
#     See Also
#     --------
#     h_strip : This function is used in `h_CPW`.
#     h_U, h_hom
#
#     Notes
#     -----
#     Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to
#
#     .. math::
#         h_\mathrm{CPW}(k)=2iA\sin\big(k(g+L)/2\big)\,h_\mathrm{strip}(k).
#
#     References
#     ----------
#     .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
#            waves in perpendicularly magnetized materials", `Physical
#            Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.
#
#     """
#     return 2 * 1j * np.sin(k * (g + L) / 2) * _kspectrum_strip(k, A, L, s)
