from __future__ import annotations

import pathlib
from functools import partial
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
from pandas import DataFrame
from scipy.optimize import minimize as scipy_minimize

from tetrax.common.bibliography import add_refereneces_to_bib
from tetrax.common.config import (
    ENERGY_NAMES,
    ENERGY_UNITS,
    RELAX_DEFAULT_METHOD,
    RELAX_DEFAULT_NOISE,
    RELAX_DEFAULT_TOLERANCE,
)
from tetrax.common.typing import GeometryType, MagneticOrder
from tetrax.experiments._result import ResultInfo, ResultType
from tetrax.sample.mesh.meshquantities import (
    flattened_spherical_to_cartesian_afm,
    flattened_spherical_to_cartesian_fm,
    make_flattened_AFM,
)

from .config import RelaxationConfiguration
from .result import RelaxationResult
from .utils import (
    jac_spherical_afm,
    jac_spherical_fm,
    relax_message,
    total_unitless_energy,
)

if TYPE_CHECKING:
    from typing import Callable, Dict, Iterable, Optional, Type

    from numpy.typing import NDArray

    from tetrax.interactions.interaction import Interaction
    from tetrax.sample.sample import Sample


def relax(  # noqa: PLR0913
    sample: Sample,
    interactions: Optional[Iterable[Interaction]] = None,
    set_initial_on_failure: bool = True,
    save: bool = True,
    label: Optional[str] = None,
    method: str = RELAX_DEFAULT_METHOD,
    tolerance: float = RELAX_DEFAULT_TOLERANCE,
    verbose: bool = True,
    options: Optional[Dict] = None,
    noise: float = RELAX_DEFAULT_NOISE,
) -> RelaxationResult:
    """
    Relax the sample by minimizing its total magnetic energy.

    This experiment performs energy minimization on a magnetic sample in order to calcalate a local magnetic
    equilibrium. By default, this will modify the magnetization of the sample. Though this method is generally
    quite fast, it is less reliable than torque minimization with
    :func:`relax_dynamic() <tetrax.experiments._relax_dynamic.integrate_llg.relax_dynamic>`.

    Parameters
    ----------
    sample : Sample
        The sample to be relaxed.
    interactions : Optional[Iterable[Interaction]]
        The list of magnetic interactions to consider during relaxation. If ``None``, uses the sample's interactions.
    set_initial_on_failure : bool
        If True, reverts to the initial magnetization configuration if relaxation fails to converge (defauls is True).
    save : bool
        If True, saves the relaxation result (default is True).
    label : Optional[str]
        A label for the relaxation result, used for saving or identification purposes (default is ``None``).
    method : str
        The optimization method used by `scipy.optimize.minimize`.
        See https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html for details. Uses
        conjugate gradient by default.
    tolerance : float, optional
        Tolerance for the minimization process. If ``None``, uses the default tolerance.
    verbose : bool, optional
        If True, prints progress and information about the relaxation process.
    options : Optional[Dict]
        Additional options to pass to the minimization method (default is ``None``). See
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html for details.
    noise : float
        Noise level added to the initial magnetization configuration to avoid being stuck in unstable fixpoints.

    Returns
    -------
    RelaxationResult
        An object containing the result of the relaxation, including the final magnetization,
        success status, and relaxation log.

    Examples
    --------
    >>> exchange = sample.get_interaction("exchange")
    >>> anisotropy = sample.get_interaction("uniaxial_anisotropy")
    >>> result = relax(sample, interactions=[exchange, anisotropy], method="CG", tolerance=1e-11)

    """
    add_refereneces_to_bib(sample, "tetrax")

    interactions = sample.interactions if interactions is None else interactions

    fact = sample.mesh.scale ** int(sample.mesh.dimension)

    if sample.magnetic_order is MagneticOrder.FERROMAGNET:
        mag_initial = sample.mag.to_flattened()
        jacobian = jac_spherical_fm
        convert_to_cartesian = flattened_spherical_to_cartesian_fm
    elif sample.magnetic_order is MagneticOrder.ANTIFERROMAGNET:
        mag_initial = make_flattened_AFM(sample.mag1, sample.mag2)
        jacobian = jac_spherical_afm
        convert_to_cartesian = flattened_spherical_to_cartesian_afm
    else:
        raise ValueError("Invalid magnetic order. Don't know how you got here.")

    config = RelaxationConfiguration(
        method,
        tolerance,
        verbose,
        set_initial_on_failure,
        options,
        mag_initial.to_unflattened(),
        [I.name for I in interactions],  # noqa: E741
        [I.name for I in interactions if I.is_toothless],  # noqa: E741
        noise,
    )

    m_ini_spherical = mag_initial.to_spherical()
    random_generator = np.random.default_rng()
    m_ini_spherical += config.noise * random_generator.standard_normal(
        size=m_ini_spherical.shape[0]
    )
    spherical_vector_type = type(m_ini_spherical)

    relax_log = pd.DataFrame(
        columns=[f"E_{interaction.name.value}" for interaction in interactions]
        + ["E_tot"]
        + [f"<mag.{name}>" for name in mag_initial.component_names]
    )

    callback_function = None
    if config.verbose:
        callback_function = partial(
            current_energy_callback,
            average=sample.average,
            geometry_type=sample.geometry_type,
            interactions=interactions,
            fact=fact,
            spherical_vector=spherical_vector_type,
            relax_log=relax_log,
        )

        print(
            f"Minimizing energy in using '{config.method}' (tolerance {config.tolerance}) ..."
        )

    scipy_result = scipy_minimize(
        total_unitless_energy,
        m_ini_spherical,
        (interactions, convert_to_cartesian, sample.mesh.nx),
        jac=jacobian,
        callback=callback_function,
        method=config.method,
        tol=config.tolerance,
        options=config.scipy_options,
    )

    mag_final = spherical_vector_type(scipy_result.x).to_cartesian()
    mag_for_sample = mag_final

    if not scipy_result.success and config.set_initial_on_failure:
        mag_for_sample = mag_initial

    if verbose:
        print(relax_message(scipy_result.success, config))

    if sample.magnetic_order is MagneticOrder.FERROMAGNET:
        sample.mag = mag_for_sample.to_unflattened()

    elif sample.magnetic_order is MagneticOrder.ANTIFERROMAGNET:
        sample.mag1, sample.mag2 = mag_for_sample.to_two_unflattened()

    result = RelaxationResult(
        ResultInfo(
            result_type=ResultType.RELAXATION,
            label=label,
            parent_path=pathlib.Path(f"./{sample.name}"),
            sample_snapshot=sample.get_snapshot(),
        ),
        config,
        scipy_result.success,
        scipy_result.message,
        scipy_result.nit,
        mag_final.to_unflattened(),
        relax_log,
    )

    if save:
        result.save()

    return result


def current_energy_callback(  # noqa: PLR0913
    mag_spherical: NDArray,
    average: Callable,
    geometry_type: GeometryType,
    interactions: Iterable[Interaction],
    fact: float,
    spherical_vector: Type,
    relax_log: DataFrame,
) -> None:
    """Print the current energy and average magnetization components during optimization."""
    mag = spherical_vector(mag_spherical).to_cartesian()

    energy_name = ENERGY_NAMES[geometry_type.value]
    energy_unit = ENERGY_UNITS[geometry_type.value]

    energies = [interaction.energy(mag) * fact for interaction in interactions]
    energy_sum = np.sum(energies)
    averaged_mag_components = [average(component) for component in mag.components]

    relax_log.loc[len(relax_log)] = [*energies, energy_sum, *averaged_mag_components]

    current_components = ", ".join(
        [
            f"<mag.{name}> = {averaged_component:.2f}"
            for (name, averaged_component) in zip(
                mag.component_names, averaged_mag_components
            )
        ]
    )
    current_energy = f"{energy_name}: {energy_sum:.15e} {energy_unit}"
    print(f"{current_energy}, {current_components}", end="\r")
