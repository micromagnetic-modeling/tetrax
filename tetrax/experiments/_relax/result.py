from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Callable

from tabulate import tabulate

from tetrax.common.io import (
    clear_folder,
    left_align_html_cols,
)
from tetrax.common.utils import deepcopy_mutable_attributes
from tetrax.experiments._result import (
    ResultInfo,
    _assemble_report,
    _save_report,
)
from tetrax.interactions.utils import (
    separate_toothless_interactions,
    separate_toothless_interactions_html,
)
from tetrax.plotting.plotly import plotly_plot_relaxation

if TYPE_CHECKING:
    from typing import List, Optional, Self

    import k3d
    import pandas

    from tetrax.experiments._relax.config import RelaxationConfiguration
    from tetrax.sample.mesh import MeshVector


@dataclass(frozen=True)
class RelaxationResult:
    """
    Immutable container storing the result of a relaxation process on a magnetic sample.

    This class encapsulates information about the final state of a magnetic relaxation by energy minization,
    including configuration settings, success status, energy and magnetization logs,
    and other relevant metadata. It provides methods for viewing, saving, and plotting
    the relaxation data.

    Parameters
    ----------
    result_info : ResultInfo
        Information generic to all results, including type, path, sample snapshot,
        creation time, and machine details.
    config : RelaxationConfiguration
        Configuration settings used during the relaxation process.
    was_success : bool
        Indicates if the relaxation was successful.
    message : str
        Message providing details on the success or failure of the relaxation.
    iterations : int
        Number of iterations performed during the relaxation.
    final_state : MeshVector
        Final magnetization state after relaxation.
    energy_mag_log : pandas.DataFrame
        Log of energies and average magnetization components over the relaxation process.

    Examples
    --------
    >>> result = relax(sample)
    >>> result.plot()

    """

    result_info: ResultInfo
    """Information generic to all results, including type, path, sample snapshot, creation time, and machine details."""

    config: RelaxationConfiguration
    """Configuration for the experiment."""

    was_success: bool
    """If the relaxation succeeded."""

    message: str
    """Reason for possible relaxation failure."""

    iterations: int
    """Number of iterations needed for the relaxation."""

    final_state: MeshVector
    """Final state of the relaxation."""

    energy_mag_log: pandas.DataFrame
    """Log of energies and average magnetization components during the relaxation."""

    @property
    def initial_state(self: Self) -> MeshVector:
        """Initial state the relaxation."""
        return self.config.initial_state

    @property
    def external_field(self: Self) -> MeshVector:
        """Static external field applied during the relaxation."""
        return self.result_info.sample_snapshot.external_field

    def __post_init__(self: Self) -> None:
        deepcopy_mutable_attributes(self)

    def _get_repr_attributes(
        self: Self, separate_interactions_function: Callable
    ) -> List:
        separated_interactions = separate_interactions_function(
            self.config.interaction_names, self.config.toothless_interaction_names
        )

        average_func = self.result_info.sample_snapshot.sample.average

        return [
            ["path", str(self.result_info.path)],
            ["created", self.result_info.created.isoformat()],
            ["geometry_type", self.result_info.sample_snapshot.geometry_type.value],
            ["magnetic_order", self.result_info.sample_snapshot.geometry_type.value],
            ["interactions", separated_interactions],
            ["external_field (avrg.)", f"{average_func(self.external_field)} T"],
            ["initial_state (avrg.)", f"{average_func(self.initial_state)}"],
            ["final_state (avrg.)", f"{average_func(self.final_state)}"],
            ["was_success", self.was_success],
            ["message", self.message],
            ["iterations", self.iterations],
        ]

    def __repr__(self: Self) -> str:
        repr_attributes = self._get_repr_attributes(separate_toothless_interactions)
        label_str = (
            "" if self.result_info.label is None else f"'{self.result_info.label}' "
        )

        return (
            f"RelaxationResult {label_str}of '{self.result_info.sample_snapshot.name}'\n"
            + tabulate(repr_attributes, headers=["attribute", "value"], tablefmt="grid")
            + "\nHint: Call plot() method to show a energy_mag_log of the energies and componentes."
            + "\nHint: Access the RelaxationResult.energy_mag_log or .config directly."
            + "\nHint: Call show() to show initial and final states."
        )

    def _repr_html_(self: Self) -> str:
        repr_attributes = self._get_repr_attributes(
            separate_toothless_interactions_html
        )

        label_str = (
            "" if self.result_info.label is None else f"'{self.result_info.label}' "
        )

        return (
            f"<h4>RelaxationResult {label_str}of '{self.result_info.sample_snapshot.name}'</h4>"
            + left_align_html_cols(
                tabulate(
                    repr_attributes,
                    headers=["attribute", "value"],
                    tablefmt="unsafehtml",
                )
            )
            + "<br><i>Hint:</i> Call <tt>plot()</tt> method to show a energy_mag_log of the energies and componentes."
            + "<br><i>Hint:</i> Access the <tt>RelaxationResult.energy_mag_log</tt> or <tt>.config</tt> directly."
            + "<br><i>Hint:</i> Call <tt>show()</tt> to show initial and final states."
        )

    def save(self: Self, path: Optional[Path] = None, delete_old: bool = True) -> None:
        """
        Save the result to the disk.

        Parameters
        ----------
        path : Optional[Path]
            Path to store the result to.
        delete_old : bool
            If true, delete an possible existing folder with the same name (default is True).

        """
        path = self.result_info.path if path is None else Path(path)

        if delete_old:
            clear_folder(path)

        outcome_report = {
            "was_success": self.was_success,
            "message": self.message,
            "iterations": self.iterations,
            "files": [],
        }

        field_files = [
            ("external_field.vtk", self.external_field, "Bext_(T)"),
            ("initial_state.vtk", self.initial_state, "mag"),
            ("final_state.vtk", self.final_state, "mag"),
        ]

        for filename, field, qty_name in field_files:
            self.result_info.sample_snapshot.sample.field_to_file(
                field, path / filename, qty_name
            )
            outcome_report["files"].append(filename)

        self.energy_mag_log.to_csv(path / "energy_mag_log.csv")
        outcome_report["files"].append("energy_mag_log.csv")

        full_report = _assemble_report(self.result_info, self.config, outcome_report)
        _save_report(full_report, path)

    def plot(self: Self, style: str = "lines", renderer: Optional[str] = None) -> None:
        """
        Plot the magnetization components and energies during the relaxation process using `plotly`.

        Parameters
        ----------
        style: str
            Style to plot the curves in (e.g. "lines", "markers" or "lines+markers").
        renderer: Optional[str]
            Renderer for displaying the `Plotly` figure.

        """
        plotly_plot_relaxation(
            self.energy_mag_log,
            self.initial_state.component_names,
            self.config.interaction_names,
            self.result_info.sample_snapshot.geometry_type,
            style,
            renderer,
        )

    def show(self: Self) -> Optional[k3d.Plot]:
        """Show initial and finial state in a k3d plot."""
        sample = self.result_info.sample_snapshot.sample
        if sample is None:
            print("No handle on original sample. Cannot show anything on it.")
            return None

        return sample.show(
            [self.initial_state, self.final_state], labels=["initial", "final"]
        )
