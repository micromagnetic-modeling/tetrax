from __future__ import annotations

from typing import TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from typing import Callable, List, Union

    from numpy.typing import ArrayLike

    from tetrax.experiments._relax.config import RelaxationConfiguration
    from tetrax.interactions.interaction import Interaction
    from tetrax.sample.mesh import (
        FlattenedAFMMeshVectorSpherical,
        FlattenedMeshVectorSpherical,
    )


def total_energy(
    mag_spherical: Union[FlattenedMeshVectorSpherical, FlattenedAFMMeshVectorSpherical],
    interactions: List[Interaction],
    convert_to_cartesian: Callable,
    nx: int,
) -> float:
    mag = convert_to_cartesian(mag_spherical, nx)

    return np.sum([interaction.energy(mag) for interaction in interactions])


def total_unitless_energy(
    mag_spherical: Union[FlattenedMeshVectorSpherical, FlattenedAFMMeshVectorSpherical],
    interactions: List[Interaction],
    convert_to_cartesian: Callable,
    nx: int,
) -> float:
    mag = convert_to_cartesian(mag_spherical, nx)

    return np.sum([interaction.unitless_energy(mag) for interaction in interactions])


def jac_spherical_fm(
    mag_spherical: FlattenedMeshVectorSpherical,
    interactions: List[Interaction],
    convert_to_cartesian: Callable,
    nx: int,
) -> ArrayLike:
    # unpack
    theta = mag_spherical[:nx]
    phi = mag_spherical[-nx:]

    # convert to carthesian coordinates
    st = np.sin(theta)
    sp = np.sin(phi)
    ct = np.cos(theta)
    cp = np.cos(phi)

    mag = convert_to_cartesian(mag_spherical, nx)

    h_eff = np.sum(
        [interaction.unitless_field(mag) for interaction in interactions], axis=0
    )

    h_x = h_eff[:nx]
    h_y = h_eff[nx:-nx]
    h_z = h_eff[-nx:]

    dm00 = cp * ct
    dm01 = -sp * st
    dm10 = sp * ct
    dm11 = cp * st
    dm20 = -st
    dm21 = 0.0

    dw_theta = -1 * (h_x * dm00 + h_y * dm10 + h_z * dm20)
    dw_phi = -1 * (h_x * dm01 + h_y * dm11 + h_z * dm21)

    return np.concatenate((dw_theta, dw_phi))


def jac_spherical_afm(
    mag_spherical: FlattenedAFMMeshVectorSpherical,
    interactions: List[Interaction],
    convert_to_cartesian: Callable,
    nx: int,
) -> ArrayLike:
    # TODO include faster scalar product!
    # e_per_V = -tetramagvec_scalar_product(mag, h_ext)

    # unpack
    theta1 = mag_spherical[:nx]
    phi1 = mag_spherical[nx : 2 * nx]

    theta2 = mag_spherical[2 * nx : 3 * nx]
    phi2 = mag_spherical[3 * nx : 4 * nx]

    st1 = np.sin(theta1)
    sp1 = np.sin(phi1)
    ct1 = np.cos(theta1)
    cp1 = np.cos(phi1)

    st2 = np.sin(theta2)
    sp2 = np.sin(phi2)
    ct2 = np.cos(theta2)
    cp2 = np.cos(phi2)

    mag = convert_to_cartesian(mag_spherical, nx)

    h_eff = np.sum(
        [interaction.unitless_field(mag) for interaction in interactions], axis=0
    )

    h_x1 = h_eff[:nx]
    h_y1 = h_eff[nx : 2 * nx]
    h_z1 = h_eff[2 * nx : 3 * nx]
    h_x2 = h_eff[3 * nx : 4 * nx]
    h_y2 = h_eff[4 * nx : 5 * nx]
    h_z2 = h_eff[5 * nx : 6 * nx]

    dm001 = cp1 * ct1
    dm011 = -sp1 * st1
    dm101 = sp1 * ct1
    dm111 = cp1 * st1
    dm201 = -st1
    dm211 = 0.0

    dm002 = cp2 * ct2
    dm012 = -sp2 * st2
    dm102 = sp2 * ct2
    dm112 = cp2 * st2
    dm202 = -st2
    dm212 = 0.0

    dw_theta1 = -1 * (h_x1 * dm001 + h_y1 * dm101 + h_z1 * dm201)
    dw_phi1 = -1 * (h_x1 * dm011 + h_y1 * dm111 + h_z1 * dm211)

    dw_theta2 = -1 * (h_x2 * dm002 + h_y2 * dm102 + h_z2 * dm202)
    dw_phi2 = -1 * (h_x2 * dm012 + h_y2 * dm112 + h_z2 * dm212)

    return np.concatenate((dw_theta1, dw_phi1, dw_theta2, dw_phi2))


def relax_message(success: bool, config: RelaxationConfiguration) -> str:
    if success:
        return "\nSuccess!\n"

    if config.set_initial_on_failure:
        return "\nFailure. Returning initial state."

    return "\nFailure. Returning last iteration step."
