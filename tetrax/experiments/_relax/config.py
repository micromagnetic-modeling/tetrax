from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, List

from tetrax.common.config import RELAX_DEFAULT_NOISE
from tetrax.common.typing import Serializable
from tetrax.interactions.interaction import InteractionName
from tetrax.interactions.utils import annotate_toothless_interactions
from tetrax.sample.mesh import MeshVector

if TYPE_CHECKING:
    from typing import Self


@dataclass(frozen=True)
class RelaxationConfiguration:
    """Configuration for a relaxation (energy minimization) experiment."""

    method: str
    """Method used to minimize the energy."""

    tolerance: float
    """Tolerance for the minimization."""

    verbose: bool
    """If relaxation progress should be output on the console."""

    set_initial_on_failure: bool
    """If True set the initial magnetization state in case the relaxation fails (else set the final state)."""

    scipy_options: Dict
    """Possible options passed to the minimize method."""

    initial_state: MeshVector
    """Initial state for the relaxation."""

    interaction_names: List[InteractionName]
    """Names of the magnetic interactions involved in the relaxation."""

    toothless_interaction_names: List[InteractionName]
    """Names of the interactions whose relevant material paramaters are zero."""

    noise: float = RELAX_DEFAULT_NOISE
    """Noise added to the initial state in order to avoid getting stuck in an unstable fixpoint."""

    def to_dict(self: Self) -> Dict[str, Serializable]:
        """Export the configuration to a dictionary."""
        annotated_interactions = annotate_toothless_interactions(
            self.interaction_names, self.toothless_interaction_names
        )

        return {
            "method": self.method,
            "tolerance": self.tolerance,
            "verbose": self.verbose,
            "set_initial_on_failure": self.set_initial_on_failure,
            "scipy_options": self.scipy_options,
            "noise": self.noise,
            "interactions": annotated_interactions,
            "_comment": "Toothless interactions (with relevant material parameters equal zero) are marked with '*'.",
        }
