from __future__ import annotations

import multiprocessing as mp
import pathlib
from dataclasses import dataclass, field
from enum import Enum
from typing import TYPE_CHECKING, List, Set, Tuple

import numpy as np

from tetrax.common.config import (
    EIGEN_DEFAULT_CPUS,
    EIGEN_DEFAULT_FREQUENCY_TOLERANCE,
    EIGEN_DEFAULT_INVERSE_MAXITER,
    EIGEN_DEFAULT_INVERSE_TOLERANCE,
    EIGEN_DEFAULT_NUM_MODES,
    EIGEN_DEFAULT_PARALLEL_BACKEND,
)
from tetrax.interactions.utils import annotate_toothless_interactions

if TYPE_CHECKING:
    from tetrax.interactions.interaction import InteractionName
    from tetrax.sample.mesh import MeshVector


class ConfigError(Exception):
    pass


class EigenOutput(Enum):
    SPATIAL_PROFILES_LOCAL = "spatial_profiles_local"
    LINEWIDTHS = "linewidths"
    MAGPHASE = "magnitude_phase"
    STIFFNESS_FIELDS = "stiffness_fields"
    PERTURBED = "perturbed_dispersion"

    @classmethod
    def _missing_(cls, value):
        raise ValueError(
            "%r is not a valid %s.  Valid output quantities during the eigenmodes calculation are: %s"
            % (
                value,
                cls.__name__,
                ", ".join([repr(m.value) for m in cls]),
            )
        )


@dataclass(frozen=True)
class EigenConfiguration:
    """Configuration for an eigenmodes experiment."""

    results_path: pathlib.Path
    km_limits: Tuple
    equilibrium: MeshVector
    interaction_names: List[InteractionName]
    toothless_interaction_names: List[InteractionName]

    num_modes: int = EIGEN_DEFAULT_NUM_MODES
    save_modes: bool = True
    optional_output: Set[EigenOutput] = field(default_factory=set)
    parallel_backend: str = EIGEN_DEFAULT_PARALLEL_BACKEND
    num_cpus: int = EIGEN_DEFAULT_CPUS
    frequency_tolerance: float = EIGEN_DEFAULT_FREQUENCY_TOLERANCE
    inverse_tolerance: float = EIGEN_DEFAULT_INVERSE_TOLERANCE
    maxiter_inverse: int = EIGEN_DEFAULT_INVERSE_MAXITER

    def __post_init__(self):
        if (
            EigenOutput.MAGPHASE in self.optional_output
            or EigenOutput.SPATIAL_PROFILES_LOCAL in self.optional_output
        ):
            object.__setattr__(self, "save_modes", True)

        if self.num_modes <= 0:
            raise ConfigError(
                f"Invalid number of modes ({self.num_modes}) specified. It needs to be greater than zero."
            )

        validate_km_limits(self.km_limits)
        available_cores = mp.cpu_count()

        if self.num_cpus == 0 or self.num_cpus < -1:
            object.__setattr__(self, "num_cpus", 1)

        if self.num_cpus == -1 or self.num_cpus > available_cores:
            object.__setattr__(self, "num_cpus", available_cores)

    def to_dict(self):
        annotated_interactions = annotate_toothless_interactions(
            self.interaction_names, self.toothless_interaction_names
        )

        k, kmin, kmax, Nk, m, mmin, mmax = self.km_limits

        if k is not None:
            kmin, kmax, Nk = None, None, None

            if np.shape(k) != ():
                k = list(k)

        if m is not None:
            mmin, mmax = None, None

            if np.shape(m) != ():
                m = list(m)

        return {
            "k": k,
            "kmin": kmin,
            "kmax": kmax,
            "Nk": Nk,
            "m": m,
            "mmin": mmin,
            "mmax": mmax,
            "save_modes": self.save_modes,
            "optional_output": [output.value for output in self.optional_output],
            "parallel_backend": self.parallel_backend,
            "num_cpus": self.num_cpus,
            "frequency_tolerance": self.frequency_tolerance,
            "inverse_tolerance": self.inverse_tolerance,
            "maxiter_inverse": self.maxiter_inverse,
            "interactions": annotated_interactions,
            "_comment": "Toothless interactions (with relevant material parameters equal zero) are marked with '*'.",
        }


def validate_km_limits(km_limits):
    """Validate the input for wave vector k and azimuthal index m."""
    try:
        k, kmin, kmax, Nk, m, mmin, mmax = km_limits
    except ValueError:
        raise ConfigError(
            f"Invalid km_limits ({km_limits}) specified. It needs to be a tuple with 7 elements."
        )

    if kmin > kmax:
        raise ConfigError(
            f"Invalid kmin ({kmin}) and kmax ({kmax}) specified. kmin must be less than kmax."
        )
    if mmin > mmax:
        raise ConfigError(
            f"Invalid mmin ({mmin}) and mmax ({mmax}) specified. mmin must be less than mmax."
        )
    if Nk <= 0:
        raise ConfigError(
            f"Invalid number of k points ({Nk}) specified. It needs to be greater than zero."
        )
