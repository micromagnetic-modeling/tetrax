from __future__ import annotations

import pathlib
from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, Optional, Union

import numpy as np
import pandas as pd
from scipy.constants import mu_0

from tetrax.common.config import (
    EIGEN_FREQUENCY_COLUMN_PATTERN,
    EIGEN_WAVEVECTOR_COLUMN_NAME,
    EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME,
)
from tetrax.common.io import clear_folder, pretty_array_print
from tetrax.common.math import inner_product
from tetrax.common.typing import (
    GeometryType,
    MagneticOrder,
    Serializable,
    SIPrefix,
)
from tetrax.experiments._result import (
    ResultInfo,
    ResultType,
    _assemble_report,
    _save_report,
)
from tetrax.experiments.eigen.postprocessing.linewidth import (
    linewidth_inhomogeneous_damping,
)
from tetrax.experiments.eigen.utils import get_single_lattice_rotation, mode_norm_local
from tetrax.plotting.plotly import (
    plotly_plot_1d_susceptibility,
    plotly_plot_2d_susceptibility,
)
from tetrax.sample.mesh import FlattenedLocalMeshVector, FlattenedMeshVector

if TYPE_CHECKING:
    from typing import Iterator, Self

    from numpy.typing import ArrayLike

    from tetrax.experiments import EigenResult
    from tetrax.experiments.antenna import MicrowaveAntenna

# if TYPE_CHECKING:


@dataclass(frozen=True)
class AbsorptionConfiguration:
    """Configuration for an absorption experiment."""

    antenna: MicrowaveAntenna
    """Antenna for which the absorption should be calculated."""

    fmin: float
    """Minimum applied microwave frequency in Hz."""

    fmax: float
    """Maximum applied microwave frequency in Hz."""

    num_f: float
    """Number of microwave-frequency points."""

    k: Optional[Union[float, ArrayLike]] = None
    """Wave-vector value(s) for which the absorption was calculated."""

    def to_dict(self: Self) -> Dict[str, Serializable]:
        """Export the configuration as a dictionary."""
        return {
            "antenna": repr(self.antenna),
            "fmin": self.fmin,
            "fmax": self.fmax,
            "num_f": self.num_f,
            "k": pretty_array_print(self.k),
        }


@dataclass(frozen=True)
class AbsorptionResult:
    """Result of an :func:`~tetrax.experiments.eigen.result.EigenResult.absorption` experiment of an eigen result."""

    result_info: ResultInfo
    """Information generic to all results, including type, path, sample snapshot, creation time, and machine details."""

    config: AbsorptionConfiguration
    """Configuration for the experiment."""

    susceptibility: ArrayLike
    r"""Complex-valued dynamic susceptibility :math:`\chi`."""

    microwave_frequency: ArrayLike
    """Applied microwave frequencies."""

    @property
    def k(self: Self) -> ArrayLike:
        """Wave-vector value(s) for which the absorption was calculated."""
        return self.config.k

    # @property
    # def m(self: Self) -> ArrayLike:
    #    """Azimuthal index(indices) for which the absorption was calculated."""
    #    return self.config.m

    def __iter__(self: Self) -> Iterator:
        """Return an iterator over the class attributes."""
        return iter((self.susceptibility, self.microwave_frequency, self.k))

    def save(
        self: Self, path: Optional[pathlib.Path] = None, delete_old: str = True
    ) -> None:
        """
        Save the result to the disk.

        Parameters
        ----------
        path : Optional[Path]
            Path to store the result to.
        delete_old: bool, optional
            Overwrite existing data (default is True)

        """
        path = self.result_info.path if path is None else pathlib.Path(path)

        if delete_old:
            clear_folder(path)

        outcome_report = {"files": []}

        components = [(np.real, "real"), (np.imag, "imag")]

        km = np.vstack((self.k, np.zeros_like(self.k))).T
        freq_headers = [
            EIGEN_FREQUENCY_COLUMN_PATTERN.format(fi) for fi in self.microwave_frequency
        ]

        for component_func, name in components:
            filename = f"susceptibility_{name}.csv"

            dataframe = pd.DataFrame(
                data=np.hstack((km, component_func(self.susceptibility))),
                columns=[
                    EIGEN_WAVEVECTOR_COLUMN_NAME,
                    EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME,
                    *freq_headers,
                ],
            )

            dataframe.to_csv(path / filename)
            outcome_report["files"].append(filename)

        full_report = _assemble_report(self.result_info, self.config, outcome_report)
        _save_report(full_report, path)

    def plot(
        self: Self,
        style: str = "lines",
        fscale: SIPrefix = SIPrefix.NONE,
        kscale: SIPrefix = SIPrefix.NONE,
    ) -> None:
        """
        Plot absorption result (real and imaginary part of the dynamic susceptibility).

        Parameters
        ----------
        style : str, optional
            Style to plot the curves in (e.g. "lines", "markers" or "lines+markers").
        fscale : SIPrefix, optional
            The scale of the frequency axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        kscale : SIPrefix, optional
            The inverse scale of the wave-vector axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').

        """
        fscale = SIPrefix(fscale)
        kscale = SIPrefix(kscale)

        # TODO in version 2.1, this funcion should get more plotting options

        if len(self.k) == 1:
            plotly_plot_1d_susceptibility(
                self.microwave_frequency,
                self.susceptibility,
                style=style,
                fscale=fscale,
            )

        elif len(self.k) != 1:
            plotly_plot_2d_susceptibility(
                self.k, self.microwave_frequency, self.susceptibility, fscale, kscale
            )


def _calculate_absorption(  # noqa: PLR0913
    eigen_result: EigenResult,
    antenna: MicrowaveAntenna,
    fmin: float,
    fmax: float,
    num_f: int,
    k: Optional[float],
    label: Optional[str] = None,
    save: Optional[bool] = True,
) -> AbsorptionResult:
    sample_snapshot = eigen_result.result_info.sample_snapshot
    if not (
        sample_snapshot.geometry_type in [GeometryType.WAVEGUIDE, GeometryType.LAYER]
        and sample_snapshot.magnetic_order is MagneticOrder.FERROMAGNET
    ):
        raise NotImplementedError(
            "At the moment, this feature is only implemented for ferromagnetic waveguides and layers."
        )

    if antenna is None:
        if sample_snapshot.sample.antenna is None:
            raise ValueError(
                "No antenna given. Either assign an antenna to the sample or pass it "
                " as an argument to this function."
            )
        antenna = sample_snapshot.sample.antenna

    k_range = eigen_result.k if k is None else [k]

    config = AbsorptionConfiguration(antenna, fmin, fmax, num_f, k_range)

    rotation = get_single_lattice_rotation(eigen_result.equilibrium.to_flattened())

    microwave_frequency = np.linspace(fmin, fmax, num_f)
    chi_ = []

    # TODO need to check whether we are doings things properly here with the inhom parameters
    h_RF = antenna.get_microwave_field_function(sample_snapshot.sample.xyz)

    for k in k_range:
        unitless_k = k * sample_snapshot.sample.mesh.scale
        chi = np.zeros_like(microwave_frequency) + 0j
        h_RF_k = FlattenedMeshVector(h_RF(unitless_k))

        for _, _, _, frequency, mode_profile in eigen_result.itermodes(k, m=0):
            if not isinstance(frequency, float):
                continue

            if np.isnan(frequency) or mode_profile is None:
                continue

            mode_profile = mode_profile.to_flattened()
            local_profile = FlattenedLocalMeshVector(rotation @ mode_profile)

            chi_nu = (
                np.abs(
                    sample_snapshot.sample.average(
                        inner_product(mode_profile.conjugate(), h_RF_k)
                    )
                    / mode_norm_local(local_profile, sample_snapshot.sample.average)
                )
                ** 2
            )

            chi += (
                chi_nu
                * sample_snapshot.sample.material["gamma"].average
                / 2
                * np.pi
                * mu_0
                * sample_snapshot.sample.material["Msat"].average
                / (
                    (frequency - microwave_frequency)
                    - 1j
                    * linewidth_inhomogeneous_damping(
                        local_profile,
                        sample_snapshot.sample.average,
                        sample_snapshot.sample.material["alpha"],
                        frequency,
                    )
                )
            )

        chi_.append(chi)

    susceptibility = np.array(chi_)

    result = AbsorptionResult(
        ResultInfo(
            result_type=ResultType.ABSORPTION,
            label=label,
            parent_path=eigen_result.result_info.path,
            sample_snapshot=eigen_result.result_info.sample_snapshot,
        ),
        config,
        susceptibility,
        microwave_frequency,
    )

    if save:
        result.save()

    return result
