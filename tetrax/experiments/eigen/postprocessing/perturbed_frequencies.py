from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Tuple, Union

import numpy as np
import pandas as pd
from scipy.constants import mu_0

from tetrax.common.config import (
    EIGEN_FREQUENCY_COLUMN_PATTERN,
    EIGEN_WAVEVECTOR_COLUMN_NAME,
    EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME,
)
from tetrax.common.io import clear_folder
from tetrax.common.math import inner_product
from tetrax.common.typing import (
    Serializable,
    SIPrefix,
)
from tetrax.common.utils import deepcopy_mutable_attributes
from tetrax.experiments._result import (
    ResultInfo,
    ResultType,
    _assemble_report,
    _save_report,
)
from tetrax.experiments.eigen.dynamic_matrix import DynamicMatrix
from tetrax.experiments.eigen.utils import (
    _reduced_dataframe,
    get_num_modes_from_dataframe,
    get_single_lattice_rotation,
    make_simplest_type,
)
from tetrax.plotting.plotly import plotly_plot_spectral_data, rescale_plotting_k
from tetrax.sample.mesh import FlattenedLocalMeshVector

if TYPE_CHECKING:
    from typing import Dict, List, Optional, Self

    from numpy.typing import ArrayLike

    from tetrax.experiments import EigenResult
    from tetrax.interactions.interaction import Interaction, InteractionName


@dataclass(frozen=True)
class PerturbationConfiguration:
    """Configuration for a perturbation experiment."""

    original_interaction_names: List[InteractionName]
    """Names of the original magnetic interactions used to calculate the unperturbed spectrum."""

    additional_interaction_names: List[InteractionName]
    """Names of the additional magnetic interactions used to calculate the perturbed spectrum."""

    def to_dict(self: Self) -> Dict[Serializable]:
        """Convert the result to a dictionary."""
        return {
            "original_interactions": [
                name.value for name in self.original_interaction_names
            ],
            "additional_interactions": [
                name.value for name in self.additional_interaction_names
            ],
        }


@dataclass(frozen=True)
class PerturbationResult:
    """Zeroth-order perturbed spectrum obtained with the :func:`~tetrax.experiments.eigen.result.EigenResult.perturb_with` experiment of an eigen result."""

    result_info: ResultInfo
    """Information generic to all results, including type, path, sample snapshot, creation time, and machine details."""

    config: PerturbationConfiguration
    """Configuration for the experiment."""

    spectrum_dataframe: pd.DataFrame
    """Dataframe containing the calculated zeroth-order frequencies."""

    def __post_init__(self: Self) -> None:  # noqa: D105
        deepcopy_mutable_attributes(self)

    # TODO proper repr

    def save(self: Self, path: Optional[Path] = None, delete_old: bool = True) -> None:
        """
        Save the result to the disk.

        Parameters
        ----------
        path : Optional[Path]
            Path to store the result to.
        delete_old: bool, optional
            Overwrite existing data (default is True)

        """
        path = self.result_info.path if path is None else Path(path)

        if delete_old:
            clear_folder(path)

        outcome_report = {
            "files": ["spectrum_dataframe.csv"],
        }
        self.spectrum_dataframe.to_csv(path / "spectrum_dataframe.csv")

        full_report = _assemble_report(self.result_info, self.config, outcome_report)
        _save_report(full_report, path)

    @property
    def m(self: Self) -> ArrayLike:
        """Set of azimuthal indices used for eigen compuation."""
        return np.sort(
            np.unique(self.spectrum_dataframe[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME].values)
        )

    @property
    def k(self: Self) -> ArrayLike:
        """Set of wave numbers used for eigen compuation."""
        return np.sort(
            np.unique(self.spectrum_dataframe[EIGEN_WAVEVECTOR_COLUMN_NAME].values)
        )

    @property
    def modes_per_km(self: Self) -> int:
        """Number of eigenvalues calculated for each combination of k and m."""
        return get_num_modes_from_dataframe(self.spectrum_dataframe)

    def frequencies(
        self: Self,
        n: Optional[int] = None,
        k: Optional[float] = None,
        m: Optional[int] = None,
    ) -> pd.DataFrame:
        """
        Return the frequencies stored in Hz, possibly for given mode indices.

        Parameters
        ----------
        n : Optional[int]
            Branch index, mode index for a given ``k`` and/or ``m``.
        k : Optional[float]
            Wave vector in rad/m.
        m : Optional[int]
            Azimuthal index

        Returns
        -------
        float or ArrayLike
            Frequency data.

        """
        return make_simplest_type(
            _reduced_dataframe(self, EIGEN_FREQUENCY_COLUMN_PATTERN, n, k, m)
        )

    def plot_frequencies(  # noqa: PLR0913
        self: Self,
        n: Optional[Union[int, Tuple, List]] = None,
        k: Optional[Union[float, Tuple, List]] = None,
        m: Optional[Union[float, Tuple, List]] = None,
        style: Optional[str] = None,
        fscale: SIPrefix = SIPrefix.NONE,
        kscale: SIPrefix = SIPrefix.NONE,
        renderer: Optional[str] = None,
    ) -> None:
        """
        Plot frequencies based on selected wave vectors and mode indices.

        Parameters
        ----------
        n : int or tuple or list, optional
            Individual mode/branch index ``ni`` or inclusive range ``[nmax, nmin]`` of indices
            to denote the modes/branches to be plotted. If ``None`` is provided, all
            modes are shown (default is ``None``).
        k : float or tuple or list, optional
            Individual wave number ``ki`` or inclusive range ``[kmin, kmax]`` of wave numbers.
            If ``None`` is provided, the full available range is shown (default is ``None``).
            If ``kscale`` is provided, the wave vector (range) will be taken as provided in this scale.
        m : float or tuple or list, optional
            Individual azimuthal index ``mi`` or inclusive range ``[mmin, mmax]`` of indices.
            If ``None`` is provided, the full available range is shown (default is ``None``).
        style : str, optional
            Style to plot the curves in (e.g. "lines", "markers" or "lines+markers").
        fscale : SIPrefix, optional
            The scale of the frequency axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        kscale : SIPrefix, optional
            The inverse scale of the wave-vector axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        renderer: Optional[str]
            Renderer for displaying the `Plotly` figure.

        See Also
        --------
        plot, plot_linewidths

        Examples
        --------
        >>> perturbation_result.plot_frequencies(n=0, k=[-3e5, None], fscale='G', kscale='c')

        This will plot the dispersion of an :class:``PerturbationResult``, showing only the wave-vector range
        between -3e5 rad/cm and the maximum available. Frequences and wave vectors are shown in
        GHz and rad/cm respectively.

        """
        fscale = SIPrefix(fscale)
        kscale = SIPrefix(kscale)

        k = rescale_plotting_k(k, kscale.factor)
        reduced_data = _reduced_dataframe(
            self, EIGEN_FREQUENCY_COLUMN_PATTERN, n, k, m, include_km=True
        )
        plotly_plot_spectral_data(
            reduced_data,
            self.result_info.sample_snapshot.geometry_type,
            style,
            fscale,
            kscale,
            renderer,
        )

    plot = plot_frequencies


def _calculate_zeroth_order_frequencies(
    eigen_result: EigenResult,
    config: PerturbationConfiguration,
    interactions: List[Interaction],
    label: Optional[str] = None,
    save: Optional[bool] = True,
) -> PerturbationResult:
    sample = eigen_result.result_info.sample_snapshot.sample
    average = sample.average

    dynamic_matrix = DynamicMatrix(
        eigen_result.equilibrium.to_flattened(),
        sample.material,
        interactions,
        eigen_result.result_info.sample_snapshot.magnetic_order,
        tolerance_inverse=1e-3,
        maxiter_inverse=1000,
    )
    rotation = get_single_lattice_rotation(eigen_result.equilibrium.to_flattened())

    perturbed_frequency_dataframe = pd.DataFrame(columns=["k (rad/m)", "m"])
    perturbed_frequency_dataframe["k (rad/m)"] = eigen_result.spectrum_dataframe[
        "k (rad/m)"
    ]
    perturbed_frequency_dataframe["m"] = eigen_result.spectrum_dataframe["m"]

    equilibrium = eigen_result.equilibrium.to_flattened()
    equilibrium_field_terms = [
        interaction.unitless_field(equilibrium) for interaction in interactions
    ]

    total_equilibrium_field = np.sum(equilibrium_field_terms, axis=0)

    # h0 = inner_product(equilibrium, FlattenedMeshVector(total_equilibrium_field))
    # h0_avrg = sample.average(h0)

    for n, k, m, _, mode_profile in eigen_result.itermodes():
        dynamic_matrix.set_km(k * sample.mesh.scale, m)

        local_profile = FlattenedLocalMeshVector(rotation @ mode_profile.to_flattened())

        # calculate orthonormal basis from mode profiles
        s1 = local_profile.copy()
        s1.v *= 0
        s2 = local_profile.copy()
        s2.u *= 0

        s1 /= np.sqrt(average(s1.u.conjugate() * s1.u))
        s2 /= np.sqrt(average(s2.v.conjugate() * s2.v))

        c11 = average(
            inner_product(s1.conjugate(), FlattenedLocalMeshVector(dynamic_matrix @ s1))
        )
        c22 = average(
            inner_product(s1.conjugate(), FlattenedLocalMeshVector(dynamic_matrix @ s1))
        )
        c12 = average(
            inner_product(s1.conjugate(), FlattenedLocalMeshVector(dynamic_matrix @ s2))
        )
        c21 = average(
            inner_product(s2.conjugate(), FlattenedLocalMeshVector(dynamic_matrix @ s1))
        )

        Im_N21 = 0.5 * (c11 + c22).real
        Re_N21 = (1j * 0.5 * (c11 - c22)).real
        N11 = c21.real
        N22 = c12.real

        omega_i = Im_N21 + np.sqrt((N11) * (N22) - Re_N21**2)
        # + h0_avrg terms are in dyn mat

        perturbed_frequency = (
            omega_i.real
            * mu_0
            * sample.material["Msat"].average
            * sample.material["gamma"].average
            / 2
            / np.pi
        )

        perturbed_frequency_dataframe.loc[
            (perturbed_frequency_dataframe[EIGEN_WAVEVECTOR_COLUMN_NAME] == k)
            & (perturbed_frequency_dataframe[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME] == m),
            EIGEN_FREQUENCY_COLUMN_PATTERN.format(n),
        ] = perturbed_frequency

    result = PerturbationResult(
        ResultInfo(
            result_type=ResultType.PERTURBATION,
            label=label,
            parent_path=eigen_result.result_info.path,
            sample_snapshot=sample.get_snapshot(),
        ),
        config,
        perturbed_frequency_dataframe,
    )

    if save:
        result.save()

    return result
