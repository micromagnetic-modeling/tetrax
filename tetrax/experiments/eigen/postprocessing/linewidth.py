from __future__ import annotations

from typing import TYPE_CHECKING

from tetrax.common.config import (
    EIGEN_LINEWIDTH_COLUMN_PATTERN,
    EIGEN_WAVEVECTOR_COLUMN_NAME,
    EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME,
)
from tetrax.common.math import inner_product
from tetrax.common.typing import MagneticOrder
from tetrax.experiments.eigen.utils import get_single_lattice_rotation, mode_norm_local
from tetrax.sample.material.parameter import MaterialParameter
from tetrax.sample.mesh import FlattenedLocalMeshVector

if TYPE_CHECKING:
    from typing import Callable

    from tetrax.experiments import EigenResult


def ellipticity_local(mode_profile: FlattenedLocalMeshVector, average: Callable):
    """
    Calculate the ellipticity factor of a mode profile in the local frame of reference.

    Parameters
    ----------
    mode : FlattenedLocalMeshVector
        The mode profile for which the ellipticity factor will be calculated.
    average : Callable
        Function used for spatial averaging.

    Returns
    -------
    float
        The ellipticity factor of the provided mode profile.

    Raises
    ------
    ValueError
        If the provided mode profile is not a FlattenedLocalMeshVector.

    """
    if type(mode_profile) is not FlattenedLocalMeshVector:
        raise ValueError(
            f"Mode profile of type '{type(mode_profile)}' needs to be provided as a FlattenedLocalMeshVector."
        )

    mode_norm = mode_norm_local(mode_profile, average)
    ellipticity_factor = (
        average(inner_product(mode_profile.conjugate(), mode_profile)) / mode_norm
    ).real

    return ellipticity_factor


def linewidth_inhomogeneous_damping(
    mode_profile: FlattenedLocalMeshVector,
    average: Callable,
    alpha: MaterialParameter,
    frequency: float,
) -> float:
    r"""
    Calculate the linear linewidth :math:`\Gamma/2\pi` of a given mode.

    This function returns the linear linewidth of a spin-wave mode due to Gilbert damping. The linewidth
    :math:`\alpha\epsilon\omega/2\pi` is calculated by first obtaining the ellipticity factor
    math:`\epsilon` of the mode profile.

    Parameters
    ----------
    mode_profile : FlattenedLocalMeshVector
        The mode profile for which the ellipticity factor will be calculated.
    average : Callable
        Function used for spatial averaging
    alpha : MaterialParameter
        Gilbert-damping coefficient (can be spatially dependent).
    frequency
        Linear frequency :math:`\omega/2\pi`

    Returns
    -------
    float

    References
    ----------
    .. [1] R. Verba et al., "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal,
       and coordinate-dependent damping," Phys. Rev. B 98, 104408 (2018)

    """
    mode_norm = mode_norm_local(mode_profile, average)
    mode_squared = inner_product(mode_profile.conjugate(), mode_profile)
    linewidh_result = frequency * average(alpha.value * mode_squared) / mode_norm

    return linewidh_result.real


def calculate_all_linewidths(eigen_result: EigenResult):
    if not eigen_result.has_profiles:
        raise ArithmeticError(
            "This EigenResult does not contain any saved mode profiles. "
            "Cannot calculate linewidths."
        )

    if (
        eigen_result.result_info.sample_snapshot.magnetic_order
        is MagneticOrder.ANTIFERROMAGNET
    ):
        raise NotImplementedError(
            "Sorry, this feature is not yet implemented for antiferromagnets."
        )

    rotation = get_single_lattice_rotation(eigen_result.equilibrium.to_flattened())

    for n, k, m, frequency, mode_profile in eigen_result.itermodes():
        local_profile = FlattenedLocalMeshVector(rotation @ mode_profile.to_flattened())
        eigen_result.spectrum_dataframe.loc[
            (eigen_result.spectrum_dataframe[EIGEN_WAVEVECTOR_COLUMN_NAME] == k)
            & (eigen_result.spectrum_dataframe[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME] == m),
            EIGEN_LINEWIDTH_COLUMN_PATTERN.format(n),
        ] = linewidth_inhomogeneous_damping(
            local_profile,
            eigen_result.result_info.sample_snapshot.sample.average,
            eigen_result.result_info.sample_snapshot.sample.material["alpha"],
            frequency,
        )

    object.__setattr__(eigen_result, "has_linewidths", True)
