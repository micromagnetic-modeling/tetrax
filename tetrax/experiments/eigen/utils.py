from __future__ import annotations

import numbers
from typing import TYPE_CHECKING, Callable, List, Optional, Protocol, Tuple, Union

import meshio
import numpy as np
from scipy.sparse import bmat, csr_matrix

from tetrax.common.config import (
    EIGEN_WAVEVECTOR_COLUMN_NAME,
    EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME,
)
from tetrax.common.typing import GeometryType, MagneticOrder
from tetrax.sample.mesh import (
    FlattenedAFMMeshVector,
    FlattenedLocalAFMMeshVector,
    FlattenedLocalMeshVector,
    FlattenedMeshVector,
)
from tetrax.sample.mesh.fem_core.cythoncore import rotation_matrix_py

from .config import (
    EigenConfiguration,
    EigenOutput,
)

if TYPE_CHECKING:
    from numpy.typing import ArrayLike
    from pandas import DataFrame

    from tetrax.sample.mesh.sample_mesh import SampleMesh


def test_rotation(mag: FlattenedMeshVector, rotation: csr_matrix, nx: int) -> None:
    """Test of rotation matrix and equilibrium vector match, i.e. rotation rotates mag into the z direction."""
    test_ez = rotation.dot(mag)
    e_z = np.concatenate([np.zeros(2 * nx), np.ones(nx)])
    if not np.allclose(test_ez, e_z):
        raise ValueError(
            "Rotation matrix and equilibrium files don't belong together. "
            "Please create a new rotation matrix."
        )


def get_two_lattice_rotation(mag: FlattenedAFMMeshVector) -> csr_matrix:
    """Return the projection matrix for two magnetization."""
    nx = mag.nx

    mag1 = mag[: 3 * nx]
    mag2 = mag[3 * nx :]

    rotation1 = rotation_matrix_py(mag1)
    test_rotation(mag1, rotation1, nx)
    rotation1 = rotation1[: 2 * nx, :]

    rotation2 = rotation_matrix_py(mag2)
    test_rotation(mag2, rotation2, nx)
    rotation2 = rotation2[: 2 * nx, :]

    rotation = bmat([[rotation1, None], [None, rotation2]])

    return rotation


def get_single_lattice_rotation(mag: FlattenedMeshVector) -> csr_matrix:
    """Return the rotation matrix to rotate into the locally orthogonal system."""
    nx = mag.nx

    rotation = rotation_matrix_py(mag)
    test_rotation(mag, rotation, nx)
    rotation = rotation[: 2 * nx, :]

    return rotation


def _prepare_km_space(geometry_type: GeometryType, scale: float, km_limits: Tuple):
    """
    Sets up the space of wave numbers and azimuthal indices such that double calculations are
    avoided. This is done by exploiting the symmetries of the eigenvalue problem.

    This function calculates the valid ranges of wavevector and mode number based on the given geometry.
    Depending on the geometry, the wavevector or mode number can be constrained. Wave vectors are

    Parameters
    ----------
    k : float, ndarray, list, tuple, optional
        The wavevector value(s) to be considered. If not provided, a range of wavevectors will be generated.
    kmin : float, optional
        The minimum value of the wavevector range in rad/m. Default is -40e6.
    kmax : float, optional
        The maximum value of the wavevector range in rad/m. Default is 40e6.
    Nk : int, optional
        The number of points in the wavevector range. Default is 81.
    m : int, ndarray, list, tuple, optional
        The azimuthal indices to be considered. If not provided, a range of mode numbers will be generated.
    mmin : int, optional
        The minimum value of the azimuthal index. Default is -10.
    mmax : int, optional
        The maximum value of the azimuthal index. Default is 10.
    geometry : str, optional
        The geometry of the system. Valid options are "waveguide", "confined", "layer", "waveguide_axial"
        and "confined_axial".
        Default is "waveguide".

    Notes
    -----
    All wave-vector arguments will be ignored for confined and confined_axial samples. All azimuthal indices will be
    ignored for confined, layer and waveguide samples.

    """
    k, kmin, kmax, Nk, m, mmin, mmax = km_limits

    # Handle cases where geometry implies constraints on k and m
    if geometry_type in [
        GeometryType.WAVEGUIDE,
        GeometryType.CONFINED,
        GeometryType.LAYER,
    ]:
        m = 0
    if geometry_type in [GeometryType.CONFINED, GeometryType.CONFINED_AXIAL]:
        k = 0

    # Process provided k values or generate a range
    if isinstance(k, numbers.Number):
        k_out = np.array([k]).real
    elif isinstance(k, (np.ndarray, list, tuple)):
        k_out = np.array(k).real
    if k is None:
        k_out = np.linspace(kmin, kmax, Nk)

    # Ensure k values are non-negative and sorted
    if np.abs(np.min(k_out)) > np.max(np.max(k_out)):
        k_out *= -1
    k_out = np.sort(k_out[k_out >= 0])

    # Process provided m values or generate a range
    if isinstance(m, int):
        m_out = np.array([m]).real
    elif isinstance(m, (np.ndarray, list, tuple)):
        m_out = np.array(m).real
    if m is None:
        m_out = np.arange(mmin, mmax + 1, 1)

    # Apply additional constraints for "confined_axial" geometry (symmetry of eigenvalue problem allows to calculate
    # only for positive m.
    if geometry_type is GeometryType.CONFINED_AXIAL:
        if np.abs(np.min(m_out)) > np.max(np.max(m_out)):
            m_out *= -1
        m_out = np.sort(m_out[m_out >= 0])

    if geometry_type is GeometryType.WAVEGUIDE_AXIAL:
        if np.abs(np.min(m_out)) > np.max(np.max(m_out)):
            m_out *= -1
        m_out = np.sort(np.concatenate((m_out[m_out >= 0], m_out[m_out > 0] * -1)))

    k_range = k_out * scale
    m_range = m_out

    km_range = np.dstack(np.meshgrid(k_range, m_range, indexing="ij")).reshape(-1, 2)

    if 0 in k_range and geometry_type is GeometryType.WAVEGUIDE_AXIAL:
        # remove negative m for k = 0 (which are at the beginning of the list)
        km_range = km_range[len(m_out) // 2 :]

    return km_range


def save_mode_profile(
    fname,
    eta_local,
    config: EigenConfiguration,
    mesh: SampleMesh,
    inverse_rotation,
    magnetic_order,
):
    """
    Save mode data to a file.

    Parameters
    ----------
    fname : str
        File name to save the mode data.
    eta_local : ndarray
        Local mode data.

    Returns
    -------
    None

    """
    is_AFM = magnetic_order is MagneticOrder.ANTIFERROMAGNET

    eta_local = (
        FlattenedLocalAFMMeshVector(eta_local)
        if is_AFM
        else FlattenedLocalMeshVector(eta_local)
    )

    # rotate eigenvector back to lab system
    eta = (
        FlattenedAFMMeshVector(inverse_rotation @ eta_local)
        if is_AFM
        else FlattenedMeshVector(inverse_rotation @ eta_local)
    )

    # Define sublattice names based on magnetic order
    sublattice_names = ["1", "2"] if is_AFM else [""]

    # Get sublattice mode profiles (lab and local) in flattened format, based on magnetic order.
    sublattice_etas = [*eta.to_two_unflattened()] if is_AFM else [eta.to_unflattened()]
    sublattice_etas_local = (
        [*eta_local.to_two_unflattened()] if is_AFM else [eta_local.to_unflattened()]
    )

    # Define complex-number components and their numpy getter functions.
    components = [("Re", np.real), ("Im", np.imag)]

    if EigenOutput.MAGPHASE in config.optional_output:
        components.append(("Abs", np.abs))
        components.append(("Arg", np.angle))

    save_dict = {}

    # Loop over complex-number components and sublattices to populate save_dict
    for comp_name, get_comp in components:
        for sublattice_name, sub_eta, sub_eta_local in list(
            zip(sublattice_names, sublattice_etas, sublattice_etas_local)
        ):
            # adds real, imag, abs or phase of 3D vectors
            save_dict[f"{comp_name}(m{sublattice_name})"] = get_comp(sub_eta)

            if EigenOutput.SPATIAL_PROFILES_LOCAL in config.optional_output:
                # adds real, imag, abs or phase of local vector components
                save_dict[f"{comp_name}(m{sublattice_name}_u)"] = get_comp(
                    sub_eta_local.u
                )
                save_dict[f"{comp_name}(m{sublattice_name}_v)"] = get_comp(
                    sub_eta_local.v
                )

    # Write the mode data to the specified file using meshio
    meshio.write_points_cells(
        filename=fname,
        points=mesh.xyz,
        cells=mesh._meshio_mesh.cells,
        point_data=save_dict,
    )


def mode_norm_local(mode_profile: FlattenedLocalMeshVector, average: Callable):
    """
    Calculate the norm of a mode profile in the local frame of reference.

    Parameters
    ----------
    mode : FlattenedLocalMeshVector
        The mode profile for which the ellipticity factor will be calculated.
    average : Callable
        Function used for spatial averaging.

    Returns
    -------
    complex
        The norm of the provided mode profile.

    """
    return 1j * average(
        mode_profile.v.conjugate() * mode_profile.u
        - mode_profile.v * mode_profile.u.conjugate()
    )


def get_num_modes_from_dataframe(dataframe) -> int:
    """Returns the number of modes from dispersion dataframe."""
    columns = dataframe.columns
    matching = [s for s in columns if "f" in s]

    # have to subtract possible columns of frequencies of perturbed modes
    matching_pert = [s for s in columns if "f_" in s]
    num_modes = len(matching) - len(matching_pert)
    return num_modes


class Spectrum(Protocol):
    spectrum_dataframe: DataFrame

    @property
    def k(self) -> ArrayLike: ...

    @property
    def m(self) -> ArrayLike: ...


def _reduced_dataframe(
    spectrum: Spectrum,
    key: str,
    n: Optional[Union[int, Tuple, List]] = None,
    k: Optional[Union[float, Tuple, List]] = None,
    m: Optional[Union[float, Tuple, List]] = None,
    include_km: bool = False,
):
    reduced_data = spectrum.spectrum_dataframe

    if isinstance(k, (int, float)):
        closest_k = spectrum.k[np.abs(spectrum.k - k).argmin()]
        if closest_k != k:
            print(f"Clostest k-value found: {closest_k} rad/m")
        reduced_data = reduced_data[
            reduced_data[EIGEN_WAVEVECTOR_COLUMN_NAME] == closest_k
        ]

    elif isinstance(k, (tuple, list)) and np.shape(k) == (2,):
        k0 = np.min(spectrum.k) if k[0] is None else k[0]
        k1 = np.max(spectrum.k) if k[1] is None else k[1]
        reduced_data = reduced_data[
            reduced_data[EIGEN_WAVEVECTOR_COLUMN_NAME].between(k0, k1)
        ]

    elif k is None:
        pass

    else:
        raise ValueError(
            "Invalid value for k supplied. Needs to be None, a number or a list/tuple "
            "of two numbers that denotes a range."
        )

    if isinstance(m, int):
        closest_m = spectrum.m[np.abs(spectrum.m - m).argmin()]
        if closest_m != m:
            print(f"Clostest m-value found: {closest_m}")

        reduced_data = reduced_data[
            reduced_data[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME] == closest_m
        ]
    elif isinstance(m, (tuple, list)) and np.shape(m) == (2,):
        m[0] = np.min(spectrum.m) if m[0] is None else m[0]
        m[1] = np.max(spectrum.m) if m[1] is None else m[1]

        reduced_data = reduced_data[
            reduced_data[EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME].between(*m)
        ]
    elif m is None:
        pass
    else:
        raise ValueError(
            "Invalid value for m supplied. Needs to be None, an int or a list/tuple "
            "of two numbers that denotes a range."
        )

    n_range = np.arange(spectrum.modes_per_km)

    if isinstance(n, int):
        n_range = [n]

    elif isinstance(n, (tuple, list)) and np.shape(n) == (2,):
        n[0] = np.min(n_range) if n[0] is None else n[0]
        n[1] = np.max(n_range) if n[1] is None else n[1]

        n_range = n_range[n_range <= n[1]]
        n_range = n_range[n_range >= n[0]]

    elif n is None:
        pass

    else:
        raise ValueError(
            f"Invalid value '{n}' of type {type(n)} for n supplied. Needs to be None, an int "
            f"or a list/tuple of two numbers that denotes a range."
        )

    columns_for_key = [key.format(n) for n in n_range]

    if include_km:
        columns_for_key.insert(0, EIGEN_WAVEVECTOR_COLUMN_NAME)
        columns_for_key.insert(1, EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME)

    reduced_data = reduced_data[columns_for_key]

    return reduced_data


def make_simplest_type(data_frame: DataFrame):
    """
    Convert a DataFrame to the simplest possible data type.

    This function takes a DataFrame and returns its data in the simplest possible format:
    - If the DataFrame contains a single value, it returns that value as a float.
    - If the DataFrame contains a single row or a single column, it returns a flattened array.
    - Otherwise, it returns the original DataFrame.

    Parameters
    ----------
    data_frame : pandas.DataFrame
        The input DataFrame to be simplified.

    Returns
    -------
    float or numpy.ndarray
        A float if the DataFrame has a single value.
        A 1D NumPy array if the DataFrame has a single row or column.
        The original DataFrame if it has multiple rows and columns.

    Examples
    --------
    >>> import pandas as pd
    >>> df = pd.DataFrame([[42]])
    >>> make_simplest_type(df)
    42.0

    >>> df = pd.DataFrame([[1, 2, 3]])
    >>> make_simplest_type(df)
    array([1, 2, 3])

    """
    values = data_frame.values
    rows, cols = data_frame.values.shape

    if (rows, cols) == (1, 1):
        return values[0][0]

    elif rows == 1 or cols == 1:
        return values.flatten()

    return data_frame
