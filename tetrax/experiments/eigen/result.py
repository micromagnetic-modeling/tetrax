"""EigenResult."""

from __future__ import annotations

import time
from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Callable, NamedTuple

import k3d
import meshio
import numpy as np
from tabulate import tabulate

from tetrax.common.config import (
    ABSORPTION_DEFAULT_MAX_RF_FREQUENCY,
    ABSORPTION_DEFAULT_MIN_RF_FREQUENCY,
    ABSORPTION_DEFAULT_NUM_FREQUENCIES,
    EIGEN_FREQUENCY_COLUMN_PATTERN,
    EIGEN_LINEWIDTH_COLUMN_PATTERN,
)
from tetrax.common.io import (
    left_align_html_cols,
    pretty_array_print,
)
from tetrax.common.typing import (
    GeometryType,
    MagneticOrder,
    SIPrefix,
)
from tetrax.common.utils import deepcopy_mutable_attributes
from tetrax.experiments._result import (
    ResultInfo,
    _assemble_report,
    _save_report,
)
from tetrax.experiments.eigen.config import (
    EigenConfiguration,
)
from tetrax.experiments.eigen.postprocessing.absorption import (
    AbsorptionResult,
    _calculate_absorption,
)
from tetrax.experiments.eigen.postprocessing.linewidth import (
    calculate_all_linewidths,
)
from tetrax.experiments.eigen.postprocessing.perturbed_frequencies import (
    PerturbationConfiguration,
    PerturbationResult,
    _calculate_zeroth_order_frequencies,
)
from tetrax.experiments.eigen.utils import (
    _reduced_dataframe,
    get_num_modes_from_dataframe,
    get_single_lattice_rotation,
    make_simplest_type,
)
from tetrax.interactions.interaction import Interaction
from tetrax.interactions.utils import (
    separate_toothless_interactions,
    separate_toothless_interactions_html,
)
from tetrax.plotting.plotly import plotly_plot_spectral_data, rescale_plotting_k
from tetrax.sample.mesh.meshquantities import (
    FlattenedAFMMeshVector,
    FlattenedLocalMeshVector,
    FlattenedMeshVector,
    MeshVector,
    make_flattened_AFM,
)

if TYPE_CHECKING:
    from typing import Generator, List, Optional, Self, Tuple, Union

    import pandas as pd
    from numpy.typing import ArrayLike

    from tetrax.experiments.antenna import MicrowaveAntenna


class ModeInfo(NamedTuple):
    """NamedTuple containing mode indices (n,k,m), frequency and profile."""

    n: int
    k: float
    m: int
    frequency: float
    profile: MeshVector


@dataclass(frozen=True)
class EigenResult:
    """
    Result of an :func:`~tetrax.experiments.eigen.solve.eigenmodes` experiment.

    Contains metadata, plotting functionality as well as post-processing methods such as
    :func:`~tetrax.experiments.eigen.result.EigenResult.absorption` and
    :func:`~tetrax.experiments.eigen.result.EigenResult.perturb_with`.

    """

    result_info: ResultInfo
    """Information generic to all results, including type, path, sample snapshot, creation time, and machine details."""

    config: EigenConfiguration
    """Configuration for the experiment."""

    has_profiles: bool
    """Whether the result contains spatial mode profiles."""

    has_linewidths: bool
    """Whether the result contains linewidth data."""

    spectrum_dataframe: pd.DataFrame
    """Dataframe containing the calculated frequencies (and possibly linewidths)."""

    @property
    def external_field(self: Self) -> MeshVector:
        """External field (in T) applied during the eigenmode calculation."""
        return self.result_info.sample_snapshot.external_field

    @property
    def equilibrium(self: Self) -> MeshVector:
        """Equilibrium state with respect to which the eigenmodes have been calculated."""
        return self.config.equilibrium

    def __post_init__(self: Self) -> None:  # noqa: D105
        deepcopy_mutable_attributes(self)

    def _get_repr_attributes(
        self: Self, separate_interactions_function: Callable
    ) -> List:
        separated_interactions = separate_interactions_function(
            self.config.interaction_names, self.config.toothless_interaction_names
        )

        average_func = self.result_info.sample_snapshot.sample.average

        return [
            ["path", self.result_info.path],
            ["created", self.result_info.created.isoformat()],
            ["geometry_type", self.result_info.sample_snapshot.geometry_type.value],
            ["magnetic_order", self.result_info.sample_snapshot.magnetic_order.value],
            ["interactions", separated_interactions],
            ["external_field (avrg.)", f"{average_func(self.external_field)} T"],
            ["equilibrium (avrg.)", f"{average_func(self.equilibrium)}"],
            ["modes_per_km", self.modes_per_km],
            ["k", pretty_array_print(self.k)],
            ["m", pretty_array_print(self.m)],
            ["has_profiles", self.has_profiles],
            ["has_linewidths", self.has_linewidths],
        ]

    def __repr__(self: Self) -> str:
        repr_attributes = self._get_repr_attributes(separate_toothless_interactions)

        label_str = (
            "" if self.result_info.label is None else f"'{self.result_info.label}' "
        )
        return (
            f"EigenResult {label_str}of '{self.result_info.sample_snapshot.name}'\n"
            + tabulate(repr_attributes, headers=["attribute", "value"], tablefmt="grid")
            + "\nHint: Call plot() method to show frequencies."
            + "\nHint: Call show_mode(n,k,m) or get_mode(...) to visualize/access mode profiles."
            + "\nHint: Call frequencies(n,k,m) or linewidths(n,k,m) or access .spectrum_dataframe."
        )

    def _repr_html_(self: Self) -> str:
        repr_attributes = self._get_repr_attributes(
            separate_toothless_interactions_html
        )
        label_str = (
            "" if self.result_info.label is None else f"'{self.result_info.label}' "
        )

        return (
            f"<h4>EigenResult {label_str}of '{self.result_info.sample_snapshot.name}'</h4>"
            + left_align_html_cols(
                tabulate(
                    repr_attributes,
                    headers=["attribute", "value"],
                    tablefmt="unsafehtml",
                )
            )
            + "<br><i>Hint:</i> Call <tt>plot()</tt> method to show frequencies."
            + "<br><i>Hint:</i> Call <tt>show_mode(n,k,m)</tt> or <tt>get_mode(...)</tt> to visualize/access mode profiles."
            + "<br><i>Hint:</i> Call <tt>frequencies(n,k,m)</tt> or <tt>linewidths(...)</tt> or access <tt>.spectrum_dataframe</tt>."
        )

    def save(self: Self, path: Optional[Path] = None) -> None:
        """
        Save the result to the disk.

        Parameters
        ----------
        path : Optional[Path]
            Path to store the result to.

        """
        path = self.result_info.path if path is None else Path(path)

        outcome_report = {
            "has_profiles": self.has_profiles,
            "has_linewidths": self.has_linewidths,
            "k (rad/m)": pretty_array_print(self.k),
            "m": pretty_array_print(self.m),
            "modes_per_km": self.modes_per_km,
            "files": [],
        }

        field_files = [
            ("external_field.vtk", self.external_field, "Bext_(T)"),
            ("equilibrium.vtk", self.equilibrium, "mag"),
        ]

        for filename, field, qty_name in field_files:
            self.result_info.sample_snapshot.sample.field_to_file(
                field, path / filename, qty_name
            )
            outcome_report["files"].append(filename)

        self.spectrum_dataframe.to_csv(path / "spectrum_dataframe.csv")
        outcome_report["files"].append("spectrum_dataframe.csv")

        if self.has_profiles:
            outcome_report["files"].append("mode_profiles/")

        full_report = _assemble_report(self.result_info, self.config, outcome_report)
        _save_report(full_report, path)

    @property
    def m(self: Self) -> ArrayLike:
        """Set of azimuthal indices used for eigen computation."""
        return np.sort(np.unique(self.spectrum_dataframe["m"].values))

    @property
    def k(self: Self) -> ArrayLike:
        """Set of wave numbers (in rad/m) used for eigen computation."""
        return np.sort(np.unique(self.spectrum_dataframe["k (rad/m)"].values))

    @property
    def modes_per_km(self: Self) -> int:
        """Number of eigenvalues calculated for each combination of k and m."""
        return get_num_modes_from_dataframe(self.spectrum_dataframe)

    def frequencies(
        self: Self,
        n: Optional[int] = None,
        k: Optional[float] = None,
        m: Optional[int] = None,
    ) -> pd.DataFrame:
        """
        Return the frequencies stored in Hz, possibly for given mode indices.

        Parameters
        ----------
        n : Optional[int]
            Branch index, mode index for a given ``k`` and/or ``m``.
        k : Optional[float]
            Wave vector in rad/m.
        m : Optional[int]
            Azimuthal index

        Returns
        -------
        float or ArrayLike
            Frequency data.

        """
        return make_simplest_type(
            _reduced_dataframe(self, EIGEN_FREQUENCY_COLUMN_PATTERN, n, k, m)
        )

    def linewidths(
        self: Self,
        n: Optional[int] = None,
        k: Optional[float] = None,
        m: Optional[int] = None,
    ) -> pd.DataFrame:
        """
        Return the linewidths stored in Hz, possibly for given mode indices.

        If the result does not have linewidth data yet, they will be calculated according to Refs. [1]_, [2]_,
        before returning them.

        Parameters
        ----------
        n : Optional[int]
            Branch index, mode index for a given ``k`` and/or ``m``.
        k : Optional[float]
            Wave vector in rad/m.
        m : Optional[int]
            Azimuthal index

        Returns
        -------
        float or ArrayLike
            Linewidth data.

        References
        ----------
        .. [1] Verba, *et al.*, "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal,
               and coordinate-dependent damping", `Phys. Rev. B 98, 104408 (2018) <https://doi.org/10.1103/PhysRevB.98.104408>`_

        .. [2] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes",
               `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

        Examples
        --------
        - :doc:`Dispersion and linewidths in thick film </examples/thick_film_dispersion_linewidth>`

        """
        if not self.has_linewidths:
            self._calculate_linewidths()

        return make_simplest_type(
            _reduced_dataframe(self, EIGEN_LINEWIDTH_COLUMN_PATTERN, n, k, m)
        )

    def get_mode(
        self: Self,
        n: int = 0,
        k: float = 0,
        m: int = 0,
        as_flattened: bool = False,
    ) -> Union[
        FlattenedAFMMeshVector,
        FlattenedMeshVector,
        MeshVector,
        Tuple[MeshVector, MeshVector],
    ]:
        """
        Return a stored spatial mode profile.

        Parameters
        ----------
        n : int, optional
            Branch index, mode index for a given ``k`` and/or ``m`` (default is 0).
        k : float, optional
            Wave vector in rad/m. Will find the closest available value (default is 0).
        m : int, optional
            Azimuthal index (default is 0).
        as_flattened : bool, optional
            If True, return the mode profile as a :class:`~tetrax.sample.mesh.FlattenedMeshVector`, otherwise
            return it as a :class:`~tetrax.sample.mesh.MeshVector` (default is False).

        Returns
        -------
        MeshVector or FlattenedMeshVector

        """
        if not self.has_profiles:
            print(
                "Modes have not been calculated for this experiment. Please run the eigensolver again."
            )
            return None

        closest_k = self.k[np.abs(self.k - k).argmin()]
        if closest_k != k:
            print(f"Clostest k-value found: {closest_k} rad/m")

        closest_m = self.m[np.abs(self.m - k).argmin()]
        if closest_m != m:
            print(f"Clostest m-value found: {closest_m}")

        if not 0 <= n <= self.modes_per_km:
            raise ValueError(
                f"Invalid mode number ({n}) given. Needs to be between 0 and {self.modes_per_km}"
            )

        filename = (
            self.result_info.path
            / f"mode_profiles/mode_k{closest_k}radperm_m{closest_m}_{n:03d}.vtk"
        )

        vtk_file = meshio.read(filename)

        if self.result_info.sample_snapshot.magnetic_order is MagneticOrder.FERROMAGNET:
            mode = MeshVector(
                vtk_file.point_data["Re(m)"] + 1j * vtk_file.point_data["Im(m)"]
            )
            return mode.to_flattened() if as_flattened else mode

        if (
            self.result_info.sample_snapshot.magnetic_order
            is MagneticOrder.ANTIFERROMAGNET
        ):
            mode1 = MeshVector(
                vtk_file.point_data["Re(m1)"] + 1j * vtk_file.point_data["Im(m1)"]
            )
            mode2 = MeshVector(
                vtk_file.point_data["Re(m2)"] + 1j * vtk_file.point_data["Im(m2)"]
            )
            return make_flattened_AFM(mode1, mode2) if as_flattened else (mode1, mode2)

    def get_mode_local(
        self: Self,
        n: int = 0,
        k: float = 0,
        m: int = 0,
        as_flattened: bool = False,
        rotation: Optional[ArrayLike] = None,
    ) -> Union[MeshVector, FlattenedMeshVector]:
        """
        Return a stored spatial mode profile in the local frame of reference.

        The corresponding spatial profile is rotated from the lab frame to the frame that is locally orthogonal
        to the equilibrium direction.

        Parameters
        ----------
        n : int, optional
            Branch index, mode index for a given ``k`` and/or ``m`` (default is 0).
        k : float, optional
            Wave vector in rad/m. Will find the closest available value (default is 0).
        m : int, optional
            Azimuthal index (default is 0).
        as_flattened : bool, optional
            If True, return the mode profile as a :class:`~tetrax.sample.mesh.FlattenedMeshVector`, otherwise
            return it as a :class:`~tetrax.sample.mesh.MeshVector` (default is False).
        rotation : Optional[ArrayLike]
            Rotation matrix to project from the lab into the local frame. If ``None`` it will be calculated from the
            stored equilibrium state.

        Returns
        -------
        MeshVector or FlattenedMeshVector

        """
        if (
            self.result_info.sample_snapshot.magnetic_order
            is MagneticOrder.ANTIFERROMAGNET
        ):
            raise NotImplementedError(
                "Getting local profiles is not yet supported for AFMs"
            )

        rotation = (
            get_single_lattice_rotation(self.equilibrium.to_flattened())
            if rotation is None
            else rotation
        )

        lab_profile = self.get_mode(n, k, m, as_flattened=True)
        local_profile = FlattenedLocalMeshVector(rotation @ lab_profile)

        return local_profile if as_flattened else local_profile.to_unflattened()

    def itermodes(
        self: Self, k: Optional[float] = None, m: Optional[int] = None
    ) -> Generator:
        """
        Return a generator that goes through all modes and returns their indices, frequencies and profiles.

        Parameters
        ----------
        k : Optional[float]
            Contrain the iterator to this wave-vector value (in rad/m).
        m : Optional[m]
            Contrain the iterator to this azimuthal index.

        Yields
        ------
        mode_info: ModeInfo
            Named tuple containing the indices, the frequency and the spatial profile of the respective mode.

        """
        reduced_dataframe = _reduced_dataframe(
            self, EIGEN_FREQUENCY_COLUMN_PATTERN, n=None, k=k, m=m, include_km=True
        )

        for _, row in reduced_dataframe.iterrows():
            k_i = row["k (rad/m)"]
            m_iter = row["m"]

            frequencies_indices = row.index.map(lambda x: x.startswith("f"))
            only_frequencies = row.loc[frequencies_indices]

            for freq_str, frequency in only_frequencies.items():
                n = int(freq_str.split("f")[1].split(" (Hz)")[0])

                try:
                    mode_profile = self.get_mode(n=n, k=k_i, m=m_iter)
                except meshio.ReadError:
                    mode_profile = None

                yield ModeInfo(n, k_i, m_iter, frequency, mode_profile)

    def show_mode(  # noqa: PLR0913
        self: Self,
        n: int = 0,
        k: float = 0,
        m: int = 0,
        scale: float = 1,
        scale_mode: float = 1,
        show_extrusion: bool = True,
        show_grid: bool = False,
        on_equilibrium: bool = True,
        animated: bool = False,
        fps: int = 24,
        periods: int = 5,
        frames_per_period: int = 24,
    ) -> None:
        r"""
        If spatial mode profiles are available, show a desired mode.

        The mode is visualized as vector field which can either only show the dynamic component or the full magnetization
        (mode on-top of equilibrium). Setting ``animated=True`` allows to animate the mode profiles by showing the oscillation
        over a desired number of periods (default is 1). The mode is automatically colored according to its magnitude.

        .. warning::
            At the moment, animation is implemented such that it will play until the end and cannot be stopped (except
            by interrupting the kernel). So be careful with setting a large number of oscillation periods.

        Parameters
        ----------
        n : int, optional
            Mode/branch index of mode to be shown (default is 0).
        k : float, optional
            Wave vector of desired mode in rad/µm (default is 0). This parameter will be ignored for confined samples.
        m : int, optional
            Azimuthal
        scale : float, optional
            Scaling all arrows (default is 1).
        scale_mode : float, optional
            Scaling of the mode (default is 1) with respect to the equilibrium state.
        show_extrusion: bool, optional
            Show the extrusion of the mesh, if available.
        show_grid: bool, optional
            Show grid in plot (default is False).
        on_equilibrium : bool, optional
            If true, show the mode on top of the equilibrium magnetization (default is True).
        animated : bool, optional
            Make a mode movie by showing the magnetic oscillation over one period in a loop.
        periods : int, optional
            Number of oscillation periods to animate (default is 1). Will be ignored if ``animated=False``.
        frames_per_period : int, optional
            Number of frames to animate per period (default is 20). Will be ignored if ``animated=False``.
        fps : int, optional
            Frames per second for animation (default is 12).

        """
        sample = self.result_info.sample_snapshot.sample
        if sample is None:
            print("No handle on original sample. Cannot show anything on it.")
            return None

        if sample.magnetic_order is MagneticOrder.ANTIFERROMAGNET:
            raise NotImplementedError(
                "Sorry but this feature does not work yet for antiferromagnets."
            )

        light_level = 0.5 if sample.geometry_type == GeometryType.CONFINED else 0
        plot = k3d.plot(lighting=light_level)
        plot.grid_visible = show_grid

        sample.mesh.add_to_plot(plot)
        if show_extrusion:
            sample.mesh.add_extrusion_to_plot(plot)

        mode_profile = self.get_mode(n, k, m)
        magnitude = abs(mode_profile.real) + abs(mode_profile.imag)

        colors = k3d.helpers.map_colors(
            magnitude,
            k3d.matplotlib_color_maps.Inferno,
            color_range=[0, 1.2 * np.max(magnitude)],
        )  # , color_range=[0.5, 0.6])
        colors = [[c, c] for c in colors]

        def field_at_time(t: float) -> ArrayLike:
            field_to_plot = scale_mode * (
                mode_profile.real * np.cos(t) - mode_profile.imag * np.sin(t)
            )
            if on_equilibrium:
                field_to_plot += self.equilibrium
            return np.float32(
                scale
                * (np.vstack([field_to_plot.x, field_to_plot.y, field_to_plot.z])).T
            )

        plt_vectors = k3d.vectors(
            np.float32(sample.mesh.xyz),
            field_at_time(0),
            head_size=2.5 * scale,
            line_width=0.05 * scale,
            colors=colors,
        )
        plot += plt_vectors
        plot.display()

        t_ = (
            np.linspace(0, 2 * np.pi * periods, frames_per_period * periods)
            if animated
            else []
        )
        for t in t_:
            plt_vectors.vectors = field_at_time(t)
            time.sleep(1 / fps)

    # def plot(self: Self, *args, **kwargs) -> None:
    #    """Alias for :func:`plot_frequencies`."""
    #    self.plot_frequencies(*args, **kwargs)

    def plot_frequencies(  # noqa: PLR0913
        self: Self,
        n: Optional[Union[int, Tuple, List]] = None,
        k: Optional[Union[float, Tuple, List]] = None,
        m: Optional[Union[float, Tuple, List]] = None,
        style: Optional[str] = None,
        fscale: SIPrefix = SIPrefix.NONE,
        kscale: SIPrefix = SIPrefix.NONE,
        renderer: Optional[str] = None,
    ) -> None:
        """
        Plot frequencies based on selected wave vectors and mode indices.

        Parameters
        ----------
        n : int or tuple or list, optional
            Individual mode/branch index ``ni`` or inclusive range ``[nmax, nmin]`` of indices
            to denote the modes/branches to be plotted. If ``None`` is provided, all
            modes are shown (default is ``None``).
        k : float or tuple or list, optional
            Individual wave number ``ki`` or inclusive range ``[kmin, kmax]`` of wave numbers.
            If ``None`` is provided, the full available range is shown (default is ``None``).
            If ``kscale`` is provided, the wave vector (range) will be taken as provided in this scale.
        m : float or tuple or list, optional
            Individual azimuthal index ``mi`` or inclusive range ``[mmin, mmax]`` of indices.
            If ``None`` is provided, the full available range is shown (default is ``None``).
        style : str, optional
            Style to plot the curves in (e.g. "lines", "markers" or "lines+markers").
        fscale : SIPrefix, optional
            The scale of the frequency axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        kscale : SIPrefix, optional
            The inverse scale of the wave-vector axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        renderer: Optional[str]
            Renderer for displaying the `Plotly` figure.

        See Also
        --------
        plot, plot_linewidths

        Examples
        --------
        >>> eigen_result.plot_frequencies(n=0, k=[-3e5, None], fscale='G', kscale='c')

        This will plot the dispersion of an :class:``EigenResult``, showing only the wave-vector range
        between -3e5 rad/cm and the maximum available. Frequences and wave vectors are shown in
        GHz and rad/cm respectively.

        """
        fscale = SIPrefix(fscale)
        kscale = SIPrefix(kscale)

        k = rescale_plotting_k(k, kscale.factor)

        reduced_data = _reduced_dataframe(
            self, EIGEN_FREQUENCY_COLUMN_PATTERN, n, k, m, include_km=True
        )
        plotly_plot_spectral_data(
            reduced_data,
            self.result_info.sample_snapshot.geometry_type,
            style,
            fscale,
            kscale,
            renderer,
        )

    plot = plot_frequencies

    def plot_linewidths(  # noqa: PLR0913
        self: Self,
        n: Optional[Union[int, Tuple, List]] = None,
        k: Optional[Union[float, Tuple, List]] = None,
        m: Optional[Union[float, Tuple, List]] = None,
        style: Optional[str] = None,
        fscale: SIPrefix = SIPrefix.NONE,
        kscale: SIPrefix = SIPrefix.NONE,
        renderer: Optional[str] = None,
    ) -> None:
        """
        Plot linewdiths based on selected wave vectors and mode indices.

        Parameters
        ----------
        n : int or tuple or list, optional
            Individual mode/branch index ``ni`` or inclusive range ``[nmax, nmin]`` of indices
            to denote the modes/branches to be plotted. If ``None`` is provided, all
            modes are shown (default is ``None``).
        k : float or tuple or list, optional
            Individual wave number ``ki`` or inclusive range ``[kmin, kmax]`` of wave numbers.
            If ``None`` is provided, the full available range is shown (default is ``None``).
            If ``kscale`` is provided, the wave vector (range) will be taken as provided in this scale.
        m : float or tuple or list, optional
            Individual azimuthal index ``mi`` or inclusive range ``[mmin, mmax]`` of indices.
            If ``None`` is provided, the full available range is shown (default is ``None``).
        style : str, optional
            Style to plot the curves in (e.g. "lines", "markers" or "lines+markers").
        fscale : SIPrefix, optional
            The scale of the linewidth axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        kscale : SIPrefix, optional
            The inverse scale of the wave-vector axis, valid prefixes are 'P', 'T', 'G', 'M', 'k', 'h',
            'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f' (default is '').
        renderer: Optional[str]
            Renderer for displaying the `Plotly` figure.

        See Also
        --------
        plot, plot_frequencies

        Examples
        --------
        >>> eigen_result.plot_linewidths(n=[0, 3], k=20, fscale='M', kscale='u')

        This will plot the linewidths of an :class:``EigenResult``, showing only the values at 20 rad/µm
        of the first four dispersion branches. Linewidths and wave vectors are shown in
        MHz and rad/µm respectively.

        """
        fscale = SIPrefix(fscale)
        kscale = SIPrefix(kscale)

        if not self.has_linewidths:
            self._calculate_linewidths()

        k = rescale_plotting_k(k, kscale.factor)
        reduced_data = _reduced_dataframe(
            self, EIGEN_LINEWIDTH_COLUMN_PATTERN, n, k, m, include_km=True
        )
        plotly_plot_spectral_data(
            reduced_data,
            self.result_info.sample_snapshot.geometry_type,
            style,
            fscale,
            kscale,
            renderer,
            ylabel="Linewidth",
        )

    def _calculate_linewidths(self: Self) -> None:
        calculate_all_linewidths(self)

    def absorption(
        self: Self,
        antenna: Optional[MicrowaveAntenna] = None,
        fmin: float = ABSORPTION_DEFAULT_MIN_RF_FREQUENCY,
        fmax: float = ABSORPTION_DEFAULT_MAX_RF_FREQUENCY,
        num_f: int = ABSORPTION_DEFAULT_NUM_FREQUENCIES,
        k: Optional[float] = None,
        label: Optional[str] = None,
        save: bool = True,
    ) -> AbsorptionResult:
        r"""
        Obtain the microwave absorption of the calculated eigensystem with respect to a microwave ``antenna``.

        This experiment requires that the spatial mode profiles have been stored.
        The wave-vector and frequency dependent power absorption is calculated by summing over all dispersion branches
        :math:`\nu` at a particular wave vector

        .. math::

            P(k, \omega_\mathrm{RF}) \propto \sum\limits_\nu \frac{\vert h_\nu \vert^2 (k)}{[\omega_\nu(k)
            - \omega_\mathrm{RF}]^2 - \Gamma_\nu^2(k)}

        with :math:`\Gamma_\nu(k)=\alpha_\mathrm{G}\epsilon_\nu(k)\omega_\nu(k)` being the linewidth of a mode with
        frequency :math:`\omega_\nu(k)`, ellipticity factor :math:`\omega_\nu(k)` and the overlap of the respective
        mode profile :math:`\mathbf{m}_\nu` with the Fourier spectrum
        :math:`\tilde{\mathbf{h}}(k)` of the microwave antenna

        .. math::

            h_\nu(k) = \frac{1}{\mathcal{N}_\nu(k)} \langle \mathbf{m}_\nu^*\cdot \tilde{\mathbf{h}}(k)\rangle

        Here :math:`\langle ... \rangle` denotes the sample
        average, :math:`\mathcal{N}_\nu(k)` is the norm of the mode and :math:`\alpha_\mathrm{G}` is the Gilbert damping
        factor of the material. For more details see Refs. [1]_ and [2]_.

        .. note::
            So far, this feature is only implemented for waveguides and layers.

        Parameters
        ----------
        antenna : Optional[MicrowaveAntenna]
            Antenna for which the absorption should be calculated. If no antenna is provided, the antenna of the sample
            will be used (default is ``None``).
        fmin : float, optional
            Minimum applied microwave frequency in Hz (default is 0).
        fmax : float, optional
            Maximum applied microwave frequency in Hz (default is 30e6).
        num_f : int, optional
            Number of microwave-frequency points (default is 200).
        k : Optional[float]
            If set to a value, the absorption line is calculated only for a single wave vector (default is ``None``).
            This argument is ignored in confined samples.
        label : Optional[str]
            A label for the eigen result, used for saving or identification purposes (default is ``None``).
        save : bool, optional
            Automatically save the result ot the disk (default is True).

        Returns
        -------
        AbsorptionResult
            Immutable container housing the absorption data and providing plotting functionality.

        References
        ----------
        .. [1] Verba, *et al.*, "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal,
               and coordinate-dependent damping", `Phys. Rev. B 98, 104408 (2018) <https://doi.org/10.1103/PhysRevB.98.104408>`_

        .. [2] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes",
               `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

        Examples
        --------
        - :doc:`Field-dependent broadband FMR with rectangular waveguide </examples/FMR_rect_waveguide>`
        - :doc:`Absorption of a nanotube with antennae </examples/round_tube_absorption>`

        See Also
        --------
        linewidths

        """
        if (
            self.result_info.sample_snapshot.sample.material.to_dict()
            != self.result_info.sample_snapshot.compressed_material
        ):
            self.result_info.sample_snapshot.sample.material = (
                self.result_info.sample_snapshot.compressed_material
            )

        return _calculate_absorption(self, antenna, fmin, fmax, num_f, k, label, save)

    def perturb_with(
        self: Self,
        additional_interactions: List[Interaction],
        label: Optional[str] = None,
        save: bool = True,
    ) -> PerturbationResult:
        """
        Perturb the eigen spectrum with additional interactions.

        This experiment calculates the zeroth-order correction to the frequencies when including additional interactions
        to the ones considered in the original eigen result. See Ref. [1]_ for details.

        Parameters
        ----------
        additional_interactions : List[Interaction]
            Additional interactions to perturb the eigen spectrum.
        label : Optional[str]
            A label for the eigen result, used for saving or identification purposes (default is ``None``).
        save : bool, optional
            Automatically save the result ot the disk (default is True).

        Returns
        -------
        PerturbationResult

        References
        ----------
        .. [1]  Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between
                numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104,
                174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_

        Examples
        --------
         - :doc:`Thin film dispersion exact vs perturbation (Kalinikos & Slavin) </examples/thick_film_dispersion_with_perturbation>`

        """
        sample = self.result_info.sample_snapshot.sample

        if (
            self.result_info.sample_snapshot.sample.material.to_dict()
            != self.result_info.sample_snapshot.compressed_material
        ):
            self.result_info.sample_snapshot.sample.material = (
                self.result_info.sample_snapshot.compressed_material
            )

        original_interactions = [
            sample.get_interaction(name) for name in self.config.interaction_names
        ]
        additional_interactions = (
            [additional_interactions]
            if isinstance(additional_interactions, Interaction)
            else additional_interactions
        )
        all_interactions = original_interactions + additional_interactions
        config = PerturbationConfiguration(
            original_interaction_names=[I.name for I in original_interactions],
            additional_interaction_names=[I.name for I in additional_interactions],
        )

        return _calculate_zeroth_order_frequencies(
            self, config, all_interactions, label, save
        )
