from __future__ import annotations

from typing import List, Union

import numpy as np
from scipy.constants import mu_0
from scipy.sparse import bmat, csc_matrix, csr_matrix
from scipy.sparse.linalg import LinearOperator, eigs, lgmres, spilu

from tetrax.common.math import (
    cross_operator,
    diag_matrix_from_field,
    flattened_AFMmesh_vec_scalar_product_separate,
    inner_product,
)
from tetrax.common.typing import MagneticOrder
from tetrax.interactions.interaction import Interaction
from tetrax.sample.material.material import SampleMaterial
from tetrax.sample.mesh import FlattenedAFMMeshVector, FlattenedMeshVector

from .utils import get_single_lattice_rotation, get_two_lattice_rotation


class DynamicMatrix(LinearOperator):
    def __init__(
        self,
        mag: Union[FlattenedMeshVector, FlattenedAFMMeshVector],
        material: SampleMaterial,
        interactions: List[Interaction],
        magnetic_order: MagneticOrder,
        tolerance_inverse: float,
        maxiter_inverse: int,
    ):
        # Get intermediate quantities
        nx = mag.nx
        Msat_average = material["Msat"].average
        gamma_average = material["gamma"].average
        unitless_gamma = material["gamma"].value / gamma_average

        self.frequency_scale = mu_0 * Msat_average * gamma_average / 2 / np.pi
        self._maxiter_inverse = maxiter_inverse

        # set up k independent parts
        # get equilibrium state and rotation matrix

        # get projection of equilibrium effective field
        equilibrium_field_terms = [
            interaction.unitless_field(mag) for interaction in interactions
        ]

        total_equilibrium_field = np.sum(equilibrium_field_terms, axis=0)

        rotation = ...
        h0: np.ndarray
        Lambda = unitless_gamma.to_dia_matrix(tiles=2) @ cross_operator(nx)
        self.shape = Lambda.shape

        if magnetic_order is MagneticOrder.FERROMAGNET:
            rotation = get_single_lattice_rotation(mag)
            h0 = inner_product(mag, FlattenedMeshVector(total_equilibrium_field))
            self.h0_operator = h0.to_dia_matrix(tiles=3)

        elif magnetic_order is MagneticOrder.ANTIFERROMAGNET:
            rotation = get_two_lattice_rotation(mag)
            h0 = flattened_AFMmesh_vec_scalar_product_separate(
                mag, total_equilibrium_field
            )
            self.h0_operator = diag_matrix_from_field(
                np.concatenate(
                    (
                        np.tile(h0[:nx], 3),
                        np.tile(h0[nx:], 3),
                    )
                )
            )

            Lambda = bmat([[Lambda, None], [None, Lambda]])

        self.leftmulmat = 1j * Lambda @ rotation
        self.inverse_rotation = rotation.transpose()

        # get effective spin-wave tensors of the different magnetic self-interactions
        self.spin_wave_tensors = [
            interaction.linearized_tensor(mag)
            for interaction in interactions
            if interaction.is_self_interaction is True
        ]

        # if all spin-wave tensors have sparse-matrix form, dynamic matrix is also assembled as matrix
        self.has_matrix_form = all(N.has_matrix_form for N in self.spin_wave_tensors)

        self.matrix_free_tensors = [
            N for N in self.spin_wave_tensors if not N.has_matrix_form
        ]
        self.sparse_tensor_sum = csr_matrix(self.shape, dtype=np.complex128)
        self.matrix_form_contribution = csr_matrix(self.shape, dtype=np.complex128)

        self.tolerance_inverse = tolerance_inverse

        self.k: float = 0
        self.m: int = 0

        self.set_km(0, 0)

        # initial vector for diaganalization
        self.v0 = np.full(self.shape[0], 1 + 1j, dtype=np.complex128)

        # Setting the shape using self.Lambda respects the change in dimension when dealing with AFMs.

        self.dtype = np.complex128
        super().__init__(self.dtype, self.shape)

    def set_km(self, k, m):
        self.k = k
        self.m = m

        for N in self.spin_wave_tensors:
            N.set_km(k, m)

        self.sparse_tensor_sum = self.h0_operator + np.sum(
            [N.sparse_mat_km for N in self.spin_wave_tensors if N.has_matrix_form],
            axis=0,
        )

        # assemble dynamic matrix as sparse matrix
        self.matrix_form_contribution = csc_matrix(
            self.leftmulmat @ self.sparse_tensor_sum @ self.inverse_rotation
        )

    def _matvec(self, vec_loc):
        if self.has_matrix_form:
            return self.matrix_form_contribution @ vec_loc

        else:
            vec_lab = self.inverse_rotation @ vec_loc
            h_lab = self.sparse_tensor_sum @ vec_lab + np.sum(
                [N @ vec_lab for N in self.matrix_free_tensors], axis=0
            )

            return self.leftmulmat @ h_lab

    # TODO find out if this tolerance is relative or absolute.
    def get_current_eigensystem(self, num_modes: int, tolerance: float):
        """
        Calculates the eigenvalues and eigenvectors for the current wave number :math:`k` and azimuthal
        index :math:`m`.
        """
        matrix_to_diagonalize = (
            self.matrix_form_contribution if self.has_matrix_form else self
        )

        inverse_dynamic_matrix = None
        if not self.has_matrix_form:
            temp_copy = self.matrix_form_contribution.copy()
            preconditioner = SuperLUInv(spilu(csc_matrix(temp_copy)))
            inverse_dynamic_matrix = InverseDynamicMatrix(
                dynamic_matrix=self,
                k=self.k,
                preconditioner=preconditioner,
                tolerance=self.tolerance_inverse,
                maxiter=self._maxiter_inverse,
            )

        freq, eigenvectors = eigs(
            matrix_to_diagonalize,
            v0=self.v0,
            which="LM",
            k=num_modes * 2,
            tol=tolerance,
            sigma=0,
            OPinv=inverse_dynamic_matrix,
        )
        freq *= self.frequency_scale

        return freq.real, eigenvectors


class InverseDynamicMatrix(LinearOperator):
    """Inverse of Dynamic Matrix"""

    def __init__(self, dynamic_matrix, k, tolerance, preconditioner, maxiter):
        self.dynamic_matrix = dynamic_matrix
        self.shape = dynamic_matrix.shape
        self.dtype = np.complex128
        min_tol = 1000 * np.sqrt(self.shape[0]) * np.finfo(self.dtype).eps
        self.tol = max(min_tol, tolerance)
        self.preconditioner = preconditioner
        self.maxiter = maxiter
        self.counter = 0
        self.k = k

        super().__init__(self.dtype, self.shape)

    def _matvec(self, x):
        # logging.debug("inverse of {} requested".format(x))
        # reset demag_op so that it recalculates the demag field on first use
        self.dynamic_matrix.counter = 0
        b, info = lgmres(
            self.dynamic_matrix,
            x,
            rtol=self.tol,
            atol=1000 * np.finfo(self.dtype).eps,
            M=self.preconditioner,
            maxiter=int(self.maxiter),
        )
        if info != 0:
            raise InvertError(
                self.k,
                info,
                self.counter,
                "Error inverting dynamic matrix at k = {} : lgmres did not converge (info = {}). "
                "The inverse was already calculated successfully {} times.".format(
                    self.k, info, self.counter
                ),
            )
        self.counter += 1
        # logging.info("inverted: {} times | gmres iterations: {}".format(self.counter, self.demag_op.counter))
        # print("inverse computed")
        return b


class SuperLUInv(LinearOperator):
    """SuperLU object as LinearOperator"""

    def __init__(self, superlu):
        self.shape = superlu.shape
        self.dtype = np.complex128
        self.superlu = superlu

    def _matvec(self, x):
        return self.superlu.solve(x)


class InvertError(Exception):
    """Exception raised for errors in inversion of total dynamic matrix."""

    def __init__(self, k, after_iter, already_sucess, message):
        self.k = k
        self.after_iter = after_iter
        self.already_sucess = already_sucess
        self.message = message
