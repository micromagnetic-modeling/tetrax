"""Eigenmodes experiment."""

from __future__ import annotations

import os
import pathlib
from functools import partial
from typing import TYPE_CHECKING, List, Optional, Union

import joblib
import numpy as np
import pandas as pd
import tqdm

from tetrax.common.bibliography import add_refereneces_to_bib
from tetrax.common.config import (
    EIGEN_DEFAULT_CPUS,
    EIGEN_DEFAULT_FREQUENCY_TOLERANCE,
    EIGEN_DEFAULT_INVERSE_MAXITER,
    EIGEN_DEFAULT_INVERSE_TOLERANCE,
    EIGEN_DEFAULT_KMAX,
    EIGEN_DEFAULT_KMIN,
    EIGEN_DEFAULT_MMAX,
    EIGEN_DEFAULT_MMIN,
    EIGEN_DEFAULT_NUM_K,
    EIGEN_DEFAULT_NUM_MODES,
    EIGEN_DEFAULT_PARALLEL_BACKEND,
    EIGEN_FREQUENCY_COLUMN_PATTERN,
    EIGEN_LINEWIDTH_COLUMN_PATTERN,
    EIGEN_WAVEVECTOR_COLUMN_NAME,
    EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME,
)
from tetrax.common.io import clear_folder
from tetrax.common.typing import MagneticOrder
from tetrax.experiments._result import ResultInfo, ResultType
from tetrax.sample.mesh.meshquantities import (
    FlattenedLocalMeshVector,
    make_flattened_AFM,
)

from .config import (
    EigenConfiguration,
    EigenOutput,
)
from .dynamic_matrix import DynamicMatrix, InvertError
from .postprocessing.linewidth import ellipticity_local
from .result import EigenResult
from .utils import _prepare_km_space, save_mode_profile

if TYPE_CHECKING:
    from numpy.typing import ArrayLike

    from tetrax.interactions.interaction import Interaction
    from tetrax.sample.mesh.sample_mesh import SampleMesh
    from tetrax.sample.sample import Sample


def eigenmodes(  # noqa: PLR0913
    sample: Sample,
    interactions: Optional[List[Interaction]] = None,
    label: Optional[str] = None,
    num_modes: int = EIGEN_DEFAULT_NUM_MODES,
    kmin: float = EIGEN_DEFAULT_KMIN,
    kmax: float = EIGEN_DEFAULT_KMAX,
    k: Optional[Union[float, ArrayLike]] = None,
    num_k: int = EIGEN_DEFAULT_NUM_K,
    mmin: int = EIGEN_DEFAULT_MMIN,
    mmax: int = EIGEN_DEFAULT_MMAX,
    m: Optional[Union[int, ArrayLike]] = None,
    save_mode_profiles: bool = True,
    num_cpus: int = EIGEN_DEFAULT_CPUS,
    verbose: bool = True,
    frequency_tolerance: float = EIGEN_DEFAULT_FREQUENCY_TOLERANCE,
    inverse_tolerance: float = EIGEN_DEFAULT_INVERSE_TOLERANCE,
    maxiter_inverse: int = EIGEN_DEFAULT_INVERSE_MAXITER,
    optional_output: tuple = (),
    parallel_backend: str = EIGEN_DEFAULT_PARALLEL_BACKEND,
) -> EigenResult:
    """
    Calculate the spin-wave eigenmodes and their frequencies/dispersion of the sample in its current state.

    This experiment calculates the spin-wave normal modes of the sample in its current state using a symmetry-adapted
    finite-element dynamic-matrix method [1]_, [2]_. Along with the frequencies, the spatial mode profiles are
    calculated and can be stored. The experiment returns an :class:`~tetrax.experiments.eigen.result.EigenResult` object
    which provides plotting routines and different post-processing methods such as calculating line widths, absorption
    etc.

    Parameters
    ----------
    sample : Sample
        Sample to calculate the spin-wave modes of.
    interactions : Optional[Iterable[Interaction]]
        The list of magnetic interactions to consider during relaxation. If ``None``, uses the sample's interactions.
    label : Optional[str]
        A label for the eigen result, used for saving or identification purposes (default is ``None``).
    num_modes : int
        Determines how many modes (for confined samples) or branches of the dispersion (for waveguides and layers
        samples) are calculated in total (default is 20).
    kmin : float
        Minimum wave vector in rad/m (default is -40e6). [3]_
    kmax : float
        Maximum wave vector in rad/m (default is 40e6). [3]_
    k : float or ArrayLike
        By supplying an ``ArrayLike`` of wave vectors arbitrary resolution is possible for particular or interesting
        parts of the dispersion.
        If set to a single ``float`` value, the eigensystem is only calculated for a single wave vector
        (default is ``None``). [3]_
    num_k : int
        Number of wave-vector values, for which the mode frequencies are calculated (default is 81). [3]_
    mmin : int
        Minimum azimuthal index. [4]_
    mmax : int
        Maximum azimuthal index. [4]_
    m : int or ArrayLike
        Manually set a single value or a range of azimuthal indices. [4]_
    save_mode_profiles : bool, optional
        Store the calculated mode profiles (default is ``True``).
    num_cpus : int, optional
        Number of CPU cores to be used in parallel for calculations. If set to ``-1`` (default), all available
        cores are used.
    verbose : bppö
        Output progress of the calculation (default is ``True``).
    frequency_tolerance : float, optional
        Frequency tolerance for the iterative eigensolver.
    inverse_tolerance : float, optional
        Tolerance to iteratively calculate the inverse dynamic matrix
        (this argument usually does not need to be changed!)
    maxiter_inverse : int, optional
        Maximum number of iterations to calculate the inverse dynamic matrix
        (this argument usually does not need to be changed!)
    optional_output : EigenOutput
        Optional output (such as line widths) calculated along with the eigen spectrum. This argument might be
        removed in the future.
    parallel_backend : str, optional
        Backend for parallelization (default is ``"loky"``).

    Returns
    -------
    EigenResult
        Immutable container housing the calculated frequencies and mode profiles,
        providing several post-processing methods.

    References
    ----------
    .. [1]  Körber, *et al.*, "Finite-element dynamic-matrix approach for
            spin-wave dispersions in magnonic waveguides with arbitrary
            cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
    .. [2]  Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between
            numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104,
            174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
    .. [3]  This argument is ignored when calculating the eigensystem for samples with :attr:`GeometryType.CONFINED <tetrax.common.typing.GeometryType.CONFINED>` or :attr:`GeometryType.CONFINED_AXIAL <tetrax.common.typing.GeometryType.CONFINED_AXIAL>`.
    .. [4]  This argument is ignored when calculating the eigensystem for samples with :attr:`GeometryType.CONFINED <tetrax.common.typing.GeometryType.CONFINED>`, :attr:`GeometryType.WAVEVGUIDE <tetrax.common.typing.GeometryType.WAVEVGUIDE>` or :attr:`GeometryType.WAVEVGUIDE_AXIAL <tetrax.common.typing.GeometryType.WAVEVGUIDE_AXIAL>`.

    Examples
    --------
     - :doc:`Spin-wave dispersion in thin vortex-state nanotube </examples/round_tube_dispersion_vortex>`
     - :doc:`Spin-wave dispersion in transversally-magnetized rectangular waveguide </examples/rectangular_waveguide_DE>`

    """
    add_refereneces_to_bib(sample, f"eigenmodes{sample.mesh.dimension}d")

    interactions = sample.interactions if interactions is None else interactions

    optional_output = [EigenOutput(o) for o in optional_output]

    folder_name = "eigen" if label is None else f"eigen_{label}"
    experiment_path = pathlib.Path(f"./{sample.name}/{folder_name}/")

    clear_folder(experiment_path)

    if save_mode_profiles:
        os.makedirs(experiment_path / "mode_profiles")

    config = EigenConfiguration(
        results_path=experiment_path,
        km_limits=(k, kmin, kmax, num_k, m, mmin, mmax),
        equilibrium=sample.mag,
        interaction_names=[I.name for I in interactions],  # noqa: E741
        toothless_interaction_names=[
            I.name
            for I in interactions  # noqa: E741
            if I.is_toothless
        ],
        num_modes=num_modes,
        save_modes=save_mode_profiles,
        optional_output=set(optional_output),
        parallel_backend=parallel_backend,
        num_cpus=num_cpus,
        frequency_tolerance=frequency_tolerance,
        inverse_tolerance=inverse_tolerance,
        maxiter_inverse=maxiter_inverse,
    )

    dispersion = _calculate_total_eigensystem(sample, interactions, config, verbose)

    if EigenOutput.LINEWIDTHS in config.optional_output:
        add_refereneces_to_bib(sample, "linewidth")

    result = EigenResult(
        ResultInfo(
            result_type=ResultType.EIGEN,
            label=label,
            parent_path=pathlib.Path(f"./{sample.name}"),
            sample_snapshot=sample.get_snapshot(),
        ),
        config=config,
        spectrum_dataframe=dispersion,
        has_profiles=config.save_modes,
        has_linewidths=EigenOutput.LINEWIDTHS in config.optional_output,
    )

    result.save()

    return result


def _calculate_total_eigensystem(
    sample: Sample,
    interactions: List[Interaction],
    config: EigenConfiguration,
    verbose: bool,
) -> pd.DataFrame:
    km_range = _prepare_km_space(
        sample.geometry_type, sample.mesh.scale, config.km_limits
    )

    if sample.magnetic_order is MagneticOrder.FERROMAGNET:
        mag = sample.mag.to_flattened()
    elif sample.magnetic_order is MagneticOrder.ANTIFERROMAGNET:
        mag = make_flattened_AFM(sample.mag1, sample.mag2)

    dynamic_matrix = DynamicMatrix(
        mag,
        sample.material,
        interactions,
        sample.magnetic_order,
        config.inverse_tolerance,
        config.maxiter_inverse,
    )

    calculate_for_km_partial = partial(
        _calculate_eigensystem_for_km,
        dynamic_matrix=dynamic_matrix,
        config=config,
        mesh=sample.mesh,
        scale=sample.mesh.scale,
        alpha=sample.material["alpha"].value,
        magnetic_order=sample.magnetic_order,
    )

    results = []
    if config.parallel_backend == "none":
        for km in km_range:
            res = calculate_for_km_partial(km)
            results.append(res)
    else:
        results = joblib.Parallel(
            n_jobs=config.num_cpus, backend=config.parallel_backend
        )(
            joblib.delayed(calculate_for_km_partial)(km)
            for km in tqdm.tqdm(km_range, disable=not verbose)
        )

    frequencies = (
        pd.concat(results, sort=False)
        .sort_values(EIGEN_WAVEVECTOR_COLUMN_NAME)
        .reset_index(drop=True)
    )  # .fillna("nan")

    return frequencies.sort_values(
        by=[EIGEN_WAVEVECTOR_COLUMN_NAME, EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME]
    )


def _calculate_eigensystem_for_km(  # noqa: PLR0913
    km: List[tuple],
    dynamic_matrix: DynamicMatrix,
    config: EigenConfiguration,
    mesh: SampleMesh,
    magnetic_order: MagneticOrder,
    scale: float,
    alpha: float,
) -> pd.DataFrame:
    modes_calculated = False
    k, m = km

    # convert k to rad/m for saving
    k_SI = k / scale

    # if conjugate modes are also physical modes (for opposite wave vector)
    conjugate_are_physical = k != 0 or m != 0
    k_SI_conj = -k_SI if k_SI != 0 else k_SI
    m_conj = -m if m != 0 else m

    # update dynamic matrix and obtain formal eigensystem
    dynamic_matrix.set_km(k, m)

    try:
        (
            formal_eigenfrequencies,
            formal_eigenvectors,
        ) = dynamic_matrix.get_current_eigensystem(
            config.num_modes, config.frequency_tolerance
        )
        modes_calculated = True

        # separate physical and conjugate modes
        gtzero_indices = formal_eigenfrequencies.real >= 0
        stzero_indices = formal_eigenfrequencies.real < 0

        physical_frequencies = formal_eigenfrequencies[gtzero_indices].real
        physical_eigenvectors = formal_eigenvectors[:, gtzero_indices]

        # conjugated eigen frequencies are negated right away!!!
        conjugate_frequencies = -formal_eigenfrequencies[stzero_indices].real
        conjugate_eigenvectors = formal_eigenvectors[:, stzero_indices].conjugate()

        formal_modes_separated = [
            (physical_frequencies, physical_eigenvectors, k_SI, m, True),
            (
                conjugate_frequencies,
                conjugate_eigenvectors,
                k_SI_conj,
                m_conj,
                conjugate_are_physical,
            ),
        ]

    except InvertError:
        dummy_frequencies = np.empty(config.num_modes) * np.nan
        dummy_vectors = np.zeros((2, config.num_modes))
        formal_modes_separated = [
            (dummy_frequencies, dummy_vectors, k_SI, m, True),
            (
                dummy_frequencies,
                dummy_vectors,
                k_SI_conj,
                m_conj,
                conjugate_are_physical,
            ),
        ]
        print(
            f"Could not calculate modes at k = {k_SI} rad/m and m = {m}. "
            f"Maybe your equilibrium state is metastable. "
            f"Inserting NaN as frequencies and not saving profiles."
        )

    # This simply avoids duplicate code between physical and conjugate modes.
    temp_list = []
    for temp in formal_modes_separated:
        frequencies, eigenvectors, k_SI_save, m_save, relevant = temp

        if not relevant:
            continue

        if modes_calculated:
            sortindices = np.argsort(frequencies)
            frequencies = frequencies[sortindices]
            eigenvectors = eigenvectors[:, sortindices]

        df_km_temp = pd.DataFrame(
            columns=[EIGEN_WAVEVECTOR_COLUMN_NAME, EIGEN_AZIMUTHAl_INDEX_COLUMN_NAME]
            + [
                EIGEN_FREQUENCY_COLUMN_PATTERN.format(j)
                for j in range(len(frequencies))
            ]
        )

        df_km_temp.loc[0] = [k_SI_save, m_save, *frequencies.tolist()]

        for nu, (_frequency, eigenvector) in enumerate(
            list(zip(frequencies, eigenvectors.T))
        ):
            if not modes_calculated:
                continue

            if config.save_modes:
                save_mode_profile(
                    f"{config.results_path}/mode_profiles/mode_k{k_SI_save}radperm_m{m}_{nu:03d}.vtk",
                    eigenvector,
                    config,
                    mesh,
                    dynamic_matrix.inverse_rotation,
                    magnetic_order,
                )

            if EigenOutput.LINEWIDTHS in config.optional_output:
                epsilon_nu = ellipticity_local(
                    FlattenedLocalMeshVector(eigenvector), mesh.sample.average
                )
                df_km_temp[EIGEN_LINEWIDTH_COLUMN_PATTERN.format(nu)] = (
                    alpha
                    * epsilon_nu
                    * df_km_temp[EIGEN_FREQUENCY_COLUMN_PATTERN.format(nu)]
                )

        temp_list.append(df_km_temp)

    df_km = (
        pd.concat(temp_list, sort=False).sort_values("k (rad/m)").reset_index(drop=True)
    )

    return df_km
