from __future__ import annotations

from typing import TYPE_CHECKING, Protocol

if TYPE_CHECKING:
    from typing import Self


class ExperimentConfig(Protocol):
    """
    Protocol representing a generic configuration of an experiment.

    Configurations are required to at most contain these attributes and methods.

    """

    def to_dict(self: Self):  # noqa: ANN202
        """Convert configuration to a dictionary."""
        ...
