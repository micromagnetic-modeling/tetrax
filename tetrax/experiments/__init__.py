"""
Experiments (:mod:`tetrax.experiments`)
=======================================

.. currentmodule:: tetrax.experiments

This module provides numerical experiments that can be performed with `TetraX`. Most expirements, such as the
:func:`~tetrax.experiments.eigen.solve.eigenmodes` experiment are functions that act on the :class:`~tetrax.sample.sample.Sample`
and return an immutable :class:`~tetrax.experiments._result.Result` object.

.. code-block:: python

    import tetrax as tx

    ...

    eigen_result = tx.experiments.eigenmodes(sample)

Depending on the particular experiment, each :class:`~tetrax.experiments._result.Result` provides methods to plot the
calculated data and perform post-processing on it. For example, the
:class:`~tetrax.experiments.eigen.result.EigenResult` allows calculating the power absorption of the a
spin-wave spectrum with respect to a specified microwave antenna using the
:func:`EigenResult.absorption() <tetrax.experiments.eigen.result.EigenResult.absorption>` method (which returns an
:class:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult`).

By default, each :class:`~tetrax.experiments._result.Result` is automatically saved to the disk. This includes the
calculated data itself, as well as some metadata and the parameters used to perform the calculations. For example,
running the :func:`~tetrax.experiments.eigen.solve.eigenmodes` experiment as written above produces the following output on the disk.

.. code-block::

    my_sample/
        eigen/
            external_field.vtk
            eigenvalue_dataframe.csv
            mode_profiles/
                ...
            equilibrium.vtk
            report.json

with a ``report.json`` file that contains material parameters, experiment configuration, metadata (such as the version of
`TetraX` that has been used) and can be used, for example, for data archiving. By default, running the same experiment
twice will overwrite the old results.
In order to circumvent this, results can be equipped with a ``label`` when running the
experiment. For example,


.. code-block:: python

    eigen_result_A = tx.experiments.eigenmodes(sample, label="A", **some_settings)

    ...

    eigen_result_B = tx.experiments.eigenmodes(sample, label="B", **different_settings)

produces

.. code-block::

    my_sample/
        eigen_A/
            ...
        eigen_B/
            ...

For more information on how to run experiments, see the :doc:`User Guide </usage/experiments>`.

The following functions and classes provide generic functionality for the :mod:`tetrax.experiments` module:

.. autosummary::
    :toctree: generated/

    ~_result.Result
    ~_result.ResultInfo
    ~_result.ResultType
    ~_result.EnvironmentMetaData
    ~_config.ExperimentConfig


Equilibration
-------------

Experiments to calculate (local) magnetic equilibria.

Energy minimization
^^^^^^^^^^^^^^^^^^^

Finding an equilibrium by minimizing the Gibbs free energy is faster than torque minimization but less reliable.

.. autosummary::
    :toctree: generated/

    ~_relax.minimize.relax
    ~_relax.result.RelaxationResult
    ~_relax.config.RelaxationConfiguration

Torque minimization
^^^^^^^^^^^^^^^^^^^

Finding an equilibrium by integrating the over-damped Landau-Lifshitz-Gilbert equation forward in time is slower
than energy minimization but more reliable.

.. autosummary::
    :toctree: generated/

    ~_relax_dynamic.integrate_llg.relax_dynamic
    ~_relax_dynamic.result.DynamicRelaxationResult
    ~_relax_dynamic.config.DynamicRelaxationConfiguration

Eigenmode calculations
----------------------

Eigenmodes are calculated using a dynamic-matrix method.

.. autosummary::
    :toctree: generated/

    ~eigen.solve.eigenmodes
    ~eigen.result.EigenResult
    ~eigen.config.EigenConfiguration
    ~eigen.result.ModeInfo

Currently, the following post-processing steps based on :class:`~eigen.result.EigenResult` s are available:

.. currentmodule:: tetrax.experiments.eigen.result

.. autosummary::
    :toctree: generated/

    EigenResult.linewidths
    EigenResult.absorption
    EigenResult.perturb_with

with the corresponding result types

.. currentmodule:: tetrax.experiments.eigen.postprocessing

.. autosummary::
    :toctree: generated/

    ~absorption.AbsorptionResult
    ~perturbed_frequencies.PerturbationResult


Microwave antennae
------------------

.. currentmodule:: tetrax.experiments

These can be used to calculate the microwave power absorption of a particular spin-wave spectrum. Different antennae
can be visualized after adding it to a :class:`~tetrax.sample.sample.Sample` object. For example

.. code-block:: python

    sample.antenna = tetrax.experiments.StripLineAntenna(width=200,spacing=30) # in mesh units (by default, nm)
    sample.show()

One can calculate the absorption of an :class:`~tetrax.experiments.eigen.result.EigenResult` with respect to an
antenna using

.. code-block:: python

    eigen_result.absorption(antenna=StripLineAntenna(width=200,spacing=30))
    eigen_result.absorption() # used the antenna of the sample

Providing no argument to the :class:`~tetrax.experiments.eigen.result.EigenResult.absorption` method will use the
antenna of the sample.

.. autosummary::
    :toctree: generated/

    ~antenna.MicrowaveAntenna
    ~antenna.CurrentLoopAntenna
    ~antenna.HomogeneousAntenna
    ~antenna.StriplineAntenna
    ~antenna.CPWAntenna
    ~antenna.MultiStriplineAntenna

"""  # noqa: D205, D400, D415
from __future__ import annotations

from tetrax.experiments.antenna import (
    CPWAntenna,
    CurrentLoopAntenna,
    HomogeneousAntenna,
    StriplineAntenna,
)
from tetrax.experiments.eigen.result import EigenResult
from tetrax.experiments.eigen.solve import eigenmodes

from ._relax.minimize import relax
from ._relax_dynamic.integrate_llg import relax_dynamic
