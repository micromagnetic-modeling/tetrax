# This file must only contain the package version number stored in the __version__ variable. Storing it in this file
# takes care that the version can be read from setup.py without importing the package and, at the same time, is only
# stored in a single place. When importing the package, it is also available as tetrax.__version__. Please note that
# version numbers need to comply with PEP 404 https://peps.python.org/pep-0440/.
#
# Here we adopt the scheme "X.Y.Z" with X and Y denoting major and minor release numbers and Z denoting hotfix numbers.
# Development versions are to be denoted by appending a ".devX" with X being a possibly increasing number. Examples
# would be "2.1.0" for a public version or "1.8.1.dev1" for a development version.
__version__ = "2.0.0"
