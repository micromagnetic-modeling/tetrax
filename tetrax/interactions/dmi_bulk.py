"""Definitions of bulk DMI."""

from __future__ import annotations

from typing import TYPE_CHECKING

import numpy as np
from scipy.constants import mu_0
from scipy.sparse import bmat, csr_matrix, dia_matrix, diags

from tetrax.interactions.interaction import (
    EffectiveSpinWaveTensor,
    Interaction,
    InteractionName,
)
from tetrax.sample.mesh.meshquantities import FlattenedMeshVector

if TYPE_CHECKING:
    from typing import Self


class BulkDMI(Interaction):
    r"""
    Bulk (Bloch-type) Dzyaloshinksii-Moriya interaction (DMI).

    This antisymmetric exchange interaction is described by the energy density

    .. math::

        w_{\mathrm{bDMI}} = D_{\mathrm{b}} \mathbf{m} \cdot (\nabla \times\mathbf{m})

    and the unitless effective field

    .. math::

        \mathbf{h}_{\mathrm{bDMI}} = \frac{2D_{\mathrm{b}}}{\mu_0 M_\mathrm{s}^2} \nabla \times\mathbf{m}

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`D_{\mathrm{b}}` the
    bulk DMI constant of the sample.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    """

    is_self_interaction = True
    name = InteractionName.DMI_BULK

    required_params = ("Msat", "Dbulk")

    open_boundary = False
    """
    Whether open boundary conditions are applied (default is False).

    Changing this parameter requires to call the :func:`~tetrax.interactions.dmi_bulk.BulkDMI.update_matrices`
    method of the interaction.
    """

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"BulkDMInteraction(sample '{self.sample.name}')"

    @property
    def is_toothless(self: Self) -> bool:  # noqa: D102
        return not np.any(self.sample.material["Dbulk"].value)

    def prepare_matrices(self: Self) -> None:  # noqa: D102
        zeros_block = csr_matrix((self.sample.mesh.nx, self.sample.mesh.nx))

        self.Sigma = bmat(
            [
                [zeros_block, diags(-1j * np.ones(self.sample.mesh.nx)), zeros_block],
                [diags(1j * np.ones(self.sample.mesh.nx)), zeros_block, zeros_block],
                [zeros_block, zeros_block, zeros_block],
            ],
            format="csr",
        )

    def update_matrices(self: Self) -> None:  # noqa: D102
        self.Dbulk = self.sample.material["Dbulk"].value
        self.Msat = self.sample.material["Msat"].value
        self.Msat_avrg = self.sample.material["Msat"].average

        # includes the boundary terms if open_boundary = False (default)
        self.rot = self.sample.mesh.get_rot(self.open_boundary)

        sparse_mat_with_Dbulk = (self.Dbulk / self.sample.mesh.scale) * self.rot

        self.shape = sparse_mat_with_Dbulk.shape
        self.sparse_mat = (
            dia_matrix(
                (np.tile(2 / (mu_0 * self.Msat * self.Msat_avrg), 3), 0),
                shape=self.shape,
            )
            @ sparse_mat_with_Dbulk
        )

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> FlattenedMeshVector:  # noqa: D102
        return -FlattenedMeshVector(self.sparse_mat.dot(mag)).real

    def linearized_tensor(self: Self, mag: FlattenedMeshVector) -> BulkDMITensor:  # noqa: D102
        return BulkDMITensor(self, mag)


class BulkDMITensor(EffectiveSpinWaveTensor):
    r"""
    Linear tensor of bulk Dzyaloshinskii-Moriya interaction.

    The linear tensor of this antisymmetric exchange interaction, acting on propagating waves, is defined as

    .. math::

        \hat{\mathbf{N}}_{\mathrm{bDMI},k} = \frac{2D_{\mathrm{b}}}{\mu_0 M_\mathrm{s}^2}
        \left(k \hat{\mathbf{\Sigma}}
        + \nabla_{\mathbf{s}} \times ...  \right)

    with :math:`M_{\mathrm{s}}` being the saturation magnetization, :math:`D_{\mathrm{b}}` the
    bulk DMI constant and :math:`\mathbf{s}` the coordinates within the mesh of the sample. Furthermore,
    the matrix :math:`\hat{\mathbf{\Sigma}}` is defined as

    .. math::

        \hat{\mathbf{\Sigma}} = \begin{pmatrix} 0 & -i & 0 \\
                                                i & 0 & 0 \\
                                                0 & 0 & 0 \\
                                \end{pmatrix}.

    For more details, see Ref. [1]_ or chapter 5 of Ref. [2]_.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    References
    ----------
    .. [1]  Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between
            numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104,
            174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
    .. [2]  Körber, "Spin waves in curved magnetic shells", `Ph.D. Thesis, TU Dresden (2023)
            <http://dx.doi.org/10.25368/2023.131>`_

    """

    def set_km(self: Self, k: float, m: int) -> None:  # noqa: D102
        # TODO axial geometry
        self.k = k
        sparse_mat = (
            self.interaction.Dbulk
            / self.interaction.sample.mesh.scale
            * (self.interaction.rot + self.k * self.interaction.Sigma)
        )

        self.sparse_mat_km = (
            dia_matrix(
                (
                    np.tile(
                        2 / (mu_0 * self.interaction.Msat * self.interaction.Msat_avrg),
                        3,
                    ),
                    0,
                ),
                shape=sparse_mat.shape,
            )
            @ sparse_mat
        )
