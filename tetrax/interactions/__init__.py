"""
Interactions (:mod:`tetrax.interactions`)
=========================================

.. currentmodule:: tetrax.interactions

This module defines all magnetic interactions that are included in `TetraX`. Each interaction provides
functionality to calculate the respective contribution to the magnetic free energy, the effective field, was well as
to the effective spin-wave tensors.

The following classes provide generic functionality for different interactions:

.. autosummary::
    :toctree: generated/

    ~interaction.Interaction
    ~interaction.InteractionName
    ~interaction.EffectiveSpinWaveTensor

Below are the individual interactions and their respective effective spin-wave tensors. For detailed information,
click on the name of the respective class.

Magnetodipolar interaction
---------------------------

.. autosummary::
    :toctree: generated/

    ~dipole.DipoleInteraction
    ~dipole.DipoleTensor


Symmetric exchange interactions
-------------------------------

.. autosummary::
    :toctree: generated/

    ~exchange.ExchangeInteraction
    ~exchange.ExchangeTensor
    ~interlayer_exchange.InterlayerExchangeInteraction
    ~interlayer_exchange.InterlayerExchangeTensor

Zeeman interaction
------------------

.. autosummary::
    :toctree: generated/

    ~zeeman.ZeemanInteraction


Magnetocrystalline anisotropies
-------------------------------

.. autosummary::
    :toctree: generated/

    ~cubic_anisotropy.CubicAnisotropy
    ~cubic_anisotropy.CubicAnisotropyTensor
    ~uniaxial_anisotropy.UniaxialAnisotropy
    ~uniaxial_anisotropy.UniaxialAnisotropyTensor


Dzyaloshinskii-Moriya interactions
----------------------------------

.. autosummary::
    :toctree: generated/

    ~dmi_bulk.BulkDMI
    ~dmi_bulk.BulkDMITensor
    ~dmi_interfacial.InterfacialDMI
    ~dmi_interfacial.InterfacialDMITensor


"""  # noqa: D205, D400, D415
