"""Definitions of interfacial DMI."""

from __future__ import annotations

from typing import TYPE_CHECKING

import numpy as np
from scipy.constants import mu_0
from scipy.sparse import bmat, dia_matrix, diags

from tetrax.common.math import cross_product
from tetrax.interactions.interaction import (
    EffectiveSpinWaveTensor,
    Interaction,
    InteractionName,
)
from tetrax.sample.mesh import FlattenedMeshVector, MeshVector

if TYPE_CHECKING:
    from typing import Self


class InterfacialDMI(Interaction):
    r"""
    Interfacial (Néel-type) Dzyaloshinksii-Moriya interaction (DMI).

    This antisymmetric exchange interaction is described by the energy density

    .. math::

        w_{\mathrm{iDMI}} = D_{\mathrm{i}} \left[\mathbf{m} \cdot \nabla (\mathbf{e}_\mathrm{d}\cdot\mathbf{m})
        - (\nabla \cdot \mathbf{m})(\mathbf{e}_\mathrm{d}\cdot\mathbf{m})\right]

    and the unitless effective field

    .. math::

        \mathbf{h}_{\mathrm{iDMI}} = -\frac{2D_{\mathrm{i}}}{\mu_0 M_\mathrm{s}^2}
        \left[\mathbf{e}_\mathrm{d}\cdot(\nabla\cdot  \mathbf{m})
        - \nabla\cdot( \mathbf{m}\cdot \mathbf{e}_\mathrm{d})\right]

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`D_{\mathrm{i}}` the
    interfacial DMI constant of the sample. Furthermore, :math:`\mathbf{e}_\mathrm{d}` denotes the direction
    along which inversion symmetry is broken.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    """

    is_self_interaction = True
    name = InteractionName.DMI_INTERFACIAL
    required_params = ("Msat", "Didmi", "e_d")

    open_boundary = False
    """
    Whether open boundary conditions are applied (default is False).

    Changing this parameter requires to call the
    :func:`~tetrax.interactions.dmi_interfacial.InterfacialDMI.update_matrices`
    method of the interaction.
    """

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"InterfacialDMI(sample '{self.sample.name}')"

    @property
    def is_toothless(self: Self) -> bool:  # noqa: D102
        return not np.any(self.sample.material["Didmi"].value)

    def update_matrices(self: Self) -> None:  # noqa: D102
        Msat = self.sample.material["Msat"].value
        Didmi = self.sample.material["Didmi"].value
        Msat_avrg = self.sample.material["Msat"].average

        e_d: MeshVector = self.sample.material["e_d"].value

        e_d_x_dia = e_d.x.to_dia_matrix()
        e_d_y_dia = e_d.y.to_dia_matrix()
        e_d_z_dia = e_d.z.to_dia_matrix()

        # zeros_block = csr_matrix((nx, nx))

        self.diff_mat = bmat(
            [
                [
                    e_d_x_dia.dot(self.sample.mesh.grad_x),
                    e_d_x_dia.dot(self.sample.mesh.grad_y),
                    e_d_x_dia.dot(self.sample.mesh.grad_z),
                ],
                [
                    e_d_y_dia.dot(self.sample.mesh.grad_x),
                    e_d_y_dia.dot(self.sample.mesh.grad_y),
                    e_d_y_dia.dot(self.sample.mesh.grad_z),
                ],
                [
                    e_d_z_dia.dot(self.sample.mesh.grad_x),
                    e_d_z_dia.dot(self.sample.mesh.grad_y),
                    e_d_z_dia.dot(self.sample.mesh.grad_z),
                ],
            ]
        ) - bmat(
            [
                [
                    self.sample.mesh.grad_x.dot(e_d_x_dia),
                    self.sample.mesh.grad_x.dot(e_d_y_dia),
                    self.sample.mesh.grad_x.dot(e_d_z_dia),
                ],
                [
                    self.sample.mesh.grad_y.dot(e_d_x_dia),
                    self.sample.mesh.grad_y.dot(e_d_y_dia),
                    self.sample.mesh.grad_y.dot(e_d_z_dia),
                ],
                [
                    self.sample.mesh.grad_z.dot(e_d_x_dia),
                    self.sample.mesh.grad_z.dot(e_d_y_dia),
                    self.sample.mesh.grad_z.dot(e_d_z_dia),
                ],
            ]
        )

        if not self.open_boundary:
            nv = self.sample.mesh.normal_vectors_on_nodes
            e_cross_nv = cross_product(e_d.to_flattened(), nv)
            boundary_matrix = -bmat(
                [
                    [None, -diags(e_cross_nv.z), diags(e_cross_nv.y)],
                    [diags(e_cross_nv.z), None, -diags(e_cross_nv.x)],
                    [-diags(e_cross_nv.y), diags(e_cross_nv.x), None],
                ],
                format="csr",
            )
            self.diff_mat -= 0.5 * boundary_matrix

        self.shape = self.diff_mat.shape
        self.sparse_mat = (
            dia_matrix(
                (
                    np.tile(
                        2 * Didmi / (mu_0 * Msat * Msat_avrg * self.sample.mesh.scale),
                        3,
                    ),
                    0,
                ),
                shape=self.shape,
            )
            @ self.diff_mat
        )

        # construct Pi matrix (for k-dep part)
        self.Pi_mat = bmat(
            [
                [None, 1j * e_d_z_dia, 1j * e_d_x_dia],
                [-1j * e_d_z_dia, None, 1j * e_d_y_dia],
                [-1j * e_d_x_dia, -1j * e_d_y_dia, None],
            ]
        )

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> FlattenedMeshVector:  # noqa: D102
        return -FlattenedMeshVector(self.sparse_mat.dot(mag)).real

    def linearized_tensor(self: Self, mag: FlattenedMeshVector) -> InterfacialDMITensor:  # noqa: D102
        return InterfacialDMITensor(self, mag)


class InterfacialDMITensor(EffectiveSpinWaveTensor):
    r"""
    Linear tensor of interfacial Dzyaloshinskii-Moriya interaction.

    The linear tensor of this antisymmetric exchange interaction, acting on propagating waves, is defined as

    .. math::

        \hat{\mathbf{N}}_{\mathrm{iDMI},k} = \frac{2D_{\mathrm{i}}}{\mu_0 M_\mathrm{s}^2}
        \left[k\hat{\mathbf{\Pi}} +
         \mathbf{e}_\mathrm{d} \otimes \nabla - \nabla \otimes  \mathbf{e}_\mathrm{d}  \right]

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`D_{\mathrm{i}}` the
    interfacial DMI constant of the sample. Furthermore, :math:`\mathbf{e}_\mathrm{d}` denotes the direction
    along which inversion symmetry is broken. The matrix :math:`\hat{\mathbf{\Pi}}` is defined as

    .. math::

        \hat{\mathbf{\Pi}} = \begin{pmatrix} 0 & 0 & ie_{\mathrm{d},x} \\
                                                0 & 0 & ie_{\mathrm{d},y}\\
                                                -ie_{\mathrm{d},x} & -ie_{\mathrm{d},y} & 0 \\
                                \end{pmatrix}.

    For more details, see chapter 5 of Ref. [1]_.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    References
    ----------
    .. [1]  Körber, "Spin waves in curved magnetic shells", `Ph.D. Thesis, TU Dresden (2023)
            <http://dx.doi.org/10.25368/2023.131>`_

    """

    def set_km(self: Self, k: float, m: int) -> None:  # noqa: D102
        # TODO axial
        Msat = self.sample.material["Msat"].value
        Didmi = self.sample.material["Didmi"].value
        Msat_avrg = self.sample.material["Msat"].average

        self.k = k

        # diff_mat includes possible boundary conditions
        sparse_mat = self.interaction.diff_mat + self.k * self.interaction.Pi_mat

        self.sparse_mat_km = (
            dia_matrix(
                (
                    np.tile(
                        2 * Didmi / (mu_0 * Msat * Msat_avrg * self.sample.mesh.scale),
                        3,
                    ),
                    0,
                ),
                shape=self.shape,
            )
            @ sparse_mat
        )
