"""Definitions of exchange interaction."""

from __future__ import annotations

from typing import TYPE_CHECKING

import numpy
import numpy as np
from scipy.constants import mu_0
from scipy.sparse import bmat, dia_matrix, diags

from tetrax.common.typing import GeometryType
from tetrax.interactions.interaction import (
    EffectiveSpinWaveTensor,
    Interaction,
    InteractionName,
)
from tetrax.sample.mesh import FlattenedMeshVector
from tetrax.sample.mesh.fem_core.cythoncore import (
    ExchangeOperator2D,
    ExchangeOperator3D,
)
from tetrax.sample.mesh.fem_core.fempreproc import ExchangeOperator1D
from tetrax.sample.mesh.sample_mesh import GEOMETRY_INFO

if TYPE_CHECKING:
    from typing import Self

EXCHANGE_MATRIX_FUNCTIONS = {
    "confined": ExchangeOperator3D,
    "confined_axial": ExchangeOperator2D,
    "waveguide": ExchangeOperator2D,
    "waveguide_axial": ExchangeOperator1D,
    "layer": ExchangeOperator1D,
}

# TODO the radial should have its own function


class ExchangeInteraction(Interaction):
    r"""
    Symmetric exchange interation.

    The symmetric exchange interaction is determined by the energy density

    .. math::

        w_{\mathrm{ex}} = A_{\mathrm{ex}} (\nabla \mathbf{m})^2

    and the corresponding unitless effective field

    .. math::

        \mathbf{h}_{\mathrm{ex}} = \frac{2}{\mu_0 M_\mathrm{s}^2} \nabla\cdot(A \nabla \mathbf{m})

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`A_{\mathrm{ex}}` being exchange-stiffness
    constant of the sample.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    """

    is_self_interaction = True
    name = InteractionName.EXCHANGE
    required_params = ("Msat", "Aex")

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"ExchangeInteraction(sample='{self.sample.name}')>"

    @property
    def is_toothless(self: Self) -> bool:  # noqa: D102
        return not numpy.any(self.sample.material["Aex"].value)

    def prepare_matrices(self: Self) -> None:  # noqa: D102
        self.cell_types = GEOMETRY_INFO[self.sample.geometry_type.value][
            "meshio_cell_type_closure"
        ]
        self.bnd_cell_types = GEOMETRY_INFO[self.sample.geometry_type.value][
            "meshio_cell_type_boundary"
        ]
        self.get_exchange_matrix = EXCHANGE_MATRIX_FUNCTIONS[
            self.sample.geometry_type.value
        ]
        self.not_radial = self.sample.geometry_type not in (
            GeometryType.WAVEGUIDE_AXIAL,
            GeometryType.CONFINED_AXIAL,
        )
        self.factor = 2 / (mu_0 * self.sample.mesh.scale**2)
        self.rho = self.sample.xyz.x.copy()

    def update_matrices(self: Self) -> None:  # noqa: D102
        Msat = self.sample.material["Msat"].value
        Aex = self.sample.material["Aex"].value
        Msat_avrg = self.sample.material["Msat"].average

        sparse_mat_with_Aex = self.get_exchange_matrix(
            self.sample.xyz,
            self.sample.mesh._meshio_mesh.get_cells_type(self.cell_types),
            self.sample.mesh._meshio_mesh.get_cells_type(self.bnd_cell_types),
            Aex,
            self.sample.geometry_type,
        )

        self.shape = sparse_mat_with_Aex.shape
        self.sparse_mat = (
            dia_matrix(
                (
                    np.tile(
                        2 / (mu_0 * Msat_avrg * Msat * self.sample.mesh.scale**2), 3
                    ),
                    0,
                ),
                shape=self.shape,
            )
            @ sparse_mat_with_Aex
        )

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> FlattenedMeshVector:  # noqa: D102
        return -FlattenedMeshVector(self.sparse_mat.dot(mag))

    def linearized_tensor(self: Self, mag: FlattenedMeshVector) -> ExchangeTensor:  # noqa: D102
        return ExchangeTensor(self, mag)


class ExchangeTensor(EffectiveSpinWaveTensor):
    r"""
    Exchange tensor.

    The linear tensor of the symmetric interaction, acting on propagating waves is defined as

    .. math::

        \hat{\mathbf{N}}_{\mathrm{ex},k} = \frac{2}{\mu_0 M_\mathrm{s}^2} \left(A_{\mathrm{ex}}k^2 \hat{\mathbf{I}}
        - \nabla_{\mathbf{s}} A_{\mathrm{ex}} \nabla_{\mathbf{s}} \right)

    with :math:`M_{\mathrm{s}}` being the saturation magnetization, :math:`A_{\mathrm{ex}}` the exchange-stiffness
    constant and :math:`\mathbf{s}` the coordinates within the mesh of the sample. Furthermore,
    :math:`\hat{\mathbf{I}}` is the identity matrix.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    """

    def set_km(self: Self, k: float, m: int) -> None:  # noqa: D102
        self.k = k
        self.m = m

        Msat = self.sample.material["Msat"].value
        Aex = self.sample.material["Aex"].value
        Msat_avrg = self.sample.material["Msat"].average

        factor_matrix = dia_matrix(
            (
                np.tile(
                    2 * Aex / (mu_0 * Msat_avrg * Msat * self.sample.mesh.scale**2), 3
                ),
                0,
            ),
            shape=self.shape,
        )
        self.sparse_mat_km = self.sparse_matkm0 + self.k**2 * factor_matrix

        # TODO God please help me
        if (self.sample.geometry_type is GeometryType.WAVEGUIDE_AXIAL) or (
            self.sample.geometry_type is GeometryType.CONFINED_AXIAL
        ):
            if np.any(self.interaction.rho == 0):
                self.per_rho = np.concatenate(
                    (
                        np.array([2 / self.interaction.rho[2]]),
                        1 / self.interaction.rho[1:],
                    )
                )
            else:
                self.per_rho = 1 / self.interaction.rho
            self.sparse_mat_km += (
                self.m**2
                * factor_matrix
                * dia_matrix((np.tile(self.per_rho**2, 3), 0), shape=self.shape)
            )
            dia = diags(self.per_rho**2)
            self.sparse_mat_km += factor_matrix * bmat(
                [
                    [diags(np.zeros(self.sample.mesh.nx)), 2j * self.m * dia, None],
                    [-2j * self.m * dia, diags(np.zeros(self.sample.mesh.nx)), None],
                    [None, None, diags(np.zeros(self.sample.mesh.nx))],
                ]
            )
