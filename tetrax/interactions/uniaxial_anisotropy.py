"""Definition of uniaxial anisotropy."""

from __future__ import annotations

from typing import TYPE_CHECKING

import numpy
import numpy as np
from scipy.constants import mu_0
from scipy.sparse import dia_matrix

from tetrax.common.math import tensor_product
from tetrax.sample.mesh import FlattenedMeshVector

from .interaction import EffectiveSpinWaveTensor, Interaction, InteractionName

if TYPE_CHECKING:
    from typing import Self


class UniaxialAnisotropy(Interaction):
    r"""
    Uniaxial magnetocrystalline anisotropy.

    Uniaxial anisootropy up to first order is determined by the energy density

    .. math::

        w_{\mathrm{uni}} = - K_{\mathrm{u1}}(\mathbf{m}\cdot\mathbf{e}_{\mathrm{u}})^2

    and the corresponding unitless effective field

    .. math::

        \mathbf{h}_{\mathrm{uni}} = \frac{2K_{\mathrm{u1}}}{\mu_0 M_\mathrm{s}^2} \mathbf{e}_\mathrm{u}
        (\mathbf{e}_\mathrm{u} \cdot \mathbf{m})

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{u1}` being the first-order
    uniaxial-anisotropy constant of the sample.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    """

    is_self_interaction = True
    name = InteractionName.UNIAXIAL_ANISOTROPY
    required_params = ("Msat", "Ku1", "e_u")

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"<UniaxialAnistropy object of sample '{self.sample.name}'>"

    @property
    def is_toothless(self: Self) -> bool:  # noqa: D102
        return not numpy.any(self.sample.material["Ku1"].value)

    def update_matrices(self: Self) -> None:  # noqa: D102
        e_u_flat = self.sample.material["e_u"].value.to_flattened()
        Ku1 = self.sample.material["Ku1"].value
        Msat = self.sample.material["Msat"].value
        Msat_avrg = self.sample.material["Msat"].average

        e_u_dyad = tensor_product(e_u_flat, e_u_flat)

        self.sparse_mat = (
            dia_matrix(
                (np.tile(-2 * Ku1 / (mu_0 * Msat * Msat_avrg), 3), 0),
                shape=e_u_dyad.shape,
            )
            @ e_u_dyad
        )
        self.shape = self.sparse_mat.shape
        self.dtype = np.complex128

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> FlattenedMeshVector:  # noqa: D102
        return -FlattenedMeshVector(self.sparse_mat.dot(mag))

    def linearized_tensor(  # noqa: D102
        self: Self, mag: FlattenedMeshVector
    ) -> UniaxialAnisotropyTensor:
        return UniaxialAnisotropyTensor(self, mag)


class UniaxialAnisotropyTensor(EffectiveSpinWaveTensor):
    r"""
    Linearized tensor of the uniaxial anisotropy.

    The linearized tensor is given by

    .. math::

        \hat{\mathbf{N}}_{\mathrm{uni}} = -\frac{2K_{u1}}{\mu_0 M_{\mathrm{s}}^2} \mathbf{e}_\mathrm{u} \otimes
        \mathbf{e}_\mathrm{u}

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{u1}` being the first-order
    uniaxial-anisotropy constant of the sample, and :math:`\otimes` being the dyadic product.

    This operator is not dependent on any wave vectors.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    """
