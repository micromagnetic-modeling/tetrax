"""Definitions of interlayer-exchange interaction."""

from __future__ import annotations

from typing import TYPE_CHECKING

import numpy as np
from scipy.constants import mu_0
from scipy.sparse import bmat, dia_matrix, lil_matrix

from tetrax.common.config import BOUNDARY_NODES_OF_BILAYER
from tetrax.interactions.interaction import (
    EffectiveSpinWaveTensor,
    Interaction,
    InteractionName,
)
from tetrax.sample.mesh import FlattenedMeshVector
from tetrax.sample.mesh.sample_mesh import GeometryType

if TYPE_CHECKING:
    from typing import Self

    from tetrax.sample.sample import Sample


class InterlayerExchangeInteraction(Interaction):
    r"""
    Interlayer-exchange interaction.

    The interlayer-exchange interaction mediated by RKKY cpupling between adjacent magnetic layers is described by the
    energy surface density

    .. math::

        w_{\mathrm{IEC}} = - J\mathbf{m}(\mathbf{r})\cdot \mathbf{m}(P(\mathbf{r}))

    and the unitless effect field (defined only at the surface)

    .. math::

        \mathbf{h}_{\mathrm{IEC}} = \frac{J_1}{\mu_0 M_\mathrm{s}^2} \mathbf{m}(P(\mathbf{r}))

    with :math:`J_1` being the bilinear interlayer-exchange constant of the sample an $P$ the mapping of a point
    :math:`\mathbf{r}` to the nearest point on the opposite surface.

    .. note::

        Up to now, this interaction is only implemented for
        :attr:`GeometryType.LAYER <tetrax.common.typing.GeometryType.LAYER>`.

    """

    is_self_interaction = True
    name = InteractionName.INTERLAYER_EXCHANGE
    required_params = ("Msat", "J1")

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"InterlayerExchangeInteraction(sample='{self.sample.name}')"

    def __init__(self: Self, sample: Sample) -> None:  # noqa: D107
        if sample.geometry_type is not GeometryType.LAYER:
            raise NotImplementedError(
                "Interlayer-exchange interaction is only implemented for layer samples."
            )

        super().__init__(sample)

    def update_matrices(self: Self) -> None:  # noqa: D102
        sample = self.sample
        self.sparse_mat = lil_matrix((3 * sample.mesh.nx, 3 * sample.mesh.nx))
        self.shape = self.sparse_mat.shape
        self.dtype = np.complex128

        if sample.mesh.nb >= BOUNDARY_NODES_OF_BILAYER:
            J1 = sample.material["J1"].value
            J1 = [J1] if np.shape(J1) == () else J1

            Msat = sample.material["Msat"].value
            Msat_avrg = self.sample.material["Msat"].average

            sort_indices = np.argsort(sample.xyz[sample.mesh.boundary_nodes, 1])
            sparse_block = lil_matrix((sample.mesh.nx, sample.mesh.nx))

            for spacer_index, (node, next_node) in enumerate(
                zip(
                    sample.mesh.boundary_nodes[sort_indices[1::2]],
                    sample.mesh.boundary_nodes[sort_indices[2::2]],
                )
            ):
                sparse_block[node, next_node] = sparse_block[next_node, node] = (
                    J1[spacer_index]
                    * 2
                    / (
                        sample.mesh.node_volumes[node]
                        + sample.mesh.node_volumes[next_node]
                    )
                )

            self.sparse_mat = bmat(
                [
                    [sparse_block, None, None],
                    [None, sparse_block, None],
                    [None, None, sparse_block],
                ]
            )

            self.sparse_mat = (
                dia_matrix(
                    (np.tile(-1 / (mu_0 * Msat * Msat_avrg * sample.mesh.scale), 3), 0),
                    shape=self.shape,
                )
                @ self.sparse_mat
            )

        self.sparse_mat.tocsr()

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> FlattenedMeshVector:  # noqa: D102
        return -FlattenedMeshVector(self.sparse_mat.dot(mag))

    def linearized_tensor(  # noqa: D102
        self: Self, mag: FlattenedMeshVector
    ) -> InterlayerExchangeTensor:
        return InterlayerExchangeTensor(self, mag)


class InterlayerExchangeTensor(EffectiveSpinWaveTensor):
    r"""
    Linear tensor of interlayer-exchange interaction.

    For :attr:`GeometryType.LAYER <tetrax.common.typing.GeometryType.LAYER>` the tensor consist of an
    off-diagonal sparse block matrix with blocks

    .. math::

        \hat{\mathbf{N}}_{\mathrm{IEC}}^{(i,j)} = \hat{\mathbf{N}}_{\mathrm{IEC}}^{(j,i)} =
        \frac{J_1}{\mu_0 M_\mathrm{s}^2} \frac{2}{c_i + c_j}\hat{\mathbf{I}}

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`J_1` the bilinear interlayer-exchange
    constant of the sample, :math:`i\neq j` the indices of the
    two couples boundary elements, $c_i$ and $c_j$ their associated nodes volumes and :math:`\hat{\mathbf{I}}` the
    identity matrix. For more details, see Ref. [1]_.

    This tensor is not wave-vector dependent.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    References
    ----------
    .. [1]  Körber, *et al.*, "Finite-element dynamic-matrix approach for propagating
            spin waves: Extension to mono- and multi-layers of arbitrary spacing
            and thickness", `AIP Advances 12, 115206 (2022) <https://doi.org/10.1063/5.0107457>`_

    """
