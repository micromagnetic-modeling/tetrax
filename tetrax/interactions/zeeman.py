"""Definition of the Zeeman interaction."""

from __future__ import annotations

from typing import TYPE_CHECKING, Self

import numpy
from scipy.constants import mu_0

from tetrax.common.math import inner_product

from .interaction import Interaction, InteractionName

if TYPE_CHECKING:
    from scipy.sparse.linalg import LinearOperator

    from tetrax.sample.mesh import FlattenedMeshVector, MeshScalar


class ZeemanInteraction(Interaction):
    r"""
    Interaction of the magnetization with an external field :math:`\mathbf{B}`.

    The Zeeman interaction is determined by the energy density

    .. math::

        w_{\mathrm{Z}} = - M_{\mathrm{s}}\mathbf{m}\cdot\mathbf{B}

    with :math:`M_{\mathrm{s}}` being the saturation magnetization of the sample.

    .. note::

        This interaction does not produce a linearized tensor.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    """

    is_self_interaction = False
    name = InteractionName.ZEEMAN
    required_params = ()

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"<ZeemanInteraction object of sample '{self.sample.name}'>"

    @property
    def is_toothless(self: Self) -> bool:  # noqa: D102
        return not numpy.any(self.sample.external_field.to_flattened())

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> FlattenedMeshVector:  # noqa: D102
        return self.sample.external_field.to_flattened() / (
            self.sample.material["Msat"].average * mu_0
        )

    def energy_density(self: Self, mag: FlattenedMeshVector) -> MeshScalar:  # noqa: D102
        return (
            -1
            * self.sample.material["Msat"].average
            * inner_product(self.sample.external_field.to_flattened(), mag)
        )

    def linearized_tensor(self: Self, mag: FlattenedMeshVector) -> LinearOperator:
        """This interaction does not produce a linearized tensor."""  # noqa: D401, D404
