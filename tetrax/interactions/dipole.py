"""Definitions of dipolar interaction."""

from __future__ import annotations

from typing import TYPE_CHECKING, List

import numpy as np
from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import LinearOperator, bicg, spsolve

import tetrax.sample.mesh.fem_core.fempreproc
from tetrax.common.config import DIPOLE_DEFAULT_BNDINT_ORDER
from tetrax.common.math import inner_product
from tetrax.common.typing import GeometryType
from tetrax.interactions.interaction import (
    EffectiveSpinWaveTensor,
    Interaction,
    InteractionName,
)
from tetrax.sample.mesh import FlattenedMeshVector, MeshScalar, MeshVector
from tetrax.sample.mesh.fem_core.cythoncore import (
    ComputeDenseMatrix_2D,
    ComputeDenseMatrix_2D_radial,
    ComputeDenseMatrix_3D,
)

if TYPE_CHECKING:
    from typing import Self

    from numpy._typing import NDArray

DENSE_MATRIX_FUNCTIONS = {
    "confined": ComputeDenseMatrix_3D,
    "confined_axial": ComputeDenseMatrix_2D_radial,
    "waveguide": ComputeDenseMatrix_2D,
    "waveguide_axial": tetrax.sample.mesh.fem_core.fempreproc.ComputeDenseMatrix_1D_radial,
    "layer": tetrax.sample.mesh.fem_core.fempreproc.ComputeDenseMatrix_1D,
}


# TODO this function needs to be cleaned up
def _calculate_dipolar_field(  # noqa: PLR0913
    mag: FlattenedMeshVector,
    k: float,
    m: int,
    rho: MeshScalar,
    ksquared_mat_a_n: csr_matrix,
    ksquared_mat_a_nb: csr_matrix,
    nx: int,
    div_x: csr_matrix,
    div_y: csr_matrix,
    div_z: csr_matrix,
    poiss: csr_matrix,
    boundary_nodes: List[int],
    dense: NDArray,
    laplace: csr_matrix,
    grad_x: csr_matrix,
    grad_y: csr_matrix,
    grad_z: csr_matrix,
    node_volumes: MeshScalar,
    dim: int,
    unitless_msat: MeshScalar,
    is_cyl: bool,
) -> NDArray:
    # calculate charges
    # dispatcher = []

    m_x = mag[:nx]
    m_y = mag[nx : 2 * nx]
    m_z = mag[2 * nx :]

    fact_inhom = 1
    fact_lapl = -1
    if dim == 1:
        fact_inhom = -1
        fact_lapl = 1

    if is_cyl:
        rhs = (
            m_x / rho * node_volumes
            + div_x.dot(unitless_msat * m_x)
            + 1j * m * unitless_msat * m_y / rho * node_volumes
            + div_z.dot(unitless_msat * m_z)
            + 1j * k * unitless_msat * m_z * node_volumes
        )
    #        rhs = m_x / rho * node_volumes + div_x.dot(unitless_msat * m_x) + 1j * m * unitless_msat * m_y / rho * node_volumes + 1j * k * unitless_msat * m_z * node_volumes
    else:
        rhs = (
            div_x.dot(unitless_msat * m_x)
            + div_y.dot(unitless_msat * m_y)
            + div_z.dot(unitless_msat * m_z)
            + 1j * k * unitless_msat * m_z * node_volumes
        )

    if dim == 1 and not is_cyl:
        psi1, exitCode = bicg(poiss - ksquared_mat_a_n, rhs)
        if exitCode:
            rhs = np.zeros_like(m_x) + 0j
            psi1, exitCode = bicg(poiss - ksquared_mat_a_n, rhs)
#            psi1 = np.zeros_like(m_x)
        #    raise ValueError(f"Calculating phi1 failed for k = {k}.")
    else:
        psi1 = spsolve(poiss - ksquared_mat_a_n, rhs)

    # get boundary conditions for psi2

    sigma = np.zeros(nx) + 0j
    sigma[boundary_nodes] = dense.dot(psi1[boundary_nodes])

    # get imhom laplace
    sigma2 = fact_inhom * (poiss - ksquared_mat_a_n).dot(sigma)
    #    sigma2 = (poiss - ksquared_mat_a_n).dot(sigma)
    sigma2[boundary_nodes] = sigma[boundary_nodes]

    # solve for psi2 potential
    psi2 = spsolve(laplace - ksquared_mat_a_n, fact_lapl * sigma2)
    # psi2, _ = bicg(laplace - ksquared_mat_a_n, fact_lapl*sigma2)

    # add up to make full potential
    psi = psi1 + psi2

    # calculate (negative) lateral dipolar field
    Nm_x = grad_x.dot(psi)
    Nm_y = grad_y.dot(psi) + 1j * m * psi / rho if is_cyl else grad_y.dot(psi)

    # calculate (negative) longitudinal dipolar field
    Nm_z = grad_z.dot(psi) + 1j * k * psi
    return np.array([Nm_x, Nm_y, Nm_z]).flatten()


class DipoleTensor(EffectiveSpinWaveTensor):
    r"""
    Magnetodipolar (or demagnetizing) tensor.

    The magnetodipolar tensor acts on the magnetization as

    .. math::

        \hat{\mathbf{N}}_{\mathrm{dip}}\mathbf{m} = \nabla \Phi[\mathbf{m}]

    Here, :math:`\Phi[\mathbf{m}]` is the unitless magnetostatic potential of the magnetization :math:`\mathbf{m}`.
    In TetraX, this potential is calculated using a
    hybrid FEM/BEM method [1]_.

    To solve eigenvalue problem for propagating spin waves, the dynamic dipolar fields are calculated using the
    effective spin-wave dipolar tensor

    .. math::

        \hat{\mathbf{N}}_{\mathrm{dip},k}\mathbf{m}_k(\mathbf{s}) = (\nabla_{\mathbf{s}} + ik\mathbf{e}_z) \psi_k(\mathbf{s})

    with :math:`\psi_{k}` being the complex lateral potential and :math:`\mathbf{s}` denoting the coordinates within
    the mesh. In `TetraX`, the lateral potential is calculated using the FEM/BEM
    method extended for propagating waves. For details see Refs. [2]_ and [3]_.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    References
    ----------
    .. [1]  D. R. Fredkin and T. R. Koehler, “Hybrid method for computing demagnetizing fields”,
            `IEEE Trans. Magn. 26, 415-417 (1990) <https://doi.org/10.1109/20.106342>`_
    .. [2]  Körber, *et al.*, "Finite-element dynamic-matrix approach for
            spin-wave dispersions in magnonic waveguides with arbitrary
            cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
    .. [3]  Körber, *et al.*, "Finite-element dynamic-matrix approach for propagating
            spin waves: Extension to mono- and multi-layers of arbitrary spacing
            and thickness", `AIP Advances 12, 115206 (2022) <https://doi.org/10.1063/5.0107457>`_

    """

    has_matrix_form = False

    def __init__(self: Self, interaction: Interaction) -> None:  # noqa: D107
        self.dim = interaction.dim
        self.beta = interaction.unitless_msat
        self.is_cyl = interaction.is_cyl
        self.N_dip = interaction.N_dip
        self.dtype = np.complex128

        self.dense_k = interaction.dense.copy()
        """Current Dirichlet (dense) matrix within the FEM/BEM method."""

        self.a_n = interaction.sample.mesh.cartesian_node_volumes
        self.rho = interaction.rho
        self.nb = interaction.sample.mesh.nb
        self.boundary_nodes = interaction.sample.mesh.boundary_nodes
        self.k = 0
        self.m = 0
        self.xyz = interaction.sample.xyz
        self.belm = interaction.sample.mesh.boundary_elements
        self.sample = interaction.sample
        self.nv = interaction.sample.mesh.normal_vectors
        self.pang = interaction.sample.mesh.boundary_angles
        self.bndint_order = interaction.bndint_order
        """Integration order for the Dirichlet (dense) matrix within the FEM/BEM method."""

        self.nx = interaction.sample.mesh.nx
        self.ksquared_matb = interaction.ksquared_matb
        self.shape = (3 * self.nx, 3 * self.nx)
        self.compute_dense_matrix = interaction.compute_dense_matrix
        self.set_km(0, 0)

    def set_km(self: Self, k: float, m: int) -> None:  # noqa: D102
        self.k = k
        self.m = m
        self.ksquared_mat = k**2 * csr_matrix(diags(self.a_n))
        if m != 0:
            self.ksquared_mat += self.m**2 * csr_matrix(diags(self.a_n / self.rho**2))

        self.dense_k = np.empty((self.nb * self.nb))
        self.dense_k = np.reshape(self.dense_k, (self.nb * self.nb))
        self.compute_dense_matrix(
            self.dense_k,
            self.belm,
            self.boundary_nodes,
            self.xyz,
            self.nv,
            self.pang,
            self.bndint_order,
            self.k,
            self.m,
        )
        self.dense_k = np.reshape(self.dense_k, (self.nb, self.nb))

    def _matvec(self: Self, vec: FlattenedMeshVector) -> FlattenedMeshVector:
        return FlattenedMeshVector(
            self.N_dip(
                vec,
                self.k,
                self.m,
                self.rho,
                self.ksquared_mat,
                self.ksquared_matb,
                self.sample.mesh.nx,
                self.sample.mesh.div_x,
                self.sample.mesh.div_y,
                self.sample.mesh.div_z,
                self.sample.mesh.poisson,
                self.sample.mesh.boundary_nodes,
                self.dense_k,
                self.sample.mesh.laplace,
                self.sample.mesh.grad_x,
                self.sample.mesh.grad_y,
                self.sample.mesh.grad_z,
                self.sample.mesh.cartesian_node_volumes,
                self.dim,
                self.beta,
                self.is_cyl,
            )
        )


class DipoleInteraction(Interaction):
    r"""
    Magnetodipolar interaction.

    The magnetic self interaction due to magneto-dipolar fields expressed in terms of the energy density

    .. math::

        w_{\mathrm{dip}} = - \frac{\mu_0 M_{\mathrm{s}}^2}{2}\mathbf{m}\cdot\mathbf{h}_{\mathrm{dip}}

    with the unitless dipolar field

    .. math::

        \mathbf{h}_{\mathrm{dip}} = -\nabla \Phi[\mathbf{m}]

    Here, :math:`M_{\mathrm{s}}` is the saturation magnetization of the sample and :math:`\Phi[\mathbf{m}]` is the
    unitless magnetostatic potential of the magnetization :math:`\mathbf{m}`. In TetraX, this potential is calculated using a
    hybrid FEM/BEM method [1]_ with its extensions ([2]_ and [3]_) for propagating modes.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    References
    ----------
    .. [1]  D. R. Fredkin and T. R. Koehler, “Hybrid method for computing demagnetizing fields”,
            `IEEE Trans. Magn. 26, 415-417 (1990) <https://doi.org/10.1109/20.106342>`_
    .. [2]  Körber, *et al.*, "Finite-element dynamic-matrix approach for
            spin-wave dispersions in magnonic waveguides with arbitrary
            cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
    .. [3]  Körber, *et al.*, "Finite-element dynamic-matrix approach for propagating
            spin waves: Extension to mono- and multi-layers of arbitrary spacing
            and thickness", `AIP Advances 12, 115206 (2022) <https://doi.org/10.1063/5.0107457>`_

    """

    is_self_interaction = True
    name = InteractionName.DIPOLE
    required_params = ("Msat",)

    bndint_order = DIPOLE_DEFAULT_BNDINT_ORDER
    """Integration order for the Dirichlet (dense) matrix within the FEM/BEM method."""

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"DipoleInteraction(sample='{self.sample.name}')>"

    def prepare_matrices(self: Self) -> None:  # noqa: D102
        self.dim = self.sample.mesh.dimension
        self.rho = self.sample.xyz.x.copy()
        self.unitless_msat = None

        self.is_cyl = self.sample.geometry_type in (
            GeometryType.WAVEGUIDE_AXIAL,
            GeometryType.CONFINED_AXIAL,
        )
        # This section has to be changed!!!
        if self.sample.geometry_type is GeometryType.WAVEGUIDE_AXIAL and np.any(
            self.rho == 0
        ):
            rho_index = np.argsort(self.rho)
            self.rho[rho_index[0]] = self.rho[rho_index[1]] / 2.0
        self.compute_dense_matrix = DENSE_MATRIX_FUNCTIONS[
            self.sample.geometry_type.value
        ]
        self.dense = np.empty((self.sample.mesh.nb * self.sample.mesh.nb))
        self.dense = np.reshape(self.dense, (self.sample.mesh.nb * self.sample.mesh.nb))
        self.compute_dense_matrix(
            self.dense,
            self.sample.mesh.boundary_elements,
            self.sample.mesh.boundary_nodes,
            self.sample.xyz,
            self.sample.mesh.normal_vectors,
            self.sample.mesh.boundary_angles,
            self.bndint_order,
            0,
            0,
        )
        self.dense = np.reshape(self.dense, (self.sample.mesh.nb, self.sample.mesh.nb))
        self.ksquared_mat = lil_matrix((self.sample.mesh.nx, self.sample.mesh.nx))
        self.ksquared_matb = lil_matrix((self.sample.mesh.nb, self.sample.mesh.nb))
        self.N_dip = _calculate_dipolar_field

    def update_matrices(self: Self) -> None:  # noqa: D102
        self.unitless_msat = (
            self.sample.material["Msat"].value / self.sample.material["Msat"].average
        )

    def unitless_field(self: Self, mag: FlattenedMeshVector) -> MeshVector:  # noqa: D102
        return -FlattenedMeshVector(
            self.N_dip(
                mag,
                0,
                0,
                self.rho,
                self.ksquared_mat,
                self.ksquared_matb,
                self.sample.mesh.nx,
                self.sample.mesh.div_x,
                self.sample.mesh.div_y,
                self.sample.mesh.div_z,
                self.sample.mesh.poisson,
                self.sample.mesh.boundary_nodes,
                self.dense,
                self.sample.mesh.laplace,
                self.sample.mesh.grad_x,
                self.sample.mesh.grad_y,
                self.sample.mesh.grad_z,
                self.sample.mesh.cartesian_node_volumes,
                self.dim,
                self.unitless_msat,
                self.is_cyl,
            ).real
        )

    def energy_density(self: Self, mag: FlattenedMeshVector) -> MeshScalar:  # noqa: D102
        return (
            -0.5
            * mu_0
            * self.sample.material["Msat"].average ** 2
            * inner_product(self.unitless_field(mag), mag)
        )

    def linearized_tensor(self: Self, mag: FlattenedMeshVector) -> LinearOperator:  # noqa: D102
        return DipoleTensor(self)
