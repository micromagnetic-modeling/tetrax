from __future__ import annotations

from typing import TYPE_CHECKING

import numpy as np
from scipy.constants import mu_0

from tetrax.common.math import inner_product, tensor_product
from tetrax.sample.mesh.meshquantities import FlattenedMeshVector

from .interaction import EffectiveSpinWaveTensor, Interaction, InteractionName

if TYPE_CHECKING:
    from typing import Self

    from tetrax.sample.mesh.meshquantities import MeshVector


class CubicAnisotropy(Interaction):
    r"""
    Cubic magnetocrystalline anisotropy.

    Cubic anisootropy up to first order is determined by the energy density

    .. math::

        w_{\mathrm{cub}} = K_{\mathrm{c1}}\sum_{i\neq j}(\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}i})^2
        (\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}j})^2

    and the corresponding unitless effective field

    .. math::

        \mathbf{h}_{\mathrm{uni}} = -\frac{2K_{\mathrm{c1}}}{\mu_0 M_\mathrm{s}^2} \sum\limits_{i=1}^3
        \left(\sum\limits_{j\neq i} (\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}j})^2 \right)
        (\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}i})\mathbf{e}_{\mathrm{c}i}

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{c1}` being the first-order
    cubic-anisotropy constant and
    :math:`\mathbf{e}_{\mathrm{c}1} \times \mathbf{e}_{\mathrm{c}2} = \mathbf{e}_{\mathrm{c}3}` being the
    anisotropy axes of the sample.

    Parameters
    ----------
    sample : Sample
        Sample on which the interaction is defined.

    """

    is_self_interaction = True
    name = InteractionName.CUBIC_ANISOTROPY
    required_params = ("Kc1", "e_c1", "e_c2", "e_c3")

    def __repr__(self: Self) -> str:  # noqa: D105
        return f"<CubicAnistropy object of sample '{self.sample.name}'>"

    @property
    def is_toothless(self: Self) -> bool:  # noqa: D102
        return not np.any(self.sample.material["Kc1"].value)

    def update_matrices(self: Self) -> None:  # noqa: D102
        Kc1 = self.sample.material["Kc1"].value
        Msat = self.sample.material["Msat"].value
        Msat_avrg = self.sample.material["Msat"].average
        self.fac = 2 * Kc1 / (mu_0 * Msat * Msat_avrg)

        self.c1 = self.sample.material["e_c1"].value.to_flattened()
        self.c2 = self.sample.material["e_c2"].value.to_flattened()
        self.c3 = self.sample.material["e_c3"].value.to_flattened()

    def unitless_field(self: Self, mag: MeshVector) -> MeshVector:  # noqa: D102
        mag = FlattenedMeshVector(mag)

        A1 = np.tile(
            (inner_product(mag, self.c2)) ** 2 + (inner_product(mag, self.c3)) ** 2, 3
        ).flatten()
        A2 = np.tile(
            (inner_product(mag, self.c1)) ** 2 + (inner_product(mag, self.c3)) ** 2, 3
        ).flatten()
        A3 = np.tile(
            (inner_product(mag, self.c1)) ** 2 + (inner_product(mag, self.c2)) ** 2, 3
        ).flatten()

        hc1 = (
            A1
            * np.tile(inner_product(mag, self.c1) * (-1) * self.fac, 3).flatten()
            * self.c1
        )
        hc2 = (
            A2
            * np.tile(inner_product(mag, self.c2) * (-1) * self.fac, 3).flatten()
            * self.c2
        )
        hc3 = (
            A3
            * np.tile(inner_product(mag, self.c3) * (-1) * self.fac, 3).flatten()
            * self.c3
        )

        return FlattenedMeshVector(hc1 + hc2 + hc3)

    def linearized_tensor(self: Self, mag: MeshVector) -> CubicAnisotropyTensor:  # noqa: D102
        return CubicAnisotropyTensor(self, mag)


class CubicAnisotropyTensor(EffectiveSpinWaveTensor):
    r"""
    Linearized tensor of the cubic anisotropy.

    The linearized tensor is given by

    .. math::

        \hat{\mathbf{N}}_{\mathrm{cub}} = -\frac{2K_{c1}}{\mu_0 M_{\mathrm{s}}^2} \sum\limits_{i=1}^3
        \left\{ \left[ \sum\limits_{j\neq i} (\mathbf{m}_0\cdot\mathbf{e}_{\mathrm{c}i})^2\right]
        \hat{\mathbf{C}}_i + 2 \hat{\mathbf{C}}_i \cdot \hat{\mathbf{M}}_0 \cdot
        \left[\sum\limits_{j\neq i} \hat{\mathbf{C}}_j \right]\right\}

    with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{c1}` being the first-order
    cubic-anisotropy constant of the sample. Furthermore

    .. math::

        \hat{\mathbf{M}}_0 = \mathbf{m}_0 \otimes \mathbf{m}_0

    and

    .. math::

        \hat{\mathbf{C}}_i = \mathbf{e}_{\mathrm{c}i} \otimes \mathbf{e}_{\mathrm{c}i}

    with :math:`\mathbf{e}_{\mathrm{c}1} \times \mathbf{e}_{\mathrm{c}2} = \mathbf{e}_{\mathrm{c}3}` being the
    anisotropy axes, :math:`\mathbf{m}_0` the equilibrium magnetization of the sample
    and :math:`\otimes` being the dyadic product.

    This operator is not dependent on any wave vectors. For the derivation of the linearized fields, see also Ref. [1]_.

    Parameters
    ----------
    interaction: Interaction
        Reference to the original interaction object.
    mag0: FlattenedMeshVector
        Equilibrium state around which the effective field is potentially linearized.

    References
    ----------
    .. [1]  Henry, *et al.*, "Propagating spin-wave normal modes:
            A dynamic matrix approach using plane-wave demagnetizating tensors",
            `arXiv:1611.06153 (2016) <https://doi.org/10.48550/arXiv.1611.06153>`_

    """

    def __init__(self: Self, interaction: CubicAnisotropy, mag: MeshVector) -> None:  # noqa: D107
        self.dtype = np.complex128

        self.m0 = mag
        self.c1 = interaction.c1
        self.c2 = interaction.c2
        self.c3 = interaction.c3
        self.fac = interaction.fac

        # create bare dyads

        self.C1 = tensor_product(self.c1, self.c1)
        self.C2 = tensor_product(self.c2, self.c2)
        self.C3 = tensor_product(self.c3, self.c3)

        M0 = tensor_product(self.m0, self.m0)

        # these are spatially dependent!
        A1 = np.tile(
            (inner_product(self.m0, self.c2)) ** 2
            + (inner_product(self.m0, self.c3)) ** 2,
            3,
        ).flatten()
        A2 = np.tile(
            (inner_product(self.m0, self.c1)) ** 2
            + (inner_product(self.m0, self.c3)) ** 2,
            3,
        ).flatten()
        A3 = np.tile(
            (inner_product(self.m0, self.c1)) ** 2
            + (inner_product(self.m0, self.c2)) ** 2,
            3,
        ).flatten()

        # A factors are pulled into the tensor product since they are spatially dependent
        Nc1 = tensor_product(A1 * self.c1, self.c1) + 2 * self.C1.dot(M0).dot(
            self.C2 + self.C3
        )
        Nc2 = tensor_product(A2 * self.c2, self.c2) + 2 * self.C2.dot(M0).dot(
            self.C3 + self.C1
        )
        Nc3 = tensor_product(A3 * self.c3, self.c3) + 2 * self.C3.dot(M0).dot(
            self.C1 + self.C2
        )

        self.sparse_matkm0 = self.fac.to_dia_matrix(3) @ (Nc1 + Nc2 + Nc3)
        self.shape = self.sparse_matkm0.shape

        self.set_km(0, 0)
