"""Generic functionality for different interactions."""

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import List

    from tetrax.interactions.interaction import InteractionName


def annotate_toothless_interactions(
    all_interaction_names: List[InteractionName],
    toothless_interaction_names: List[InteractionName],
) -> List[str]:
    """
    Annotate toothless interactions by appending a '*' to their names.

    Parameters
    ----------
    all_interaction_names : List[InteractionName]
        List of all interaction names to be annotated.

    toothless_interaction_names : List[InteractionName]
        List of interaction names that are toothless.

    Returns
    -------
    List[str]
        A list of annotated interaction names where toothless interactions are marked with a '*'.

    Notes
    -----
    Toothless interactions are those whose relevant material parameters are zero and which, therefore, do not
    contribute ot the total effective field.

    """
    annotated_interactions = []
    for name in all_interaction_names:
        annotated_name = (
            name.value + "*" if name in toothless_interaction_names else name.value
        )
        annotated_interactions.append(annotated_name)

    return annotated_interactions


def separate_toothless_interactions_html(
    all_interaction_names: List[InteractionName],
    toothless_interaction_names: List[InteractionName],
) -> str:
    """
    Separate toothless interactions from relevant interactions and return a formatted HTML string.

    Parameters
    ----------
    all_interaction_names : List[InteractionName]
        List of all interaction names.
    toothless_interaction_names : List[InteractionName]
        List of toothless interaction names.

    Returns
    -------
    str
        A formatted HTML string containing relevant interactions followed by toothless interactions in gray, enclosed in parentheses.

    Notes
    -----
    Toothless interactions refer to interactions that do not contribute to the total energy or effective field.

    Examples
    --------
    >>> all_interactions = [InteractionName.A, InteractionName.B, InteractionName.C]
    >>> toothless_interactions = [InteractionName.B]
    >>> separate_toothless_interactions_html(all_interactions, toothless_interactions)
    'A, C, <font color="gray">(B)</font>'

    """
    relevant_interaction_names = [
        name
        for name in all_interaction_names
        if name not in toothless_interaction_names
    ]

    formated_string = f'{", ".join(name.value for name in relevant_interaction_names)}, <font color="gray">({", ".join(name.value for name in toothless_interaction_names)})</font>'
    return formated_string


def separate_toothless_interactions(
    all_interaction_names: List[InteractionName],
    toothless_interaction_names: List[InteractionName],
) -> str:
    """
    Separate toothless interactions from relevant interactions and return a formatted string.

    Parameters
    ----------
    all_interaction_names : List[InteractionName]
        List of all interaction names.
    toothless_interaction_names : List[InteractionName]
        List of toothless interaction names.

    Returns
    -------
    str
        A formatted string containing relevant interactions followed by toothless interactions enclosed in parentheses.

    Notes
    -----
    Toothless interactions refer to interactions that do not contribute to the total energy or effective field.

    Examples
    --------
    >>> all_interactions = [InteractionName.A, InteractionName.B, InteractionName.C]
    >>> toothless_interactions = [InteractionName.B]
    >>> separate_toothless_interactions(all_interactions, toothless_interactions)
    'A, C, (B)'

    """
    relevant_interaction_names = [
        name
        for name in all_interaction_names
        if name not in toothless_interaction_names
    ]

    formated_string = f"{', '.join(name.value for name in relevant_interaction_names)}, ({', '.join(name.value for name in toothless_interaction_names)})"
    return formated_string
