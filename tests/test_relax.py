import numpy as np

import tetrax


class TestClass:

    def test_ring_helical_azimuthal_field(self):
        sample = tetrax.create_sample()
        sample.read_mesh("ring_3nm.msh")
        m_final_test = tetrax.vectorfields.helical(sample.xyz, 90, 1)

        sample.mag = tetrax.vectorfields.helical(sample.xyz, 60, 1)

        exp = tetrax.create_experimental_setup(sample)
        exp.Bext = tetrax.vectorfields.helical(sample.xyz, 90, 1) * 0.08

        exp.relax(tolerance=1e-11)
        m_final = sample.mag
        print("max", np.max(np.abs(m_final - m_final_test)))
        assert np.allclose(m_final, m_final_test, atol=1e-3)

    def test_ring_helical_easyplane_anis(self):
        sample = tetrax.create_sample()
        sample.read_mesh("ring_3nm.msh")
        m_final_test = tetrax.vectorfields.helical(sample.xyz, 90, 1)

        sample.mag = tetrax.vectorfields.helical(sample.xyz, 60, 1)
        sample.e_u = np.array([0, 0, 1])
        sample.Ku1 = -50000

        exp = tetrax.create_experimental_setup(sample)

        exp.relax(tolerance=1e-11)
        m_final = sample.mag
        print("max", np.max(np.abs(m_final - m_final_test)))
        assert np.allclose(m_final, m_final_test, atol=1e-3)
