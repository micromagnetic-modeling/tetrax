import unittest
from tetrax import geometries
import numpy as np

class Test1DGeometries(unittest.TestCase):
    def test_monolayer_line_trace(self) -> None:
        msh = geometries.monolayer(50, 2)
        assert msh.points.shape == (26, 3) and msh.points[0, 1] == -25 and msh.points[1, 1] == 25

    def test_bilayer_line_trace(self) -> None:
        msh = geometries.bilayer(20, 30, 1.5, [2, 2])
        assert msh.points.shape == (27, 3)
        assert msh.points[0, 1] == -25.75 and msh.points[1, 1] == -5.75
        assert msh.points[2, 1] == -4.25 and msh.points[3, 1] == 25.75

    def test_multilayer_line_trace(self) -> None:
        msh = geometries.multilayer([20, 5, 30], [1.5, 2], [2, 1, 2])
        assert msh.points.shape == (33, 3)
        assert msh.points[0, 1] == -29.25 and msh.points[1, 1] == -9.25
        assert msh.points[2, 1] == -7.75 and msh.points[3, 1] == -2.75
        assert msh.points[4, 1] == -0.75 and msh.points[5, 1] == 29.25

class Test1DradialGeometries(unittest.TestCase):
    def test_round_wire_line_trace(self) -> None:
        msh = geometries.round_wire(50, 2)
        assert msh.points.shape == (int(50/2)+1, 3) and msh.points[0, 0] == 0 and msh.points[1, 0] == 50

    def test_tube_line_trace(self) -> None:
        msh = geometries.tube(20, 30, 0.5)
        assert msh.points.shape == (int((30-20)/0.5+1), 3)
        assert msh.points[0, 0] == 20 and msh.points[1, 0] == 30

    def test_multitube_line_trace(self) -> None:
        msh = geometries.multitube([5, 20, 25, 50], [2, 3])
        assert msh.points.shape == (int((20 - 5) / 2 + 1) + int((50 - 25) / 3 + 1), 3)
        assert msh.points[0, 0] == 5 and msh.points[1, 0] == 20
        assert msh.points[2, 0] == 25 and msh.points[3, 0] == 50

class Test2DGeometries(unittest.TestCase):
    def test_tube_cross_section(self) -> None:
        msh = geometries.tube(20, 30, 2)
        assert msh.points.shape != 0
        epsilon = 1e-2
        assert abs(np.max(msh.points) - 30) <= epsilon and abs(np.min(msh.points) + 30) <= epsilon

    def test_tube_segment_cross_section(self) -> None:
        arc_length = 2 * np.pi * 25
        angle = 45
        thickness = 10
        msh = geometries.tube_segment(arc_length, thickness, angle, cell_size=3)
        assert msh.points.shape != 0
        ang = angle * np.pi / 180
        r_avrg = arc_length / ang
        r = r_avrg + thickness / 2
        min_max = r * np.sin(ang / 2)
        epsilon = 1e-12
        assert abs(np.min(msh.points[:,0]) + min_max) < epsilon and abs(np.max(msh.points[:,0]) - min_max) < epsilon

    def test_round_wire_cross_section(self) -> None:
        radius = 77
        msh = geometries.round_wire(radius, cell_size=3, x0=0, y0=0)
        epsilon = 1e-12
        assert msh.points.shape != 0
        assert abs(np.min(msh.points[:, 0]) + radius) < epsilon and abs(np.max(msh.points[:, 0]) - radius) < epsilon

    def test_polygonal_cross_section(self) -> None:
        points = [(0,0),(10,0),(10,10),(0,10)]
        msh = geometries.polygonal(points, 5)
        assert msh.points.shape == (12,3)
        assert msh.points[0,0] == 0 and msh.points[2,0] == 10

    def test_rectangle_cross_section(self) ->None:
        msh = geometries.rectangular(125, 24, cell_size_width=5, cell_size_thickness=3)
        assert msh.points.shape == (234,3)
        assert msh.points[0, 0] == -62.5 and msh.points[0, 1] == -12
        assert msh.points[3, 0] == 62.5 and msh.points[3, 1] == 12

class Test3DGeometries(unittest.TestCase):

    def test_disk(self) -> None:
        R = 200
        t = 33
        msh = geometries.disk(R, t)
        epsilon = 1e-6
        assert msh.points.shape != 0
        assert abs(np.min(msh.points[:, 0]) + R) < epsilon and abs(np.max(msh.points[:, 0]) - R) < epsilon
        assert abs(np.min(msh.points[:, 1]) + R) < epsilon and abs(np.max(msh.points[:, 1]) - R) < epsilon
        assert abs(np.min(msh.points[:, 2]) +t/2) < epsilon and abs(np.max(msh.points[:, 2]) - t/2) < epsilon

    def test_tube(self) -> None:
        r = 120
        R = 140
        t = 10
        msh = geometries.tube(r, R, t)
        epsilon = 1e-6
        assert msh.points.shape != 0
        assert abs(np.min(msh.points[:, 0]) + R) < epsilon and abs(np.max(msh.points[:, 0]) - R) < epsilon
        assert abs(np.min(msh.points[:, 1]) + R) < epsilon and abs(np.max(msh.points[:, 1]) - R) < epsilon
        assert np.min(msh.points[:, 2]) == 0 and abs(np.max(msh.points[:, 2]) - t) < epsilon


    def test_prism(self) -> None:
        msh = geometries.prism(500, 125, 5, 5, 5, 2)
        assert msh.points.shape == ((500/5+1)*(125/5+1)*3,3)


if __name__ == '__main__':
    unittest.main()