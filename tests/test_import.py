def _can_import():
    try:
        import tetrax
        return True
    except ImportError:
        return False


class TestClass:

    def test_import_tetraeigen(self):
        assert _can_import()
