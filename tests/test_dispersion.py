import numpy as np
import pandas as pd
import tetrax
import unittest

class MyTestCase(unittest.TestCase):

    def test_tube_vortex_dispersion_parallel(self):
        # number of modes to throw away at the top of the spectrum
        idx_throw = 2

        dispersion_test = pd.read_csv("../reference/test_dispersion_tube_vortex.csv")
        dispersion_test = dispersion_test.iloc[:, :-2]

        sample = tetrax.create_sample()
        sample.read_mesh("../ring_3nm.msh")

        sample.mag = tetrax.vectorfields.helical(sample.xyz, 90, 1)

        exp = tetrax.create_experimental_setup(sample)
        exp.Bext = tetrax.vectorfields.helical(sample.xyz, 90, 1) * 0.08

        dispersion = exp.eigenmodes(num_cpus=-1, num_modes=10, save_mode_profiles=False, num_k=21)
        dispersion = dispersion.iloc[:, :-idx_throw]

        branch_is_correct = []
        for i in range(len(dispersion.columns) - 2):
            branch_is_correct.append(np.allclose(dispersion[f"f{i} (GHz)"], dispersion_test[f"f{i} (GHz)"]))
        assert all(branch_is_correct)
