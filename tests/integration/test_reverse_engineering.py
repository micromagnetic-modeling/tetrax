import unittest


import numpy as np
import pandas as pd
import tetrax as tx
import unittest



def omega(k, gamma, M_s, A, T, B_0=0, kind="BVW", n=0, mu_0=4*np.pi*1e-7):

    omega_M = gamma * mu_0 * M_s
    omega_0 = gamma * B_0
    Lambda = np.sqrt(2*A / (mu_0 *M_s**2))


    k_n = np.sqrt(k**2 + (n*np.pi/T)**2)

    rat = k**2 / k_n**2
    kron_n = (n == 0)
    P_nn = rat*(1 - (2/(1+kron_n))*rat*(1-(-1)**n *np.exp(-k*T))/(k*T))
    if kind == "BVW":
        return np.sqrt(
            (omega_0 + omega_M* Lambda**2 * k_n**2) *\
                (omega_0 + omega_M* Lambda**2 *k_n**2 + omega_M*(1-P_nn))
        )
    if kind == "SW":
        return np.sqrt(
            (omega_0 + omega_M * Lambda**2 *k_n**2 + omega_M*P_nn) *\
                (omega_0 + omega_M* Lambda**2 *k_n**2 + omega_M*(1-P_nn))
        )
    else:
        print("No valid spin wave kind specified in dispersion. "
              "Please use BVW or SW.")
        return k*0

class TestReverse(unittest.TestCase):

    def test_monolayer_vs_ks(self):

        sample = tx.create_sample(geometry="layer")
        msh = tx.geometries.monolayer(50, 1)
        sample.set_geom(msh)
        # number of modes to throw away at the top of the spectrum
        idx_throw = 2

        Msat = 796000.  # A/m
        Aex = 13e-12  # J/m
        sample.Msat = Msat
        sample.Aex = Aex
        Bext = 20e-3
        sample.mag = [1, 0, 0]
        exp = tx.create_experimental_setup(sample, name="DE-geometry")
        exp.Bext = [Bext, 0, 0]

        dispersion_pert = exp.eigenmodes(kmin=0.1, kmax=40e6, num_k=81, num_modes=5, num_cpus=-1, no_dip=True,
                                         perturbed_dispersion=True)

        k_ = dispersion_pert["k (rad/m)"]


        dispersion = dispersion_pert.iloc[:, :-idx_throw]

        branch_is_correct = []

        for i in range(2):
            print(i)
            frequencies_ref = omega(np.abs(k_), sample.gamma, Msat, Aex, 50 * 1e-9, Bext, kind="SW", n=i,
                                    mu_0=4 * np.pi * 1e-7) / (
                                      2 * np.pi * 1e9)
            branch_is_correct.append(np.allclose(dispersion[f"f_pert_{i} (GHz)"], frequencies_ref.values, rtol=1e-3))

        assert all(branch_is_correct)




if __name__ == '__main__':
    unittest.main()
