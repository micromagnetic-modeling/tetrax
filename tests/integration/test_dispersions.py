import unittest


import numpy as np
import pandas as pd
import tetrax as tx
import unittest


class TestWaveguideDispersions(unittest.TestCase):

    def test_tube_vortex_dispersion_parallel(self):
        # number of modes to throw away at the top of the spectrum
        idx_throw = 2

        dispersion_test = pd.read_csv("../reference/test_dispersion_tube_vortex.csv")
        dispersion_test = dispersion_test.iloc[:, :-2]

        sample = tx.create_sample()
        sample.read_mesh("../reference/ring_3nm.msh")

        sample.mag = tx.vectorfields.helical(sample.xyz, 90, 1)

        exp = tx.create_experimental_setup(sample)
        exp.Bext = tx.vectorfields.helical(sample.xyz, 90, 1) * 0.08

        dispersion = exp.eigenmodes(num_cpus=-1, num_modes=10, save_mode_profiles=False, num_k=21)
        dispersion = dispersion.iloc[:, :-idx_throw]

        branch_is_correct = []
        for i in range(len(dispersion.columns) - 2):
            branch_is_correct.append(np.allclose(dispersion[f"f{i} (GHz)"], dispersion_test[f"f{i} (GHz)"]))
        assert all(branch_is_correct)


class TestMultilayerDispersion(unittest.TestCase):

    T = 50
    sample1 = tx.create_sample(name=f"Layer_{T}nm", geometry="layer")
    mesh1 = tx.geometries.monolayer(T, cell_size=0.2)

    Msat = 800e3
    Aexc = 11e-12
    Bext = 20e-3


    sample1.set_geom(mesh1)
    sample1.Msat = Msat
    sample1.Aex = Aexc

    def test_monolayer_backward_volume(self):
        dispersion_BVM_ref = np.genfromtxt(f"../reference/Py_BVW_50nm_20mT.dat")
        kref = dispersion_BVM_ref[:, 0] * 1e6
        kref[0] = 0
        exp1 = tx.create_experimental_setup(self.sample1, name="BVM")
        self.sample1.mag = [0, 0, 1]
        exp1.Bext = [0, 0, self.Bext]
        dispersion_BVM = exp1.eigenmodes(k=kref, num_modes=10, save_mode_profiles=False, num_cpus=-1)

        gamma_ref = 1.822123739e11
        branch_is_correct = []
        for i in range(len(dispersion_BVM.columns) - 2):
            branch_is_correct.append(np.allclose(dispersion_BVM[f"f{i} (GHz)"],
                                                 dispersion_BVM_ref[:, i + 1] * 1e-9 / (2 * np.pi) * self.sample1.gamma / gamma_ref,
                                                 rtol=.5e-03))

        assert all(branch_is_correct)

    def test_monolayer_surface_wave(self):
        dispersion_SW_ref = np.genfromtxt(f"../reference/Py_SW_50nm_20mT.dat")
        kref = dispersion_SW_ref[:, 0] * 1e6
        kref[0] = 0
        exp1 = tx.create_experimental_setup(self.sample1, name="SM")
        self.sample1.mag = [1, 0, 0]
        exp1.Bext = [self.Bext, 0, 0]
        dispersion_tx = exp1.eigenmodes(k=kref, num_modes=10, save_mode_profiles=False, num_cpus=-1)

        gamma_ref = 1.822123739e11
        branch_is_correct = []
        for i in range(len(dispersion_tx.columns) - 2):
            branch_is_correct.append(np.allclose(dispersion_tx[f"f{i} (GHz)"],
                                                 dispersion_SW_ref[:, i + 1] * 1e-9 / (2 * np.pi) * self.sample1.gamma / gamma_ref,
                                                 rtol=.5e-03))

        assert all(branch_is_correct)

if __name__ == '__main__':
    unittest.main()
