.. _tetrax:
tetrax
======

Subpackages and submodules
--------------------------

.. toctree::
   :maxdepth: 4

   tetrax.common
   tetrax.experiments
   tetrax.geometries
   tetrax.interactions
   tetrax.materials
   tetrax.logging
   tetrax.plotting
   tetrax.sample
   tetrax.vectorfields


