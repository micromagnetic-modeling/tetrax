API Reference
=============

.. toctree::
   :maxdepth: 4
   :hidden:

   tetrax

This reference provides detailed information for all subpackages, modules, functions and objects included in *TetraX*. 
This is the place, if you need detailed information on what certain parts of the package are
and what they do. If you want to learn how to use *TetraX*, please refer to the :doc:`usage` or the :doc:`examples`.

User packages
-------------

These are the subpackages and modules that are regularly consulted by the user.

- TetraX :mod:`tetrax <tetrax>`
- Experiments :mod:`tetrax.experiments`
- Geometries :mod:`tetrax.geometries`
- Vectorfields :mod:`tetrax.vectorfields`
- Materials :mod:`tetrax.materials`

Internal modules
----------------

Packages/modules that are important to *TetraX*, but not often directly used.

- Common :mod:`tetrax.common`
- Interactions :mod:`tetrax.interactions`
- Plotting :mod:`tetrax.plotting`
- Sample :mod:`tetrax.sample`







