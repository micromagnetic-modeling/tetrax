.. TetraX documentation master file, created by sphinx-quickstart on Sat Jan 15 20:14:29 2022. You can adapt this file completely to your liking, but it should at least contain the root `toctree` directive.

..
   image:: ../logo_large.png
   :width: 600

|
|


TetraX Documentation
====================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   :hidden:

   Getting started <quickstart>
   Examples <examples>
   User Guide <usage>
   API Reference <modules>
   Discussions <https://discussions.tetrax.software/>
   Publications <publications>
   Release Notes <release_notes>
   New in Version 2 <new_in_v2>


.. automodule:: tetrax


.. grid:: 2

    .. grid-item-card::  :octicon:`rocket;6em;`
        :text-align: center

        For a quick introduction, how to start your own FEM micromagnetic simulations, visit our :doc:`quickstart` page and take a look at the provided :doc:`examples`.

    .. grid-item-card:: :octicon:`book;6em;`
        :text-align: center

        Detailed information on the usage of TetraX is found in the :doc:`usage`.

.. grid:: 2

    .. grid-item-card:: :octicon:`comment;6em;`
        :text-align: center

        For help and discussions, head over to the TetraX `user forum <https://discussions.tetrax.software/>`_.

    .. grid-item-card::  :octicon:`file-code;6em;`
        :text-align: center

        The `source code <https://gitlab.hzdr.de/micromagnetic-modeling/tetrax>`_ of TetraX is licensed under the GNU GPL v3.0 Open-Source license.


**Version**: 2.0.0

TetraX is a package for finite-element-method (FEM) micromagnetic modeling with the aim to provide user friendly and versatile micromagnetic workflows. At its core, it allows to calculate and analyse spin-wave spectra using efficient and in-house developed symmetry-adapted eigensolver methods that use FEM discretization to model different geometries such as infinitely long waveguides, or infinitely extended multilayers or waveguides with axial symmetry. The underlying magnetic equilibra can be calculated with energy minimization or integration of the overdamped equation of motion of the magnetization. The results can be further processed with integrated post-processing tools or visualized with provided plotting methods.


.. grid:: 2

    .. grid-item-card:: Spin-wave dispersion in waveguides with arbitrary cross-section`
        :link: examples/round_tube_dispersion_vortex.html
        :img-bottom: _static/tube_disp.png

        Calculation of spin-wave dispersions is possible using an efficient finite-element dynamic-matrix method, that allows to model a wide range of geometries, including translationally invariant waveguides with arbitrary cross-section.

    .. grid-item-card:: Calculation of microwave absorption
        :link: examples/FMR_rect_waveguide.html
        :img-bottom: _static/bb_fmr.png

        The microwave power absorption of spin-wave spectra can be calculated for various parameter ranges.



.. grid:: 2

    .. grid-item-card:: Magnetic equilibria
            :link: examples/hysteresis_rect_waveguide.html
            :img-bottom: _static/example_hysteresis_rect_waveguide.png

            Equilibrium states can be calculated with efficient energy minimization or more reliable integration of the overdamped equation of motion.

    .. grid-item-card:: Inhomogeneous material parameters
            :link: examples/double_layer_Py25_CoFeB25_Grassi.html
            :img-bottom: _static/double_layer_Grassi.png

            Heterogeneous materials (such as bilayers or multilayers) can be modelled by varying exchange stiffness, saturation magnetization, anisotropies and other material parameters.


Features
--------

- symmetry-adapted dynamic-matrix methods to calculate spin-wave normal modes
- linewidths and microwave absorption obtained from mode profiles
- energy minimizer and overdamped dynamic equation to calculate magnetic equilibria
- flexible geometries (with templates) due to finite-element modeling
- infinitely extended systems (waveguides, multilayers) or axially symmetric systems represented by 1D and 2D meshes
- magnetic interactions: exchange, dipole, uniaxial and cubic anistropy, iDMI, bulk DMI, interlayer exchange
- efficient calculation of dipolar fields with extended hybrid FEM/BEM method
- inhomogeneous material parameters (including anisotropy axes)
- interactive visualization of samples and results in notebooks
- read and write of meshes and quantities from/to vtk files
- experimental support for antiferromagnets


Planned features
----------------

- weakly nonlinear perturbation analysis based on normal modes
- time-integration of Landau-Lifshitz-Gilbert equation
- full support for antiferromagnets

.. _howtocite:

Cite us
-------

If you use TetraX for your research, please cite

.. [1] L. Körber, G. Quasebarth, A. Hempel, F. Zahn, A. Otto, E. Westphal, R. Hertel and A. Kákay (2022).
       "TetraX: Finite-Element Micromagnetic-Modeling Package",
       Rodare. DOI: `10.14278/rodare.1418 <https://doi.org/10.14278/rodare.1418>`_
.. [2] L. Körber, G. Quasebarth, A. Otto and A. Kákay, "Finite-element dynamic-matrix approach for
       spin-wave dispersions in magnonic waveguides with arbitrary
       cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_


.. code-block:: TeX

   @misc{TetraX,
     author = {Körber, Lukas and
               Quasebarth, Gwendolyn and
               Hempel, Alexander and
               Zahn, Friedrich and
               Otto, Andreas and
               Westphal, Elmar and
               Hertel, Riccardo and
               Kakay, Attila},
        title = {{TetraX: Finite-Element Micromagnetic-Modeling
                  Package}},
        month = jan,
        year = 2022,
        doi = {10.14278/rodare.1418},
        url = {https://doi.org/10.14278/rodare.1418}
    }

   @article{korberFiniteelementDynamicmatrixApproach2021a,
		title = {Finite-element dynamic-matrix approach for spin-wave dispersions
		         in magnonic waveguides with arbitrary cross section},
		volume = {11},
		doi = {10.1063/5.0054169},
		language = {en},
		journal = {AIP Advances},
		author = {Körber, L and Quasebarth, G and Otto, A and Kákay, A},
		year = {2021},
		pages = {095006},
	}


The numerical experiments implemented in `TetraX` are often based on seminal papers.
In order to give credit to these works, when conducting a numerical experiment, `TetraX` saves references
important for this experiment to a bibtex file called ``references.bib``,
found in the sample directory. In this file, each entry contains a ``comment``
field describing how the reference was important for the computation.
When publishing results calculated with `TetraX` in your research, please
also give credit to the works which are important for the numerical experiments
you conducted.