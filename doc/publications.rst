Publications
============

If you use `TetraX` for your research, please see :any:`here <howtocite>` on how to cite us.

Publications relevant for the this package
------------------------------------------

The following publications were essential in the development of `TetraX`.

.. [T1] L. Körber, G. Quasebarth, A. Hempel, F. Zahn, A. Otto, E. Westphal, R. Hertel and A. Kákay (2022).
       "TetraX: Finite-Element Micromagnetic-Modeling Package",
       Rodare. DOI: `10.14278/rodare.1418 <https://doi.org/10.14278/rodare.1418>`_
.. [T2] Körber, *et al.*, "Finite-element dynamic-matrix approach for
       spin-wave dispersions in magnonic waveguides with arbitrary
       cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
.. [T3] Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104, 174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
.. [T4] Körber, *et al.*, "Finite-element dynamic-matrix approach for propagating spin waves: Extension to mono- and multilayers of arbitrary spacing and thickness", `AIP Advances 12, 115206 (2022) <https://doi.org/10.1063/5.0107457>`_


Research using/citing TetraX
----------------------------

The following publications either directly used or mention `TetraX`, our developed dynamic-matrix method, or a previous version of it (ordered anti-chronologically). 

#. Kuznetsov, *et al.* "Optical control of spin waves in hybrid magnonic-plasmonic structures", `Sci. Adv. 11, eads2420 (2025) <https://doi.org/10.1126/sciadv.ads2420>`_
#. Brevis *et al.*, "Curvature-induced parity loss and hybridization of magnons: Exploring the connection of flat and tubular magnetic shells", `Phys. Rev. B 110, 134428 (2024) <https://doi.org/10.1103/PhysRevB.110.134428>`_
#. Berchialla, *et al.* "Focus on three-dimensional artificial spin ice ", `Appl. Phys. Lett. 125, 220501 (2024) <https://doi.org/10.1063/5.0229120>`_
#. Volkov, *et al.* "Three-dimensional magnetic nanotextures with high-order vorticity in soft magnetic wireframes" `Nature Communications 15 (2024) 2193 <https://doi.org/10.1038/s41467-024-46403-8>`_
#. Gonzales-Chaves, *et al.* "Solutions to the Landau–Lifshitz–Gilbert equation in the frequency space: Discretization schemes for the dynamic-matrix approach", `JMMM 603 (2024) 172179 <https://doi.org/10.1016/j.jmmm.2024.172179>`_
#. Kraft, *et al.* "Parallel-in-time integration of the Landau–Lifshitz–Gilbert equation with the parallel full approximation scheme in space and time", `JMMM 597, 171998 (2024) <https://doi.org/10.1016/j.jmmm.2024.171998>`_
#. Körber, "Spin waves in curved magnetic shells", PhD Thesis, TU Dresden (2023)
#. Riedel, "Local Control and Manipulation of Propagating Spin Waves Studied by Time-Resolved Kerr Microscopy", PhD Thesis, TU Müchchen (2023)
#. Chumak, *et al.* "Advances in magnetics roadmap on spin-wave computing", `IEEE Transactions on Magnetics 58, 0800172 (2022) <https://doi.org/10.1109/TMAG.2022.3149664>`_
#. Riedel, *et al.*, "Hybridization-Induced Spin-Wave Transmission Stop Band within a 1D Diffraction Grating", `Advanced Physics Research 2, 2200104 (2023) <https://doi.org/10.1002/apxr.202200104>`_
#. Gallardo, *et al.* "High spin-wave asymmetry and emergence of radial standing modes in thick ferromagnetic nanotubes", `Physical Review B 105, 104435 (2022) <https://doi.org/10.1103/PhysRevB.105.104435>`_
#. Gladii, *et al.*, "Spin-wave nonreciprocity at the spin-flop transition region in synthetic antiferromagnets", `Physical Review B 107, 104419 (2023) <https://doi.org/10.1103/PhysRevB.107.104419>`_
#. Gallardo, *et al.* "Unidirectional Chiral Magnonics in Cylindrical Synthetic Antiferromagnets", `Physical Review Applied 18, 054044 (2022) <http://dx.doi.org/10.1103/PhysRevApplied.18.054044>`_
#. Iurchuk , *et al.* "Tailoring crosstalk between localized 1D spin-wave nanochannels using focused ion beams", `Scientific Reports volume 13, Article number: 764 (2023) <https://doi.org/10.1038/s41598-022-27249-w>`_
#. Hache , *et al.* "Control of 4-magnon-scattering in a magnonic waveguide by pure spin current", `Phys. Rev. Applied 20, 014062 (2023) <https://doi.org/10.1103/PhysRevApplied.20.014062>`_
#. Riedel , *et al.* "Hybridization‐Induced Spin‐Wave Transmission Stop Band within a 1D Diffraction Grating", `Advanced Physics Research (2023) <https://doi.org/10.1002/apxr.202200104>`_
#. Körber, *et al.*, "Curvilinear spin-wave dynamics beyond the thin-shell approximation: Magnetic nanotubes as a case study", `Phys. Rev. B 106, 014405 (2022) <https://doi.org/10.1103/PhysRevB.106.014405>`_
#. Körber, *et al.*, "Mode splitting of spin waves in magnetic nanotubes with discrete symmetries", `Phys. Rev. B 105, 184435 (2022) <https://doi.org/10.1103/PhysRevB.105.184435>`_
#. Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes", `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_


