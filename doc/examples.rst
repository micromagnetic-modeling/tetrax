Examples
========

This page provides a couple of examples showcasing the different use cases of TetraX. The examples are available in the form of Jupyter notebooks.

.. nbgallery::
    :caption: Examples:
    :name: python gallery


    examples/round_tube_dispersion_vortex
    examples/round_tube_absorption
    examples/field_sweep_yig_film
    examples/double_layer_Py25_CoFeB25_Grassi
    examples/magnetization_graded_waveguide
    examples/thick_film_dispersion_linewidth
    examples/rectangular_waveguide_DE
    examples/FMR_rect_waveguide
    examples/hysteresis_rect_waveguide
    examples/round_tube_spatial_dep_anis
    examples/thick_film_dispersion_with_perturbation
    examples/exchange_coupled_bilayers
    examples/mode_movie_of_modes_in_films
    examples/monolayer_cubic_anisotropy
    examples/example_neel_wall_iDMI
    examples/solid_wire_dispersion
