# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath(".."))

# -- Project information -----------------------------------------------------

project = "TetraX"
copyright = "2022, Lukas Körber, Attila Kákay"
author = "Lukas Körber, Attila Kákay"

# The full version, including alpha/beta/rc tags
release = "2022"

# -- General configuration ---------------------------------------------------


# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_design",
    "nbsphinx",
    "sphinx_copybutton",  # for "copy to clipboard" buttons
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "numpydoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.autosummary",
    "sphinx_gallery.load_style",
    "sphinx_toolbox.collapse",
]

nbsphinx_thumbnails = {
    "examples/round_tube_absorption": "_static/current_loop_nanotube_antenna.png",
    "examples/FMR_rect_waveguide": "_static/bb_fmr.png",
    "examples/round_tube_dispersion_vortex": "_static/tube_disp.png",
    "examples/solid_wire_dispersion": "_static/solid_wire_axial.png",
    "examples/round_tube_spatial_dep_anis": "_static/spatial_dep_anis.png",
    "examples/thick_film_dispersion_with_perturbation": "_static/pert_disp.png",
    "examples/exchange_coupled_bilayers": "_static/disp_afm_bilayers.png",
    "examples/mode_movie_of_modes_in_films": "_static/modemovie.gif",
    "examples/monolayer_cubic_anisotropy": "_static/cube_anis.png",
    "examples/rectangular_waveguide_DE": "_static/rect_wg.png",
    "examples/double_layer_Py25_CoFeB25_Grassi": "_static/double_layer_Grassi.png",
    "examples/thick_film_dispersion_linewidth": "_static/example_linewidth.png",
    "examples/magnetization_graded_waveguide": "_static/Gallardo_example.png",
    "examples/field_sweep_yig_film": "_static/stop_band.png",
    "examples/example_neel_wall_iDMI": "_static/neel_dw_idmi.png",
    "examples/hysteresis_rect_waveguide": "_static/example_hysteresis_rect_waveguide.png",
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#'sphinx_book_theme'#
html_theme = "pydata_sphinx_theme"
html_logo = "logo.png"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
html_context = {"default_mode": "dark"}
html_theme_options = {
    "show_prev_next": False,
    "announcement": "Already an experienced TetraX user? Check out our <a href='new_in_v2.html'>What's new in TetraX.2</a> page to get a quick overview of the new features!",
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://codebase.helmholtz.cloud/micromagnetic-modeling/tetrax",
            "icon": "fa-brands fa-square-gitlab",
            "type": "fontawesome",
        },
    ],
}

# -- Extension configuration -------------------------------------------------

autodoc_default_options = {"inherited-members": False, "undoc-members": False}

autosummary_generate = True

numpydoc_show_class_members = False
numpydoc_show_inherited_class_members = False
numpydoc_attributes_as_param_list = False

numpydoc_xref_param_type = True
numpydoc_xref_ignore = {"type", "optional", "default", "or", "of", "method"}
numpydoc_xref_aliases = {
    # python
    "sequence": ":term:`python:sequence`",
    "iterable": ":term:`python:iterable`",
    "string": "str",
    # numpy
    "array": "numpy.ndarray",
    "dtype": "numpy.dtype",
    "ndarray": "numpy.ndarray",
    "array-like": ":term:`numpy:array_like`",
    "array_like": ":term:`numpy:array_like`",
    # tetrax
    "Sample": "tetrax.sample.sample.Sample",
    "SIPrefix": "tetrax.common.typing.SIPrefix",
    "GeometryType": "tetrax.common.typing.GeometryType",
    "UnitaryNumber": "tetrax.common.typing.UnitaryNumber",
    "MagneticOrder": "tetrax.common.typing.MagneticOrder",
    "ParameterType": "tetrax.sample.material.parameter.ParameterType",
    "LabVector": "tetrax.sample.mesh.meshquantities.LabVector",
    "AnyVector": "tetrax.sample.mesh.meshquantities.AnyVector",
    "MeshVector": "tetrax.sample.mesh.meshquantities.MeshVector",
    "FlattenedMeshVector": "tetrax.sample.mesh.meshquantities.FlattenedMeshVector",
    "LocalMeshVector": "tetrax.sample.mesh.meshquantities.LocalMeshVector",
    "MeshScalar": "tetrax.sample.mesh.meshquantities.MeshScalar",
    "FlattenedLocalMeshVector": "tetrax.sample.mesh.FlattenedLocalMeshVector",
    "MeshioMesh": "tetrax.sample.mesh.sample_mesh.MeshioMesh",
}

intersphinx_mapping = {
    "python": ("http://docs.python.org/", None),
    "numpy": ("https://www.numpy.org/devdocs", None),
    "scipy": ("http://docs.scipy.org/doc/scipy/reference/", None),
    "pandas": ("http://pandas.pydata.org/pandas-docs/dev", None),
}


html_css_files = ["css/custom.css"]
