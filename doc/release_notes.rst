Release Notes
=============

Version 2.0.0
-------------
`2025-02-01`

This is a major release. For a comprehensive explanation of all changes, see :doc:`/new_in_v2`.



Major changes
^^^^^^^^^^^^^
- The :class:`~tetrax.sample.sample.Sample` class has been completely restructured to be much simpler. Samples are now configured through composition and can have a :class:`~tetrax.common.typing.GeometryType` and :class:`~tetrax.common.typing.MagneticOrder` attribute. The old ``create_sample()`` factory has been removed. Instead, samples are now created simply by creating an instance of the :class:`~tetrax.sample.sample.Sample` class.
- The mesh and sample geometry is now managed by the :class:`~tetrax.sample.mesh.sample_mesh.SampleMesh` class. The mesh of a sample can be addressed as ``sample.mesh``. Similarly, one now has to do ``sample.mesh.nx``.
- Material parameters are now handeled by the :class:`~tetrax.sample.material.material.SampleMaterial` and :class:`~tetrax.sample.material.parameter.MaterialParameter` classes. The material of a sample is addressed as its ``sample.material`` attribute. Instead of ``sample.Msat``, one should now use ``sample.material["Msat"]``, etc. Almost all material parameters can now be inhomogeneous. For details, see :doc:`/new_in_v2`.
- The way experiments are being run has been changed completely. There is no more ``ExperimentalSetup`` object. Instead, major experiments act as functions on the sample and generate a :class:`~tetrax.experiments._result.Result` as an output. Depending on the particular experiment, each :class:`~tetrax.experiments._result.Result` provides methods to plot the calculated data and perform post-processing on it. For example, the :class:`~tetrax.experiments.eigen.result.EigenResult` allows calculating the power absorption of the a spin-wave spectrum with respect to a specified microwave antenna using the :func:`EigenResult.absorption() <tetrax.experiments.eigen.result.EigenResult.absorption>` method (which returns an :class:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult`).
- By default, each :class:`~tetrax.experiments._result.Result` is automatically saved to the disk. This includes the calculated data itself, as well as some metadata and the parameters used to perform the calculations. It also comes with a ``report.json`` file that contains material parameters, experiment configuration, metada (such as the version of `TetraX` that has been used) and can be used, for example, for data archiving. By default, running the same experiment twice will overwrite the old results.
- Experiments can now be labeled to avoid overwriting existing data.
- Magnetic interactions are now modularized as :class:`~tetrax.interactions.interaction.Interaction` s in the :class:`tetrax.interactions` module. The different interactions of a sample are now listed in its ``interactions`` attribute. Each interaction has an :class:`~tetrax.interactions.interaction.InteractionName` that allows to address it. For more details, see :doc:`/new_in_v2`.


New features
^^^^^^^^^^^^
- Integrated plotting for results of TetraX experiments.
- New experiment :func:`~tetrax.experiments._relax.minimize.relax` for more reliable equilibration based on integration of the overdamped LLG.
- New :attr:`~tetrax.common.typing.GeometryType.WAVEGUIDE_AXIAL`. This geometry type combines axial with translational symmetries and allow to efficiently model
  round wires, tubes, or multitubes by only modeling the magnetic system within a one-dimensional mesh along the radial direction. The
  spin-wave modes are directly calculated as function of wave vector :math:`k` and azimuthal mode index :math:`m` in the angular direction.
- A **new microwave antenna** (:class:`~tetrax.experiments.antenna.MultiStriplineAntenna`) has been added, which allows to model an
  arbitrary series of stripline antennae (e.g. to model a meandering antenna).
- The symmetry breaking direction ``material["e_d"]`` of the :class:`~tetrax.interactions.dmi_interfacial.InterfacialDMI` can now be chosen freely.
- Both :class:`~tetrax.interactions.dmi_interfacial.InterfacialDMI` and :class:`~tetrax.interactions.dmi_bulk.BulkDMI` now satisfy
  **proper boundary conditions** at the sample boundary. Open boundary conditions can be recovered by setting the ``open_boundary`` attribute
  of the respective interaction to True, which requires to call the ``update_matrices()`` function of the interaction. Setting open boundary
  conditions should be done with care (that's why this feature is kind of hidden).
- A couple of **template materials** have been added, which are available in the new :mod:`tetrax.materials` module. They can be listed
  by accessing :attr:`tetrax.materials.available`.

Examples
^^^^^^^^
- All examples have been revised to comply with version 2.
- New example :doc:`examples/example_neel_wall_iDMI`. Initial version kindly provided by Joo-Von Kim.
- New example :doc:`examples/solid_wire_dispersion` based on `Journal of Physics D: Applied Physics 52, 075003 (2019) <https://iopscience.iop.org/article/10.1088/1361-6463/aaf2fc>`_.

Minor Changes
^^^^^^^^^^^^^
- Samples cannot be created anymore without specifying a mesh.
- By default the magnetization of a sample will now be initalized to the :math:`z` direction. Before, it was undefined.
- The number of CPU cores ``num_cpus`` when calculating an eigen spectrum is now ``-1`` by default (all cores are used).
- Frequencies are now stored in Hz, wave vectors in rad/m.
- The ``plot`` method of the sample has been removed and its functionality absorbed into the ``show`` method.
- The library of template geometries :class:`tetrax.geometries` has been restructured and subdivided into more meaningful submodules such
  as :mod:`tetrax.geometries.waveguide` or :mod:`tetrax.geometries.layer`. All template geometries have been cleaned up (e.g. keywords have
  more meaningful names) and can be listed using :attr:`tetrax.geometries.available`.
- All template vectorfields have been cleaned up and can be accessed with :attr:`tetrax.vectorfields.available`.
- Both **vectorfields and geometries can now be read from a file** using :func:`tetrax.geometries.from_file`
  or :func:`tetrax.vectorfields.from_file`.

Other
^^^^^
- The user guide, API reference and publication list have been updated.
- Links to the `TetraX user forum <https://discussions.tetrax.software/>`_ have been added.


Version 1.3.3
-------------
`2023-12-20`

Hotfixes
^^^^^^^^
- fixed wrong scaling factor in iDMI tensor (We thank Joo-Von Kim for highlighting this issue)

Version 1.3.2
-------------
`2023-10-17`

Hotfixes
^^^^^^^^
- fixed problem related to mode visualization that was appearing on windows machines.


Version 1.3.1
-------------
`2023-06-12`

Hotfixes
^^^^^^^^
- fixed installation problem on Windows related to compilation
- fixed problem related to multiprocessing on Windows
- fixed error on calculating absorption when some frequencies are ``NaN`` (now it properly skips these modes)

Other
^^^^^

- included description in :any:`documentation <howtoscript>` on how to use `TetraX` in standalone python scripts.

Version 1.3.0
-------------
`2023-05-23`

tl;dr
^^^^^

- inhomogeneous material parameters
- extract data along curves
- new sample visualization
- added calculation of mode linewidths
- 5 new examples
- 3D confined samples and antiferromagnets (experimental)
- other

New features
^^^^^^^^^^^^

- TetraX now supports **inhomogeneous material parameters**, namely variations
  in exchange stiffess constant :math:`A_\mathrm{ex}` and saturation magnetization
  :math:`M_\mathrm{s}`. Both attributes can now supplied to a ``sample`` as scalar
  arrays. For example, to set a linearly varying saturation magnetization, the
  following code snippet can be used.

  .. code-block:: python

     Mavrg = 800e6
     dMdx = 10e6

     sample.Msat = Mavrg + dMdx * sample.xyz.x

  The current average magnetization is now obtained with ``sample.Msat_avrg``.
  For more details, see Examples :doc:`examples/double_layer_Py25_CoFeB25_Grassi` and :doc:`examples/magnetization_graded_waveguide`.


- The effective-field terms that correspond to the current magnetization state of
  a sample can now be accessed with

  - ``sample.Bexc`` (exchange field)
  - ``sample.Bdip`` (dipolar field)
  - ``sample.Buni`` (uniaxial-anistropy field)
  - ``sample.Bcub`` (cubic-anistropy field)
  - ``sample.Bdmi`` (bulk DMI field)
  - ``sample.Bidmi`` (interfacial-DMI field)

  All fields are return as ``MeshVector`` fields in units of Tesla.

- One can now **extract data along a curve** of scalar or vector fields on a mesh.
  This can be done using :func:`sample.scan_along_curve() <tetrax.core.sample.AbstractSample.plot>`
  which accepts curves specified either by their start end and end points (will result in a straight line)
  or by supplying all interpolation points along the curve (enables arbitrary curves). This method calls the
  new helper function :func:`tx.helpers.math.interpolate_along_curve() <tetrax.helpers.math.interpolate_along_curve>`.
  For a demonstration, see Example :doc:`examples/linescans_waveguides`.
- The **linewidths** of spin-wave modes can now be calculated separately from the ``absorption()`` experiment.
  For this one can use the new ``linewidths()`` experiment or, alternatively, set ``linewidths=True`` when calculating
  the dispersion.

  .. code-block:: python

     exp.eigenmodes()
     linewidths = exp.linewidths()

     # Or shorter
     linewidths = exp.eigenmodes(..., linewidths=True)

  Here the new ``linewidths`` data frame is the familiar ``dispersion`` table extended
  by the linedwidth columns ("Gamma0/2pi (GHz)" and so on). An application is found in the new example
  :doc:`examples/thick_film_dispersion_linewidth`.

New Examples
^^^^^^^^^^^^
- :doc:`examples/double_layer_Py25_CoFeB25_Grassi` according to Grassi `et al.`, Phys. Rev. Applied 14, 024047 (2020)
- :doc:`examples/magnetization_graded_waveguide` according to Gallardo `et al.`, Nanomaterials 2022, 12(16), 2785
- :doc:`examples/thick_film_dispersion_linewidth`
- :doc:`examples/field_sweep_yig_film`, as used for `Riedel et al., Advanced Physics Research (2023) <https://doi.org/10.1002/apxr.202200104>`_
- :doc:`examples/linescans_waveguides`

Minor Changes
^^^^^^^^^^^^^

- The visualization of waveguides and multilayers has been changed to include an
  additional mesh that hints at the full three-dimensional shape. For this, both :func:`sample.show() <tetrax.core.sample.AbstractSample.show()>` and
  :func:`ExperimentalSetup.show() <tetrax.core.experimental_setup.ExperimentalSetup.show()>`
  now include the option ``show_extrusion`` which is ``True`` by default.
  Furthermore, the grid is now hidden by default which, in both, cases, can be adjusted with the ``show_grid`` option.
- Apart from using :func:`tetrax.sample_average() <tetrax.helpers.math.sample_average>` one can also now use the more convenient
  :func:`sample.average() <tetrax.core.sample.AbstractSample.average>` which only accepts the vector/scalar field as an argument
  and does not require to pass the ``sample`` object itself.


Other
^^^^^

- Updated :doc:`User Guide <usage>` with new features.
- Updated :doc:`Publications <publications>`.
- Internal restructuring of magnetic tensors to allow for more unified implementation with antiferromagnets.
- Magnetic tensors are equipped with an ``update`` method that handles changes in material parameters but
  avoids unneccessary recalations.
- We now support **antiferromagnets** as an experimental set of features including

  - all interactions except dipole-dipole
  - additionally, we support non-uniform DMI
  - visualiziation of AFM mode profiles

  Use on your own risk (and tell us your experiences).
- **3D confined samples** are now supported for FMs (without dipolar interaction) and for AFMs, both as an experimental feature.

Bug fixes
^^^^^^^^^
- Fixed a bug that gave an error when relaxation with least-square method failed.
- Error related to numpy datatype and scipy sparse matrices fixed.

Version 1.2.0
-------------
`2022-07-15`

New features
^^^^^^^^^^^^

- TetraX now supports **layer samples**, which can be created with :func:`tetrax.create_sample(geometry="layer") <tetrax.core.sample.create_sample>` and are represented by 1D line-trace mesh along the normal direction of the layer(s). For this, the plane-wave Fredkin-Koehler method has been expanded to infinitely-extended layers (preprint available at `arXiv <https://arxiv.org/abs/2207.01519>`_, submitted to AIP Advances). See Example: :doc:`examples/thick_film_dispersion_with_perturbation`.
- Templates for layer samples were added, in form of mono, bi and multilayers as possible 1D geometries in  the :mod:`tetrax.geometries <tetrax.geometries.geometries1D>` submodule:

    * :func:`monolayer_line_trace() <tetrax.geometries.geometries1D.monolayer_line_trace>`
    * :func:`bilayer_line_trace() <tetrax.geometries.geometries1D.bilayer_line_trace>`
    * :func:`multilayer_line_trace() <tetrax.geometries.geometries1D.multilayer_line_trace>`
- Bilinear **interlayer-exchange interaction** is now available (only for layered systems), see Example :doc:`examples/exchange_coupled_bilayers`. The interlayer-exchange constant ``sample.J1`` (in J/m\ :sup:`2`) can also have different values between different layers in a multilayered sample, by supplying a list of values (see User Guide).
- The eigenmode calculation :func:`ExperimentalSetup.eigenmodes(k=k_list,...) <tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes>` now accepts lists or 1D arrays as a parameter for ``k``, e.g. to specify a non-equidistant wave-vector range.
- **Cubic anistropy** (first order) is now properly implemented and can be used for both waveguides and layer samples. See Example: :doc:`examples/monolayer_cubic_anisotropy`.
- Rudimentary **mode-profile visualization** and animation has been added. If available, a mode profile can be visualized using :func:`ExperimentalSetup.show_mode(k=...,N=...) <tetrax.core.experimental_setup.ExperimentalSetup.show_mode>`. See User Guide for details and Example: :doc:`examples/mode_movie_of_modes_in_films`.
- The numerical experiments implemented in `TetraX` are often based on seminal papers. In order to give credit to these works, when conducting a numerical experiment, `TetraX` now **saves references** important for this experiment to a **bibtex file** called ``references.bib``, found in the sample directory. In this file, each entry contains a ``comment`` field describing how the reference was important for the computation. When publishing results calculated with `TetraX` in your research, please also give credit to the works which are important for the numerical experiments you conducted.

Minor changes
^^^^^^^^^^^^^
- When modes at a specific wave vector cannot be calculated, now, ``NaN``\ s are inserted into the dispersion dataframe. Previously, this simply caused an error.
- Warning message for magnetization pointing into the z direction removed.
- Additional geometry added for waveguides, :func:`round_wire_cross_section_refined() <tetrax.geometries.geometries2D.round_wire_cross_section_refined>` which supports local mesh refinement (see documentation).

Bug fixes
^^^^^^^^^

- :func:`ExperimentalSetup.Bext <tetrax.core.experimental_setup.ExperimentalSetup.Bext>` is now initialized as MeshVector filled with zeros. Previously, an error occurred when not specifying any external field.
- Automatic dispersion saving and perturbate-dispersion analysis have been reimplemented. Somehow they got lost in a previous merge. Saving of dispersions can be controlled with save_disp=True and :func:`ExperimentalSetup.eigenmodes(fname="dispersion.csv",...) <tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes>`.
- The director of uniaxial anisotropy ``e_u`` can now be specified as a triplet, e.g. [0, 0, 1], just like magnetization or external field. Previously, this caused complications with the possibility to set multiple uniaxial anistropies.

Other
^^^^^

- To make upcoming implementation of antiferromagnets easier, spherical mesh vectors for FM and AFM, conversion methods to cartesian and back, and local mesh vectors for AFM have been implemented.
- Updated documentation.
- Added instructions for installation on Windows computers.
- 4 examples included to the package:

    - thick film dispersion + perturbation and comparison with Kalinikos-Slavin: :doc:`examples/thick_film_dispersion_with_perturbation`.
    - antiferromagnetically coupled films with interlayer exchange: :doc:`examples/exchange_coupled_bilayers`.
    - mode movie for magnetostatic surface waves in films: :doc:`examples/mode_movie_of_modes_in_films`.
    - cubic anisotropy in films: :doc:`examples/monolayer_cubic_anisotropy`.


Version 1.1.0
-------------
`2022-03-31`

New features
^^^^^^^^^^^^
- Added the :func:`tetrax.sample_average() <tetrax.helpers.math.sample_average>` function available, which takes the average of a vector or scalar field in a given sample (can be volume, surface or line)
- ``verbose={True, False}`` added to all numerical experiments, allows to silence all output (except warnings)
- Saving vector/scalar fields to vtk files is now possible using :func:`tetrax.write_field_to_file() <tetrax.helpers.io.write_field_to_file>` by

  .. code-block:: python

     tetrax.write_field_to_file(field, sample)

  or with the optional keywords

  .. code-block:: python

     tetrax.write_field_to_file(field, sample, fname, qname)

  As an alternative, the method :func:`AbstractSample.field_to_file() <tetrax.core.sample.AbstractSample.field_to_file>` can be used, where the sample parameters is obviously omitted.
- Added new equilibrium states :func:`bloch_wall <tetrax.vectorfields.bloch_wall>` and :func:`neel_wall <tetrax.vectorfields.neel_wall>` to :mod:`tetrax.vectorfields`.
- Added new geometry :func:`tube_segment_cross_section <tetrax.geometries.geometries2D.tube_segment_cross_section>` for waveguide samples to :mod:`tetrax.geometries`.
- Plotting of scalar and vector fields on a sample is now possible using the :func:`plot() <tetrax.core.sample.AbstractSample.plot>` method of a sample object.
- Pertubation analysis and reverse-engineering of general spin-wave dispersions according to `Phys. Rev. B 104, 174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_ is now possible within the :func:`eigenmodes() <tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes>` experiment, see User Guide :doc:`Numerical Experiments </usage/experiments>`.
- Implemented cubic anistropy (linearized dynamic field not properly tested yet).

Minor changes
^^^^^^^^^^^^^
- :func:`relax() <tetrax.core.experimental_setup.ExperimentalSetup.relax>` experiment now returns only a boolean denoting the relaxation success
- magnetization is automatically saved into folder of experimental setup after running :func:`relax() <tetrax.core.experimental_setup.ExperimentalSetup.relax>`
- dispersion is automatically saved to into folder of experimental setup after running :func:`eigenmodes() <tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes>`

Other
^^^^^
- Populated User Guide and API reference.
- Added more examples.
- Indroduced :class:`MeshVector <tetrax.core.mesh.MeshVector>`, :class:`MeshScalar <tetrax.core.mesh.MeshScalar>` and related data types.

Version 1.0.1
-------------
`2022-03-07`

Initial release.
