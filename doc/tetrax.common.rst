Common (:mod:`tetrax.common`)
=============================

.. automodule:: tetrax.common

   
.. automodule:: tetrax.common.bibliography

.. automodule:: tetrax.common.config

.. automodule:: tetrax.common.io

.. automodule:: tetrax.common.math


.. automodule:: tetrax.common.typing

.. automodule:: tetrax.common.utils
