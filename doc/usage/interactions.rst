Magnetic Interactions
=====================

.. contents:: Table of Contents
   :depth: 2
   :local:
   :backlinks: none

Magnetic interactions are a fundamental aspect of any simulation or numerical experiment performed with `TetraX`. In previous versions, these interactions were fixed and predefined for every sample. However, `TetraX` has now modularized magnetic interactions, providing a more flexible and powerful way to configure simulations.

Each magnetic interaction is implemented as part of the :mod:`tetrax.interactions` module, and these interactions are attached to a sample through its ``interactions`` attribute. This modular design allows users to examine, modify, and selectively enable or disable specific interactions for their experiments. This chapter provides an overview of the available interactions, their functionalities, and how to use them in numerical simulations.

Interaction classes
-------------------

Different interactions are implement to inherit from the :class:`~tetrax.interactions.interaction.Interaction` class, which defines methods to compute the effective field, energy density, and total energy for a given magnetization. To this end, each interaction provides methods such as :func:`~tetrax.interactions.interaction.Interaction.energy`, :func:`~tetrax.interactions.interaction.Interaction.energy_density`, or :func:`~tetrax.interactions.interaction.Interaction.unitless_field`. Furthermore, if the interaction is a self interaction (i.e. not an interaction with the environment), it provides the :func:`~tetrax.interactions.interaction.Interaction.linearized_tensor` method which calculates the :class:`~tetrax.interactions.interaction.EffectiveSpinWaveTensor` linearized in the current equilibrium state. This is needed for eigenmode calculations. Lastly, every interaction is identified by its :class:`~tetrax.interactions.interaction.InteractionName` (see more on the individual interactions below).

.. note::

   Currently, magnetic interactions are defined through their effective fields that must implement all necessary boundary and interface terms. Energy and energy density are calculated using the effective field. This might change in a future release.


Accessing interactions, energies and fields
-------------------------------------------

The interactions in a sample can be accessed and manipulated using several convenient methods. To view all the interactions available for a given sample, simply query the `interactions` attribute:

.. code-block:: python
   
   sample.interactions

This will return a list of all interactions currently associated with the sample. An individual interaction Each interaction can be addressed by its name using the :func:`~tetrax.sample.sample.Sample.get_interaction` of a sample:

.. code-block:: python

   exchange = sample.get_interaction("exchange")
   zeeman = sample.get_interaction("zeeman")

This retrieves the desired interaction object, which can then be used in simulations or calculations. Here, each interaction is addressed either with its :class:`~tetrax.interactions.interaction.InteractionName` or, more conveniently, by the corresponding string alias. For example, the alias of :attr:`InteractionName.DMI_BULK <tetrax.interactions.interaction.InteractionName.DMI_BULK>` is simply its snake_case version ``"dmi_bulk"``. Thus

.. code-block:: python

   dmi_bulk = sample.get_interaction(InteractionName.DMI_BULK)
   dmi_bulk = sample.get_interaction("dmi_bulk")

will have the same effect. One of the most significant advantages of the modularized interactions is the ability to selectively include or exclude interactions in numerical experiments. This makes it possible to study the effects of specific interactions in isolation or in combination (see :doc:`usage/experiments`). 

The corresponding energies, energy densities or effective fields of the different interactions can also be directly accessed from the sample itself using the :func:`~tetrax.sample.sample.Sample.get_energy`, :func:`~tetrax.sample.sample.Sample.get_energy_density` and :func:`~tetrax.sample.sample.Sample.get_field` methods in the following way:

.. code-block:: python

   cubic_energy = sample.get_energy("cubic_anisotropy")
   exchange_density = sample.get_energy_density("exchange")
   dipolar_field = sample.get_field("dipole")

Without providing any additional arguments to these methods, the corresponding quantities will always be calculated with respect to the current magnetization ``sample.mag`` of the sample. However, one can also simply calculate, for example, the energy with respect to some arbitrary vector field.

.. code-block:: python

   exchange_energy = sample.get_field("exchange", some_vector_field)

.. note:: 
   
   If you cannot remember the interaction names, just type a wrong one, and the error message will give you a list of the correct ones. 


Availalable interactions
------------------------

`TetraX` includes a wide range of magnetic interactions, each implemented as a dedicated class. These classes provide functionality for calculating contributions to the free energy, effective fields, and effective spin-wave tensors. Below is an overview of the interactions supported by `TetraX`. For more details, simply click on the individual classes.

Magnetodipolar Interaction
^^^^^^^^^^^^^^^^^^^^^^^^^^

**Class:** :class:`~tetrax.interactions.dipole.DipoleInteraction`

**Tensor:** :class:`~tetrax.interactions.dipole.DipoleTensor`

The magnetic self interaction due to magneto-dipolar fields expressed in terms of the energy density

.. math::

   w_{\mathrm{dip}} = - \frac{\mu_0 M_{\mathrm{s}}^2}{2}\mathbf{m}\cdot\mathbf{h}_{\mathrm{dip}}

with the unitless dipolar field

.. math::

   \mathbf{h}_{\mathrm{dip}} = -\nabla \Phi[\mathbf{m}]

Here, :math:`M_{\mathrm{s}}` is the saturation magnetization of the sample and :math:`\Phi[\mathbf{m}]` is the
unitless magnetostatic potential of the magnetization :math:`\mathbf{m}`. In TetraX, this potential is calculated using a
hybrid FEM/BEM method [1]_ with its extensions ([2]_ and [3]_) for propagating modes.

To solve eigenvalue problem for propagating spin waves, the dynamic dipolar fields are calculated using the
effective spin-wave dipolar tensor

.. math::

   \hat{\mathbf{N}}_{\mathrm{dip},k}\mathbf{m}_k(\mathbf{s}) = (\nabla_{\mathbf{s}} + ik\mathbf{e}_z) \psi_k(\mathbf{s})

with :math:`\psi_{k}` being the complex lateral potential and :math:`\mathbf{s}` denoting the coordinates within
the mesh. In `TetraX`, the lateral potential is calculated using the FEM/BEM
method extended for propagating waves. For details see Refs. [2]_ and [3]_.

Exchange interaction
^^^^^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.exchange.ExchangeInteraction`
| **Tensor:** :class:`~tetrax.interactions.exchange.ExchangeTensor`

The symmetric exchange interaction is determined by the energy density

.. math::

   w_{\mathrm{ex}} = A_{\mathrm{ex}} (\nabla \mathbf{m})^2

and the corresponding unitless effective field

.. math::

   \mathbf{h}_{\mathrm{ex}} = \frac{2}{\mu_0 M_\mathrm{s}^2} \nabla\cdot(A \nabla \mathbf{m})

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`A_{\mathrm{ex}}` being exchange-stiffness
constant of the sample. 

The linear tensor of the symmetric interaction, acting on propagating waves is defined as

.. math::

   \hat{\mathbf{N}}_{\mathrm{ex},k} = \frac{2}{\mu_0 M_\mathrm{s}^2} \left(A_{\mathrm{ex}}k^2 \hat{\mathbf{I}}
   - \nabla_{\mathbf{s}} A_{\mathrm{ex}} \nabla_{\mathbf{s}} \right)

with :math:`M_{\mathrm{s}}` being the saturation magnetization, :math:`A_{\mathrm{ex}}` the exchange-stiffness
constant and :math:`\mathbf{s}` the coordinates within the mesh of the sample. Furthermore,
:math:`\hat{\mathbf{I}}` is the identity matrix.


Interlayer exchange interaction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.interlayer_exchange.InterlayerExchangeInteraction`
| **Tensor:** :class:`~tetrax.interactions.interlayer_exchange.InterlayerExchangeTensor`

The interlayer-exchange interaction mediated by RKKY cpupling between adjacent magnetic layers is described by the
energy surface density

.. math::

   w_{\mathrm{IEC}} = - J\mathbf{m}(\mathbf{r})\cdot \mathbf{m}(P(\mathbf{r}))

and the unitless effect field (defined only at the surface)

.. math::

   \mathbf{h}_{\mathrm{IEC}} = \frac{J_1}{\mu_0 M_\mathrm{s}^2} \mathbf{m}(P(\mathbf{r}))

with :math:`J_1` being the bilinear interlayer-exchange constant of the sample an $P$ the mapping of a point
:math:`\mathbf{r}` to the nearest point on the opposite surface.

For :attr:`GeometryType.LAYER <tetrax.common.typing.GeometryType.LAYER>` the tensor consist of an
off-diagonal sparse block matrix with blocks

.. math::

   \hat{\mathbf{N}}_{\mathrm{IEC}}^{(i,j)} = \hat{\mathbf{N}}_{\mathrm{IEC}}^{(j,i)} =
   \frac{J_1}{\mu_0 M_\mathrm{s}^2} \frac{2}{c_i + c_j}\hat{\mathbf{I}}

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`J_1` the bilinear interlayer-exchange
constant of the sample, :math:`i\neq j` the indices of the
two couples boundary elements, $c_i$ and $c_j$ their associated nodes volumes and :math:`\hat{\mathbf{I}}` the
identity matrix. For more details, see Ref. [3]_.

.. note::

   Up to now, this interaction is only implemented for
   :attr:`GeometryType.LAYER <tetrax.common.typing.GeometryType.LAYER>`.

Zeeman interaction
^^^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.zeeman.ZeemanInteraction`
| **Tensor:** Not a self interaction

The Zeeman interaction is determined by the energy density

.. math::

   w_{\mathrm{Z}} = - M_{\mathrm{s}}\mathbf{m}\cdot\mathbf{B}

with :math:`M_{\mathrm{s}}` being the saturation magnetization of the sample.

.. note::

   This interaction does not produce a linearized tensor.

Cubic anisotropy
^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.cubic_anisotropy.CubicAnisotropy`
| **Tensor:** :class:`~tetrax.interactions.cubic_anisotropy.CubicAnisotropyTensor`

Cubic anisootropy up to first order is determined by the energy density

.. math::

   w_{\mathrm{cub}} = K_{\mathrm{c1}}\sum_{i\neq j}(\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}i})^2
   (\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}j})^2

and the corresponding unitless effective field

.. math::

   \mathbf{h}_{\mathrm{uni}} = -\frac{2K_{\mathrm{c1}}}{\mu_0 M_\mathrm{s}^2} \sum\limits_{i=1}^3
   \left(\sum\limits_{j\neq i} (\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}j})^2 \right)
   (\mathbf{m}\cdot\mathbf{e}_{\mathrm{c}i})\mathbf{e}_{\mathrm{c}i}

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{c1}` being the first-order
cubic-anisotropy constant and
:math:`\mathbf{e}_{\mathrm{c}1} \times \mathbf{e}_{\mathrm{c}2} = \mathbf{e}_{\mathrm{c}3}` being the
anisotropy axes of the sample.

The linearized tensor is given by

.. math::

   \hat{\mathbf{N}}_{\mathrm{cub}} = -\frac{2K_{c1}}{\mu_0 M_{\mathrm{s}}^2} \sum\limits_{i=1}^3
   \left\{ \left[ \sum\limits_{j\neq i} (\mathbf{m}_0\cdot\mathbf{e}_{\mathrm{c}i})^2\right]
   \hat{\mathbf{C}}_i + 2 \hat{\mathbf{C}}_i \cdot \hat{\mathbf{M}}_0 \cdot
   \left[\sum\limits_{j\neq i} \hat{\mathbf{C}}_j \right]\right\}

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{c1}` being the first-order
cubic-anisotropy constant of the sample. Furthermore

.. math::

   \hat{\mathbf{M}}_0 = \mathbf{m}_0 \otimes \mathbf{m}_0

and

.. math::

   \hat{\mathbf{C}}_i = \mathbf{e}_{\mathrm{c}i} \otimes \mathbf{e}_{\mathrm{c}i}

with :math:`\mathbf{e}_{\mathrm{c}1} \times \mathbf{e}_{\mathrm{c}2} = \mathbf{e}_{\mathrm{c}3}` being the
anisotropy axes, :math:`\mathbf{m}_0` the equilibrium magnetization of the sample
and :math:`\otimes` being the dyadic product.

This operator is not dependent on any wave vectors. For the derivation of the linearized fields, see also Ref. [4]_.

Uniaxial anisotropy
^^^^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.uniaxial_anisotropy.UniaxialAnisotropy`
| **Tensor:** :class:`~tetrax.interactions.uniaxial_anisotropy.UniaxialAnisotropyTensor`

Uniaxial anisootropy up to first order is determined by the energy density

.. math::

   w_{\mathrm{uni}} = - K_{\mathrm{u1}}(\mathbf{m}\cdot\mathbf{e}_{\mathrm{u}})^2

and the corresponding unitless effective field

.. math::

   \mathbf{h}_{\mathrm{uni}} = \frac{2K_{\mathrm{u1}}}{\mu_0 M_\mathrm{s}^2} \mathbf{e}_\mathrm{u}
   (\mathbf{e}_\mathrm{u} \cdot \mathbf{m})

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{u1}` being the first-order
uniaxial-anisotropy constant of the sample.

The linearized tensor is given by

.. math::

   \hat{\mathbf{N}}_{\mathrm{uni}} = -\frac{2K_{u1}}{\mu_0 M_{\mathrm{s}}^2} \mathbf{e}_\mathrm{u} \otimes
   \mathbf{e}_\mathrm{u}

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`K_{u1}` being the first-order
uniaxial-anisotropy constant of the sample, and :math:`\otimes` being the dyadic product.



Bulk (Bloch-type) Dzyaloshinksii-Moriya interaction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.dmi_bulk.BulkDMI`
| **Tensor:** :class:`~tetrax.interactions.dmi_bulk.BulkDMITensor`


This antisymmetric exchange interaction is described by the energy density

.. math::

   w_{\mathrm{bDMI}} = D_{\mathrm{b}} \mathbf{m} \cdot (\nabla \times\mathbf{m})

and the unitless effective field

.. math::

   \mathbf{h}_{\mathrm{bDMI}} = \frac{2D_{\mathrm{b}}}{\mu_0 M_\mathrm{s}^2} \nabla \times\mathbf{m}

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`D_{\mathrm{b}}` the
bulk DMI constant of the sample.

The linear tensor of this antisymmetric exchange interaction, acting on propagating waves, is defined as

.. math::

   \hat{\mathbf{N}}_{\mathrm{bDMI},k} = \frac{2D_{\mathrm{b}}}{\mu_0 M_\mathrm{s}^2}
   \left(k \hat{\mathbf{\Sigma}}
   + \nabla_{\mathbf{s}} \times ...  \right)

with :math:`M_{\mathrm{s}}` being the saturation magnetization, :math:`D_{\mathrm{b}}` the
bulk DMI constant and :math:`\mathbf{s}` the coordinates within the mesh of the sample. Furthermore,
the matrix :math:`\hat{\mathbf{\Sigma}}` is defined as

.. math::

   \hat{\mathbf{\Sigma}} = \begin{pmatrix} 0 & -i & 0 \\
                                          i & 0 & 0 \\
                                          0 & 0 & 0 \\
                           \end{pmatrix}.

For more details, see Ref. [5]_ or chapter 5 of Ref. [6]_.


Interfacial (Néel-type) Dzyaloshinksii-Moriya interaction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **Class:** :class:`~tetrax.interactions.dmi_interfacial.InterfacialDMI`
| **Tensor:** :class:`~tetrax.interactions.dmi_interfacial.InterfacialDMITensor`

This antisymmetric exchange interaction is described by the energy density

.. math::

   w_{\mathrm{iDMI}} = D_{\mathrm{i}} \left[\mathbf{m} \cdot \nabla (\mathbf{e}_\mathrm{d}\cdot\mathbf{m})
   - (\nabla \cdot \mathbf{m})(\mathbf{e}_\mathrm{d}\cdot\mathbf{m})\right]

and the unitless effective field

.. math::

   \mathbf{h}_{\mathrm{iDMI}} = -\frac{2D_{\mathrm{i}}}{\mu_0 M_\mathrm{s}^2}
   \left[\mathbf{e}_\mathrm{d}\cdot(\nabla\cdot  \mathbf{m})
   - \nabla\cdot( \mathbf{m}\cdot \mathbf{e}_\mathrm{d})\right]

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`D_{\mathrm{i}}` the
interfacial DMI constant of the sample. Furthermore, :math:`\mathbf{e}_\mathrm{d}` denotes the direction
along which inversion symmetry is broken.

The linear tensor of this antisymmetric exchange interaction, acting on propagating waves, is defined as

.. math::

   \hat{\mathbf{N}}_{\mathrm{iDMI},k} = \frac{2D_{\mathrm{i}}}{\mu_0 M_\mathrm{s}^2}
   \left[k\hat{\mathbf{\Pi}} +
   \mathbf{e}_\mathrm{d} \otimes \nabla - \nabla \otimes  \mathbf{e}_\mathrm{d}  \right]

with :math:`M_{\mathrm{s}}` being the saturation magnetization and :math:`D_{\mathrm{i}}` the
interfacial DMI constant of the sample. Furthermore, :math:`\mathbf{e}_\mathrm{d}` denotes the direction
along which inversion symmetry is broken. The matrix :math:`\hat{\mathbf{\Pi}}` is defined as

.. math::

   \hat{\mathbf{\Pi}} = \begin{pmatrix} 0 & 0 & ie_{\mathrm{d},x} \\
                                          0 & 0 & ie_{\mathrm{d},y}\\
                                          -ie_{\mathrm{d},x} & -ie_{\mathrm{d},y} & 0 \\
                           \end{pmatrix}.

For more details, see chapter 5 of Ref. [6]_.


References
----------
.. [1]  D. R. Fredkin and T. R. Koehler, “Hybrid method for computing demagnetizing fields”,
      `IEEE Trans. Magn. 26, 415-417 (1990) <https://doi.org/10.1109/20.106342>`_
.. [2]  Körber, *et al.*, "Finite-element dynamic-matrix approach for
      spin-wave dispersions in magnonic waveguides with arbitrary
      cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
.. [3]  Körber, *et al.*, "Finite-element dynamic-matrix approach for propagating
      spin waves: Extension to mono- and multi-layers of arbitrary spacing
      and thickness", `AIP Advances 12, 115206 (2022) <https://doi.org/10.1063/5.0107457>`_
.. [4]  Henry, *et al.*, "Propagating spin-wave normal modes:
      A dynamic matrix approach using plane-wave demagnetizating tensors",
      `arXiv:1611.06153 (2016) <https://doi.org/10.48550/arXiv.1611.06153>`_
.. [5]  Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between
      numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104,
      174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
.. [6]  Körber, "Spin waves in curved magnetic shells", `Ph.D. Thesis, TU Dresden (2023)
      <http://dx.doi.org/10.25368/2023.131>`_

