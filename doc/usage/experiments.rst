Numerical Experiments
=====================

.. contents:: Table of Contents
   :depth: 2
   :local:
   :backlinks: none

This chapter explains how to perform numerical experiments (such as relaxation, and eigenmode or absorption calculations) with TetraX, analyze the results, and customize experiments to suit specific research needs.

Running experiments
-------------------

The TetraX package provides a streamlined interface for running numerical experiments on magnetic systems. Designed to be intuitive and modular, the main experiments in TetraX are implemented as top-level functions within the :mod:`tetrax.experiments` module. These functions take a Sample object as input, perform the specified calculations, and return an immutable :class:`~tetrax.experiments._result.Result` object. This result encapsulates the calculated data and includes tools for post-processing, visualization, and metadata storage.

Before performing any experiment, create and prepare a :class:`~tetrax.sample.sample.Sample` (see also :doc:`/usage/sample`), possibly by setting an external magnetic field, or by adding a microwave antenna.

.. code-block:: python

   sample.external_field = (20e-3, 0, 0)  # in Tesla
   sample.antenna =  tetrax.experiments.StripLineAntenna(width=200,spacing=30) # in mesh units (by default, nm)

The external field can also have a more complicated distribution within the sample, for example, by using one of the 
template vectorfields in the :mod:`tetrax.vectorfields` module. The available microwave antennae are documented in the :mod:`tetrax.experiments` module.

Once the sample is ready, choose an appropriate experiment and pass the sample to it. Optionally, additional settings can be provided as keyword arguments to customize the experiment. For example, running the :func:`~tetrax.experiments.eigen.solve.eigenmodes` experiment on a sample would look like this:

.. code-block:: python

   import tetrax as tx

   # Create a sample with desired geometry and material
   sample = tx.Sample(tx.geometries.waveguide.tube(20, 30, 2), name="my_sample")

   # Run the eigenmodes experiment and plot it
   eigen_result = tx.experiments.eigenmodes(sample)
   eigen_result.plot()


In this example, the :func:`~tetrax.experiments.eigen.solve.eigenmodes` experiment computes the spin-wave eigenmodes of the sample and stores the results in an :class:`~tetrax.experiments.eigen.result.EigenResult` object.

Depending on the particular experiment, each :class:`~tetrax.experiments._result.Result` provides methods to plot the
calculated data and perform post-processing on it. For example, the
:class:`~tetrax.experiments.eigen.result.EigenResult` allows calculating the power absorption of the a
spin-wave spectrum with respect to a specified microwave antenna using the
:func:`EigenResult.absorption() <tetrax.experiments.eigen.result.EigenResult.absorption>` method (which returns an
:class:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult`).

.. code-block:: python

   # Use methods in the Result object for analysis
   absorption_result = eigen_result.absorption(antenna=tx.experiments.antenna.StriplineAntenna())
   absorption_result.plot()


TetraX provides significant flexibility for customizing experiments. Most experiment functions accept additional keyword arguments to fine-tune their behavior. For instance, you can control precision, specify damping factors, or select which interactions to include in the calculations.

You can also limit the interactions considered during an experiment (see also :doc:`/usage/interactions`). This is useful when studying the influence of specific physical effects or isolating certain terms in the effective field. For example, to consider only Zeeman and exchange interactions in a relaxation and eigenmode calculation:

.. code-block:: python

   # Get references to specific interactions
   zeeman = sample.get_interaction("zeeman")
   exchange = sample.get_interaction("exchange")

   # Run relaxation considering only Zeeman and exchange interactions
   tx.experiments.relax(sample, interactions=[zeeman, exchange])

   # Calculate eigenmodes with the same interactions
   eigen_result = tx.experiments.eigenmodes(sample, interactions=[zeeman, exchange])


Additionally, some experiments support perturbing results with additional interactions. For example, you can perturb an eigen spectrum with dipole and bulk DMI interactions:

.. code-block:: python

   # Add dipole and bulk DMI interactions as perturbations
   dipole = sample.get_interaction("dipole")
   dmi_bulk = sample.get_interaction("dmi_bulk")

   # Perturb the eigen spectrum
   eigen_result.perturb_with([dipole, dmi_bulk])


Storing and post-processing of results
-------------------------------------

Result containers
^^^^^^^^^^^^^^^^^

All data obtained with a numerical experiment is stored together with necessary metadata in a :class:`~tetrax.experiments._result.Result` container. Every experiment is accompanied by specific methods and properties in its corresponding result container, enabling detailed analysis and visualization. By default, each :class:`~tetrax.experiments._result.Result` is automatically saved to the disk. This includes the
calculated data itself, as well as some metadata and the parameters used to perform the calculations. For example,
running the :func:`~tetrax.experiments.eigen.solve.eigenmodes` experiment as written above produces the following output on the disk.

.. code-block::

    my_sample/
        eigen/
            external_field.vtk
            eigenvalue_dataframe.csv
            mode_profiles/
                ...
            equilibrium.vtk
            report.json


Metadata
^^^^^^^^

As seen above, every result is automatically stored  with a ``report.json`` file. This file contains material parameters, experiment configuration, metadata (such as the version of `TetraX` that has been used) and can be used, for example, for data archiving. 

.. collapse:: See an example file.

   .. code-block:: json

      {
         "result_type": "eigen",
         "config": {
            "k": null,
            "kmin": -40000000.0,
            "kmax": 40000000.0,
            "Nk": 81,
            "m": null,
            "mmin": -5,
            "mmax": 5,
            "save_modes": true,
            "optional_output": [],
            "parallel_backend": "loky",
            "num_cpus": 8,
            "frequency_tolerance": 0.001,
            "inverse_tolerance": 0.0001,
            "maxiter_inverse": 1000.0,
            "interactions": [
                  "exchange",
                  "dipole",
                  "zeemann",
                  "uniaxial_anisotropy*",
                  "cubic_anisotropy*",
                  "dmi_bulk*",
                  "dmi_interfacial*"
            ],
            "_comment": "Toothless interactions (with relevant material parameters equal zero) are marked with '*'."
         },
         "outcome": {
            "has_profiles": true,
            "has_linewidths": false,
            "k (rad/m)": "-40000000.0, -39000000.0, ..., 39000000.0, 40000000.0",
            "m": "0.0",
            "modes_per_km": 11,
            "files": [
                  "external_field.vtk",
                  "equilibrium.vtk",
                  "spectrum_dataframe.csv",
                  "mode_profiles/"
            ]
         },
         "result_info": {
            "type": "eigen",
            "label": null,
            "path": "nanotube_r20_R30/eigen",
            "created": "2024-07-28T18:53:39.504147",
            "environment": {
                  "tetrax_version": "2.0.0dev0",
                  "platform": "Darwin",
                  "cpu": "Apple M3"
            },
            "sample_snapshot": {
                  "name": "nanotube_r20_R30",
                  "geometry_type": "waveguide",
                  "magnetic_order": "FM",
                  "material": {
                     "Msat": 810000.0,
                     "Aex": 1.3e-11,
                     "gamma": 185982240000.0,
                     "alpha": 0.008,
                     "Ku1": 0,
                     "e_u": [
                        0.0,
                        0.0,
                        1.0
                     ],
                     "Dbulk": 0,
                     "Didmi": 0,
                     "e_d": [
                        0.0,
                        1.0,
                        0.0
                     ],
                     "Kc1": 0,
                     "e_c1": [
                        1.0,
                        0.0,
                        0.0
                     ],
                     "e_c2": [
                        0.0,
                        1.0,
                        0.0
                     ]
                  }
            }
         }
      }


One can see in the example above that the material parameters of the sample are also stored in the ``report.json``. This is also the case, when some of them (such ``"Msat"``) have heterogeneous values. However, if a locally defined material parametered has homogeneous values, only a single value will be saved.


Labeling of results
^^^^^^^^^^^^^^^^^^^

By default, running the same experiment twice will overwrite the old results.
In order to circumvent this, results can be equipped with a ``label`` when running the
experiment. For example,


.. code-block:: python

    eigen_result_A = tx.experiments.eigenmodes(sample, label="A", **some_settings)

    ...

    eigen_result_B = tx.experiments.eigenmodes(sample, label="B", **different_settings)

produces

.. code-block::

    my_sample/
        eigen_A/
            ...
        eigen_B/
            ...

This labeling mechanism ensures that results from different configurations are kept separate and are not inadvertently overwritten.


Availalable experiments
-----------------------

Here, we give a summary of the numerical experiments available in `TetraX`. For details, for example which parameters can be set, click on the individual functions/classes or refer to the API reference of the :mod:`tetrax.experiments` module.

Equilibration
^^^^^^^^^^^^^

For many purposes it is important to calculate the magnetic equilibrium state of a sample under given external conditions. There are many different ways to do this, with the simplest ones being either minimizing the total magnetic energy (the energy length density for waveguide samples and the energy area density for layer samples) or solving the equation of motion of the magnetization with large damping. Both approaches are implemented in `TetraX`. The corresponding experiments are listed below.


- | :func:`~tetrax.experiments._relax.minimize.relax` → :class:`~tetrax.experiments._relax.result.RelaxationResult`
  | This experiment performs energy minimization on a magnetic sample in order to calcalate a local magnetic equilibrium. By default, this will modify the magnetization of the sample. Though this method is generally quite fast, it is less reliable than torque minimization with :func:`~tetrax.experiments._relax_dynamic.integrate_llg.relax_dynamic`. The :class:`~tetrax.experiments._relax.result.RelaxationResult` container encapsulates information about the final state of a magnetic relaxation by energy minization, including configuration settings, success status, energy and magnetization logs, and other relevant metadata. It provides methods for viewing, saving, and plotting the relaxation data.

- | :func:`~tetrax.experiments._relax_dynamic.integrate_llg.relax_dynamic` → :class:`~tetrax.experiments._relax_dynamic.result.DynamicRelaxationResult`
  | This experiment performs torque minimization on a magnetic sample in order to calculate a local magnetic equilibrium. By default, this will modify the magnetization of the sample. Though this method is generally slower than energy minimization using :func:`~tetrax.experiments._relax.minimize.relax`, it is much more reliable. The :class:`~tetrax.experiments._relax_dynamic.result.DynamicRelaxationResult` provides analogous functionality.


Eigenmode calculations
^^^^^^^^^^^^^^^^^^^^^^

`TetraX` provides a variety of tools to calculate and analyze spin-wave spectra. Among them are several post-processing experiments that act as methods on an :class:`~tetrax.experiments.eigen.result.EigenResult`. Importantly, all of them allow to specify a ``label`` upon calling them, allowing to run them with different parameters without overwriting old data.

- | :func:`~tetrax.experiments.eigen.solve.eigenmodes` → :class:`~tetrax.experiments.eigen.result.EigenResult`
  | This experiment calculates the spin-wave normal modes of the sample in its current state using a symmetry-adapted finite-element dynamic-matrix method developed in Refs. [1]_ and [2]_, which solve the linearized equation of motion of magnetization. Along with the frequencies, the spatial mode profiles are calculated and can be stored. The experiment returns an :class:`~tetrax.experiments.eigen.result.EigenResult` object which provides plotting routines and different post-processing methods such as calculating line widths, absorption etc.
- | :func:`EigenResult.linewidths() <tetrax.experiments.eigen.result.EigenResult.linewidths>`
  | This post-processing experiment allows to calculate the linear damping rates (linewidths) of the spin-wave spectrum stored in an :class:`~tetrax.experiments.eigen.result.EigenResult`. The linewidths :math:`\Gamma_\nu = \alpha_\mathrm{G} \epsilon_\nu \omega_\nu` are calculated from the spatial mode profiles according to Refs. [3]_ and [4]_, with :math:`\alpha_\mathrm{G}` being the Gilbert-damping parameter and :math:`\epsilon_\nu` being the ellipticity coefficient of the respective mode which is calculated from the mode profile :math:`\mathbf{m}_\nu`.
- | :func:`EigenResult.absorption() <tetrax.experiments.eigen.result.EigenResult.absorption>` → :class:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult`
  | This post-processing experiment allows to obtain the microwave absorption of the calculated eigensystem with respect to a :class:`~tetrax.experiments.antenna.MicrowaveAntenna`. The microwave absorption is calculated from the frequencies and spatial mode profiles. If no antenna is provided, the antenna of the sample will be used. For details, see Refs. [3]_ and [4]_. The corresponding :class:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult` provides methods to plot the calculated data.
- | :func:`EigenResult.perturb_with() <tetrax.experiments.eigen.result.EigenResult.perturb_with>` → :class:`~tetrax.experiments.eigen.postprocessing.perturbed_frequencies.PerturbationResult`
  | This experiment calculates the zeroth-order correction to the frequencies when including additional interactions to the ones considered in the original eigen result (see also :doc:`/usage/interactions`). Details about the calculations are found in Ref. [5]_. The corresponding :class:`~tetrax.experiments.eigen.postprocessing.perturbed_frequencies.PerturbationResult` is similar to an :class:`~tetrax.experiments.eigen.result.EigenResult` but only provides methods to plot the perturbed frequencies. 

Nonlinear LLG dynamics
^^^^^^^^^^^^^^^^^^^^^^

This feature will be implemented in a future release.


References
----------

.. [1]  Körber, *et al.*, "Finite-element dynamic-matrix approach for spin-wave dispersions in magnonic waveguides with arbitrary cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_

.. [2] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes", `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

.. [3] Verba, *et al.*, "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal, and coordinate-dependent damping", `Phys. Rev. B 98, 104408 (2018) <https://doi.org/10.1103/PhysRevB.98.104408>`_

.. [4] Körber, et al., “Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes”, `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

.. [5] Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104, 174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
