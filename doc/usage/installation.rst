Installation
============

For Windows
-----------

On Windows computers the installation of `TetraX` requires to install a C compiler first. The easiest way is to do the following steps:

- Downloaded Microsoft Visual C++ Build Tools from this link: https://visualstudio.microsoft.com/downloads/
- Run the installer
- Select: Workloads → Visual C++ build tools.
- Install options: select only the “Windows 10 SDK” (assuming the computer is Windows 10)

(Source: `<https://www.scivision.co/python-windows-visual-c++-14-required/>`_)

After this, proceed with the next section.

Setting up python distribution
------------------------------

On all systems we recommend to use the `Miniconda <https://docs.anaconda.com/miniconda/>`_ python distribution.
To be able to install `TetraX` from our repository, you might need to run the following commands in a command line shell (on Windows using the Miniconda prompt):

.. code-block:: bash

  conda install pip
  pip install git


Installing TetraX from HZDR repository
--------------------------------------

To install this package on a computer with administrator rights use:

.. code-block:: bash

   pip install git+https://codebase.helmholtz.cloud/micromagnetic-modeling/tetrax.git

On a cluster with no administrator rights you need to add the `--user` option:

.. code-block:: bash

   pip install git+https://codebase.helmholtz.cloud/micromagnetic-modeling/tetrax.git --user

During the installation the pip package manager will check the dependencies and install all python related packages the `TetraX` uses.
 
.. note::
   
   To avoid updating your packages, please create a `python virtual environment <https://docs.python.org/3/tutorial/venv.html>`_, activate it and install both the Jupyter notebook as well as the `TetraX` in this virtual environment. After your finite element simulations are done you can `deactivate <https://docs.python.org/3/tutorial/venv.html>`_ your virtual environment.
   When installing in a freshly virtual environment, you first need to ``pip`` install the ``wheel`` and ``setuptools`` packages by:

    .. code-block:: bash

       pip install wheel setuptools

To allow for 3D visualization in Jupyter notebooks, you additionally need to activate the k3d extension in your shell using

.. code-block:: bash

   $ jupyter nbextension install --py --sys-prefix k3d
   $ jupyter nbextension enable --py --sys-prefix k3d

.. note:: 
   
   In newer python environments and jupyter packages the activation of the k3d extension is not needed anymore.

Now you are ready to use TetraX in your python scripts or Jupyter notebook.

.. code-block:: python

   import tetrax as tx
