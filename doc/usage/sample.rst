Samples
=======

.. contents:: Table of Contents
   :depth: 2
   :local:
   :backlinks: none

The sample object
-----------------

The central object in a `TetraX` workflow is the sample, represented by an instance of the
:class:`~tetrax.sample.sample.Sample` class. They most important attributes are

- :attr:`Sample.mesh <tetrax.sample.sample.Sample.mesh>` describing the geometry of the sample,
- :attr:`Sample.material <tetrax.sample.sample.Sample.material>` containing the material parameters,
- :attr:`Sample.interactions <tetrax.sample.sample.Sample.interactions>` containing the involved :doc:`magnetic interactions </usage/interactions>`,
- :attr:`Sample.mag <tetrax.sample.sample.Sample.mag>` the current magnetization state and
- :attr:`Sample.external_field <tetrax.sample.sample.Sample.mag>` the applied static external field.

Apart from that, samples also provide methods for post-processing and visualization such as

- :func:`~tetrax.sample.sample.Sample.average` to compute averages of quantities,
- :func:`~tetrax.sample.sample.Sample.scan_along_curve` to extract line traces or
- :func:`~tetrax.sample.sample.Sample.show` to display scalar or vector fileds on the sample.

More details about these methods found in the chapter :doc:`/usage/visualization`.

A sample has to be created by supplying a mesh. For this, one can use, for example, one of the many provided
template geometries from the :mod:`tetrax.geometries` module.

.. code-block:: python

   >>> import tetrax as tx
   >>> sample = tx.Sample(tx.geometries.waveguide.tube(inner_radius=20, outer_radius=30, cell_size=3), name="foobar")

This will initialize a sample with a set of default material parameters.

.. code-block:: python

   >>> sample.name
   "foobar"

The name of the sample is set purely for the purpose of storing data in a meaningful way, as all data obtained in
numerical experiments is saved in a directory named after the sample. Material parameters can be accessed via the
``material`` attribute of the sample.

.. code-block:: python

   >>> sample.material["Msat"] = 800e3 # A/m
   >>> sample.material["alpha"] = 0.08
   >>> sample.material["alpha"].average
   0.08
   >>> sample.material["Msat"].value
   MeshScalar([800000.0, 800000.0, 800000.0, 800000.0, ...])

Note that, most material parameters such as the saturation magnetization ``"Msat"`` or the exchange-stiffness
constant ``"Aex"`` can be inhomogeneous within the sample (more details under :ref:`Material parameters <usage_material_parameters>`).

By default, magnetization direction of the sample is set in the :math:`z` direction

.. code-block:: python

   >>> sample.mag
   MeshVector([[0, 0, 1], [0, 0, 1], [0, 0, 1], ...])

which can be easily changed, for example, by providing a single vector

.. code-block:: python

   >>> sample.mag = (0, 1, 0)

To initialize an inhomogeneous magnetization, an array of shape ``(nx, 3)`` needs to be supplied which can be
seen as a list of triplets, with each triplet representing the magnetization vector at
a specific node. For convenience, a couple of template vector fields (such as domain walls, vortices, etc.)
can be found in the :mod:`tetrax.vectorfields` submodule. These fields can be called by supplying ``xyz``, the
coordinates of the mesh nodes.

.. code-block:: python

   >>> sample.mag = tx.vectorfields.bloch_wall(sample.xyz, ...)

When setting a magnetization field, the input field is automatically normalized and internally converted
to a :class:`~tetrax.sample.mesh.meshquantities.MeshVector`, which is a data structure that allows to easily access the different components
of the magnetization.

.. code-block:: python

   >>> sample.mag.x
   MeshScalar([0.0, 0.0, 0.12, 0.15, ...])

Here, we see that the individual components of a mesh vector are :class:`~tetrax.sample.mesh.meshquantities.MeshScalar` s,
another basic data structure in `TetraX`. We can easily calculate the averages of these objects using the
:func:`~tetrax.sample.sample.Sample.average` method which takes some vector- or scalar field as an input.

.. code-block:: python

   >>> sample.average(sample.mag)
   [0.98, 0.02, 0.0]

   >>> sample.average(sample.mag.x)
   0.98

Similar to setting the magnetization, it is also possible to apply an external field to the sample,

.. code-block:: python

   >>> sample.external_field = (0, 200e-3, 0) # in Tesla
   >>> sample.external_field = tx.vectorfields.homogeneous(sample.xyz, ...)
   # also accessible as sample.Bext = (0, 200e-3, 0) # in Tesla, par example

or to specify a microwave antenna that can be used for absorption calculations.

.. code-block:: python

   >>> sample.antenna = tx.vectorfields.homogeneous(sample.xyz, ...)      

More details on the numerical experiments for which these quantities are relevant is found in :doc:`/usage/experiments` .

The mesh, current magnetization and microwave antenna (if available) of a sample can always be visualized using the
:func:`~tetrax.sample.sample.Sample.show` method.

.. code-block:: python

   >>> sample.show()

For further details, see :doc:`/usage/visualization`.

Geometry 
--------

Sample Mesh
^^^^^^^^^^^

In the :mod:`tetrax.geometries` submodule, we supply a couple of templates to generate common meshes, such as
cuboids, prisms, tubes, wires, and so forth. However, desired meshes can be generated also using `pygmsh <https://pygmsh.readthedocs.io/en/latest/>`_
or even any external software, as long as the generated mesh files are readable by `meshio <https://github.com/nschloe/meshio>`_. Mesh files can be read in
using

.. code-block:: python

   >>> sample = Sample(tx.geometries.from_file(filename))

When setting a mesh/geometry, under the hood, certain preprocessing necessary for later computations is performed. For example,
the differential operators on the mesh or the normal vector field of the sample are calculated. After these calculations
have been performed, the important numerical parameters are stored within the ``mesh`` attribute, for example

- ``sample.mesh.nx``, the total number of nodes in the mesh,
- ``sample.mesh.nb``, the number of boundary nodes in the mesh, and
- ``sample.mesh.xyz`` (also accessible as ``sample.xyz``), the coordinates at each node as a :class:`~tetrax.sample.mesh.meshquantities.MeshVector`. These coordinates are expressed in units of
- ``sample.mesh.scale``, the characteristic length of the sample. The default is ``1e-9`` which means that the coordinates on the mesh are given in nanometers.

In a notebook environment, a quick summary of the sample parameters can be viewed by

.. code-block:: python

   >>> sample.mesh

**SampleMesh (Sample.mesh) of 'foobar'**

========================= =============== ==============
attribute                 value           description
========================= =============== ==============
geometry_type             waveguide       geometry type
nx                        283             total nodes
nb                        83              boundary nodes
scale                     1e-9
========================= =============== ==============


All attritubes are document in the API reference of :class:`~tetrax.sample.mesh.sample_mesh.SampleMesh`.

Geometry Types
^^^^^^^^^^^^^^

The geometry type should be chosen at the sample creation when calling :class:`~tetrax.sample.sample.Sample`.
However, when supplying the template meshes, the geometry type is already defined in as an attribute of the mesh.
When using desired meshes generated by external packages, please always supply the geometry type at the sample creation as for example:

.. code-block:: python

    >>> my_mesh = ... # generated by pygmsh or gmsh... or other external softwares
    >>> sample = tx.Sample(mesh=my_mesh, geometry_type="waveguide")

if the generated mesh ``my_mesh`` is a waveguide.

As geometry types, the followings can be selected:

- ``confined`` samples such as disks, cuboids, spheres or Möbius ribbons. These types of samples are often the subject of `standard` three-dimensional micromagnetic simulations. Only in such samples, an LLG solver can be used. Using a confined-wave dynamic-matrix approach, the normal modes of confined samples can be calculated.
- ``waveguide`` samples are infinitely long in one direction (here, the :math:`z` direction). Moreover, their (equilibrium) magnetization is taken to be translationally invariant in this direction. Such samples are modelled by discretizing only a line trace along the thickness of the waveguide in the radial direction. A propagating-wave dynamic-matrix approach can be used to calculate the normal modes as a function of the wave vector :math:`k` along the :math:`z` direction.
- ``waveguide_axial`` samples are infinitely long in one direction (here, the :math:`z` direction) and show axial symmetry (in :math:`\phi`, where (:math:`\rho`, :math:`\phi`, :math:`z`) represent the cylindrical coordinates) around the axis in this direction. Moreover, their (equilibrium) magnetization is taken to be translationally as well as axially invariant. Such samples are modelled by discretizing only a single cross section of the waveguide. A propagating-wave dynamic-matrix approach can be used to calculate the normal modes as a function of the wave vector pairs (:math:`k`, :math:`m`) along the :math:`z` and :math:`\phi` directions, respectively.
- ``layer`` samples consist of one or multiple infinitely extended layers which are modelled by discretizing a line trace along the thickness of the layer(s). The (equilibrium) magnetization is taken to be homogeneous within the layer planes. A propagating-wave dynamic-matrix approach can be used to calculate the normal modes as a function of the wave vector :math:`k` along the :math:`z` direction.

For the different types of sample geometries, an energy minimizer as well as a dynamic relaxation, based on the time integration of the Landau-Lifshitz-Gilbert equation of motion is implemented, to calculated the magnetic ground states.

.. image:: sample_types.png
   :width: 800

.. Note::

   As of version `2.0.0`, only ferromagnetic waveguides, axial waveguides and multilayers are fully supported.


.. _usage_material_parameters:

Material parameters
-------------------

SampleMaterial and MaterialParameter classes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Material properties of samples are managed by the :class:`~tetrax.sample.material.material.SampleMaterial` that is accessed through the :attr:`~tetrax.sample.material` attribute of a sample. Typing the following command one can easily check the current material parameters.

.. code-block:: python

   sample.material

**SampleMaterial (Sample.material) of 'foobar'**

============ ======================================= ================================
name	        average	                                description
============ ======================================= ================================
Msat            800000.0 A/m                            saturation magnetization
Aex             1.1e-11 J/m                             exchange stiffness
gamma           176085964400.0 radHz/T                  gyromagnetic ratio
alpha           0.008 (is_global)	                    Gilbert damping
Ku1             0.0 J/m\ :sup:`3`                       first-order unaxial-anistropy constant
e_u             [0. 0. 1.]                              uniaxial anistropy direction
Dbulk           0 J/m\ :sup:`2` (is_global)             bulk DMI constant
Didmi           0 J/m\ :sup:`2` (is_global)             interfacial DMI constant
e_d             [0. 1. 0.]                              interfacial DMI direction
Kc1             0.0 J/m\ :sup:`3`                       first-order cubic-anistropy constant
e_c1            [1. 0. 0.]                              first cubic anistropy direction
e_c2            [0. 1. 0.]                              second cubic anistropy direction
e_c3            [0. 0. 1.]                              third cubic anistropy direction
J1              [-0.0003] J/m\ :sup:`2` (is_global)     interlayer-exchange constant
============ ======================================= ================================

Once a sample object is created, it will be initialized with the material parameters of Ni\ :sub:`80`\Fe\ :sub:`20`\ (permalloy). However, the material parameters can be set individually to match the system in study. Any of the listed parameters can easily be changed using:

.. code-block:: python

   sample.material["Msat"] = 796e3
   sample.material["Aex"] = 13e-12
   sample.material["alpha"] = 0.007
   sample.mesh.scale = 10e-9

We can also assign all material parameters at once by assigning a python dictionary to the ``Sample.material``. The :mod:`tetrax.materials` submodule provides a couple of template materials. Here is an example for setting permalloy material parameters:

.. code-block:: python

    sample.material = tx.materials.permalloy

Each parameter like ``"Msat"`` is stored as a :class:`~tetrax.sample.material.parameter.MaterialParameter` object that provides metadata and some convencience methods. For example, in a notebook environment ´, the following will print a summary of the material parameter, including unit, current value, average, etc.

.. code-block::

   sample.material["Msat"]

**MaterialParameter**

===========   ===============================
attribute	   value
===========   ===============================
name	         Msat
description	   saturation magnetization
sample	      my_sample
param_type	   scalar
is_global	   False
read_only	   False
constraint	   is_strictly_positive
unit	         A/m
average	      [MeshScalar(795000.)]
===========   ===============================

The invidivual attributes can be accessed in the usual way.

.. code-block:: python

   sample.material["Msat"].value
   # will return the current value of the parameter

   sample.material["Msat"].average
   # will return the sample average of the parameter

   sample.material["Msat"].unit
   # will return the unit of the parameter

For more details, see :class:`~tetrax.sample.material.material.SampleMaterial` 
and :class:`~tetrax.sample.material.parameter.MaterialParameter` within the API reference.

Inhomogeneous material parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All material parameters that are not global (``is_global=True``) can be spatially dependent within the sample. Even though the attributes ``sample.material["Msat"]`` and ``sample.material["Aex"]`` can be set by
specifying a single number, they are internally converted to
:class:`~tetrax.sample.mesh.meshquantities.MeshScalar` of length ``sample.mesh.nx``. One can check this by

.. code-block:: python

   >>> sample.material["Msat"] = 796e3
   >>> sample.material["Msat"].value
   MeshScalar([796000., 796000., 796000., 796000., 796000., 796000., 796000.,
            .....
            796000., 796000., 796000., 796000., 796000., 796000.])
 

or, similarly,

.. code-block:: python

   >>> sample.material["Aex"].value
   MeshScalar([1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11,
               ....
               1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11, 1.3e-11])

The average of these scalar fields can always be obtained using

.. code-block:: python

   >>> sample.material["Aex"].average
   MeshScalar(1.3e-11)

   >>> sample.material["Msat"].average
   MeshScalar(796000.)

Please note that, the average for any scalar or vector field can also be computed by using the :func:`~tetrax.sample.sample.Sample.average` method of the respective sample

.. code-block:: python

   >>> sample.average(sample.material["Aex"].value)
   MeshScalar(1.3e-11)

Of course, by supplying the saturation or the exchange stiffness as an array, list or
:class:`~tetrax.sample.mesh.meshquantities.MeshScalar` one can specify inhomogeneous
material parameters and, thereby, different materials. For example, we can create
a tube with a saturation magnetization that is linearly varying along the :math:`y`
direction.

.. code-block:: python

   >>> Mavrg = 1200e3
   >>> dMdy = 20e3
   >>> sample.mag = [0, 0, 1]
   >>> sample.material["Msat"] = Mavrg + dMdy * sample.xyz.y
   >>> sample.show(show_extrusion=False)


.. raw:: html

    <iframe src="../_static/usage_inhom_saturation.html" frameborder="0" height="600px" width="600px"></iframe>

Notice that the arrows in the 3D plot are now scaled according to their local value of
``sample.material["Msat"]`` (can be deactivated using ``show_scaled_mag=False``). Importantly,
the attribute ``sample.mag`` is still a unit vector (:math:`\mathbf{m}`) and denotes the local direction of
magnetization!

.. code-block:: python

   >>> sample.mag
   MeshVector([[0., 0., 1.],
               [0., 0., 1.],
               [0., 0., 1.],
               ...

As result, when initializing a magnetization state, we supply only
the unit vector (field) for the direction, as above. The full magnetization
:math:`\mathbf{M}=M_\mathrm{s}\mathbf{m}` (in units of A/m) can be retrieved using

>>> sample.mag_full
MeshVector([[0., 0., 0.12],
            [0., 0., 0.12],
            [0., 0., 0.125],
            ...

This quantity cannot be set directly. For examples including inhomogeneous exchange and/or
saturation magnetization, see :doc:`../examples/double_layer_Py25_CoFeB25_Grassi`
and :doc:`../examples/magnetization_graded_waveguide`.

Template vectorfields in :mod:`tetrax.vectorfields` are available to define spatially dependent material directions. Here is an example for a uniaxial anisotropy with radial easy axis and homogeneous anisotropy constant:

.. code-block:: python

    >>> sample.material["Ku1"] = 2e4 #J/m^3
    >>> sample.material["e_u"] = tx.vectorfields.radial(sample.xyz,1)

The defined vectorfield of the uniaxial anisotropy can be visualized to check if the aimed
spatial dependence has been set or not. This can be done with the :func:`show() <tetrax.sample.sample.Sample.show>` method of the sample.
For further details please check the provided example: :doc:`../examples/round_tube_spatial_dep_anis`.


Available parameters
^^^^^^^^^^^^^^^^^^^^

Here we describe the material parameters that are available for ferromagnetic samples. Each material parameters is typically important for a particular magnetic interaction (see :doc:`/usage/interactions` for details).

Saturation magnetization
""""""""""""""""""""""""

| **Name:** ``"Msat"``
| **Unit:** A/m

The saturation magnetization of a sample can be set to be uniform or non-uniform using

.. code-block:: python

   sample.material["Msat"] = 1200e3   # A/m
   sample.material["Msat"] = 1200e3 + 20 * sample.xyz


Gyromagnetic ratio 
"""""""""""""""""""

| **Name:** ``"gamma"``
| **Unit:** rad Hz/T

The gyromagnetic ratio of the sample can be set via

.. code-block::

   sample.material["gamma"] = 176085964400.0 # rad Hz/T


Gilbert-damping factor
""""""""""""""""""""""

| **Name:** ``"alpha"``
| **Unit:** unitless


The Gilbert-damping factor of the sample can be set via

.. code-block::

   sample.material["alpha"] = 0.01


Exchange-stiffness constant
"""""""""""""""""""""""""""

| **Name:** ``"Aex"``
| **Unit:** J/m
| **Interaction:** :class:`~tetrax.interactions.exchange.ExchangeInteraction`

Similarly, the exchange-stiffness constant of a sample can be set to be uniform or non-uniform using

.. code-block:: python

   sample.material["Aex"] = 12e-12    # J/m
   sample.material["Aex"] = 12e-12 + 20e-13 * sample.xyz



Bulk DMI constant
"""""""""""""""""

| **Name:** ``"Dbulk"``
| **Unit:** J/m\ :sup:`2`
| **Interaction:** :class:`~tetrax.interactions.dmi_bulk.BulkDMI`

The constant of bulk (Bloch-type) Dzyaloshinksii-Moriya interaction can be set using

.. code-block:: python

   sample.material["Dbulk"] = 1e-3  # J/m^2

This parameter cannot be inhomogeneous.

Interfacial DMI constant
""""""""""""""""""""""""

| **Names:** ``"Didmi"``
| **Unit:** J/m\ :sup:`2`
| **Interaction:** :class:`~tetrax.interactions.dmi_interfacial.InterfacialDMI`

The constant of interfacial (Néel-type) Dzyaloshinksii-Moriya interaction and its symmetry-breaking direction can be set using

.. code-block:: python

   sample.material["Didmi"] = 1e-3  # J/m^2
   sample.material["e_d"] = (0, 1, 0)

While, ``"Didmi"`` is currently only uniform, the direction ``"e_d"`` can be spatially inhomogeneous.

Interlayer exchange
"""""""""""""""""""

| **Name:** ``"J1"``
| **Unit:** J/m\ :sup:`2`
| **Interaction:** :class:`~tetrax.interactions.interlayer_exchange.InterlayerExchangeInteraction`

Bilinear interlayer-exchange interaction can be used, implemented for layered systems only. The bilinear term can be set
using the J1 constant (in J/m^). The constant can also have different values between different layers in a multilayered sample,
by supplying a list of values.

Example for bilayers:

.. code-block:: python

    >>> sample = tx.Sample(tx.geometries.layer.bilayer(20,20,2,1), name="bilayer_iec")
    >>> sample.material["J1"] = -3e-4


Example for multilayers consisting of 3 magnetic layers and 2 non-magnetic spacers:
- antiferromagnetic coupling between the first two layers and ferromagnetic coupling between the second and the third layer

.. code-block:: python

    >>> sample = tx.Sample(tx.geometries.layer.multilayer[50,20,10],[2,1],[5,5,5]), name="trilayer_iec")
    >>> sample.material["J1"] = [-3e-4,1e-3]

For further details please check the provided example: :doc:`../examples/exchange_coupled_bilayers`.


Uniaxal anisotropy
""""""""""""""""""

| **Names:** ``"Ku1"``, ``"e_u"``
| **Unit:** J/m\ :sup:`3`
| **Interaction:** :class:`~tetrax.interactions.uniaxial_anisotropy.UniaxialAnisotropy`

A homogeneous uniaxial anisotropy can be set using a triplet for the direction and a single constant.
For a single crystal ferromagnetic sample would be like:

.. code-block:: python

   >>> sample.material["Ku1"] = 48e3 # in J/m^3
   >>> sample.material["e_u"] = [1, 0, 0] # along the x axis

Easy plane anisotropies can also be defined, by setting the anisotropy constant to negative value.
An example for an xy easy plane would be:

.. code-block:: python

   >>> sample.material["Ku1"] = -20e3 # in J/m^3
   >>> sample.material["e_u"] = [0, 0, 1] # along the z axis

For an example with non-uniform unaxial-anistropy see :doc:`../examples/round_tube_spatial_dep_anis`.

Cubic anisotropy
""""""""""""""""

| **Names:** ``"Kc1"``, ``"e_c1"``, ``"e_c2"``
| **Unit:** J/m\ :sup:`3`
| **Interaction:** :class:`~tetrax.interactions.cubic_anisotropy.CubicAnisotropy`

A homogeneous cubic anisotropy can be defined by an anisotropy constant Kc1 and two triplets defining the
orientation of the easy axes (the third axis will be automatically calculated as the cross product of the other two).
The higher order term is not yet included.

Here is an example for a cubic anisotropy with axes along the (100) directions, typical for iron (Fe):

.. code-block:: python

    >>> sample.material["Kc1"] = 48e3 #J/m^3
    >>> sample.material["e_c1"] = (1,0,0)
    >>> sample.material["e_c2"] = (0,1,0)

Third direction ``sample.material["e_c3"]`` is automatically calculated as the cross product of the first two.

For further details please check the provided example: :doc:`../examples/monolayer_cubic_anisotropy`.
