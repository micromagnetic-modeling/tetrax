Visualization and evaluation
============================

`TetraX` provides several built-in tools to visualize the magnetic system at hand, plot results or evaluate data further.
This chapter provides an introduction to the basic methods and workflows. For more details on the invidual methods, for example, which parameters can be set, just click on the respective function name to jump to the :doc:`API reference </modules>`.


Sample and field visualization
------------------------------

The :class:`~tetrax.sample.sample.Sample` provides several methods to visualize and evaluate data. Firstly, the sample itself can always visualized with the :func:`~tetrax.sample.sample.Sample.show` method

.. code-block::

   sample.show()

which will display a 3D view using the `k3d` package. For samples whose mesh only represents a single cross-section of a full 3D mesh, we show an extrusion that hints at the full volumetric geometry and that can always be hidden by setting ``show_extrusion``. If available, :func:`~tetrax.sample.sample.Sample.show` will also visualize the ``antenna`` that is currently
assigned to the sample. 

By default, :func:`~tetrax.sample.sample.Sample.show` will automatically display the current magnetization (``mag``) stored in the sample. However, the same method can also be used to visualize arbitrary scalar or vector fields that are defined on the sample.

.. code-block::

   vector_field = tx.vectorfields.helical(sample.xyz, 90, 1)
   sample.show(vector_field)

   sample.show(vector_field.x) # is a scalar field

One can even plot multiple scalar or vector fields at the same time by providing a list of fields in the following way.

.. code-block::

   sample.show([sample.material["Msat"].value, sample.mag, sample.external_field])

This will plot the spatially-dependent saturation magnetization, the magnetization direciton of the sample, as well as the currently applied external field. There are many different ways in which the visualization can be configured. For details, simply head over to the documentation of :func:`~tetrax.sample.sample.Sample.show`.

Averages and linescans on samples
---------------------------------

In order to calculate the average of a given vector or scalar field on a sample, one can use the :func:`~tetrax.sample.sample.Sample.average` method of the respective sample.

.. code-block:: python

   m_average = sample.average(sample.mag) # is a single vector

This, for example, calculates the average magnetization direction of the sample. Similarly, one can also calculate the averages of scalar fields.

.. code-block:: python

   mx_average = sample.average(sample.mag.x) # is a single number
   Msat_average = sample.average(sample.material["Msat"].value) # is a single number

.. note:: 

   The average of material parameters can also be obtained through their own methods, for example ``sample.material["Msat"].average``.

Instead of calculating averages of scalar/vector fields, it is also possible to extract line traces from those fields, using the :func:`~tetrax.sample.sample.Sample.scan_along_curve` method. This method allows to either specificy the end points of a straight line, or the set of points of an arbitrarily-shaped curve.

.. code-block::

   # by specifying endpoints
   Msat_along_line, line_coords = sample.scan_along_curve(
                                                       sample.material["Msat"].value,
                                                       curve = ((-W/2,0,0), (W/2,0,0)),
                                                       num_points=int(W/res_W),
                                                       return_curve=True
                                                      )
   # by specifying the full curve
   Msat_along_line = sample.scan_along_curve(sample.material["Msat"].value, curve = line_coords)

Depending on whether ``return_curve=True`` has been set or not, the coordinates of the path along which the data is extracted are returned as the second output (default is ``False``). Importantly, this is independent on whether endpoints or a full curve has been specified.

Visualization of results
------------------------

All :class:`~tetrax.experiments._result.Result` s implemented in the :mod:`~tetrax.experiments` module come with (sometimes several) possibilities to visualize the calculated data or show the progress of the respective experiment, for example, through their :func:`~tetrax.experiments._result.Result.plot` method. The most important methods to visualize results are shown below. For details, for example, which parameters can be set, simply click on the individual functions.

:class:`~tetrax.experiments._relax.result.RelaxationResult`

- :func:`~tetrax.experiments._relax.result.RelaxationResult.plot` displays the evolution of all energies and magnetization components during energy minimization.
- :func:`~tetrax.experiments._relax.result.RelaxationResult.show` shows initial and final state of an energy minimization as a 3D plot on the mesh.

:class:`~tetrax.experiments._relax_dynamic.result.DynamicRelaxationResult`

- :func:`~tetrax.experiments._relax_dynamic.result.DynamicRelaxationResult.plot` displays the evolution of all energies and magnetization components during a dynamic relaxation (integration of overdamped equation of motion).
- :func:`~tetrax.experiments._relax_dynamic.result.DynamicRelaxationResult.show` shows initial and final state of a dynamic relaxation (integration of overdamped equation of motion) as a 3D plot on the mesh.

:class:`~tetrax.experiments.eigen.result.EigenResult`

- :func:`~tetrax.experiments.eigen.result.EigenResult.plot` displays the calculated frequencies based on selected wave vectors and mode indices.
- :func:`~tetrax.experiments.eigen.result.EigenResult.plot_linewidths` displays the calculated linewidths based on selected wave vectors and mode indices.
- :func:`~tetrax.experiments.eigen.result.EigenResult.show_mode` shows and potentially animates a desired spatial mode profile in a 3D plot on the mesh (if profiles have been calculated).

:class:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult`

- :func:`~tetrax.experiments.eigen.postprocessing.absorption.AbsorptionResult.plot` displays real and imaginary part of the dynamic susceptibility.

:class:`~tetrax.experiments.eigen.postprocessing.perturbed_frequencies.PerturbationResult`

- :func:`~tetrax.experiments.eigen.postprocessing.perturbed_frequencies.PerturbationResult.plot` displays the calculated frequencies based on selected wave vectors and mode indices.