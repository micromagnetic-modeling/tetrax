# TetraX

![](doc/_static/LogoV2.png)


TetraX is a package for finite-element-method (FEM) micromagnetic modeling with the aim to provide user friendly and versatile micromagnetic workflows. At its core, it allows to calculate and analyse spin-wave spectra using efficient and in-house developed symmetry-adapted eigensolver methods that use FEM discretization to model different geometries such as infinitely long waveguides, or infinitely extended multilayers or waveguides with axial symmetry. The underlying magnetic equilibra can be calculated with energy minimization or integration of the overdamped equation of motion of the magnetization. The results can be further processed with integrated post-processing tools or visualized with provided plotting methods. Features include

- symmetry-adapted dynamic-matrix methods to calculate spin-wave normal modes
- linewidths and microwave absorption obtained from mode profiles
- energy minimizer and overdamped dynamic equation to calculate magnetic equilibria
- flexible geometries (with templates) due to finite-element modeling
- infinitely extended systems (waveguides, multilayers) or axially symmetric systems represented by 1D and 2D meshes
- magnetic interactions: exchange, dipole, uniaxial and cubic anistropy, iDMI, bulk DMI, interlayer exchange
- efficient calculation of dipolar fields with extended hybrid FEM/BEM method
- inhomogeneous material parameters (including anisotropy axes)
- interactive visualization of samples and results in notebooks
- read and write of meshes and quantities from/to vtk files
- experimental support for antiferromagnets

## Documentation

All information on how to use the package is found in the [TetraX Documentation](https://www.tetrax.software/).

## Using TetraX for your research

If you use TetraX for your research, please cite

- L. Körber, G. Quasebarth, A. Hempel, F. Zahn, A. Otto, E. Westphal, R. Hertel and A. Kákay (2022). “TetraX: Finite-Element Micromagnetic-Modeling Package”, Rodare. DOI: [10.14278/rodare.1418](https://doi.org/10.14278/rodare.1418)
- L. Körber, G. Quasebarth, A. Otto and A. Kákay, “Finite-element dynamic-matrix approach for spin-wave dispersions in magnonic waveguides with arbitrary cross section”, [AIP Advances 11, 095006 (2021)](https://doi.org/10.1063/5.0054169)

```
@misc{TetraX,
  author = {Körber, Lukas and
            Quasebarth, Gwendolyn and
            Hempel, Alexander and
            Zahn, Friedrich and
            Otto, Andreas and
            Westphal, Elmar and
            Hertel, Riccardo and
            Kakay, Attila},
     title = {{TetraX: Finite-Element Micromagnetic-Modeling
               Package}},
     month = jan,
     year = 2022,
     doi = {10.14278/rodare.1418},
     url = {https://doi.org/10.14278/rodare.1418}
 }

@article{korberFiniteelementDynamicmatrixApproach2021a,
             title = {Finite-element dynamic-matrix approach for spin-wave dispersions
                      in magnonic waveguides with arbitrary cross section},
             volume = {11},
             doi = {10.1063/5.0054169},
             language = {en},
             journal = {AIP Advances},
             author = {Körber, L and Quasebarth, G and Otto, A and Kákay, A},
             year = {2021},
             pages = {095006},
     }
```

The numerical experiments implemented in TetraX are often based on seminal papers. In order to give credit to these works, when conducting a numerical experiment, TetraX saves references important for this experiment to a bibtex file called ``references.bib``, found in the sample directory. In this file, each entry contains a comment field describing how the reference was important for the computation. When publishing results calculated with TetraX in your research, please also give credit to the works which are important for the numerical experiments you conducted.

## Feedback and problems

**User forum**: [discussions.tetrax.software](https://discussions.tetrax.software/)

TetraX is maintained by the [Micromagnetic Modeling Group](https://www.hzdr.de/db/Cms?pOid=55944&pNid=107) of Dr. Attila Kákay at the [Helmholtz-Zentrum Dresden - Rossendorf](https://www.hzdr.de). In case you experience any problem, being related with the TetraX package, if you have questions on how to use it for your projects, want to propose new features, or if you just want to discuss micromagnetism, head over to the [TetraX User Forum](https://discussions.tetrax.software/). There you can discuss with other users or the developers. Alternatively, contact either [Lukas Körber](mailto:l.koerber@hzdr.de) or [Attila Kákay](mailto:a.kakay@hzdr.de). However, please always consider asking your questions in the user forum. We are happy to help you with implementing new features for your studies. Please approach us to discuss the details and find the optimal way.

## Installation

Install this package using

```bash
   pip install git+https://codebase.helmholtz.cloud/micromagnetic-modeling/tetrax.git
```

If you encounter any problems, please see the [User Guide: Installation](https://www.tetrax.software/usage/installation.html).
To allow for 3D visualization in Jupyter notebooks, you additionally need to activate the k3d extension in your shell
using

```bash
   jupyter nbextension install --py --sys-prefix k3d
   jupyter nbextension enable --py --sys-prefix k3d
```

In newer Python environments and jupyter packages the activation of the k3d extension is not needed anymore.

## Getting started

To get started, take a look our [Getting started](https://www.tetrax.software/quickstart.html) page or the many [Examples](https://www.tetrax.software/examples.html) within the TetraX Documentation.

## Acknowledgements

The developers are very thankful to Claas Abert, Joo-Von Kim, Steffen Boerm, Burkhard Clauß, Pedro Landeros, Jorge A. Otálora, Jürgen Lindner and Jürgen Fassbender for fruitful discussions and advise in various parts of developing TetraX. We are grateful to Henrik Schulz and Jens Lasch for their continuous support of our computational infrastructure. Financial support from the Deutsche Forschungsgemeinschaft within the programs under Grant Nos. KA 5069/1-1 and KA 5069/3-1 is gratefully acknowledged.