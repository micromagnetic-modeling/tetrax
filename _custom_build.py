
import os
import platform
from pathlib import Path

from setuptools import Extension
from setuptools.command.build_py import build_py as _build_py

from numpy import get_include

MODULE_DIR = os.path.dirname(os.path.abspath(__file__))
LOCAL_DIR = os.path.join(MODULE_DIR, "local")
LIB_DIR = os.path.join(LOCAL_DIR, "lib")
LIB_DIR64 = os.path.join(LOCAL_DIR, "lib64")

print(MODULE_DIR)
fem_core_path = Path("tetrax/sample/mesh/fem_core").resolve()

c_sourcefiles = [
    str(fem_core_path / p)
    for p in [
        "dense2D_onthefly.c",
        "bessel_K1.c",
        "akmag.c",
        "rotation_mat.c",
        "matutils.c",
        "sorting.c",
        "utils.c",
        "bnd2d.c",
        "opmatspm.c",
        "galerkin.c",
        "fempreproc.c",
        "hotriang.c",
        "dense3D.c",
        "integration_of_functions.c",
    ]
]

compiler_extra_args = ["-O3", "-Wno-cpp", "-Wno-unused-function", "-Wall"]
if platform.system() == "Windows":
    compiler_extra_args = []
# compiler_extra_args.append("-fopenmp")
compiler_extra_args.append("-std=c99")
#compiler_extra_links = ["-Wl,-rpath,{},-rpath,{}".format(LIB_DIR, LIB_DIR64)]
# com_link.append('-fopenmp')

class build_py(_build_py):
    def run(self):
        self.run_command("build_ext")
        return super().run()

    def initialize_options(self):
        super().initialize_options()
        if self.distribution.ext_modules == None:
            self.distribution.ext_modules = []

        self.distribution.ext_modules.append(
            Extension(
                "tetrax.sample.mesh.fem_core.cythoncore",
                sources=[*c_sourcefiles, "tetrax/sample/mesh/fem_core/cythoncore.pyx"],
                libraries=[],
                library_dirs=[],
                include_dirs=[str(fem_core_path), "main", get_include()],
                extra_compile_args=compiler_extra_args,
#                extra_link_args=compiler_extra_links,
            )
        )